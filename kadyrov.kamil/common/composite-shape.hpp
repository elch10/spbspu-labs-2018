#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include "memory"

namespace kadyrov
{
  class CompositeShape : public kadyrov::Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & obj);
    CompositeShape(CompositeShape && obj);
    CompositeShape(const std::shared_ptr<kadyrov::Shape> & obj);

    CompositeShape & operator= (const CompositeShape & elem) noexcept;
    CompositeShape & operator= (CompositeShape && elem);
    std::shared_ptr<kadyrov::Shape> operator [] (const size_t index) const;

    void addShape(const std::shared_ptr<kadyrov::Shape> & obj);
    void deleteShape(const size_t index);
    void move(const double dx, const double dy) override;
    void move(const point_t & newPos) override;
    void scale(const double coefficient) override;
    void rotate(double angle);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    size_t getSize() const;

  private:
    std::unique_ptr< std::shared_ptr<kadyrov::Shape>[]> figures_;
    size_t size_;
  };
}

#endif
