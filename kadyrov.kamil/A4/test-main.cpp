#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(Matrix_Layers_Test)
{
  kadyrov::Rectangle testRect1({ 4.0, 2.0, { 5.0, -5.0 } });
  kadyrov::Circle testCirc1({ 8.0, -5.0 }, 3.0);
  kadyrov::Circle testCirc2({ 4.0, 8.0 }, 1.0);

  std::shared_ptr<kadyrov::Shape> rectPtr1 = std::make_shared<kadyrov::Rectangle>(testRect1);
  std::shared_ptr<kadyrov::Shape> circPtr1 = std::make_shared<kadyrov::Circle>(testCirc1);
  std::shared_ptr<kadyrov::Shape> circPtr2 = std::make_shared<kadyrov::Circle>(testCirc2);

  kadyrov::Matrix matrix(rectPtr1);
  matrix.addShape(circPtr1);
  matrix.addShape(circPtr2);

  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> layer1 = matrix[0];
  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> layer2 = matrix[1];

  BOOST_CHECK(layer1[0] == rectPtr1);
  BOOST_CHECK(layer1[1] == circPtr2);
  BOOST_CHECK(layer2[0] == circPtr1);
}

BOOST_AUTO_TEST_CASE(Matrix_Check_LayerSize)
{
  kadyrov::Rectangle testRect1({ 1.0, 1.0, { 25.0, 25.0 } });
  kadyrov::Circle testCirc1({ 0.0, 0.0 }, 1.0);
  kadyrov::Circle testCirc2({ -5.0, -8.0 }, 1.0);

  std::shared_ptr<kadyrov::Shape> rectPtr1 = std::make_shared<kadyrov::Rectangle>(testRect1);
  std::shared_ptr<kadyrov::Shape> circPtr1 = std::make_shared<kadyrov::Circle>(testCirc1);
  std::shared_ptr<kadyrov::Shape> circPtr2 = std::make_shared<kadyrov::Circle>(testCirc2);

  kadyrov::Matrix matrix(rectPtr1);
  matrix.addShape(circPtr1);
  matrix.addShape(circPtr2);
  int count = 3;

  BOOST_CHECK_EQUAL(matrix.getLayerSize(), count);
}

BOOST_AUTO_TEST_CASE(Matrix_Check_Layers)
{
  std::shared_ptr<kadyrov::Shape> testRect1 =
      std::make_shared<kadyrov::Rectangle>(kadyrov::Rectangle({ 4.0, 2.0, { 0.0, 0.0 } }));
  std::shared_ptr<kadyrov::Shape> testCirc1 =
      std::make_shared<kadyrov::Circle>(kadyrov::Circle({ 0.0, 0.0 }, 2.0));
  std::shared_ptr<kadyrov::Shape> testCirc2 =
      std::make_shared<kadyrov::Circle>(kadyrov::Circle({ 0.0, 0.0 }, 1.0));

  kadyrov::Matrix matrix(testRect1);
  matrix.addShape(testCirc1);
  matrix.addShape(testCirc2);

  int count = 3;
  BOOST_CHECK_EQUAL(count, matrix.getLayers());
}

BOOST_AUTO_TEST_CASE(Matrix_Copy_Constructor)
{
  kadyrov::Rectangle testRect1({ 1.0, 2.0, { 3.0, 4.0 } });
  std::shared_ptr<kadyrov::Shape> rectPtr1 = std::make_shared<kadyrov::Rectangle>(testRect1);
  kadyrov::Circle testCirc1({ -1.0, -2.0 }, 1.0);
  std::shared_ptr<kadyrov::Shape> circPtr1 = std::make_shared<kadyrov::Circle>(testCirc1);

  kadyrov::Matrix matrix1(rectPtr1);
  matrix1.addShape(circPtr1);
  kadyrov::Matrix matrix2(matrix1);

  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> layer1 = matrix1[0];
  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> layer2 = matrix2[0];

  BOOST_CHECK(layer1[0] == rectPtr1);
  BOOST_CHECK(layer1[1] == circPtr1);
  BOOST_CHECK(layer2[0] == rectPtr1);
  BOOST_CHECK(layer2[1] == circPtr1);
}

BOOST_AUTO_TEST_CASE(Matrix_Move_Constructor)
{
  kadyrov::Rectangle testRect1({ 1.0, 2.0, { 3.0, 4.0 } });
  std::shared_ptr<kadyrov::Shape> rectPtr1 = std::make_shared<kadyrov::Rectangle>(testRect1);
  kadyrov::Circle testCirc1({ -1.0, -2.0 }, 1.0);
  std::shared_ptr<kadyrov::Shape> circPtr1 = std::make_shared<kadyrov::Circle>(testCirc1);

  kadyrov::Matrix matrix1(rectPtr1);
  matrix1.addShape(circPtr1);
  kadyrov::Matrix matrix2(std::move(matrix1));

  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> layer1 = matrix2[0];

  BOOST_CHECK(layer1[0] == rectPtr1);
  BOOST_CHECK(layer1[1] == circPtr1);
}

BOOST_AUTO_TEST_CASE(Matrix_Copy_Operator)
{
  kadyrov::Rectangle testRect1({ 1.0, 2.0, { 3.0, 4.0 } });
  std::shared_ptr<kadyrov::Shape> rectPtr1 = std::make_shared<kadyrov::Rectangle>(testRect1);
  kadyrov::Circle testCirc1({ -1.0, -2.0 }, 1.0);
  std::shared_ptr<kadyrov::Shape> circPtr1 = std::make_shared<kadyrov::Circle>(testCirc1);

  kadyrov::Matrix matrix1(rectPtr1);
  matrix1.addShape(circPtr1);
  kadyrov::Matrix matrix2;
  matrix2 = matrix1;

  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> layer1 = matrix1[0];
  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> layer2 = matrix2[0];

  BOOST_CHECK(layer1[0] == rectPtr1);
  BOOST_CHECK(layer1[1] == circPtr1);
  BOOST_CHECK(layer2[0] == rectPtr1);
  BOOST_CHECK(layer2[1] == circPtr1);
}

BOOST_AUTO_TEST_CASE(Matrix_Move_Operator)
{
  kadyrov::Rectangle testRect1({ 1.0, 2.0, { 3.0, 4.0 } });
  std::shared_ptr<kadyrov::Shape> rectPtr1 = std::make_shared<kadyrov::Rectangle>(testRect1);
  kadyrov::Circle testCirc1({ -1.0, -2.0 }, 1.0);
  std::shared_ptr<kadyrov::Shape> circPtr1 = std::make_shared<kadyrov::Circle>(testCirc1);

  kadyrov::Matrix matrix1(rectPtr1);
  matrix1.addShape(circPtr1);
  kadyrov::Matrix matrix2;
  matrix2 = std::move(matrix1);

  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> layer1 = matrix2[0];

  BOOST_CHECK(layer1[0] == rectPtr1);
  BOOST_CHECK(layer1[1] == circPtr1);
}

BOOST_AUTO_TEST_CASE(Matrix_rotate_changes)
{
  std::shared_ptr< kadyrov::Shape > ptr1(new kadyrov::Circle({ 1.0, 1.0 }, 2));
  std::shared_ptr< kadyrov::Shape > ptr2(new kadyrov::Rectangle({ 2.0, 4.0, { 4.0, 4.0 } }));

  kadyrov::CompositeShape testComp1;
  testComp1.addShape(ptr1);
  testComp1.addShape(ptr2);
  kadyrov::Matrix matrix1(testComp1);
  int count = 2;
  BOOST_CHECK_EQUAL(matrix1.getLayerSize(),count);
  testComp1.rotate(45.0);
  kadyrov::Matrix matrix2(testComp1);
  BOOST_CHECK_EQUAL(matrix2.getLayerSize(), count);
}

BOOST_AUTO_TEST_CASE(Add_Composite_Shape_and_new_function_of_getting_nubmer_of_figures_in_layer)
{
  kadyrov::Rectangle rect1( {5.0, 5.0, {0.0, 0.0}} );
  kadyrov::Circle circ1( {{8.0, 8.0}, 3} );
  kadyrov::Rectangle rect2( {3.5, 2.4, {-8.0, -8.0}} );
  std::shared_ptr <kadyrov::Shape> rectPtr1 = std::make_shared<kadyrov::Rectangle>(rect1);
  std::shared_ptr <kadyrov::Shape> circPtr1 = std::make_shared<kadyrov::Circle>(circ1);
  std::shared_ptr <kadyrov::Shape> rectPtr2 = std::make_shared<kadyrov::Rectangle>(rect2);

  kadyrov::Matrix matrix;
  kadyrov::CompositeShape testComp;
  testComp.addShape(rectPtr1);
  testComp.addShape(circPtr1);
  testComp.addShape(rectPtr2);

  matrix.addCompShape(testComp);
  int count = 3;

  BOOST_CHECK_EQUAL(matrix.getFiguresInLayer(0), count);
}


BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(rotateTests)

BOOST_AUTO_TEST_CASE(rectangle_Tests)
{
  kadyrov::Rectangle rect1( {5.0, 4.0, {7.0, 7.0}} );
  kadyrov::Rectangle rect2( {5.0, 4.0, {7.0, 7.0}} );
  rect1.rotate(30);
  rect1.move(24.0, 24.0);
  BOOST_CHECK_CLOSE(rect1.getArea(), rect2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(rect1.getFrameRect().pos.x, rect2.getFrameRect().pos.x + 24.0, EPSILON);
  BOOST_CHECK_CLOSE(rect1.getFrameRect().pos.y, rect2.getFrameRect().pos.y + 24.0, EPSILON);
  rect1.scale(2);
  BOOST_CHECK_CLOSE(rect1.getArea(), 4 * rect2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(rect1.getRectangle().width, 2 * rect2.getRectangle().width, EPSILON);
  BOOST_CHECK_CLOSE(rect1.getRectangle().height, 2 * rect2.getRectangle().height, EPSILON);
}

BOOST_AUTO_TEST_CASE(rotateCompositeShape)
{
  std::shared_ptr< kadyrov::Shape > ptr1(new kadyrov::Rectangle({2.0, 2.0, {10.0, 10.0}}));
  std::shared_ptr< kadyrov::Shape > ptr2(new kadyrov::Rectangle({4.0, 4.0, {0.0, 0.0}}));
  kadyrov::CompositeShape testComp1;
  testComp1.addShape(ptr1);
  testComp1.addShape(ptr2);
  kadyrov::CompositeShape testComp2(testComp1);
  testComp1.rotate(60);
  testComp1.move(5.0, 5.0);
  kadyrov::point_t center = {10.0, 9.5};

  BOOST_CHECK_CLOSE(testComp1.getFrameRect().pos.x, center.x, EPSILON);
  BOOST_CHECK_CLOSE(testComp1.getFrameRect().pos.y, center.y, EPSILON);
  BOOST_CHECK_CLOSE(testComp1.getArea(), testComp2.getArea(),EPSILON);
  BOOST_CHECK_CLOSE(testComp1.getArea(),20, EPSILON);
  BOOST_CHECK_CLOSE(testComp2.getArea(),20, EPSILON);
  BOOST_CHECK_CLOSE(testComp1.getFrameRect().width, 7.75833024, EPSILON);
  BOOST_CHECK_CLOSE(testComp1.getFrameRect().height, 17.75833024, EPSILON);
  BOOST_CHECK_CLOSE(testComp1.getFrameRect().pos.x, center.x, EPSILON);
  BOOST_CHECK_CLOSE(testComp1.getFrameRect().pos.y, center.y, EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
