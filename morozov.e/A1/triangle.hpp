#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t &side1, const point_t &side2, const point_t &side3);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &purpose) override;
  void move(double dx, double dy) override;

private:
  point_t pos_;
  point_t side1_;
  point_t side2_;
  point_t side3_;
};

#endif
