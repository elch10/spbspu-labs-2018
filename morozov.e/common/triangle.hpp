#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace morozov {
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &side1, const point_t &side2, const point_t &side3);
    double getArea() const override;
    double getSide1();
    double getSide2();
    double getSide3();
    point_t getPos() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &purpose) override;
    void move(double dx, double dy) override;
    virtual void scale(double coefficient) override;
    void rotate(double phi) override;

  private:
    point_t pos_;
    point_t apex1_;
    point_t apex2_;
    point_t apex3_;
  };
}

#endif
