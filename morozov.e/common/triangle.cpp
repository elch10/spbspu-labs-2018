#define _USE_MATH_DEFINES
#include "triangle.hpp"
#include <iostream>
#include <cmath>

morozov::Triangle::Triangle(const point_t &side1, const point_t &side2, const point_t &side3) :pos_({ (side1.x + side2.x + side3.x) / 3, (side1.y + side2.y + side3.y) / 3 })
{
  apex1_ = side1;
  apex2_ = side2;
  apex3_ = side3;
  if (getArea() == 0.0)
    throw std::invalid_argument("Degenerate triangle\n");
}

double morozov::Triangle::getArea() const
{
  return std::abs(0.5 * ((apex2_.x - apex1_.x) * (apex3_.y - apex1_.y) - (apex3_.x - apex1_.x) * (apex2_.y - apex1_.y)));
}

morozov::rectangle_t morozov::Triangle::getFrameRect() const
{
  double max_x = apex1_.x;
  double max_y = apex1_.y;

  double min_x = max_x;
  double min_y = max_y;

  if (apex2_.x < apex3_.x)
  {
    if (apex3_.x > max_x)
    {
      max_x = apex3_.x;
    }

    if (apex2_.x < min_x)
    {
      min_x = apex2_.x;
    }
  }
  else
  {
    if (apex2_.x > max_x)
    {
      max_x = apex2_.x;
    }

    if (apex3_.x < min_x)
    {
      min_x = apex3_.x;
    }
  }

  if (apex2_.y < apex3_.y)
  {
    if (apex3_.y > max_y)
    {
      max_y = apex3_.y;
    }

    if (apex2_.y < min_y)
    {
      min_y = apex2_.y;
    }
  }
  else
  {
    if (apex2_.y > max_y)
    {
      max_y = apex2_.y;
    }

    if (apex3_.y < min_y)
    {
      min_y = apex3_.y;
    }
  }

  return{ max_x - min_x, max_y - min_y,{ min_x, min_y } };
}

void morozov::Triangle::move(const point_t &purpose)
{
  double dx = purpose.x - pos_.x;
  double dy = purpose.y - pos_.y;

  pos_ = purpose;

  apex1_.x = apex1_.x + dx;
  apex1_.y = apex1_.y + dy;

  apex2_.x = apex2_.x + dx;
  apex2_.y = apex2_.y + dy;

  apex3_.x = apex3_.x + dx;
  apex3_.y = apex3_.y + dy;
}

void morozov::Triangle::move(double dx, double dy)
{
  pos_.x = pos_.x + dx;
  pos_.y = pos_.y + dy;

  apex1_.x = apex1_.x + dx;
  apex1_.y = apex1_.y + dy;

  apex2_.x = apex2_.x + dx;
  apex2_.y = apex2_.y + dy;

  apex3_.x = apex3_.x + dx;
  apex3_.y = apex3_.y + dy;
}

void morozov::Triangle::scale(const double coefficient)
{
  if (coefficient <= 0.0)
    throw std::invalid_argument("Scale coefficient must be >= 0");

  apex1_.x = pos_.x + (apex1_.x - pos_.x)*coefficient;
  apex1_.y = pos_.y + (apex1_.y - pos_.y)*coefficient;
  apex2_.x = pos_.x + (apex2_.x - pos_.x)*coefficient;
  apex2_.y = pos_.y + (apex2_.y - pos_.y)*coefficient;
  apex3_.x = pos_.x + (apex3_.x - pos_.x)*coefficient;
  apex3_.y = pos_.y + (apex3_.y - pos_.y)*coefficient;
}

double morozov::Triangle::getSide1()
{
  return sqrt((apex1_.x - apex2_.x) * (apex1_.x - apex2_.x) + (apex1_.y - apex2_.y) * (apex1_.y - apex2_.y));
}

double morozov::Triangle::getSide2()
{
  return sqrt((apex1_.x - apex3_.x) * (apex1_.x - apex3_.x) + (apex1_.y - apex3_.y) * (apex1_.y - apex3_.y));
}

double morozov::Triangle::getSide3()
{
  return sqrt((apex3_.x - apex2_.x) * (apex3_.x - apex2_.x) + (apex3_.y - apex2_.y) * (apex3_.y - apex2_.y));
}

morozov::point_t morozov::Triangle::getPos() const
{
  return pos_;
}

void morozov::Triangle::rotate(double phi)
{
  point_t tmp;
  double radAngle = (phi*M_PI) / 180.0;

  //apex1
  tmp.x = apex1_.x;
  tmp.y = apex1_.y;

  tmp.x = tmp.x - pos_.x;
  tmp.y = tmp.y - pos_.y;

  apex1_.x = tmp.x* cos(radAngle) - tmp.y* sin(radAngle);
  apex1_.y = tmp.x* sin(radAngle) + tmp.y* cos(radAngle);

  apex1_.x = apex1_.x + pos_.x;
  apex1_.y = apex1_.y + pos_.y;

  //apex2
  tmp.x = apex2_.x;
  tmp.y = apex2_.y;

  tmp.x = tmp.x - pos_.x;
  tmp.y = tmp.y - pos_.y;

  apex2_.x = tmp.x* cos(radAngle) - tmp.y* sin(radAngle);
  apex2_.y = tmp.x* sin(radAngle) + tmp.y* cos(radAngle);

  apex2_.x = apex2_.x + pos_.x;
  apex2_.y = apex2_.y + pos_.y;

  //apex3
  tmp.x = apex3_.x;
  tmp.y = apex3_.y;

  tmp.x = tmp.x - pos_.x;
  tmp.y = tmp.y - pos_.y;

  apex3_.x = tmp.x* cos(radAngle) - tmp.y* sin(radAngle);
  apex3_.y = tmp.x* sin(radAngle) + tmp.y* cos(radAngle);

  apex3_.x = apex3_.x + pos_.x;
  apex3_.y = apex3_.y + pos_.y;
}
