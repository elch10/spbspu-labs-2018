#define _USE_MATH_DEFINES
#include <cmath>
#include "rectangle.hpp"
#include <iostream>

morozov::Rectangle::Rectangle(const point_t &center, double width, double height) :pos_(center), angle_(0)
{
  if ((width >= 0.0) && (height >= 0.0))
  {
    width_ = width;
    height_ = height;
  }
  else
    throw std::invalid_argument("Rectanhle does not exist\n");
}

double morozov::Rectangle::getArea() const
{
  return height_ * width_;
}

morozov::rectangle_t morozov::Rectangle::getFrameRect() const
{
  const double phi = angle_*M_PI / 180;

  const double width = height_ * fabs(sin(phi)) + width_ * fabs(cos(phi));
  const double height = height_  * fabs(cos(phi)) + width_ * fabs(sin(phi));

  return{ width, height, pos_ };
}

void morozov::Rectangle::move(const point_t &goal)
{
  pos_ = goal;
}

void morozov::Rectangle::move(double dx, double dy)
{
  pos_.x = pos_.x + dx;
  pos_.y = pos_.y + dy;
}

void morozov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0.0)
    throw std::invalid_argument("Scale coefficient must be > 0");

  height_ *= coefficient;
  width_ *= coefficient;
}

double morozov::Rectangle::getHeight()
{
  return this->height_;
}

double morozov::Rectangle::getWidth()
{
  return this->width_;
}

morozov::point_t morozov::Rectangle::getPos() const
{
  return pos_;
}

void morozov::Rectangle::rotate(double phi)
{
  angle_ += phi;
}
