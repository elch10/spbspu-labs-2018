#include "matrix.hpp"
#include <cmath>

morozov::Matrix::Matrix() :size_(0), layerNumber_(0), array_(new Shape*[0]), layer_(new size_t[0])
{}

morozov::Matrix::Matrix(std::initializer_list <Shape*> shapes)
{
  std::initializer_list<Shape*>::const_iterator iter;

  for (iter = shapes.begin(); iter != shapes.end(); iter++)
  {
    if (*iter == nullptr)
      throw std::invalid_argument("no shapes added");    
  }

  if (shapes.size() == 0)
    throw std::invalid_argument("the list is empty");  

  iter = shapes.begin();
  array_ = boost::shared_array<Shape*>{ new Shape*[1] };
  layer_ = boost::shared_array<size_t>{ new size_t[1] };
  array_[0] = *iter;
  layer_[0] = 1;
  size_ = 1;
  layerNumber_ = 1;
  iter++;

  for (; iter != shapes.end(); iter++)
  {
    addShape(*iter);
  }
}

morozov::Matrix::Matrix(const Matrix & matr) :
  size_(matr.getSize()),
  layerNumber_(matr.getLayerNum()),
  array_(new Shape*[matr.getSize()]),
  layer_(new size_t[matr.getLayerNum()])
{
  for (size_t i = 0; i < size_; i++)
  {
    array_[i] = matr.array_[i];
  }

  for (size_t i = 0; i < layerNumber_; i++)
  {
    layer_[i] = matr.layer_[i];
  }
}

morozov::Matrix::~Matrix()
{
  array_.reset();
  layer_.reset();
}

morozov::Matrix& morozov::Matrix::operator=(const Matrix & matr)
{
  if (this != &matr)
  {
    boost::shared_array <Shape*> tmpArray{ new Shape*[matr.getSize()] };
    boost::shared_array <size_t> tmpLayer{ new size_t[matr.getLayerNum()] };

    for (size_t i = 0; i < matr.getSize(); i++)
    {
      tmpArray[i] = matr.array_[i];
    }

    for (size_t i = 0; i < matr.getLayerNum(); i++)
    {
      tmpLayer[i] = matr.layer_[i];
    }

    size_ = matr.getSize();
    layerNumber_ = matr.getLayerNum();

    array_.reset();
    layer_.reset();

    array_ = tmpArray;
    layer_ = tmpLayer;
  }
  return *this;
}

void morozov::Matrix::addShape(Shape* shape)
{
  size_t line;
  size_t pos = 0;
  line = searchPos(shape);

  if (line == layerNumber_)
  {
    boost::shared_array <size_t> tmpLayer{ new size_t[layerNumber_ + 1] };

    for (size_t i = 0; i < layerNumber_; i++)
    {
      tmpLayer[i] = layer_[i];
    }

    tmpLayer[layerNumber_] = 0;
    layer_.reset(new size_t[layerNumber_ + 1]);
    layer_ = tmpLayer;
    layerNumber_++;
  }

  for (size_t i = 0; i < line; i++)
  {
    pos += layer_[i];
  }
  pos += layer_[line];

  boost::shared_array <Shape*> tmpArray{ new Shape*[size_ + 1] };

  for (size_t i = 0; i < pos; i++)
  {
    tmpArray[i] = array_[i];
  }

  for (size_t i = pos + 1; i < size_ + 1; i++)
  {
    tmpArray[i] = array_[i - 1];
  }

  tmpArray[pos] = shape;
  array_.reset(new Shape*[size_ + 1]);
  array_ = tmpArray;
  layer_[line]++;
  size_++;
}

morozov::Matrix morozov::Matrix::createMatrix(morozov::CompositeShape& compShape)
{
  if (compShape.getAmount() < 1)
    throw std::invalid_argument("compositeShape is empty");

  size_t size = compShape.getAmount();
  Matrix mat_x{ compShape[0] };

  for (size_t i = 1; i < size; i++)
  {
    mat_x.addShape(compShape[i]);
  }

  return mat_x;
}

void morozov::Matrix::resetMatrix()
{
  array_.reset();
  layer_.reset();
  size_ = 0;
  layerNumber_ = 0;
}

size_t morozov::Matrix::getLayerNum() const
{
  return layerNumber_;
}

size_t morozov::Matrix::getLayerSize(size_t line) const
{
  if (line > layerNumber_)
    throw std::invalid_argument("the layer does not exist");

  return layer_[line];
}

morozov::Shape* morozov::Matrix::getShape(size_t line, size_t column) const
{
  if (line > layerNumber_)
    throw std::invalid_argument("the shape does not exist");

  if (column > layer_[line])
    throw std::invalid_argument("the shape does not exist");

  size_t curr = 0;
  for (size_t i = 0; i < line; i++)
  {
    curr += layer_[i];
  }
  curr += column;

  return array_[curr];
}

size_t morozov::Matrix::getSize() const
{
  return size_;
}

size_t morozov::Matrix::searchPos(const Shape* shape) const
{
  if (layerNumber_ == 0)
  {
    return 0;
  }

  size_t line = layerNumber_;
  if (layerNumber_ == 1)
  {
    if (checkOverLapping(shape, 0))
    {
      return 1;
    }
    return 0;
  }

  for (size_t i = layerNumber_ - 1; i >= 1; i--)
  {
    if (checkOverLapping(shape, i))
    {
      return i + 1;
    }

    if (i == 1)
    {
      if (checkOverLapping(shape, 0))
      {
        return 1;
      }

      return 0;
    }
  }

  return line;
}

bool morozov::Matrix::checkOverLapping(const Shape* shape, size_t line) const
{
  rectangle_t re_le1;
  rectangle_t re_le2 = shape->getFrameRect();
  size_t pos = 0;
  double dx, dy, dh, dw;

  for (size_t i = 0; i < line; i++)
  {
    pos += layer_[i];
  }

  for (size_t i = 0; i < layer_[line]; i++)
  {
    re_le1 = array_[pos + i]->getFrameRect();

    dx = std::abs((re_le1.pos.x) - (re_le2.pos.x));
    dy = std::abs((re_le1.pos.y) - (re_le2.pos.y));

    dh = (re_le1.height / 2) + (re_le2.height / 2);
    dw = (re_le1.width / 2) + (re_le2.width / 2);
    
    if ((dx <= dw) & (dy <= dh))
    {
      return true;
    }
  }
  return false;
}
