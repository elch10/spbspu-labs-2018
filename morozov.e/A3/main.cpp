#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include <iostream>

void testCompShape(const morozov::Shape & Shape1)
{
  std::cout << "Current position: " << Shape1.getPos().x << ", " << Shape1.getPos().y << "" << std::endl;
  std::cout << "Area: " << Shape1.getArea() << std::endl;
  morozov::rectangle_t Frame1 = Shape1.getFrameRect();
  std::cout << "Position: x=  " << Frame1.pos.x << ", y=  " << Frame1.pos.y << "" << std::endl;
  std::cout << "Height: " << Frame1.height << " Width: " << Frame1.width << std::endl << std::endl;
}

int main()
{
  morozov::Circle ci_le({ 0.0, 0.0 }, 1.0);
  morozov::Rectangle re_le({ 0.0, 0.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });
  morozov::CompositeShape co_te;

  co_te.pushNew(&re_le);
  co_te.pushNew(&ci_le);
  co_te.pushNew(&tr_le);

  testCompShape(co_te);

  co_te.move(11.0, 11.0);
  testCompShape(co_te);

  co_te.move({ 10.0,10.0 });
  testCompShape(co_te);

  co_te.scale(3.0);
  testCompShape(co_te);

  return 0;
}
