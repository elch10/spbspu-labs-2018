#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

using namespace burcev;

int main()
{
  try
  {
    Rectangle rect({24.0, 48.0, {128.0, 256.0}});
    Circle circle(5.0, {10.0, 10.0});
    Triangle triangle{ { 0, 0 }, { 10, 10 }, { 20, 0 } };
  
    Shape * pointer = &rect;

    std::cout << "Rectangle:" << std::endl;
    std::cout << "Area of Rectangle before scaling = " << pointer -> getArea() << std::endl;
    pointer -> scale(2.0);
    std::cout << "Area of Rectangle after scaling = " << pointer -> getArea() << std::endl;
    std::cout << std::endl;
    

    pointer = &circle;

    std::cout << "Circle:" << std::endl;
    std::cout << "Area of Circle before scaling = " << pointer -> getArea() << std::endl;
    pointer -> scale(3.0);
    std::cout << "Area of Circle after scaling = " << pointer -> getArea() << std::endl;

    pointer = &triangle;

    std::cout << "triangle:" << std::endl;
    std::cout << "Area of triangle before scaling = " << pointer -> getArea() << std::endl;
    pointer -> scale(3.0);
    std::cout << "Area of triangle after scaling = " << pointer -> getArea() << std::endl;
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}

