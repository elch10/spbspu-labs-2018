#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

using namespace burcev;

const double EPS = 0.0001;

BOOST_AUTO_TEST_SUITE(Rectangle_Tests)

  BOOST_AUTO_TEST_CASE(Rect_InvarienceOfParametersTest)
  {
    Rectangle rect({24.0, 48.0, {128.0, 256.0}});
    double area = rect.getArea();
    rect.move(16.0, 32.0);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().width, 24.0);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().height, 48.0);
    BOOST_REQUIRE_EQUAL(rect.getArea(), area);
  }

  BOOST_AUTO_TEST_CASE(Rect_ScaleTest)
  {
    Rectangle rect({24.0, 48.0, {128.0, 256.0}});
    double area = rect.getArea();
    rect.scale(2.0);
    BOOST_REQUIRE_CLOSE_FRACTION(rect.getArea(), area * pow(2.0, 2.0), EPS);
  }

  BOOST_AUTO_TEST_CASE(Rect_InvalidArgumentConstructorTest)
  {
    BOOST_CHECK_THROW(Rectangle rect({-2.0, -2.0, {24.0, 48.0}}), std::invalid_argument);
    BOOST_CHECK_THROW(Rectangle rect({2.0, -2.0, {24.0, 48.0}}), std::invalid_argument);
    BOOST_CHECK_THROW(Rectangle rect({-2.0, 2.0, {24.0, 48.0}}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rect_InvalidArgumentScaleTest)
  {
    Rectangle rect({24.0, 48.0, {128.0, 256.0}});
    BOOST_CHECK_THROW(rect.scale(-1.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Circle_Tests)

  BOOST_AUTO_TEST_CASE(Circle_InvarienceOfParametersTest)
  {
    Circle circle(5.0, {10.0, 10.0});
    double area = circle.getArea();
    circle.move(16.0, 32.0);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().width, 10.0);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().height, 10.0);
    BOOST_REQUIRE_EQUAL(circle.getArea(), area);
  }

  BOOST_AUTO_TEST_CASE(Circle_ScaleTest)
  {
    Circle circle(5.0, {10.0, 10.0});
    double area = circle.getArea();
    circle.scale(3.0);
    BOOST_REQUIRE_CLOSE_FRACTION(circle.getArea(), (area * pow(3.0, 2.0)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Cirlce_InvalidArgumentConstructorTest)
  {
    BOOST_CHECK_THROW(Circle circle(-2.0, {10.0, 10.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Circle_InvalidArgumentScaleTest)
  {
    Circle circle(5.0, {10.0, 10.0});
    BOOST_CHECK_THROW(circle.scale(-1.0), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END() 


BOOST_AUTO_TEST_SUITE(triangle_Tests)

  BOOST_AUTO_TEST_CASE(triangle_InvarienceOfParametersTest)
  {
    Triangle triangle({5.0, 11.0}, {11.0, 10.0}, {10.0, 12.0});
    double area = triangle.getArea();
    triangle.move(16.0, 32.0);
    BOOST_REQUIRE_EQUAL(triangle.getFrameRect().width, 6.0);
    BOOST_REQUIRE_EQUAL(triangle.getFrameRect().height, 2.0);
    BOOST_REQUIRE_EQUAL(triangle.getArea(), area);
  }

  BOOST_AUTO_TEST_CASE(triangle_ScaleTest) 
  {
    Triangle triangle({5.0, 11.0}, {11.0, 10.0}, {10.0, 15.0});
    double area = triangle.getArea();
    triangle.scale(3.0);
    BOOST_REQUIRE_CLOSE_FRACTION(triangle.getArea(), (area * pow(3.0, 2.0)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Circle_InvalidArgumentScaleTest)
  {
    Triangle triangle({5.0, 11.0}, {11.0, 10.0}, {10.0, 15.0});
    BOOST_CHECK_THROW(triangle.scale(-1.0), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()
