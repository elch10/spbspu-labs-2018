#include "circle.hpp"
#include <math.h>
#include <stdexcept>

Circle::Circle(const point_t & circlecenter, double rad):
  rad_(rad),
  circlecenter_(circlecenter)
{
  if (rad_ < 0.0) 
  {
    throw std::invalid_argument("Error. Invalid radius of circle.");
  }
}

double Circle::getArea() const
{
  return (rad_ * rad_ * M_PI);
}

rectangle_t Circle::getFrameRect() const
{
  rectangle_t result = {circlecenter_, 2 * rad_, 2 * rad_};
  return result;
}

void Circle::move(const point_t & newpoint)
{
  circlecenter_ = newpoint;
}

void Circle::move(double disx, double disy)
{
  circlecenter_.x += disx;
  circlecenter_.y += disy;
}
