#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include <iostream>

using namespace burcev;

int main()
{
  try
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle({24.0, 48.0, {128.0, 256.0}}));
    CompositeShape comp_shape(rectPtr);
    rectangle_t frameRect = comp_shape.getFrameRect();
    std::cout << "Frame Rects of Composite Shape: " << std::endl;
    std::cout << "Coords: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
    std::cout << "Width = " << frameRect.width << " Height = " << frameRect.height << std::endl;
    std::cout << "Area of Composite Shape = " << comp_shape.getArea() << std::endl;
    
    std::cout << std::endl;

    std::shared_ptr<Shape> circlePtr = std::make_shared<Circle>(Circle(5.0, {10.0, 10.0}));
    comp_shape.addShape(circlePtr);
    frameRect = comp_shape.getFrameRect();
    std::cout << "Frame Rects of Composite Shape: " << std::endl;
    std::cout << "Coords: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
    std::cout << "Width = " << frameRect.width << " Height = " << frameRect.height << std::endl;
    std::cout << "Area of Composite Shape after adding new shape = " << comp_shape.getArea() << std::endl;

    std::cout << std::endl;

    std::shared_ptr<Shape> trianglePtr = std::make_shared<Triangle>(Triangle({5.0, 11.0}, {11.0, 10.0}, {10.0, 15.0}));
    comp_shape.addShape(trianglePtr);
    frameRect = comp_shape.getFrameRect();
    std::cout << "Frame Rects of Composite Shape: " << std::endl;
    std::cout << "Coords: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
    std::cout << "Width = " << frameRect.width << " Height = " << frameRect.height << std::endl;
    std::cout << "Area of Composite Shape after adding new shape = " << comp_shape.getArea() << std::endl;

    comp_shape.removeShape(0);
    frameRect = comp_shape.getFrameRect();
    std::cout << "Frame Rects of Composite Shape: " << std::endl;
    std::cout << "Coords: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
    std::cout << "Width = " << frameRect.width << " Height = " << frameRect.height << std::endl;
    std::cout << "Area of Composite Shape after deleting first shape = " << comp_shape.getArea() << std::endl;

    std::cout << std::endl;

    comp_shape.scale(2.0);
    frameRect = comp_shape.getFrameRect();
    std::cout << "Frame Rects of Composite Shape: " << std::endl;
    std::cout << "Coords: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
    std::cout << "Width = " << frameRect.width << " Height = " << frameRect.height << std::endl;
    std::cout << "Area of scaled Composite Shape = " << comp_shape.getArea() << std::endl;
  
    std::cout << std::endl;

    comp_shape.move({256.0, 128.0});
    frameRect = comp_shape.getFrameRect();
    std::cout << "Frame Rects of Composite Shape: " << std::endl;
    std::cout << "Coords: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
    std::cout << "Width = " << frameRect.width << " Height = " << frameRect.height << std::endl;
    std::cout << "Area of moved Composite Shape = " << comp_shape.getArea() << std::endl;
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}
