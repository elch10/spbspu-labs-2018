#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "base-types.hpp"
#include "triangle.hpp"

int main()
{
  zyukin::Rectangle r({ 20, 30 }, 5, 6);
  r.move(5, 10);
  r.move({ 44.31, 18.19 });
  r.getFrameRect();
  r.getArea();
  r.scale(5.0);
  zyukin::Circle c({ 14, 18 }, 3);
  c.move({ 11.31, 10 });
  c.move(5.5, 21.1);
  c.getFrameRect();
  c.getArea();
  c.scale(5.0);
  zyukin::Triangle t({ 20, 42 }, { 34, 33 }, { 15, 10 });
  t.move(1.8, 5);
  t.move({ 33, 107 });
  t.getFrameRect();
  t.getArea();

  return 0;
}
