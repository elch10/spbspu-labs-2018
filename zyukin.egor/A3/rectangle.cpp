#include <stdexcept>
#include <cmath>
#include "base-types.hpp"
#include "rectangle.hpp"

zyukin::Rectangle::Rectangle(const point_t &center, double width, double height):
  center_(center),
  width_(width),
  height_(height),
  alpha_(0.0)
{
  if(width < 0.0 || height < 0.0)
  {
    throw std::invalid_argument("Invalid Rectangle parameters!");
  }
}

double zyukin::Rectangle::getArea() const
{
  return width_*height_ ;
}

zyukin::rectangle_t zyukin::Rectangle::getFrameRect() const
{
  return rectangle_t{center_,width_, height_};
}

void zyukin::Rectangle::move(const point_t &point)
{
  center_ = point;
}

void zyukin::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void zyukin::Rectangle::scale(const double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("coefficient should be not negative");
  }
  width_ *= ratio;
  height_ *= ratio;
}

void zyukin::Rectangle::rotate(const double alpha)
{
  alpha_ += alpha;
  if (alpha_ >= 360.0)
  {
    alpha_ = fmod(alpha_, 360.0);
  }
}
