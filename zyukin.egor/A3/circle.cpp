#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <math.h>
#include <stdexcept>
#include "base-types.hpp"

zyukin::Circle::Circle(const point_t &center, double radius) :
  center_(center),
  radius_(radius),
  alpha_(0.0)
{
  if (radius_ < 0.0)
  {
    throw std::invalid_argument("Invalid circle parameters!");
  }
}

double zyukin::Circle::getArea() const
{
  return radius_*radius_*M_PI;
}

zyukin::rectangle_t zyukin::Circle::getFrameRect() const
{
  return rectangle_t{ center_, radius_ * 2, radius_ * 2 };
}

void zyukin::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void zyukin::Circle::move(const point_t &p)
{
  center_ = p;
}

void zyukin::Circle::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Wrong Circle scale coefficient!");
  }
  radius_ *= ratio;
}

void zyukin::Circle::rotate(const double alpha)
{
  alpha_ += alpha;
  if (alpha >= 360.0)
  {
    alpha_ = fmod(alpha_, 360.0);
  }
}
