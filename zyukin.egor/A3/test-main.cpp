#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

double EPSILON = 1e-4;

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)
  BOOST_AUTO_TEST_CASE(Area)
  {
    std::shared_ptr<zyukin::Shape> rectPtr
    = std::make_shared<zyukin::Rectangle>(zyukin::Rectangle({128.0, 256.0},24.0, 48.0));
    zyukin::CompositeShape shape(rectPtr);

    std::shared_ptr<zyukin::Shape> circlePtr
    = std::make_shared<zyukin::Circle>(zyukin::Circle({10.0, 10.0},5.0));
    shape.addShape(circlePtr);

    std::shared_ptr<zyukin::Shape> trianglePtr
    = std::make_shared<zyukin::Triangle>(zyukin::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);

    double area = shape.getArea();
    shape.move(16.0, 32.0);
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getArea(), area, EPSILON);
  }

BOOST_AUTO_TEST_CASE(Coords)
  {
    std::shared_ptr<zyukin::Shape> rectPtr
    = std::make_shared<zyukin::Rectangle>(zyukin::Rectangle({128.0, 256.0}, 24.0, 48.0));
    zyukin::CompositeShape shape(rectPtr);

    std::shared_ptr<zyukin::Shape> circlePtr
    = std::make_shared<zyukin::Circle>(zyukin::Circle({128.0, 256.0}, 4.0));
    shape.addShape(circlePtr);

    std::shared_ptr<zyukin::Shape> trianglePtr
    = std::make_shared<zyukin::Triangle>(zyukin::Triangle({2.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);

    double posX = shape.getFrameRect().pos.x;
    double posY = shape.getFrameRect().pos.y;
    const double ratio = 1.7;
    shape.scale(ratio);

    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.x, posX, EPSILON);
    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.y, posY, EPSILON);
  }

BOOST_AUTO_TEST_CASE(Scale)
  {
    const double ratio = 2.0;
    zyukin::point_t point = {6.0, 4.0};

    std::shared_ptr<zyukin::Shape> rectPtr (new zyukin::Rectangle(point, 6.0, 4.0));
    std::shared_ptr<zyukin::Shape> circlePtr (new zyukin::Circle(point, 40.0));
    std::shared_ptr<zyukin::Shape> trianglePtr (new zyukin::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));

    zyukin::CompositeShape shape(rectPtr);
    shape.addShape(circlePtr);
    shape.addShape(trianglePtr);

    double area = shape.getArea();
    double posX = shape.getFrameRect().pos.x;
    double posY = shape.getFrameRect().pos.y;

    shape.scale(ratio);

    BOOST_CHECK_CLOSE(shape.getArea(), area*ratio*ratio, EPSILON);
    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.x, posX, EPSILON);
    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.y, posY, EPSILON);
  }

BOOST_AUTO_TEST_CASE(InvalidArgumentConstructor)
  {
    std::shared_ptr<zyukin::Shape> rectPtr
    = std::make_shared<zyukin::Rectangle>(zyukin::Rectangle({128.0, 256.0},24.0, 48.0));
    zyukin::CompositeShape shape(rectPtr);

    std::shared_ptr<zyukin::Shape> circlePtr
    = std::make_shared<zyukin::Circle>(zyukin::Circle({10.0, 10.0}, 5.0));
    shape.addShape(circlePtr);

    std::shared_ptr<zyukin::Shape> trianglePtr
    = std::make_shared<zyukin::Triangle>(zyukin::Triangle({4.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);

    BOOST_REQUIRE_THROW(zyukin::CompositeShape shape(nullptr), std::invalid_argument);
  }

BOOST_AUTO_TEST_CASE(InvalidArgumentAddShape)
  {
    std::shared_ptr<zyukin::Shape> rectPtr
    = std::make_shared<zyukin::Rectangle>(zyukin::Rectangle({128.0, 256.0},24.0, 48.0));
    zyukin::CompositeShape shape(rectPtr);

    BOOST_REQUIRE_THROW(shape.addShape(nullptr), std::invalid_argument);
  }

BOOST_AUTO_TEST_CASE(InvalidArgumentScale)
  {
    zyukin::point_t point = { 6.0, 8.0 };
    std::shared_ptr<zyukin::Shape> rectPtr (new zyukin::Rectangle(point, 40.0, 50.0));
    zyukin::CompositeShape shape(rectPtr);
    BOOST_CHECK_THROW(shape.scale(-5.0), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()
