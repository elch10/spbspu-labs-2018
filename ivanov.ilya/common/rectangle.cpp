#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

ivanov::Rectangle::Rectangle(const rectangle_t & rect):
  bar_(rect)
{
  if(rect.width < 0.0)
  {
    throw std::invalid_argument("Rectangle negative width");
  }
  if (rect.height < 0.0)
  {
    throw std::invalid_argument("Rectangle negative height");
  }
}

double ivanov::Rectangle::getArea() const noexcept
{
  return bar_.width * bar_.height;
}

ivanov::rectangle_t ivanov::Rectangle::getFrameRect() const noexcept
{
  return bar_;
}

void ivanov::Rectangle::move(const point_t & point) noexcept
{
  bar_.pos = point;
}

void ivanov::Rectangle::move(double dx, double dy) noexcept
{
  bar_.pos.x += dx;
  bar_.pos.y += dy;
}

void ivanov::Rectangle::scale(double koeff)
{
  if (koeff < 0.0)
  {
    throw std::invalid_argument("Koefficient must be positive or equal zero");
  }
  bar_.width *= koeff;
  bar_.height *= koeff;
}

void ivanov::Rectangle::rotate (double angle) noexcept
{
  angle_ += angle;
}
