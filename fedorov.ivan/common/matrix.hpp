#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "composite-shape.hpp"

namespace fedorov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& matrix);
    Matrix(Matrix&& matrix);
    ~Matrix() = default;
    Matrix& operator=(const Matrix& matrix);
    Matrix& operator=(Matrix&& matrix);
    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](const int index);
    void addShape(const std::shared_ptr<Shape> shape);
    int getRowsCount() const;
    int getColumnsCount() const;
    void view() const;

  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> lineArr_;
    int rows_;
    int columns_;
    inline bool checkIntersection(const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2);
  };
}

#endif
