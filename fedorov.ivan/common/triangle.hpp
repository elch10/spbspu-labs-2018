#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace fedorov
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t& a, const point_t& b, const point_t& c);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& newPos) override;
    void move(const double dx, const double dy) override;
    void scale(const double multiplier) override;
    void info() const override;
    void rotate(const double angle) override;
    void printName() const override;
  private:
    point_t a_, b_, c_;
    point_t center_;
  };
}

#endif //TRIANGLE_HPP
