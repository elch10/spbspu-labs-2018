#include <iostream>
#include <cmath>
#include "circle.hpp"

fedorov::Circle::Circle(const point_t &startPos, const double radius):
  pos_{startPos},
  radius_(0.0),
  angle_(0.0)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("Invalid Radius ! ");
  }
  radius_ = radius;
}

double fedorov::Circle::getArea() const
{
  return M_PI*radius_*radius_;
}

fedorov::rectangle_t fedorov::Circle::getFrameRect() const
{
  return {pos_, 2*radius_, 2*radius_};
}

void fedorov::Circle::move(const point_t &newPos)
{
  pos_ = newPos;
}

void fedorov::Circle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void fedorov::Circle::scale(const double multiplier)
{
  if (multiplier < 0.0)
  {
    throw std::invalid_argument("Invalid mulptiplier !");
  }
  radius_ *= multiplier;
}

void fedorov::Circle::info() const
{
  const rectangle_t temp = getFrameRect();
  std::cout << "X = " << pos_.x << "; Y = " << pos_.y << ";" << std::endl;
  std::cout << "Radius = " << radius_ << ";" << std::endl;
  std::cout << "Circle Area: " << getArea() << std::endl;
  std::cout << "Frame properties: " << "pos = {" << temp.pos.x << ", " << temp.pos.y << "} ";
  std::cout << "Height = " << temp.height << "; Width = " << temp.width << "." << std::endl;
}

void fedorov::Circle::rotate(const double angle)
{
  angle_ += angle;
}

void fedorov::Circle::printName() const
{
  std::cout << "Circle ";
}
