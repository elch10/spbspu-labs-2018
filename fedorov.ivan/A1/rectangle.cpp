#include "rectangle.hpp"

#include <iostream>

Rectangle::Rectangle(const point_t &startPos, const double height, const double width):
  data_{startPos, 0.0, 0.0}
{
  if (height < 0.0 || width < 0.0)
  {
    throw std::invalid_argument("Invalid height/width !");
  }
  data_.height = height;
  data_.width = width;
}

double Rectangle::getArea() const
{
  return data_.height * data_.width;
}

rectangle_t Rectangle::getFrameRect() const
{
  return data_;
}

void Rectangle::move(const point_t &newPos)
{
  data_.pos = newPos;
}

void Rectangle::move(const double dx, const double dy)
{
  data_.pos.x += dx;
  data_.pos.y += dy;
}

void Rectangle::info() const
{
  std::cout << "X = " << data_.pos.x << "; Y = " << data_.pos.y << ";" << std::endl;
  std::cout << "Height = " << data_.height << "; Width = " << data_.width << ";" << std::endl;
  std::cout << "Rectangle Frame - same." << std::endl;
}
