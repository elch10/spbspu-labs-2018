#include "triangle.hpp"

#include <iostream>
#include <cmath>

Triangle::Triangle(const point_t &a, const point_t &b, const point_t &c):
  a_(a),
  b_(b),
  c_(c),
  center_{0.0, 0.0}
{
  center_={(a_.x+b_.x+c_.x)/3,(a_.y+b_.y+c_.y)/3};
}

double Triangle::getArea() const
{
  return fabs((a_.x - c_.x)*(b_.y - c_.y) - (a_.y - c_.y)*(b_.x - c_.x))*0.5;
}

rectangle_t Triangle::getFrameRect() const
{
  double maxX = a_.x > b_.x ? (a_.x > c_.x ? a_.x : c_.x) : (b_.x > c_.x ? b_.x : c_.x);
  double minX = a_.x < b_.x ? (a_.x < c_.x ? a_.x : c_.x) : (b_.x < c_.x ? b_.x : c_.x);
  double maxY = a_.y > b_.y ? (a_.y > c_.y ? a_.y : c_.y) : (b_.y > c_.y ? b_.y : c_.y);
  double minY = a_.y < b_.y ? (a_.y < c_.y ? a_.y : c_.y) : (b_.y < c_.y ? b_.y : c_.y);
  return {{minX + (maxX - minX)/2, minY + (maxY - minY)/2}, maxY - minY, maxX - minX};
}

void Triangle::move(const point_t &newPos)
{
  a_.x -= center_.x - newPos.x;
  a_.y -= center_.y - newPos.y;
  b_.x -= center_.x - newPos.x;
  b_.y -= center_.y - newPos.y;
  c_.x -= center_.x - newPos.x;
  c_.y -= center_.y - newPos.y;
  center_ = newPos;
}

void Triangle::move(double dx, const double dy)
{
  a_.x += dx;
  a_.y += dy;
  b_.x += dx;
  b_.y += dy;
  c_.x += dx;
  c_.y += dy;
  center_.x += dx;
  center_.y += dy;
}

void Triangle::info() const
{
  const rectangle_t temp = getFrameRect();
  std::cout << "A = (" << a_.x << ", " << a_.y << ");" << std::endl;
  std::cout << "B = (" << b_.x << ", " << b_.y << ");" << std::endl;
  std::cout << "C = (" << c_.x << ", " << c_.y << ");" << std::endl;
  std::cout << "CenterX = " << center_.x << "; CenterY = " << center_.y << ";" << std::endl;
  std::cout << "Triangle Area: " << getArea() << std::endl;
  std::cout << "Frame properties: " << "pos = {" << temp.pos.x << ", " << temp.pos.y << "} ";
  std::cout << "Height = " << temp.height << "; Width = " << temp.width << "." << std::endl;
}
