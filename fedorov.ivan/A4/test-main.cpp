#define BOOST_TEST_MODULE testMain

#include <boost/test/included/unit_test.hpp>
#include <cmath>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace fedorov;

double precision=0.0001;

Matrix objMatrix;
fedorov::Rectangle matrixRect({7.0, 8.0}, 14.0, 14.0);
fedorov::Circle matrixCirc({4.0, 4.0}, 8.0);
std::shared_ptr <fedorov::Rectangle> rectPtr = std::make_shared <fedorov::Rectangle> (matrixRect);
std::shared_ptr <fedorov::Circle> circPtr = std::make_shared <fedorov::Circle> (matrixCirc);

BOOST_AUTO_TEST_SUITE(MatrixTests)

  BOOST_AUTO_TEST_CASE(AddToMatrix1)
  {
    BOOST_CHECK_EQUAL(objMatrix.getRowsCount(), 0);
    BOOST_CHECK_EQUAL(objMatrix.getColumnsCount(), 0);
    objMatrix.addShape(rectPtr);
    BOOST_CHECK_EQUAL(objMatrix.getRowsCount(), 1);
    BOOST_CHECK_EQUAL(objMatrix.getColumnsCount(), 1);
  }

  BOOST_AUTO_TEST_CASE(AddToMatrix2)
  {
    objMatrix.addShape(circPtr);
    BOOST_CHECK_EQUAL(objMatrix.getRowsCount(), 1);
    BOOST_CHECK_EQUAL(objMatrix.getColumnsCount(), 2);
  }

  BOOST_AUTO_TEST_CASE(InvalidParameters)
  {
    BOOST_CHECK_THROW(objMatrix.addShape(nullptr), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Rotation)
  fedorov::Rectangle rect({20.0,20.0}, 5.0, 7.0);
  fedorov::Circle circ({5.0,5.0}, 5.0);
  BOOST_AUTO_TEST_CASE(Rotation1)
  {
    rect.rotate(90);
    BOOST_CHECK_EQUAL(rect.getFrameRect().width, 5.0);
    BOOST_CHECK_EQUAL(rect.getFrameRect().height, 7.0);
  }

  BOOST_AUTO_TEST_CASE(Rotation2)
  {
    rect.rotate(270);
    BOOST_CHECK_CLOSE(rect.getFrameRect().width, 7.0, precision);
    BOOST_CHECK_CLOSE(rect.getFrameRect().height, 5.0, precision);
  }

  BOOST_AUTO_TEST_CASE(Rotation3)
  {
    rect.rotate(55);
    BOOST_CHECK_EQUAL(rect.getFrameRect().pos.x, 20.0);
    BOOST_CHECK_EQUAL(rect.getFrameRect().pos.y, 20.0);
  }

  BOOST_AUTO_TEST_CASE(Rotation4)
  {
    circ.rotate(45);
    BOOST_CHECK_EQUAL(circ.getFrameRect().width / 2, 5.0);
    BOOST_CHECK_EQUAL(circ.getFrameRect().height / 2, 5.0);
  }
BOOST_AUTO_TEST_SUITE_END()
