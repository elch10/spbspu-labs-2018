#ifndef COMPOSITESHAPE_HPP_INCLUDED
#define COMPOSITESHAPE_HPP_INCLUDED
#include "shape.hpp"
#include <memory>

namespace turgunov
{
  class CompositeShape:
    public Shape
  {
  public:
    CompositeShape(const std::shared_ptr<Shape> &);
    CompositeShape(const CompositeShape &);
    CompositeShape & operator=(const CompositeShape &);
    void addShape(const std::shared_ptr<Shape> &);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printFeatures() const override {};
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void scale(double k) override;

  private:
    std::unique_ptr <std::shared_ptr<Shape>[]> shapes_;
    size_t size_;
  };
}

#endif
