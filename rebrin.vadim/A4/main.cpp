#include <iostream>
#include <memory>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace std;
using namespace rebrin;

int main()
{
  try
  {
    shared_ptr <Shape> rect0 = make_shared<Rectangle>(Rectangle({2.0, 2.0, {-5.0, 4.0}}));
    shared_ptr <Shape> rect1 = make_shared<Rectangle>(Rectangle({4.0, 2.0, {-2.0, 4.0}}));
    shared_ptr <Shape> rect2 = make_shared<Rectangle>(Rectangle({6.0, 2.0, {-3.0, 2.0}}));
    shared_ptr <Shape> circ0 = make_shared<Circle>(Circle({0.0, 6.0}, 2.0));

    CompositeShape my_composite_shape(rect0);

    my_composite_shape.addShape(rect1);
    my_composite_shape.addShape(rect2);
    my_composite_shape.addShape(circ0);

    cout << "FrameRect of Composite shape: " << endl
         << "width = " << my_composite_shape.getFrameRect().width << endl
         << "height = " << my_composite_shape.getFrameRect().height << endl
         << "center (" << my_composite_shape.getFrameRect().pos.x << ", " << my_composite_shape.getFrameRect().pos.y <<")" << endl
         << "Area: " << my_composite_shape.getArea() << endl << endl;
    my_composite_shape.rotate(90);
    cout << "After rotating on 90 degrees:" << endl
        << "width = " << my_composite_shape.getFrameRect().width << endl
        << "height = " << my_composite_shape.getFrameRect().height << endl
        << "center (" << my_composite_shape.getFrameRect().pos.x << ", " << my_composite_shape.getFrameRect().pos.y <<")" << endl
        << "Area: " << my_composite_shape.getArea() << endl << endl;

    Matrix my_matrix = my_composite_shape.createMatrix();
    cout << "Matrix [layers] [layer size]:" << endl << "["
         << my_matrix.getLayers() << "] [" << my_matrix.getLayerSize() << "]" <<endl;

  }
  catch (invalid_argument & error)
  {
    cerr << "Error : " << error.what() << endl;
    return 1;
  }
  catch (out_of_range & error)
  {
    cerr << "Error : " << error.what() << endl;
    return 1;
  }
  return 0;
}
