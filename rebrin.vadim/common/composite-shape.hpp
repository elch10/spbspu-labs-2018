#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"
#include "matrix.hpp"

namespace rebrin
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape(const std::shared_ptr< rebrin::Shape > newShape);
    CompositeShape(const rebrin::CompositeShape & composite_shape);
    CompositeShape(rebrin::CompositeShape && composite_shape);
    CompositeShape & operator= (const rebrin::CompositeShape & composite_shape);
    CompositeShape & operator= (rebrin::CompositeShape && composite_shape);
    std::shared_ptr< rebrin::Shape > & operator[] (const int index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double dx, const double dy) override;
    void move(const point_t & newPos) override;
    void scale(const double factor) override;
    void rotate(const double alpha) override;
    Matrix createMatrix() const;

    void addShape(const std::shared_ptr< rebrin::Shape > newShape);
    void removeShape(const int index);
    void clear();

  private:
    int count_;
    std::unique_ptr < std::shared_ptr < rebrin::Shape >[] > list_;
  };
}

#endif //COMPOSITE_SHAPE_HPP
