#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include <memory>

namespace rebrin
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix);
    Matrix & operator= (const Matrix & matrix);
    Matrix & operator= (Matrix && matrix);
    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](const int index) const;

    void addShape(const std::shared_ptr <Shape> & shape);
    int getLayers() const;
    int getLayerSize() const;

  private:
    int layers_;
    int layer_size_;
    std::unique_ptr <std::shared_ptr <Shape> []> matrix_;
    bool isOverlapping(const std::shared_ptr <Shape> & shape1, const std::shared_ptr <Shape> & shape2) const;
  };
}

#endif //MATRIX_HPP
