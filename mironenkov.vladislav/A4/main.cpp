#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace mironenkov;

void testShape(Shape & shapeObject)
{
  std::cout << "Start parameters:" << std::endl;
  std::cout << "Area: " << shapeObject.getArea() << std::endl;
  shapeObject.getFrameRect();
  
  std::cout << "Scale(2)" << std::endl;
  shapeObject.scale(2.0);
  std::cout << "Area: " << shapeObject.getArea() << std::endl;
  
  std::cout << "After moving in (3.0, 3.0)" << std::endl;
  shapeObject.move(3.0, 3.0);
  std::cout << "Area: " << shapeObject.getArea() << std::endl;
  shapeObject.getFrameRect();
  
  std::cout << "After moving to ({100, 100})" << std:: endl;
  shapeObject.move({100, 100});
  std::cout << "Area: " << shapeObject.getArea() << std::endl << std::endl;
  shapeObject.getFrameRect();
}

int main()
{
  try
  {
    Circle circleObject({30.0, 30.0}, 10.0);
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    
    std::shared_ptr <Shape> rectangleObjectPtr = std::make_shared <Rectangle> (rectangleObject);
    std::shared_ptr <Shape> circleObjectPtr = std::make_shared <Circle> (circleObject);
    std::shared_ptr <Shape> triangleObjectPtr = std::make_shared <Triangle> (triangleObject);
    
    testShape(circleObject);
    testShape(rectangleObject);
    testShape(triangleObject);

    CompositeShape compositeShapeObject({0, 0});
    compositeShapeObject.addShape(rectangleObjectPtr);
    compositeShapeObject.addShape(circleObjectPtr);
    compositeShapeObject.addShape(triangleObjectPtr);
    
    std::cout << "Now composite shape" << std::endl;

    testShape(compositeShapeObject);
    rectangleObject.rotate(270);
    std::cout << rectangleObject.getFrameRect().width << ' ' << rectangleObject.getFrameRect().height << std::endl;
    rectangleObject.scale(10);
    std::cout << rectangleObject.getFrameRect().width << ' ' << rectangleObject.getFrameRect().height << std::endl;
    compositeShapeObject.rotate(90);
    mironenkov::Rectangle rect2({10, 10}, 4, 5);
    rect2.rotate(45);
    std::cout << rect2.getFrameRect().width << ' ' << rect2.getFrameRect().height << std::endl;
    mironenkov::Circle circ2({0, 0}, 5);
    std::shared_ptr <mironenkov::Rectangle> ptrrect2 = std::make_shared <mironenkov::Rectangle> (rect2);
    std::shared_ptr <mironenkov::Circle> ptrcirc2 = std::make_shared <mironenkov::Circle> (circ2);
    mironenkov::Matrix matrix;
    std::cout << "Rows: " << matrix.getRows() << " " << "Columns: " << matrix.getColumns() << std::endl;
    matrix.add(rectangleObjectPtr);
    std::cout << "Rows: " << matrix.getRows() << " " << "Columns: " << matrix.getColumns() << std::endl;
    matrix.add(ptrcirc2);
    std::cout << "Rows: " << matrix.getRows() << " " << "Columns: " << matrix.getColumns() << std::endl;
    matrix.add(ptrcirc2);
    std::cout << "Rows: " << matrix.getRows() << " " << "Columns: " << matrix.getColumns() << std::endl;
    matrix.viewMap();  }
  catch(std::invalid_argument & invArg)
  {
    std::cerr << invArg.what() << std::endl;
    return 1;
  }
  catch(const std::out_of_range &e)
  {
    std::cerr << "error: " << e.what();
    return 1;
  }
  catch(const std::bad_alloc &e)
  {
    std::cerr << "error: allocation of memory" << e.what();
    return 1;
  }
  catch(const std::exception &e)
  {
    std::cerr << "Unknown error" << e.what();
  }
  return 0;
}
