#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "shape.hpp"
namespace usov {
  class Rectangle :
    public Shape {
  public:
    Rectangle(double width, double height, const point_t& rect);
    double getWidth() const;
    double getHeight() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t & center) override;
    void scale(double dscale) override;
    void rotate(double alpha) override;
    point_t getVertex(const int n);
    void print() const override;
  private:
    point_t vertex_[4];
    point_t center_;
    double width_;
    double height_;
  };
}

#endif // RECTANGLE_HPP
