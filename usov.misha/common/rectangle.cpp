#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <cmath>

usov::Rectangle::Rectangle(double width, double height, const point_t& rect):
  vertex_{
    {rect.x - width/2,rect.y - height/2},
    {rect.x - width/2,rect.y + height/2},
    {rect.x + width/2,rect.y + height/2},
    {rect.x + width/2,rect.y - height/2}},
  center_(rect),
  width_(width),
  height_(height)
{
  if (width < 0.0)
  {
    throw std::invalid_argument("Width could be > 0.0");
  }
  if (height < 0.0)
  {
    throw std::invalid_argument("Width could be > 0.0");
  }
}

double usov::Rectangle::getWidth() const
{
  return width_;
}
double usov::Rectangle::getHeight() const
{
  return height_;
}
double usov::Rectangle::getArea() const
{
  return width_ * height_;
}
usov::rectangle_t usov::Rectangle::getFrameRect() const
{
  double width = sqrt((vertex_[0].x - vertex_[3].x) * (vertex_[0].x - vertex_[3].x) +
                      (vertex_[0].y - vertex_[3].y) * (vertex_[0].y - vertex_[3].y));
  double height = sqrt((vertex_[0].x - vertex_[1].x) * (vertex_[0].x - vertex_[1].x) +
                       (vertex_[0].y - vertex_[1].y) * (vertex_[0].y - vertex_[1].y));
  return{width, height, center_};
}

void usov::Rectangle::move(double dx, double dy)
{
  for(int i = 0; i < 4; i++)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }
  center_.x += dx;
  center_.y += dy;
}

void usov::Rectangle::move(const point_t & center)
{
  Rectangle::move(center.x - center_.x, center.y - center_.y);
  center_ = center;
}

void usov::Rectangle::scale(double dscale)
{
  if (dscale < 0.0)
  {
    throw std::invalid_argument("Scale could be > 0.0");
  }
  for(int i = 0; i < 4; i++)
  {
    vertex_[i].x *= dscale;
    vertex_[i].y *= dscale;
  }
  width_ *= dscale;
  height_ *= dscale;
}

void usov::Rectangle::rotate(double alpha)
{
  alpha = (alpha * M_PI)/ 180;
  for(int i = 0; i < 4; i++)
  {
    vertex_[i] = {center_.x +(vertex_[i].x - center_.x) * cos(alpha)-
        (vertex_[i].y - center_.y) * sin(alpha),
            center_.y + (vertex_[i].y - center_.y) * cos(alpha) +
                 (vertex_[i].x - center_.x) * sin(alpha)
    };
  }
}

usov::point_t usov::Rectangle::getVertex(const int n)
{
  return vertex_[n];
}

void usov::Rectangle::print() const
{
  std::cout << "Rectangle info: " << std::endl;
  std::cout << "center: " << center_.x << " " << center_.y << std::endl;
  std::cout << "Down left vertex: " << "x: " << vertex_[0].x << " y: " << vertex_[0].y << std::endl;
  std::cout << "Up left vertex: " << "x: " << vertex_[1].x << " y: " << vertex_[1].y << std::endl;
  std::cout << "Up right vertex: " << "x: " << vertex_[2].x << " y: " << vertex_[2].y << std::endl;
  std::cout << "Down right vertex: " << "x: " << vertex_[3].x << " y: " << vertex_[3].y << std::endl;
  std::cout << std::endl;
}
