
#include <iostream>
#include <memory>

#include "composite-shape.hpp"
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"

using namespace kargalov;

void testShape(Shape &myShape)
{
    std::cout << "Area: " << myShape.getArea() << "\n";
    std::cout << "Get FrameRect.\n";
    std::cout << "height: " << myShape.getFrameRect().height << "\n";
    std::cout << "width: " << myShape.getFrameRect().width << "\n";
    std::cout << "Ox: "<< myShape.getFrameRect().pos.x << "\n";
    std::cout << "Oy: "<< myShape.getFrameRect().pos.y << "\n";
    
    myShape.rotate(90);
    std::cout << "Rotate\n";
    std::cout << "Area: " << myShape.getArea() << "\n";
    std::cout << "height: " << myShape.getFrameRect().height << "\n";
    std::cout << "width: " << myShape.getFrameRect().width << "\n";
    std::cout << "Ox: " << myShape.getFrameRect().pos.x << "\n";
    std::cout << "Oy: " << myShape.getFrameRect().pos.y << "\n";
}

int main()
{
    try {
        Rectangle myRect({12, 45, {11, 23}});
        Circle myCirc({4, 21}, 12);
        
        Rectangle compRect({31, 33, {16, 67}});
        Circle compCirc({11, 32}, 2);
        CompositeShape myComp;
        auto ptrRectComp = std::make_shared<Rectangle>(compRect);
        auto ptrCircComp = std::make_shared<Circle>(compCirc);
        myComp.addShape(ptrCircComp);
        myComp.addShape(ptrRectComp);
        
        std::cout << "\nRectangle:\n\n";
        testShape(myRect);
        
        std::cout << "\nCircle:\n\n";
        testShape(myCirc);
        
        std::cout << "\nCompositeShape:\n\n";
        testShape(myComp);
        
        auto ptrRect = std::make_shared<Rectangle>(myRect);
        auto ptrCirc = std::make_shared<Circle>(myCirc);
     
        std::cout << "\nMatrix:\n\n";
        std::cout << "Wake up, Neo. . .\n";
        std::cout << "The Matrix has you. .\n";
        std::cout << "Knock, knock, Neo.\n";
        Matrix myMatrix;
        myMatrix.addShape(ptrRect);
        myMatrix.addShape(ptrCirc);
        std::cout << "number of Layers:" << myMatrix.getLayers();
        std::cout << "\nnumber of elemnts in Layer:" << myMatrix.getLayerWidth() << "\n";
        
    }
    catch(const std::invalid_argument &e)
    {
        std::cerr << "error: invalid argument(s): " << e.what();
        return 1;
    }
    catch(const std::bad_alloc &e)
    {
        std::cerr << "error: allocation of memory" << e.what();
        return 1;
    }
    catch(const std::exception &e)
    {
        std::cerr << "Unknown error" << e.what();
    }

}
