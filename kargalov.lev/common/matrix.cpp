
#include <stdexcept>
#include <cmath>
#include "matrix.hpp"

using namespace kargalov;

Matrix::Matrix()
: matrix_(nullptr),
layers_(0),
layerWidth_(0)

{}

void Matrix::addShape(const element_type shape)
{
    if  (!shape) {
        throw std::invalid_argument("Incorrect paremetr");
        
    }
    if (layers_ == 0)
    {
        matrix_type newElements(new element_type [(layers_ + 1) * (layerWidth_ + 1)]);
        layers_ ++;
        layerWidth_ ++;
        matrix_.swap(newElements);
        matrix_[0] = shape;
    }
    else
    {
        bool addedShape = false;
        for (size_t i = 0; !addedShape ; ++i)
        {
            for (size_t j = 0; j < layerWidth_; ++j)
            {
                if (!matrix_[i * layerWidth_ + j])
                {
                    matrix_[i * layerWidth_ + j] = shape;
                    addedShape = true;
                    break;
                }
                else
                {
                    if (overLapping(shape, i * layerWidth_ + j))
                    {
                        break;
                    }
                }
                
                if (j == (layerWidth_ - 1))
                {
                    matrix_type newElements(new element_type[layers_ * (layerWidth_ + 1)]);
                    layerWidth_++;
                    for (size_t n = 0; n < layers_; ++n)
                    {
                        for (size_t m = 0; m < layerWidth_ - 1; ++m)
                        {
                            newElements[n * layerWidth_ + m] = matrix_[n * (layerWidth_ - 1) + m];
                        }
                        newElements[(n + 1) * layerWidth_ - 1] = nullptr;
                    }
                    newElements[(i + 1) * layerWidth_ - 1] = shape;
                    matrix_.swap(newElements);
                    addedShape = true;
                    break;
                }
            }
            if ((i == (layers_ - 1)) && !addedShape)
            {
                matrix_type newElements(new element_type[(layers_ + 1) * layerWidth_]);
                layers_++;
                for (size_t n = 0; n < ((layers_ - 1) * layerWidth_); ++n)
                {
                    newElements[n] = matrix_[n];
                }
                for (size_t n = ((layers_ - 1) * layerWidth_) ; n < (layers_ * layerWidth_); ++n)
                {
                    newElements[n] = nullptr;
                }
                newElements[(layers_ - 1) * layerWidth_ ] = shape;
                matrix_.swap(newElements);
                addedShape = true;
            }
        }
    }
    }

size_t Matrix::getLayers() const
{
    return layers_;
}

size_t Matrix::getLayerWidth() const
{
    return layerWidth_;
}

bool Matrix::overLapping(const element_type shape, const size_t i) const
{
    bool overlap = false;
    
    
        rectangle_t matParam = matrix_[i] -> getFrameRect();
        double maxX_m = matParam.pos.x + matParam.width / 2;
        double minX_m = matParam.pos.x - matParam.width / 2;
    
        double maxY_m = matParam.pos.x + matParam.height / 2;
        double minY_m = matParam.pos.x - matParam.height / 2;
        
        rectangle_t shapeParam = shape -> getFrameRect();
        double maxX_s = shapeParam.pos.x + shapeParam.width / 2;
        double minX_s = shapeParam.pos.x - shapeParam.width / 2;
        
        double maxY_s = shapeParam.pos.x + shapeParam.height / 2;
        double minY_s = shapeParam.pos.x - shapeParam.height / 2;
        
        if (((minX_m <= maxX_s) && (maxX_s <= maxX_m)) || ((minX_m <= minX_s) && (minX_s <= maxX_m))) {
            if (((minY_m <= maxY_s) && (maxY_s <= maxY_m)) || ((minY_m <= minY_s) && (minY_s <= maxY_m))) {
                overlap = true;
            }
        }
    
        if (((minX_s <= maxX_m) && (maxX_m <= maxX_s)) || ((minX_s <= minX_m) && (minX_m <= maxX_s))) {
            if (((minY_s <= maxY_m) && (maxY_m <= maxY_s)) || ((minY_s <= minY_m) && (minY_m <= maxY_s))) {
                overlap = true;
            }
        }
    
    
    return overlap;
}

