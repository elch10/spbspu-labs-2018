
#ifndef Rectangle1_hpp
#define Rectangle1_hpp

#include "shape.hpp"

namespace kargalov {
    class Rectangle : public Shape
    {
    public:
        Rectangle(const rectangle_t& parametrs);
        void move(const point_t& point) override;
        void move(const double dx, const double dy) override;
        void scale(const double increment) override;
        double getArea() const override;
        rectangle_t getFrameRect() const override;
        void rotate(const double angle) override;
    private:
        rectangle_t parametrs_;
        double angle_;
    };
}
#endif /* Rectangle_hpp */
