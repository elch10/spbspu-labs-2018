#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"
#include "base-types.hpp"

namespace babaev
{
  class Circle : public Shape
  {
    public:
      //  Radius + cords of center (x, y)
      Circle(double r, double x, double y);

      double getArea() const override;

      rectangle_t getFrameRect() const override;

      void move(point_t c) override;

      void move(double dx, double dy) override;
      
      void scale(double ratio) override;

      void printInfo() const override;
  
      double getRad() const;

    private:

      double rad_;

      point_t center_;

  };
}

#endif
