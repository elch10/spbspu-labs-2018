#include "polygon.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

using namespace burov;

Polygon::Polygon (std::initializer_list<point_t> points)
{
  if (points.size() < 3)
  {
    throw (std::invalid_argument ("Polygon must have 3 vertexes at least!"));
  }
  else
  {
    count_ = points.size();
    vertexes_ = std::unique_ptr <point_t[]> (new point_t[count_]);
    int i = 0;
    for (std::initializer_list<point_t>::const_iterator vertex = points.begin(); vertex != points.end(); vertex++)
    {
      vertexes_[i] = *vertex;
      i++;
    }
    bool emptyAreaIndicator = true;
    bool convexIndicator = true;
    for (size_t i = 0; i < (count_ - 3); i++)
    {
      double dx1 = vertexes_[i + 1].x - vertexes_[i].x;
      double dy1 = vertexes_[i + 1].y - vertexes_[i].y;
      double dx2 = vertexes_[i + 2].x - vertexes_[i + 1].x;
      double dy2 = vertexes_[i + 2].y - vertexes_[i + 1].y;
      double dx3 = vertexes_[i + 3].x - vertexes_[i + 2].x;
      double dy3 = vertexes_[i + 3].y - vertexes_[i + 2].y;
      if ((((dx1 * dy2) - (dx2 * dy1)) < 0.0) ^ (((dx2 * dy3) - (dx3 * dy2)) < 0.0))
      {
        convexIndicator = false;

      }
      if ((((dx1 * dy2) - (dx2 * dy1)) != 0.0) | (((dx2 * dy3) - (dx3 * dy2)) != 0.0))
      {
        emptyAreaIndicator = false;
      }
    }
    if (!convexIndicator)
    {
      throw (std::invalid_argument ("Polygon is not convex!"));
    }

    if (emptyAreaIndicator)
    {
      throw(std::invalid_argument ("Polygon's area is zero!"));
    }
    center_ = {0.0, 0.0};
    for (size_t i = 0; i < count_; i++)
    {
      center_.x += vertexes_[i].x;
      center_.y += vertexes_[i].y;
    }
    center_.x /= count_;
    center_.y /= count_;
  }

}
rectangle_t Polygon::getFrameRect() const
{
  double maxX = vertexes_[0].x;
  double maxY = vertexes_[0].y;
  double minX = maxX;
  double minY = maxY;
  for (size_t i = 1; i < count_; i++)
  {
    maxX = std::max(maxX, vertexes_[i].x);
    maxY = std::max(maxY, vertexes_[i].y);
    minX = std::min(minX, vertexes_[i].x);
    minY = std::min(minY, vertexes_[i].y);
  }
  rectangle_t framerect;
  framerect.pos = {((maxX + minX) / 2.0), ((maxY + minY) / 2.0)};
  framerect.height = maxY - minY;
  framerect.width = maxX - minX;
  return (framerect);
}
double Polygon::getArea() const
{
  double dx1 = vertexes_[count_ - 1].x - center_.x;
  double dy1 = vertexes_[count_ - 1].y - center_.y;
  double dx2 = vertexes_[0].x - center_.x;
  double dy2 = vertexes_[0].y - center_.y;
  double totalArea = (dx1 * dy2 - dx2 * dy1) / 2.0;
  for (size_t i = 0; i < (count_ - 1); i++)
  {
    double dx1 = vertexes_[i].x - center_.x;
    double dy1 = vertexes_[i].y - center_.y;
    double dx2 = vertexes_[i + 1].x - center_.x;
    double dy2 = vertexes_[i + 1].y - center_.y;
    totalArea += (dx1 * dy2 - dx2 * dy1) / 2.0;
  }
  return (abs (totalArea));
}
void Polygon::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; i++)
  {
    vertexes_[i].x += dx;
    vertexes_[i].y += dy;
  }
  center_.x += dx;
  center_.y += dy;
}

void Polygon::move(point_t point)
{
  move ((point.x - center_.x), (point.y - center_.y));
}
point_t Polygon::operator [] (size_t index) const
{
  if (index >= count_)
  {
    throw (std::out_of_range ("Index is out of range!"));
  }
  else
  {
    return (vertexes_[index]);
  }
}
void Polygon::scale(const double coefficient)
{
  if (coefficient < 0.0)
  {
    throw (std::invalid_argument("Scaling coefficient can not be negative!"));
  }
  else
  {
    for (size_t i = 0; i < count_; i++)
    {
      double dx = vertexes_[i].x - center_.x;
      double dy = vertexes_[i].y - center_.y;
      vertexes_[i].x += dx * (coefficient - 1);
      vertexes_[i].y += dy * (coefficient - 1);
    }
  }
}
point_t Polygon::getCenter() const
{
  return (center_);
}
void Polygon::rotate (const double angle)
{
  double angCos = std::cos (angle * M_PI / 180);
  double angSin = std::sin (angle * M_PI / 180);
  for (size_t i = 0; i < count_; i++)
  {
    point_t newVertex = vertexes_[i];
    newVertex.x = center_.x + angCos * (vertexes_[i].x - center_.x) - angSin * (vertexes_[i].y - center_.y);
    newVertex.y = center_.y + angCos * (vertexes_[i].y - center_.y) - angSin * (vertexes_[i].x - center_.x);
    vertexes_[i] = newVertex;

  }
}
void Polygon::getInfo() const
{
  std::cout << "Center of polygon is in " << center_.x << " " << center_.y << std::endl;
  std::cout << "It has " << count_ << " vertexes" << std::endl;
  rectangle_t frameRect = getFrameRect();
  std::cout << "Frame rectangle height " << frameRect.height << " and width " << frameRect.width << std::endl;
}

