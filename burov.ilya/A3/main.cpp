#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "cmath"

using namespace burov;

int main()
{
  std::shared_ptr<Shape> Rect = std::make_shared<Rectangle>(Rectangle({{5.0, 15.0}, 45.0, 45.0}));
  std::shared_ptr<Shape> Circ = std::make_shared<Circle>(Circle({3.0, 2.5}, 105));
  CompositeShape testComp;
  testComp.addShape(Rect);
  testComp.addShape(Circ);
  std::cout << "   Figure Area" << std::endl;
  std::cout <<  testComp.getArea() << std::endl;
  std::cout << "   Number of parts" << std::endl;
  std::cout << testComp.getSize() <<  std::endl;
  std::cout << " height = " << testComp.getFrameRect().height<< std::endl;
  std::cout << " width = " << testComp.getFrameRect().width << std::endl;
  testComp.move(8.0,8.0);
  std::cout << "   Parametrs after moving" << std::endl;
  std::cout << "   Figure Area" << std::endl;
  std::cout <<  testComp.getArea() << std::endl;
  std::cout << "   Number of parts" << std::endl;
  std::cout << testComp.getSize() <<  std::endl;
  std::cout << " height = " << testComp.getFrameRect().height<< std::endl;
  std::cout << " width = " << testComp.getFrameRect().width << std::endl;
  testComp.scale(2.0);
  std::cout << "   Parametrs after scaling" << std::endl;
  std::cout << "   Figure Area" << std::endl;
  std::cout <<  testComp.getArea() << std::endl;
  std::cout << "   Number of parts" << std::endl;
  std::cout << testComp.getSize() <<  std::endl;
  std::cout << " height = " << testComp.getFrameRect().height<< std::endl;
  std::cout << " width = " << testComp.getFrameRect().width << std::endl;
  testComp.removeShape(0);
  return 0;
}
