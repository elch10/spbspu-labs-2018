#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace burov;

int main()
{
  std::shared_ptr< Shape > rectangleptr = std::make_shared<Rectangle >(Rectangle({{5.8, 8.0}, 1.4, 3.6}));
  std::shared_ptr< Shape > circleptr = std::make_shared<Circle >(Circle({ 6.3, 2.7 }, 5.0));
  CompositeShape TestComp;
  TestComp.addShape(rectangleptr);
  TestComp.addShape(circleptr);
  std::cout << "   Figure Area" << std::endl;
  std::cout <<  TestComp.getArea() << std::endl;
  std::cout << "   Number of parts" << std::endl;
  std::cout << TestComp.getSize() <<  std::endl;
  std::cout << " height = " << TestComp.getFrameRect().height<< std::endl;
  std::cout << " width = " << TestComp.getFrameRect().width << std::endl;
  TestComp.rotate(90);
  std::cout << "   Figure Area" << std::endl;
  std::cout <<  TestComp.getArea() << std::endl;
  std::cout << "   Number of parts" << std::endl;
  std::cout << TestComp.getSize() <<  std::endl;
  std::cout << " height = " << TestComp.getFrameRect().height<< std::endl;
  std::cout << " width = " << TestComp.getFrameRect().width << std::endl;
  std::cout << "Create matrix " << std::endl;
  Matrix matr;
  matr.addShape(rectangleptr);
  std::cout << "There are " << matr.getLayers() << " figures inside." << std::endl;
  std::cout << "There are " << matr.getLayerSize() << "layers." << std::endl;
  std::cout << "Add circle in matrix" << std::endl;
  matr.addShape(circleptr);
  std::cout << "There are " << matr.getLayers() << " figures inside." << std::endl;
  std::cout << "There are " << matr.getLayerSize() << "layers." << std::endl;
  return 0;
}
