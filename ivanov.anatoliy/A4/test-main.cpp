#define BOOST_TEST_MODULE A4_Test
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(TestMatrix)
  BOOST_AUTO_TEST_CASE(Lines)
  {
    ivanov::Rectangle testRec({2.0,2.0},2.0,2.0);
    ivanov::Circle testCir1({5.0,6.0},2.0);
    ivanov::Circle testCir2({3.0,4.0},2.0);
    ivanov::Matrix testMatrix;
    testMatrix.add(&testRec);
    testMatrix.add(&testCir1);
    testMatrix.add(&testCir2);
    BOOST_CHECK_EQUAL(testMatrix[0][0]->getCenter().x,2.0);
    BOOST_CHECK_EQUAL(testMatrix[0][0]->getCenter().y,2.0);
    BOOST_CHECK_EQUAL(testMatrix[0][1]->getCenter().x,5.0);
    BOOST_CHECK_EQUAL(testMatrix[0][1]->getCenter().y,6.0);
    BOOST_CHECK_EQUAL(testMatrix[1][0]->getCenter().x,3.0);
    BOOST_CHECK_EQUAL(testMatrix[1][0]->getCenter().y,4.0);
  }
  BOOST_AUTO_TEST_CASE(InvalidIndex)
  {
    ivanov::Rectangle testRec({2.0,2.0},2.0,2.0);
    ivanov::Circle testCir1({5.0,6.0},2.0);
    ivanov::Circle testCir2({3.0,4.0},2.0);
    ivanov::Matrix testMatrix;
    testMatrix.add(&testRec);
    testMatrix.add(&testCir1);
    testMatrix.add(&testCir2);
    BOOST_CHECK_THROW(testMatrix[3][0],std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(AddNullptr)
  {
    ivanov::Matrix testMatrix;
    BOOST_CHECK_THROW(testMatrix.add(nullptr),std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestCompositeShape)
  BOOST_AUTO_TEST_CASE(ConstantParametersAfterMoveToPoint)
  {
    ivanov::Rectangle testRec1({3.0,4.0},5.0,6.0);
    ivanov::Circle testCir1({4.0,5.0},6.0);
    ivanov::Triangle testTri1({10.0,9.0},{8.0,7.0},{6.0,5.0});
    ivanov::CompositeShape testCompShape(&testCir1);
    testCompShape.add(&testRec1);
    testCompShape.add(&testTri1);
    double initialWidth = testCompShape.getFrameRect().width;
    double initialHeight = testCompShape.getFrameRect().height;
    double initialArea = testCompShape.getArea();
    testCompShape.move({11.0,12.0});
    BOOST_CHECK_EQUAL(testCompShape.getFrameRect().width,initialWidth);
    BOOST_CHECK_EQUAL(testCompShape.getFrameRect().height,initialHeight);
    BOOST_CHECK_CLOSE(testCompShape.getArea(),initialArea,EPSILON);
  }
  BOOST_AUTO_TEST_CASE(ConstantParametersAfterMoveOnDXandDY)
  {
    ivanov::Rectangle testRec1({3.0,4.0},5.0,6.0);
    ivanov::Circle testCir1({4.0,5.0},6.0);
    ivanov::Triangle testTri1({10.0,9.0},{8.0,7.0},{6.0,5.0});
    ivanov::CompositeShape testCompShape(&testCir1);
    testCompShape.add(&testRec1);
    testCompShape.add(&testTri1);
    double initialWidth = testCompShape.getFrameRect().width;
    double initialHeight = testCompShape.getFrameRect().height;
    double initialArea = testCompShape.getArea();
    testCompShape.move(6.0,7.0);
    BOOST_CHECK_EQUAL(testCompShape.getFrameRect().width,initialWidth);
    BOOST_CHECK_EQUAL(testCompShape.getFrameRect().height,initialHeight);
    BOOST_CHECK_CLOSE(testCompShape.getArea(),initialArea,EPSILON);
  }
  BOOST_AUTO_TEST_CASE(AreaAfterScale)
  {
    ivanov::Rectangle testRec1({3.0,4.0},5.0,6.0);
    ivanov::Circle testCir1({4.0,5.0},6.0);
    ivanov::Triangle testTri1({10.0,9.0},{8.0,7.0},{6.0,5.0});
    ivanov::CompositeShape testCompShape(&testCir1);
    testCompShape.add(&testRec1);
    testCompShape.add(&testTri1);
    double initialArea = testCompShape.getArea();
    testCompShape.scale(5.0);
    BOOST_CHECK_CLOSE(testCompShape.getArea(),5.0*5.0*initialArea,EPSILON);
  }
  BOOST_AUTO_TEST_CASE(Size)
  {
    ivanov::Rectangle testRec1({3.0,4.0},5.0,6.0);
    ivanov::Circle testCir1({4.0,5.0},6.0);
    ivanov::Triangle testTri1({10.0,9.0},{8.0,7.0},{6.0,5.0});
    ivanov::CompositeShape testCompShape(&testCir1);
    testCompShape.add(&testRec1);
    testCompShape.add(&testTri1);
    BOOST_CHECK_EQUAL(testCompShape.getSize(),3);
  }
  BOOST_AUTO_TEST_CASE(AddNullptr)
  {
    ivanov::CompositeShape testCompShape;
    BOOST_CHECK_THROW(testCompShape.add(nullptr),std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(RemoveWithInvalidIndex)
  {
    ivanov::Rectangle testRec1({3.0,4.0},5.0,6.0);
    ivanov::CompositeShape testCompShape(&testRec1);
    BOOST_CHECK_THROW(testCompShape.remove(4),std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(InvalidParametersInScale)
  {
    ivanov::Rectangle testRec1({3.0,4.0},5.0,6.0);
    ivanov::CompositeShape testCompShape(&testRec1);
    BOOST_CHECK_THROW(testCompShape.scale(-2.0),std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(AreaAndCenterAfterRotate)
  {
    ivanov::Rectangle testRec1({3.0,4.0},5.0,6.0);
    ivanov::Circle testCir1({4.0,5.0},6.0);
    ivanov::Triangle testTri1({10.0,9.0},{8.0,7.0},{6.0,5.0});
    ivanov::CompositeShape testCompShape(&testCir1);
    double initialArea = testCompShape.getArea();
    ivanov::point_t initialCenter = testCompShape.getCenter();
    testCompShape.rotate(30);
    BOOST_CHECK_CLOSE(testCompShape.getArea(),initialArea,EPSILON);
    BOOST_CHECK_CLOSE(testCompShape.getCenter().x,initialCenter.x,EPSILON);
    BOOST_CHECK_CLOSE(testCompShape.getCenter().y,initialCenter.y,EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestCircle)
  BOOST_AUTO_TEST_CASE(ConstantParametersAfterMoveToPoint)
  {
    ivanov::Circle testCir({3.0,4.0},5.0);
    testCir.move({6.0,7.0});
    BOOST_CHECK_EQUAL(testCir.getFrameRect().width,10.0);
    BOOST_CHECK_EQUAL(testCir.getFrameRect().height,10.0);
    BOOST_CHECK_CLOSE(testCir.getArea(),M_PI*5.0*5.0,EPSILON);
  }
  BOOST_AUTO_TEST_CASE(ConstantParametersAfterMoveOnDXandDY)
  {
    ivanov::Circle testCir({3.0,4.0},5.0);
    testCir.move(8.0,9.0);
    BOOST_CHECK_EQUAL(testCir.getFrameRect().width,10.0);
    BOOST_CHECK_EQUAL(testCir.getFrameRect().height,10.0);
    BOOST_CHECK_CLOSE(testCir.getArea(),M_PI*5.0*5.0,EPSILON);
  }
  BOOST_AUTO_TEST_CASE(AreaAfterScale)
  {
    ivanov::Circle testCir({3.0,4.0},5.0);
    testCir.scale(10.0);
    BOOST_CHECK_CLOSE(testCir.getArea(),10.0*10.0*M_PI*5.0*5.0,EPSILON);
  }
  BOOST_AUTO_TEST_CASE(InvalidParametersInConstructor)
  {
    BOOST_CHECK_THROW(ivanov::Circle testCir({3.0,4.0},-6.0),std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(InvalidParametersInScale)
  {
    ivanov::Circle testCir({3.0,4.0},5.0);
    BOOST_CHECK_THROW(testCir.scale(-9.0),std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(AreaAndCenterAfterRotate)
  {
    ivanov::Circle testCir({3.0,4.0},5.0);
    double initialArea = testCir.getArea();
    ivanov::point_t initialCenter = testCir.getCenter();
    testCir.rotate(30);
    BOOST_CHECK_CLOSE(testCir.getArea(),initialArea,EPSILON);
    BOOST_CHECK_CLOSE(testCir.getCenter().x,initialCenter.x,EPSILON);
    BOOST_CHECK_CLOSE(testCir.getCenter().y,initialCenter.y,EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestRectangle)
  BOOST_AUTO_TEST_CASE(ConstantParametersAfterMoveToPoint)
  {
    ivanov::Rectangle testRec({3.0,4.0},5.0,6.0);
    testRec.move({6.0,7.0});
    BOOST_CHECK_EQUAL(testRec.getFrameRect().width,5.0);
    BOOST_CHECK_EQUAL(testRec.getFrameRect().height,6.0);
    BOOST_CHECK_EQUAL(testRec.getArea(),5.0*6.0);
  }
  BOOST_AUTO_TEST_CASE(ConstantParametersAfterMoveOnDXandDY)
  {
    ivanov::Rectangle testRec({3.0,4.0},5.0,6.0);
    testRec.move(8.0,9.0);
    BOOST_CHECK_EQUAL(testRec.getFrameRect().width,5.0);
    BOOST_CHECK_EQUAL(testRec.getFrameRect().height,6.0);
    BOOST_CHECK_EQUAL(testRec.getArea(),5.0*6.0);
  }
  BOOST_AUTO_TEST_CASE(AreaAfterScale)
  {
    ivanov::Rectangle testRec({3.0,4.0},5.0,6.0);
    testRec.scale(4.5);
    BOOST_CHECK_EQUAL(testRec.getArea(),4.5*4.5*5.0*6.0);
  }
  BOOST_AUTO_TEST_CASE(InvalidParametersInConstructor)
  {
    BOOST_CHECK_THROW(ivanov::Rectangle testRec({3.0,4.0},-6.0,9.0),std::invalid_argument);
    BOOST_CHECK_THROW(ivanov::Rectangle testRec({3.0,4.0},6.0,-9.0),std::invalid_argument);
    BOOST_CHECK_THROW(ivanov::Rectangle testRec({3.0,4.0},-6.0,-9.0),std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(InvalidParametersInScale)
  {
    ivanov::Rectangle testRec({3.0,4.0},5.0,6.0);
    BOOST_CHECK_THROW(testRec.scale(-9.0),std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(AreaAndCenterAfterRotate)
  {
    ivanov::Rectangle testRec({3.0,4.0},5.0,6.0);
    double initialArea = testRec.getArea();
    ivanov::point_t initialCenter = testRec.getCenter();
    testRec.rotate(30);
    BOOST_CHECK_CLOSE(testRec.getArea(),initialArea,EPSILON);
    BOOST_CHECK_CLOSE(testRec.getCenter().x,initialCenter.x,EPSILON);
    BOOST_CHECK_CLOSE(testRec.getCenter().y,initialCenter.y,EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestTriangle)
  BOOST_AUTO_TEST_CASE(ConstantParametersAfterMoveToPoint)
  {
    ivanov::Triangle testTri({1.0,3.0},{4.0,1.0},{6.0,4.0});
    double initialWidth = testTri.getFrameRect().width;
    double initialHeight = testTri.getFrameRect().height;
    double initialArea = testTri.getArea();
    testTri.move({6.0,7.0});
    BOOST_CHECK_CLOSE(testTri.getFrameRect().width,initialWidth,EPSILON);
    BOOST_CHECK_CLOSE(testTri.getFrameRect().height,initialHeight,EPSILON);
    BOOST_CHECK_CLOSE(testTri.getArea(),initialArea,EPSILON);
  }
  BOOST_AUTO_TEST_CASE(ConstantParametersAfterMoveOnDXandDY)
  {
    ivanov::Triangle testTri({1.0,3.0},{4.0,1.0},{6.0,4.0});
    double initialWidth = testTri.getFrameRect().width;
    double initialHeight = testTri.getFrameRect().height;
    double initialArea = testTri.getArea();
    testTri.move(8.0,7.0);
    BOOST_CHECK_CLOSE(testTri.getFrameRect().width,initialWidth,EPSILON);
    BOOST_CHECK_CLOSE(testTri.getFrameRect().height,initialHeight,EPSILON);
    BOOST_CHECK_CLOSE(testTri.getArea(),initialArea,EPSILON);
  }
  BOOST_AUTO_TEST_CASE(AreaAfterScale)
  {
    ivanov::Triangle testTri({1.0,3.0},{4.0,1.0},{6.0,4.0});
    double initialArea = testTri.getArea();
    testTri.scale(6.0);
    BOOST_CHECK_EQUAL(testTri.getArea(),6.0*6.0*initialArea);
  }
  BOOST_AUTO_TEST_CASE(InvalidParametersInScale)
  {
    ivanov::Triangle testTri({1.0,3.0},{4.0,1.0},{6.0,4.0});
    BOOST_CHECK_THROW(testTri.scale(-9.0),std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(AreaAndCenterAfterRotate)
  {
    ivanov::Triangle testTri({1.0,3.0},{4.0,1.0},{6.0,4.0});
    double initialArea = testTri.getArea();
    ivanov::point_t initialCenter = testTri.getCenter();
    testTri.rotate(30);
    BOOST_CHECK_CLOSE(testTri.getArea(),initialArea,EPSILON);
    BOOST_CHECK_CLOSE(testTri.getCenter().x,initialCenter.x,EPSILON);
    BOOST_CHECK_CLOSE(testTri.getCenter().y,initialCenter.y,EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()
