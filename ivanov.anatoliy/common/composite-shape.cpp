#include "composite-shape.hpp"
#include <iostream>
#include <cmath>

using namespace ivanov;

CompositeShape::CompositeShape():
  shapes_(nullptr),
  size_(0)
{
}

CompositeShape::CompositeShape(Shape *rhs):
  shapes_(new Shape *[1]),
  size_(1)
{
  shapes_[0] = rhs;
}

CompositeShape::CompositeShape(const CompositeShape & rhs):
  shapes_((rhs.size_ != 0) ? new Shape *[rhs.size_] : nullptr),
  size_(rhs.size_)
{
  for (size_t i = 0;i < size_;i++)
  {
    shapes_[i] = rhs.shapes_[i];
  }
}

CompositeShape::CompositeShape(CompositeShape && rhs):
  shapes_(nullptr),
  size_(0)
{
  *this = std::move(rhs);
}

CompositeShape &CompositeShape::operator=(CompositeShape & rhs)
{
  if (this != &rhs)
  {
    std::unique_ptr < Shape *[] > tmpArr ((rhs.size_ != 0) ? new Shape *[rhs.size_] : nullptr);
    for (size_t i=0;i < rhs.size_;i++)
    {
      tmpArr[i] = rhs.shapes_[i];
    }
    shapes_.swap(tmpArr);
    size_ = rhs.size_;
  }
  return *this;
}

CompositeShape &CompositeShape::operator=(CompositeShape && rhs)
{
  if (this != &rhs)
  {
    shapes_.swap(rhs.shapes_);
    size_= rhs.size_;
    rhs.shapes_ = nullptr;
    rhs.size_ = 0;
  }
  return *this;
}

Shape* CompositeShape::operator[](size_t index) const
{
  if ((size_ <= 0) || (index >= size_))
  {
    throw std::invalid_argument("Invalid index.");
  }
  return shapes_[index];
}

size_t CompositeShape::getSize() const
{
  return size_;
}

void CompositeShape::add(Shape *newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("No new shape.");
  }
  std::unique_ptr < Shape *[] > tmpArr (new Shape *[size_ + 1]);
  for (size_t i = 0;i < size_;i++)
  {
    tmpArr[i] = shapes_[i];
  }
  tmpArr[size_] = newShape;
  size_++;
  shapes_.swap(tmpArr);
}

void CompositeShape::remove(const size_t index)
{
  if ((size_ <= 0) || (index >= size_))
  {
    throw std::invalid_argument("Invalid index.");
  }
  if (size_ == 1)
  {
    shapes_.reset();
    shapes_ = nullptr;
    size_ = 0;
  }
  else
  {
    std::unique_ptr < Shape *[] > tmpArr (new Shape *[size_ - 1]);
    for (size_t i = 0;i < index;i++)
    {
      tmpArr[i] = shapes_[i];
    }
    for (size_t i = index;i < size_ - 1;i++)
    {
      tmpArr[i] = shapes_[i+1];
    }
    shapes_.swap(tmpArr);
    size_--;
  }
}

point_t CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

double CompositeShape::getArea() const
{
  double compArea = 0.0;
  for (size_t i = 0;i < size_;i++)
  {
    compArea += shapes_[i]->getArea();
  }
  return compArea;
}

rectangle_t CompositeShape::getFrameRect() const
{
  rectangle_t currentRect = shapes_[0]->getFrameRect();
  double minx = currentRect.pos.x - currentRect.width/2.0;
  double miny = currentRect.pos.y - currentRect.height/2.0;
  double maxx = currentRect.pos.x + currentRect.width/2.0;
  double maxy = currentRect.pos.y + currentRect.height/2.0;
  for (size_t i = 1;i < size_;i++)
  {
    currentRect = shapes_[i]->getFrameRect();
    double nMinx = currentRect.pos.x - currentRect.width/2.0;
    double nMiny = currentRect.pos.y - currentRect.height/2.0;
    double nMaxx = currentRect.pos.x + currentRect.width/2.0;
    double nMaxy = currentRect.pos.y + currentRect.height/2.0;
    minx = std::min(minx,nMinx);
    miny = std::min(miny,nMiny);
    maxx = std::max(maxx,nMaxx);
    maxy = std::max(maxy,nMaxy);
  }
  return rectangle_t{{(minx+maxx)/2.0,(miny+maxy)/2.0},maxx-minx,maxy-miny};
}

void CompositeShape::move(const point_t & transferPoint)
{
  point_t centerComp = getFrameRect().pos;
  for (size_t i = 0;i < size_;i++)
  {
    shapes_[i]->move(transferPoint.x - centerComp.x,transferPoint.y - centerComp.y);
  }
}

void CompositeShape::move(const double dx, const double dy)
{
  for (size_t i = 0;i < size_;i++)
  {
    shapes_[i]->move(dx,dy);
  }
}

void CompositeShape::scale(const double scaleFactor)
{
  if (scaleFactor < 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }
  point_t centerComp = getFrameRect().pos;
  for (size_t i = 0;i < size_;i++)
  {
    point_t centerCurrentShape = shapes_[i]->getCenter();
    double dx = (centerCurrentShape.x-centerComp.x)*scaleFactor;
    double dy = (centerCurrentShape.y-centerComp.y)*scaleFactor;
    shapes_[i]->move({centerComp.x+dx,centerComp.y+dy});
    shapes_[i]->scale(scaleFactor);
  }
}

void CompositeShape::rotate(const double angle)
{
  point_t centerComp = getFrameRect().pos;
  double tmpAngle = (angle*M_PI)/180.0;
  for (size_t i = 0;i < size_;i++)
  {
    point_t centerCurrentShape = shapes_[i]->getCenter();
    double dx = (centerCurrentShape.x-centerComp.x)*cos(tmpAngle)-(centerCurrentShape.y-centerComp.y)*sin(tmpAngle);
    double dy = (centerCurrentShape.x-centerComp.x)*sin(tmpAngle)+(centerCurrentShape.y-centerComp.y)*cos(tmpAngle);
    shapes_[i]->rotate(angle);
    shapes_[i]->move({centerComp.x+dx,centerComp.y+dy});
  }
}

void CompositeShape::printCurrentInfo() const
{
  std::cout << "\nComposition size: " << getSize() << std::endl;
  std::cout << "Composition center (x,y): " << getFrameRect().pos.x << ", " << getFrameRect().pos.y << std::endl;
  std::cout << "Composition width: " << getFrameRect().width << std::endl;
  std::cout << "Composition height: " << getFrameRect().height << std::endl;
  std::cout << "\nSTART Composition" << std::endl;
  for (size_t i = 0;i < size_;i++)
  {
    std::cout << "\nShapes number: " << i+1 << std::endl;
    shapes_[i]->printCurrentInfo();
  }
  std::cout << "\nEND Composition" << std::endl;
}
