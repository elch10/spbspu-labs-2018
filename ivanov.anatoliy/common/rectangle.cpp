#include "rectangle.hpp"
#include <iostream>
#include <cmath>

using namespace ivanov;

Rectangle::Rectangle(const point_t & center, const double width, const double height):
  center_(center),
  width_(width),
  heigth_(height),
  currentAngle_(0.0)
{
  if (width < 0.0 || height < 0.0)
  {
    throw std::invalid_argument("Invalid width or height.");
  }
}

point_t Rectangle::getCenter() const
{
  return center_;
}

double Rectangle::getArea() const
{
  return width_*heigth_;
}

rectangle_t Rectangle::getFrameRect() const
{
  double tmpSin = abs(sin((currentAngle_*M_PI)/180.0));
  double tmpCos = abs(cos((currentAngle_*M_PI)/180.0));
  return rectangle_t{center_,width_*tmpCos+heigth_*tmpSin,width_*tmpSin+heigth_*tmpCos};
}

void Rectangle::move(const point_t & transferPoint)
{
  center_ = transferPoint;
}

void Rectangle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Rectangle::scale(const double scaleFactor)
{
  if (scaleFactor < 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }
  width_ *= scaleFactor;
  heigth_ *= scaleFactor;
}

void Rectangle::rotate(const double angle)
{
  currentAngle_ = std::fmod(currentAngle_+angle,360.0);
}

void Rectangle::printCurrentInfo() const
{
  std::cout << "\nRectangle center (x,y): " << center_.x << ", " << center_.y << std::endl;
  std::cout << "Rectangle width: " << width_ << std::endl;
  std::cout << "Rectangle height: " << heigth_ << std::endl;
  std::cout << "Rectangle current angle: " << currentAngle_ << std::endl;
}
