#include "composite-shape.hpp"
#include <cmath>
#include <stdexcept>
#include <iostream>

solovev::CompositeShape::CompositeShape() noexcept:
  countShapes_(0),
  shapes_(nullptr)
{
}

solovev::CompositeShape::CompositeShape(const std::shared_ptr<solovev::Shape> &shape):
  countShapes_(0),
  shapes_(nullptr)
{
  if (!shape)
  {
    throw std::invalid_argument("Error. Invalid pointer shape");
  }
  addShape(shape);
}

solovev::CompositeShape::CompositeShape(const solovev::CompositeShape& object):
  countShapes_(object.getCount())
{
  std::unique_ptr<std::shared_ptr <Shape>[]> copyObject (new std::shared_ptr<solovev::Shape>
  [object.getCount()]);
  for (size_t i = 0; i < object.getCount(); i++)
  {
    copyObject[i] = object.shapes_[i];
  }
  shapes_.swap(copyObject);
}

solovev::CompositeShape::CompositeShape(solovev::CompositeShape&& object) noexcept:
  countShapes_(object.getCount())
{
  shapes_.swap(object.shapes_);
}

solovev::CompositeShape& solovev::CompositeShape::operator=(const solovev::CompositeShape& otherObj)
{
  if (this != &otherObj)
  {
    countShapes_ = otherObj.getCount();
    std::unique_ptr<std::shared_ptr<Shape>[]> copyObject (new std::shared_ptr<Shape>[otherObj.getCount()]);
    for (size_t i=0; i < otherObj.getCount(); i++)
    {
      copyObject[i] = otherObj.shapes_[i];
    }
    shapes_.swap(copyObject);
  }
  return *this;
}

solovev::CompositeShape & solovev::CompositeShape::operator=(solovev::CompositeShape&&
otherObj) noexcept
{
  if (this != & otherObj)
  {
    countShapes_ = otherObj.countShapes_;
    shapes_.swap(otherObj.shapes_);
    otherObj.shapes_ = nullptr;
    otherObj.countShapes_ = 0;
  }
  return *this;
}

std::shared_ptr<solovev::Shape> solovev::CompositeShape::operator[](const unsigned int index) const
{
  if (index >= countShapes_ || index <= 0)
  {
    throw std::out_of_range("Index is out of range");
  }
  return shapes_[index];
}

void solovev::CompositeShape::addShape(const std::shared_ptr<solovev::Shape>& shape)
{
  if (!shape)
  {
    throw std::invalid_argument("Incorrect pointer");
  }

  if (shape.get() == this)
  {
    throw std::invalid_argument("This is the same shape (Addresses are the same)");
  }

  std::unique_ptr <std::shared_ptr<Shape>[]> tempArray (new std::shared_ptr<Shape>[countShapes_ + 1]);

  for (size_t i=0; i < countShapes_; i++)
  {
    tempArray[i] = shapes_[i];
  }

  tempArray[countShapes_] = shape;
  shapes_.swap(tempArray);
  countShapes_++;
}

void solovev::CompositeShape::rmShape(const unsigned int index)
{
  if (index >= countShapes_)
  {
    throw std::out_of_range("Index is out of range");
  }
  if (countShapes_ <= 0)
  {
    throw std::invalid_argument("Composite figure consist of 0 figures");
  }
  if (countShapes_ == 1)
  {
    clear();
    return;
  }
  else
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> temp (new std::shared_ptr<Shape>[countShapes_-1]);
    for (size_t i = 0; i < index; i++)
    {
      temp[i] = shapes_[i];
    }
    for (size_t i = index; i<countShapes_ - 1; i++)
    {
      temp[i] = shapes_[i + 1];
    }
    shapes_.swap(temp);
    countShapes_ --;
  }
}

size_t solovev::CompositeShape::getCount() const noexcept
{
  return countShapes_;
}

void solovev::CompositeShape::move(const solovev::point_t newxy) noexcept
{
  const point_t pos = getFrameRect().pos;
  move(newxy.x - pos.x, newxy.y - pos.y);
}

void solovev::CompositeShape::move(const double dx, const double dy) noexcept
{
  for (size_t i = 0; i < countShapes_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void solovev::CompositeShape::scale(double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Invalid factor");
  }
  const point_t pos = getFrameRect().pos;
  for (size_t i = 0; i < countShapes_; i++)
  {
    shapes_[i]->move({pos.x + (shapes_[i]->getFrameRect().pos.x - pos.x) * factor,
    pos.y + (shapes_[i]->getFrameRect().pos.y - pos.y) * factor});
    shapes_[i]->scale(factor);
  }
}

void solovev::CompositeShape::rotate(double angle) noexcept
{
  double mcos = cos (angle * M_PI / 180.0);
  double msin = sin (angle * M_PI / 180.0);

  point_t center = getFrameRect().pos;
  for (size_t i = 0; i < 4; i++)
  {
    point_t shapePos = shapes_[i]->getFrameRect().pos;
    double newX = center.x + (shapePos.x - center.x) * mcos - (shapePos.y - center.y) * msin;
    double newY = center.y + (shapePos.y - center.y) * mcos + (shapePos.x - center.x) * msin;

    shapes_[i]->move({newX, newY});
    shapes_[i]->rotate(angle);
  }
}

double solovev::CompositeShape::getArea() const noexcept
{
  double area = 0.0;
  for (size_t i = 0; i < countShapes_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

solovev::rectangle_t solovev::CompositeShape::getFrameRect() const noexcept
{
  if (countShapes_ <= 0)
  {
    return {NAN, NAN, {NAN, NAN}};
  }
  else
  {
    rectangle_t rectangle = shapes_[0]->getFrameRect();
    double maxX = rectangle.pos.x + rectangle.width / 2;
    double maxY = rectangle.pos.y + rectangle.height / 2;
    double minX = rectangle.pos.x - rectangle.width / 2;
    double minY = rectangle.pos.y - rectangle.height / 2;

    for (size_t i = 1; i < countShapes_; i++)
    {
      rectangle = shapes_[i]->getFrameRect();
      point_t pos = shapes_[i]->getFrameRect().pos;
      double nowMaxX = pos.x + rectangle.width / 2;
      double nowMinX = pos.x - rectangle.width / 2;
      double nowMaxY = pos.y + rectangle.height / 2;
      double nowMinY = pos.y - rectangle.height / 2;
      if (nowMinX < minX)
      {
        minX = nowMinX;
      }
      if (nowMaxX > maxX)
      {
        maxX = nowMaxX;
      }
      if (nowMinY < minY)
      {
        minY = nowMinY;
      }
      if (nowMaxY > maxY)
      {
        maxY = nowMaxY;
      }
    }
    return {(maxX - minX), (maxY - minY), {((maxX + minX) / 2), ((maxY + minY) / 2)}};
  }
}

void solovev::CompositeShape::clear() noexcept
{
  shapes_.reset();
  shapes_ = nullptr;
  countShapes_ = 0;
}

solovev::Matrix solovev::CompositeShape::split() const
{
  Matrix matrix;
  for (size_t i = 0; i < countShapes_; i++)
  {
    matrix.add(shapes_[i]);
  }
  return matrix;
}

void solovev::CompositeShape::printInfo() const noexcept
{
  std::cout<<"Area: "<<getArea()<<std::endl
           <<"Width of rectangle frame: "<<getFrameRect().width<<std::endl
           <<"Height of rectangle frame: "<<getFrameRect().height<<std::endl
           <<"x: "<<getFrameRect().pos.x<<std::endl
           <<"y: "<<getFrameRect().pos.y<<std::endl
           <<"Shapes: "<<countShapes_<<std::endl;
}
