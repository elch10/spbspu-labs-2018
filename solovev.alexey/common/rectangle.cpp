#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include <cmath>

solovev::Rectangle::Rectangle(solovev::point_t pos, double width, double height):
  width_(width),
  height_(height),
  center_(pos),
  vertices_{{pos.x - width / 2.0, pos.y + height / 2.0},
    {pos.x + width / 2.0, pos.y + height / 2.0},
    {pos.x + width / 2.0, pos.y - height / 2.0},
    {pos.x - width / 2.0, pos.y - height / 2.0}}
{
  if (height_<0.0)
  {
    throw std::invalid_argument("height must be >=0");
  }
  if (width_<0.0)
  {
    throw std::invalid_argument("width must be >=0");
  }
}

double solovev::Rectangle::getArea() const noexcept
{
  return (width_ * height_);
}

solovev::rectangle_t solovev::Rectangle::getFrameRect() const noexcept
{
  double left = std::min(std::min(vertices_[0].x, vertices_[3].x), std::min(vertices_[1].x,
  vertices_[2].x));
  double right = std::max(std::max(vertices_[0].x, vertices_[3].x), std::max(vertices_[1].x,
  vertices_[2].x));
  double down = std::min(std::min(vertices_[0].y, vertices_[3].y), std::min(vertices_[1].y,
  vertices_[2].y));
  double up = std::max(std::max(vertices_[0].y, vertices_[3].y), std::max(vertices_[1].y,
  vertices_[2].y));

  return { (right - left), (up - down), center_};
}

void solovev::Rectangle::move(solovev::point_t pos) noexcept
{
  move (pos.x - center_.x, pos.y - center_.y);
}

void solovev::Rectangle::move(double dx, double dy) noexcept
{
  center_.x += dx;
  center_.y += dy;
  for (int i = 0; i < 4; i++)
  {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

void solovev::Rectangle::scale(double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Factor is less than 0");
  }
  else
  {
    width_ *= factor;
    height_ *= factor;
  }
}

void solovev::Rectangle::rotate(double angle) noexcept
{
  double msin = sin(angle * M_PI / 180.0);
  double mcos = cos(angle * M_PI / 180.0);

  for (int i = 0; i < 4; i++)
  {
    vertices_[i] = {center_.x + (vertices_[i].x - center_.x) * mcos - (vertices_[i].y -
    center_.y) * msin,
    center_.y + (vertices_[i].y - center_.y) * mcos + (vertices_[i].x - center_.x) *
    msin};
  }
}

void solovev::Rectangle::printInfo() const noexcept
{
  std::cout<<"Area: "<<width_ * height_<<std::endl;
  std::cout<<"Position of center: ("<<center_.x<<", "<<center_.y<<")"<<std::endl;
  std::cout<<"Width of rectangle frame: "<<width_<<std::endl;
  std::cout<<"Height of rectangle frame: "<<height_<<std::endl;
  std::cout<<"Vertices: ( "<< vertices_[0].x << ", " << vertices_[0].y << " )"
  <<" (" <<vertices_[1].x << ", "<<vertices_[1].y<<"  ) ( "<< vertices_[2].x<<", "
  <<vertices_[2].y<<" ) ( "<<vertices_[3].x<<", "<< vertices_[3].y<<" )"<<std::endl;
}

