#ifndef MATRIX_HPP_INCLUDED
#define MATRIX_HPP_INCLUDED
#include <memory>
#include "shape.hpp"

namespace solovev
{
  class Matrix
  {
  public:
    Matrix() noexcept;
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix) noexcept;

    std::unique_ptr<std::shared_ptr<Shape>[]> operator[] (size_t index) const;
    Matrix & operator= (const Matrix & rhs);
    Matrix & operator= (Matrix && rhs) noexcept;

    void add (const std::shared_ptr<Shape> &shape);
    void clear() noexcept;
    size_t getLayers() const noexcept;
    size_t getLayerSize() const noexcept;

    bool checkIntersec(const std::shared_ptr<Shape> &shapeFirst,
    const std::shared_ptr<Shape> &shapeSecond) const;

    void printInfo() const noexcept;

  private:
    std::unique_ptr <std::shared_ptr <Shape>[]> matrix_;
    size_t layers_;
    size_t layerSize_;
  };
}//solovev
#endif // MATRIX_HPP_INCLUDED
