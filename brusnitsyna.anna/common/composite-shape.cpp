#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>

brusnitsyna::CompositeShape::CompositeShape(const std::shared_ptr<Shape> & object):
  compositeShape_(new std::shared_ptr<Shape>[1]),
  size_(0)
{
  if (!object) {
    compositeShape_.reset();
    throw std::invalid_argument("Invalid pointer");
  }
  compositeShape_[0] = object;
  size_++;
}

brusnitsyna::CompositeShape::CompositeShape(const CompositeShape & object):
  compositeShape_(new std::shared_ptr<brusnitsyna::Shape>[object.size_]),
  size_(object.size_)
{
  for (size_t i = 0; i < object.size_; i++) {
    compositeShape_[i] = object.compositeShape_[i];
  }
}

brusnitsyna::CompositeShape::CompositeShape(CompositeShape && object):
  compositeShape_(std::move(object.compositeShape_)),
  size_(object.size_)
{
  object.size_ = 0;
  object.compositeShape_.reset();
}

std::shared_ptr<brusnitsyna::Shape> brusnitsyna::CompositeShape::operator[] (const size_t index) const
{
  if (index >= size_) {
    throw std::out_of_range("Index is out of range");
  }
  return compositeShape_[index];
}

brusnitsyna::CompositeShape & brusnitsyna::CompositeShape::operator= (const brusnitsyna::CompositeShape & object)
{
  if (this == &object) {
    return *this;
  }
  std::unique_ptr <std::shared_ptr<brusnitsyna::Shape>[]>
    compShapeCopy(new std::shared_ptr<brusnitsyna::Shape>[object.size_]);
  size_ = object.size_;
  for (size_t i = 0; i < size_; i++) {
    compShapeCopy[i] = object.compositeShape_[i];
  }
  compositeShape_.swap(compShapeCopy);
  return *this;
}

brusnitsyna::CompositeShape & brusnitsyna::CompositeShape::operator= (brusnitsyna::CompositeShape && object)
{
  compositeShape_ = std::move(object.compositeShape_);
  size_ = object.size_;
  object.size_ = 0;
  return *this;
}

void brusnitsyna::CompositeShape::add(const std::shared_ptr<Shape> & object)
{
  if (object == nullptr) {
    throw std::invalid_argument("Invalid pointer");
  }
  std::unique_ptr< std::shared_ptr<Shape>[] > newArray(new std::shared_ptr<Shape>[size_ + 1]);
  for (size_t i = 0; i < size_; i++) {
    newArray[i] = compositeShape_[i];
  }
  newArray[size_] = object;
  size_++;
  compositeShape_.swap(newArray);
}

void brusnitsyna::CompositeShape::remove(const size_t index)
{
  if (index >= size_) {
    throw std::out_of_range("Index is out of range");
  }
  if (size_ == 1) {
    compositeShape_.reset();
    size_ = 0;
    return;
  }
  std::unique_ptr< std::shared_ptr<Shape>[] > newArray(new std::shared_ptr<Shape>[size_ - 1]);
  for (size_t i = 0; i < index; i++) {
    newArray[i] = compositeShape_[i];
  }
  for (size_t i = index; i < size_; i++) {
    newArray[i] = compositeShape_[i + 1];
  }
  size_--;
  compositeShape_.swap(newArray);
}

size_t brusnitsyna::CompositeShape::getSize() const
{
  return size_;
}

double brusnitsyna::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < size_; i++) {
    area += compositeShape_[i] -> getArea();
  }
  return area;
}

brusnitsyna::rectangle_t brusnitsyna::CompositeShape::getFrameRect() const
{
  if (size_ == 0) {
    return { 0.0, 0.0, {0.0, 0.0} };
  }
  brusnitsyna::rectangle_t frame = compositeShape_[0] -> getFrameRect();
  double minX = frame.pos.x - frame.width / 2;
  double maxX = frame.pos.x + frame.width / 2;
  double minY = frame.pos.y - frame.height / 2;
  double maxY = frame.pos.y + frame.height / 2;
  for (size_t i = 1; i < size_; i++) {
    frame = compositeShape_[i] -> getFrameRect();
    if (frame.pos.x - frame.width / 2 < minX) {
      minX = frame.pos.x - frame.width / 2;
    }
    if (frame.pos.x + frame.width / 2 > maxX) {
      maxX = frame.pos.x + frame.width / 2;
    }
    if (frame.pos.y - frame.height / 2 < minY) {
      minY = frame.pos.y - frame.height/ 2;
    }
    if (frame.pos.y + frame.height / 2 > maxY) {
      maxY = frame.pos.y + frame.height / 2;
    }
  }
  return { (maxX - minX), (maxY - minY), {(minX + (maxX - minX) / 2), (minY + (maxY - minY) / 2)} };
}

void brusnitsyna::CompositeShape::move(const point_t & pos)
{
  move(pos.x - getFrameRect().pos.x, pos.y - getFrameRect().pos.y);
}

void brusnitsyna::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++) {
    compositeShape_[i] -> move(dx, dy);
  }
}

void brusnitsyna::CompositeShape::scale(double k)
{
  if (k < 0.0) {
    throw std::invalid_argument("Invalid coefficient");
  }
  brusnitsyna::point_t initPos = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++) {
    brusnitsyna::point_t shapePos = compositeShape_[i] -> getFrameRect().pos;
    compositeShape_[i] -> scale(k);
    compositeShape_[i] -> move((1 - k) * (initPos.x - shapePos.x), (1 - k) * (initPos.y - shapePos.y));
  }
}
