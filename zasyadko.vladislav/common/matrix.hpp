#ifndef MATRIX_HEADER
#define MATRIX_HEADER
#include "shape.hpp"
#include "composite-shape.hpp"
#include <memory>

namespace zasyadko
{
  class Matrix
  {
    public:
    Matrix(const std::shared_ptr< Shape > shape);
    Matrix(const std::shared_ptr <CompositeShape> &compShape);
    Matrix(const Matrix & copymatrix);
    Matrix(Matrix && movematrix);
    Matrix & operator = (const Matrix & copymatrix);
    Matrix & operator = (Matrix && movematrix);
  
    bool overlapCheck(const std::shared_ptr<Shape> sh1, const std::shared_ptr<Shape> sh2) const;
  
    void addCompShape(const std::shared_ptr <CompositeShape> &compShape);
    void adding(const std::shared_ptr <Shape> new_shape);
    void printInfo();
    int getSize() const;
    int getRowAmount() const;
    int getAmountInLayer(int n) const;
    std::shared_ptr<Shape> getShape(const int numb,const int column);
  
    private:
    std::unique_ptr< std::shared_ptr <Shape> [] > shapes_;
  
    int size_;
    int rowAmount_;
    std::unique_ptr<int []> amountInLayer_;
  };
}

#endif
