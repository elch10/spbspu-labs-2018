#include <iostream>
#include <stdexcept>
#include "composite-shape.hpp"
#include <memory>
#include <cmath>

using namespace zasyadko;

CompositeShape::CompositeShape(const std::shared_ptr< Shape > shape):
  shapes_(nullptr),
  amount_(0)
{
  if(shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }
  addShape(shape);
}

CompositeShape::CompositeShape(const CompositeShape & copyshape):
  shapes_(new std::shared_ptr< Shape >[copyshape.amount_]),
  amount_(copyshape.amount_)
{
  for (int i = 0;i < amount_;++i)
  {
    shapes_[i] = copyshape.shapes_[i];
  }
}

CompositeShape::CompositeShape(CompositeShape && moveShape):
  shapes_(nullptr),
  amount_(0)
{
  if(this != &moveShape)
  {
    shapes_.swap(moveShape.shapes_);
    amount_ = moveShape.amount_;
    moveShape.amount_ = 0;
    moveShape.shapes_.reset();
  }
  else
  {
    throw std::invalid_argument("invalid shape");
  }
}

CompositeShape & CompositeShape::operator= (const CompositeShape & copyshape)
{
  if(this == & copyshape)
  {
    return *this;
  }
  shapes_.reset(new std::shared_ptr<Shape> [copyshape.amount_] );
  amount_ = copyshape.amount_;
  for (int i = 0; i < amount_; ++i)
  {
    shapes_[i] = copyshape.shapes_[i];
  }
    
  return *this;
}

CompositeShape & CompositeShape::operator= (CompositeShape && moveShape)
{
  if(this == &moveShape)
  {
    return *this;
  }
  shapes_.reset(new std::shared_ptr<Shape> [moveShape.amount_] );
  shapes_ = std::move(moveShape.shapes_);
  amount_ = moveShape.amount_;
  moveShape.amount_ = 0;
  moveShape.shapes_.reset();
  return *this;
}
  

double CompositeShape::getArea() const
{
  double shapesArea = 0;
  for (int i = 0; i < amount_; ++i)
  {
    shapesArea += shapes_[i]->getArea();
  }
  return shapesArea;
}

rectangle_t CompositeShape::getFrameRect() const
{
  if (amount_ == 0)
  {
    return {{0,0},0,0};
  }
  else
  {
    rectangle_t frame = shapes_[0]->getFrameRect();
    double right = frame.pos.x + frame.width / 2.0;
    double left = frame.pos.x - frame.width / 2.0;
    double top = frame.pos.y + frame.height / 2.0;
    double bottom = frame.pos.y - frame.height / 2.0;
    for(int i = 0; i < amount_; i++)
    {
      frame = shapes_[i]->getFrameRect();
      double right_ = frame.pos.x + frame.width / 2.0;
      double left_ = frame.pos.x - frame.width / 2.0;
      double top_ = frame.pos.y + frame.height / 2.0;
      double bottom_ = frame.pos.y - frame.height / 2.0;
    
      if(right_ > right)
      {
        right = right_;
      }
      if(left_ < left)
      {
        left = left_;
      }
      if(top_ > top)
      {
        top = top_;
      }
      if(bottom_ < bottom)
      {
        bottom = bottom_;
      }
    }
    return {{((right + left) / 2.0),((top + bottom) / 2.0)},(top - bottom),(right - left)};
  }
}

void CompositeShape::print() const
{
  std::cout << "Composite Shape "<< center_.x<<" : "<<center_.y << std::endl;
}

void CompositeShape::rotate(const double degree)
{
  point_t center = getFrameRect().pos;
  double radians = (degree*M_PI)/180; 
  double x = 0;
  double y = 0;
  for(int i = 0;i < amount_; i++)
  {
    x = center.x + (shapes_[i]->getFrameRect().pos.x - center.x)*cos(radians)-
    (shapes_[i]->getFrameRect().pos.y - center.y)*sin(radians);
    y = center.y + (shapes_[i]->getFrameRect().pos.y - center.y)*cos(radians)+
    (shapes_[i]->getFrameRect().pos.x - center.x)*sin(radians);
    std::cout<<x<<";"<<y<<std::endl;
    shapes_[i]->move({x,y});
    shapes_[i]->rotate(degree);
  }
}

void CompositeShape::move(const point_t & Center)
{
  const point_t centre = getFrameRect().pos;
  move(Center.x - centre.x, Center.y - centre.y);
}

void CompositeShape::move(const double add_x, const double add_y)
{
  for (int i = 0; i < amount_; i++)
  {
    shapes_[i]->move(add_x, add_y);
  }
}

void CompositeShape::addShape(const std::shared_ptr <Shape> new_shape)
{
  if(new_shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer on shape");
  }
  for(int i = 0; i < amount_; ++i)
  {
    if (new_shape == shapes_[i])
    {
      throw std::invalid_argument("this shape is already in this comp. shape");
    }
  }
  std::unique_ptr< std::shared_ptr< Shape >[] > newshapes(new std::shared_ptr< Shape >[amount_ + 1]);
  for(int i = 0; i < amount_; i++)
  {
    newshapes[i] = shapes_[i];
  }
  newshapes[amount_++] = new_shape;
  shapes_.swap(newshapes);
}

void CompositeShape::scale(const double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Invalid factor");
  }
  point_t center = getFrameRect().pos;
  for(int i = 0;i < amount_; i++)
  {
    shapes_[i]->move({factor*(shapes_[i]->getFrameRect().pos.x-center.x)+center.x,
      factor*(shapes_[i]->getFrameRect().pos.y-center.y)+center.y});
    shapes_[i]->scale(factor);
  }
}

std::shared_ptr<Shape> CompositeShape::getShapeInfo(const int number) const
{
  return shapes_[number];
}

void CompositeShape::deleteShape(const int number)
{
  if(amount_ == 0)
  {
    throw std::out_of_range("invalid number");
  }
  else
  {
    if ((number >= amount_) || (number <=0))
    {
      throw std::invalid_argument("Number of shape is invalid");
    }
  }
  if(amount_ == 1)
  {
    deleteAll();
  }
  else
  {
    std::unique_ptr< std::shared_ptr< Shape >[] > newshapes(new std::shared_ptr< Shape >[amount_ - 1]);
    for(int i = 0; i < (number-1);++i)
    {
      newshapes[i] = shapes_[i];
    }
    for(int i = number; i < amount_;++i)
    {
      newshapes[i-1] = shapes_[i];
    }
    shapes_.swap(newshapes);
    --amount_;
  }
}

void CompositeShape::deleteAll()
{
  shapes_.reset();
  shapes_ = nullptr;
  amount_ = 0;
}

int CompositeShape::getAmount()
{
  return amount_;
}
