#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "../common/shape.hpp"

namespace razuvaev { 
  class Matrix {
  public:
    Matrix(const std::shared_ptr<Shape>& shape);
    Matrix();

    Matrix & operator=(const Matrix& matrix);

    //actions
    void addShape(const std::shared_ptr<Shape>& shape);

    //setters and getters
    int getRows() const;
    int getColumns() const;

    // write info
    void writeInfo() const;

  private:
    bool checkIntersection(const std::shared_ptr<Shape>& shape1, const std::shared_ptr<Shape>& shape2);
    std::unique_ptr<std::shared_ptr<Shape>[]> matrix_;
    int columns_;
    int rows_;
  };
}
#endif
