#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "../common/shape.hpp"

namespace razuvaev {
  class Circle : public Shape {
  public:   
    Circle(double radius, double x_center, double y_center);

    //actions
    void move(point_t center) override;
    void move(double dx, double dy) override;
    void scale(double ratio) override;
    void rotate(double deg) override;
    
    //setters and getters
    void        setAngle(double deg) override;
    double      getHeight();
    double      getWidth();
    double      getRadius();
    rectangle_t getFrameRect() const override; 
    double      getArea() const override;
    double      getAngle() const override;

    //write info
    void writeInfoOfObject() const override; 
    
  private:
    double radius_;
    point_t center_;
    double angle_;
  };
}
#endif
