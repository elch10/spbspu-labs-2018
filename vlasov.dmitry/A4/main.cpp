#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace vlasov;

int main()
{
  try {
    Rectangle objRect({5.0,5.0,0.0,0.0});
    Circle objCirc(5.0,{11.0,11.0});
    Rectangle objRect2({5.0,5.0,3.0,3.0});
    Circle objCirc2(5.0,{3.0,3.0});
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(objRect);
    std::shared_ptr<Shape> rectPtr2 = std::make_shared<Rectangle>(objRect2);
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(objCirc);
    std::shared_ptr<Shape> circPtr2 = std::make_shared<Circle>(objCirc2);
    Matrix objMatrix(rectPtr);
    objMatrix.addShape(circPtr);
    objMatrix.addShape(rectPtr2);
    objMatrix.addShape(circPtr2);
    std::cout << "Layers: " << objMatrix.getLayerSize() << "\n";
    std::cout << "Shapes: " << objMatrix.getShapeSize() << "\n";
  }
  catch(std::invalid_argument &error)
  {
    std::cerr << error.what() << "\n";
    return 1;
  }
  return 0;
}
