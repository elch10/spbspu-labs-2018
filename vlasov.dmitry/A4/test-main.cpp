#define BOOST_TEST_MODULE test-Main

#include <boost/test/included/unit_test.hpp>
#include <cmath>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace vlasov;

double EPS=0.0001;
Matrix objMatrix;
vlasov::Rectangle matrixRect({14.0, 14.0, 7.0, 8.0});
vlasov::Circle matrixCirc(8.0, {4.0, 4.0});
std::shared_ptr <vlasov::Rectangle> rectPtr = std::make_shared <vlasov::Rectangle> (matrixRect);
std::shared_ptr <vlasov::Circle> circPtr = std::make_shared <vlasov::Circle> (matrixCirc);

BOOST_AUTO_TEST_SUITE(MatrixTests)

  BOOST_AUTO_TEST_CASE(TestMatrixAdd)
  {
    BOOST_CHECK_EQUAL(objMatrix.getLayerSize(), 0);
    BOOST_CHECK_EQUAL(objMatrix.getShapeSize(), 0);
    objMatrix.addShape(rectPtr);
    BOOST_CHECK_EQUAL(objMatrix.getLayerSize(), 1);
    BOOST_CHECK_EQUAL(objMatrix.getShapeSize(), 1);
  }

  BOOST_AUTO_TEST_CASE(TestMatrixAdd2)
  {
    objMatrix.addShape(circPtr);
    BOOST_CHECK_EQUAL(objMatrix.getLayerSize(), 2);
    BOOST_CHECK_EQUAL(objMatrix.getShapeSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(TestMatrixErrors)
  {
    BOOST_CHECK_THROW(objMatrix.addShape(nullptr), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Rotation)
  vlasov::Rectangle rect({5.0,7.0,20.0,20.0});
  vlasov::Circle circ(5.0,{5.0,5.0});
  BOOST_AUTO_TEST_CASE(test_rotate)
  {
    rect.rotate(90);
    BOOST_CHECK_EQUAL(rect.getFrameRect().width, 5.0);
    BOOST_CHECK_EQUAL(rect.getFrameRect().height, 7.0);
  }

  BOOST_AUTO_TEST_CASE(test_rotate2)
  {
    rect.rotate(270);
    BOOST_CHECK_CLOSE(rect.getFrameRect().width, 5.0, EPS);
    BOOST_CHECK_CLOSE(rect.getFrameRect().height, 7.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(test_rotate3)
  {
    rect.rotate(55);
    BOOST_CHECK_EQUAL(rect.getFrameRect().pos.x, 20.0);
    BOOST_CHECK_EQUAL(rect.getFrameRect().pos.y, 20.0);
  }

  BOOST_AUTO_TEST_CASE(test_rotate4)
  {
    circ.rotate(45);
    BOOST_CHECK_EQUAL(circ.getFrameRect().width / 2, 5.0);
    BOOST_CHECK_EQUAL(circ.getFrameRect().height / 2, 5.0);
  }
BOOST_AUTO_TEST_SUITE_END()
