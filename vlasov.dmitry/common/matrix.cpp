#include <iostream>
#include <cmath>
#include "matrix.hpp"

using namespace vlasov;

Matrix::Matrix(const std::shared_ptr<vlasov::Shape> shape):
  layerSize_(0),
  shapeSize_(0),
  shapeArr_(nullptr)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Wrong shape");
  }
  if ((shape->getFrameRect().height < 0.0) || (shape->getFrameRect().width < 0.0))
  {
    throw std::invalid_argument("Wrong values");
  }
  addShape(shape);
}

Matrix::Matrix():
  layerSize_(0),
  shapeSize_(0),
  shapeArr_(nullptr)
{}

Matrix::~Matrix()
{}

Matrix::Matrix(const Matrix & matrix):
  layerSize_(matrix.layerSize_),
  shapeSize_(matrix.shapeSize_),
  shapeArr_(new std::shared_ptr<Shape>[matrix.layerSize_ * matrix.shapeSize_])
{
  for (int i = 0; i < layerSize_*shapeSize_; ++i)
  {
    shapeArr_[i] = matrix.shapeArr_[i];
  }
}

void Matrix::addShape(const std::shared_ptr<vlasov::Shape> newshape)
{
  if (newshape == nullptr)
  {
    throw std::invalid_argument("Invalid");
  }
  if ((newshape->getFrameRect().height < 0.0) || (newshape->getFrameRect().width < 0.0))
  {
    throw std::invalid_argument("Bad values");
  }
  if ((layerSize_ == 0) && (shapeSize_ == 0))
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(new std::shared_ptr<Shape>[1]);
    tempArr[0] = newshape;
    shapeArr_.swap(tempArr);
    layerSize_ = 1;
    shapeSize_ = 1;
    return;
  }
  int count1 = 0;
  for (int i = 0; i < shapeSize_; i++)
  {
    int count2 = 0;
    for (int j = 0; j < layerSize_; j++)
    {
      if (shapeArr_[i*layerSize_+j] == nullptr)
      {
        shapeArr_[i*layerSize_+j] = newshape;
        return;
      }
      if (crossing(shapeArr_[i*layerSize_+j],newshape))
      {
        count2 = j + 1;
        break;
      }
      count2 = j+1;
    }
    if (count2 == layerSize_)
    {
      std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(new std::shared_ptr<Shape>[shapeSize_*(layerSize_+1)]);
      for (int m = 0; m < shapeSize_; m++)
      {
        for (int n = 0; n < layerSize_; n++)
        {
          tempArr[m*layerSize_+n+m] = shapeArr_[m*layerSize_+n];
        }
      }
      layerSize_++;
      shapeSize_++;
      tempArr[(i+1)*layerSize_ - 1] = newshape;
      shapeArr_ = std::move(tempArr);
      return;
    }
    count1 = i+1;
  }
  if (count1 == shapeSize_)
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(new std::shared_ptr<Shape>[(shapeSize_+1)*layerSize_]);
    for (int i = 0; i < shapeSize_; i++)
    {
      tempArr[i] = shapeArr_[i];
    }
    tempArr[shapeSize_*layerSize_] = newshape;
    shapeSize_++;
    shapeArr_ = std::move(tempArr);
    }

}

bool Matrix::crossing(const std::shared_ptr<vlasov::Shape> shapeCurr, const std::shared_ptr<vlasov::Shape> shapeNew)
{
  if (shapeCurr == nullptr || shapeNew == nullptr)
  {
    throw std::invalid_argument("Enter a figure");
  }
  vlasov::rectangle_t frameCurr = shapeCurr->getFrameRect();
  vlasov::rectangle_t frameNew = shapeNew->getFrameRect();
  return((fabs(frameCurr.pos.x-frameNew.pos.x) < frameCurr.width/2 + frameNew.width/2)
          && fabs(frameCurr.pos.y - frameNew.pos.y) < frameCurr.height/2 + frameNew.height/2);
}


int Matrix::getLayerSize()
{
  return layerSize_;
}

int Matrix::getShapeSize()
{
  return shapeSize_;
}
