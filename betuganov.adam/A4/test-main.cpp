#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "polygon.hpp"

const double EPSILON = 0.0001;

BOOST_AUTO_TEST_SUITE(Rotate_Tests)

  BOOST_AUTO_TEST_CASE(CircleTest)
  {
    betuganov::point_t point = {6.0, 8.0};
    betuganov::Circle circle(point, 14.0);
    betuganov::rectangle_t rectBefore = circle.getFrameRect();
    circle.rotate(50.0);
    betuganov::rectangle_t rectAfter = circle.getFrameRect();
    BOOST_CHECK_CLOSE(rectBefore.height, rectAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(rectBefore.width, rectAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(rectBefore.pos.x, rectAfter.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(rectBefore.pos.y, rectAfter.pos.y, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(RectangleTest)
  {
    betuganov::point_t point = {6.0, 8.0};
    betuganov::Rectangle rect(point, 10.0, 20.0);
    betuganov::rectangle_t rectBefore = rect.getFrameRect();
    rect.rotate(50.0);
    betuganov::rectangle_t rectAfter = rect.getFrameRect();
    BOOST_CHECK_CLOSE(rectBefore.height, rectAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(rectBefore.width, rectAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(rectBefore.pos.x, rectAfter.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(rectBefore.pos.y, rectAfter.pos.y, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(TriangleTest)
  {
    betuganov::Triangle triangle({ 0, 0 }, { 0, 1 }, { 1, 0 });
    triangle.rotate(M_PI / 6);
    betuganov::rectangle_t frame = triangle.getFrameRect();
    BOOST_CHECK_CLOSE(frame.width, 1.0090966393901766, EPSILON);
    BOOST_CHECK_CLOSE(frame.height, 0.9999582439930006, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), 0.5, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeTest)
  {
    betuganov::point_t point = {4.0, 4.0};
    std::shared_ptr<betuganov::Shape> rectPtr (new betuganov::Rectangle(point, 50.0, 40.0));
    std::shared_ptr<betuganov::Shape> circlePtr (new betuganov::Circle(point, 56.0));
    std::shared_ptr<betuganov::Shape> trianglePtr (new betuganov::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    betuganov::CompositeShape shape(rectPtr);
    shape.addShape(circlePtr);
    shape.addShape(trianglePtr);
    betuganov::rectangle_t shapeBefore = shape.getFrameRect();
    shape.rotate(50.0);
    betuganov::rectangle_t shapeAfter = shape.getFrameRect();
    BOOST_CHECK_CLOSE(shapeBefore.height, shapeAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(shapeBefore.width, shapeAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(shapeBefore.pos.x, shapeAfter.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(shapeBefore.pos.y, shapeAfter.pos.y, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Matrix_Tests)

  BOOST_AUTO_TEST_CASE(AddShapeTest)
  {
    betuganov::point_t point = {4.0, 4.0};
    std::shared_ptr<betuganov::Shape> rectPtr (new betuganov::Rectangle(point, 50.0, 40.0));
    std::shared_ptr<betuganov::Shape> circlePtr (new betuganov::Circle(point, 56.0));
    std::shared_ptr<betuganov::Shape> trianglePtr(new betuganov::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    betuganov::Matrix matrix(rectPtr);
    matrix.addShape(circlePtr);
    matrix.addShape(trianglePtr);

    BOOST_CHECK_CLOSE(matrix[0][0] -> getFrameRect().width, rectPtr -> getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(matrix[0][0] -> getFrameRect().height, rectPtr -> getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(matrix[1][0] -> getFrameRect().width, circlePtr -> getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(matrix[1][0] -> getFrameRect().height, circlePtr -> getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(matrix[2][0] -> getFrameRect().width, trianglePtr -> getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(matrix[2][0] -> getFrameRect().height, trianglePtr -> getFrameRect().height, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(AddCompositeShapeTest)
  {
    betuganov::point_t point = {4.0, 4.0};
    std::shared_ptr<betuganov::Shape> rectPtr1 (new betuganov::Rectangle(point, 50.0, 40.0));
    std::shared_ptr<betuganov::Shape> circlePtr (new betuganov::Circle(point, 56.0));
    std::shared_ptr<betuganov::Shape> trianglePtr(new betuganov::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    point = {6.0, 7.0};
    std::shared_ptr<betuganov::Shape> rectPtr2 (new betuganov::Rectangle(point, 23.0, 50.0));
    betuganov::CompositeShape shape(rectPtr1);
    betuganov::Matrix matrix(rectPtr1);
    shape.addShape(rectPtr2);
    matrix.addShape(rectPtr2);

    std::shared_ptr<betuganov::Shape> compPtr (new betuganov::CompositeShape(shape));
    matrix.addShape(compPtr);

    BOOST_CHECK_CLOSE(matrix[0][0] -> getFrameRect().width, rectPtr1 -> getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(matrix[0][0] -> getFrameRect().height, rectPtr1 -> getFrameRect().height, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentConstructorTest)
  {
    BOOST_REQUIRE_THROW(betuganov::Matrix matrix_shape(nullptr), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(PolygonTests)
const double EPSILON = 0.00001;

BOOST_AUTO_TEST_CASE(Constructor_Area)
  {
    size_t size = 4;
    betuganov::point_t ver[size] = {{0, 0},{0, 0},{0, 0},{0, 0}};
    std::unique_ptr<betuganov::point_t[]> array(new betuganov::point_t[size]);
    for (int i = 0; i < 4; ++i)

      array[i] = ver[i];

    BOOST_CHECK_THROW(betuganov::Polygon pol(array, size), std::invalid_argument);
  }

BOOST_AUTO_TEST_CASE(Constructor_size)
  {
    size_t size = 3;
    betuganov::point_t ver[size] = {{0, 1},{1, 0},{3, 3}};
    std::unique_ptr<betuganov::point_t[]> array(new betuganov::point_t[size]);
    for (int i = 0; i < 3; ++i)

      array[i] = ver[i];

    BOOST_CHECK_THROW(betuganov::Polygon pol(array, size), std::invalid_argument);
  }

BOOST_AUTO_TEST_CASE(Constructor_convex)
  {
    size_t size = 4;
    betuganov::point_t ver[size] = {{-2, 1},{-5, 1},{-3, 2},{-2, 4}};
    std::unique_ptr<betuganov::point_t[]> array(new betuganov::point_t[size]);
    for (int i = 0; i < 4; ++i)

      array[i] = ver[i];

    BOOST_CHECK_THROW(betuganov::Polygon pol(array, size), std::invalid_argument);
  }

BOOST_AUTO_TEST_CASE(Move_ToPoint)
  {
    size_t size = 4;
    betuganov::point_t ver[size] = {{0, 0},{2, 2},{4, 2},{2, 0}};
    std::unique_ptr<betuganov::point_t[]> array(new betuganov::point_t[size]);
    for (int i = 0; i < 4; ++i)

      array[i] = ver[i];

    betuganov::Polygon pol(array, size);
    pol.move({4.0, 5.0});
    BOOST_CHECK_CLOSE(pol.getFrameRect().width, 4, EPSILON);
    BOOST_CHECK_CLOSE(pol.getFrameRect().height, 2, EPSILON);
    BOOST_CHECK_CLOSE(pol.getFrameRect().pos.x, 4, EPSILON);
    BOOST_CHECK_CLOSE(pol.getFrameRect().pos.y, 5, EPSILON);
    BOOST_CHECK_CLOSE(pol.getArea(), 4, EPSILON);
  }


BOOST_AUTO_TEST_CASE(Move_Delta)
  {
    size_t size = 4;
    betuganov::point_t ver[size] = {{0, 0},{2, 2},{4, 2},{2, 0}};
    std::unique_ptr<betuganov::point_t[]> array(new betuganov::point_t[size]);
    for (int i = 0; i < 4; ++i)

      array[i] = ver[i];

    betuganov::Polygon pol(array, size);
    pol.move(4.0, 5.0);
    BOOST_CHECK_CLOSE(pol.getFrameRect().width, 4, EPSILON);
    BOOST_CHECK_CLOSE(pol.getFrameRect().height, 2, EPSILON);
    BOOST_CHECK_CLOSE(pol.getFrameRect().pos.x, 6, EPSILON);
    BOOST_CHECK_CLOSE(pol.getFrameRect().pos.y, 6, EPSILON);
    BOOST_CHECK_CLOSE(pol.getArea(), 4, EPSILON);
  }

BOOST_AUTO_TEST_CASE(getArea)
  {
    size_t size = 4;
    betuganov::point_t ver[size] = {{0, 0},{2, 2},{4, 2},{2, 0}};
    std::unique_ptr<betuganov::point_t[]> array(new betuganov::point_t[size]);
    for (int i = 0; i < 4; ++i)

      array[i] = ver[i];

    betuganov::Polygon pol(array, size);
    BOOST_CHECK_CLOSE(pol.getArea(), 4, EPSILON);
  }


BOOST_AUTO_TEST_CASE(scale)
  {
    size_t size = 4;
    betuganov::point_t ver[size] = {{0, 0},{2, 2},{4, 2},{2, 0}};
    std::unique_ptr<betuganov::point_t[]> array(new betuganov::point_t[size]);
    for (int i = 0; i < 4; ++i)

      array[i] = ver[i];

    betuganov::Polygon pol(array, size);
    pol.scale(2);
    BOOST_CHECK_CLOSE(pol.getArea(), 16, EPSILON);
  }

BOOST_AUTO_TEST_CASE(rotate)
  {
    size_t size = 4;
    betuganov::point_t ver[size] = {{0, 0},{2, 2},{4, 2},{2, 0}};
    std::unique_ptr<betuganov::point_t[]> array(new betuganov::point_t[size]);
    for (int i = 0; i < 4; ++i)

      array[i] = ver[i];

    betuganov::Polygon pol1(array, size);
    betuganov::Polygon pol2(array, size);

    double angle = 100.0;
    pol1.rotate(angle);
    BOOST_CHECK_CLOSE(pol1.getFrameRect().pos.x, pol2.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(pol1.getFrameRect().pos.y, pol2.getFrameRect().pos.y, EPSILON);
    BOOST_CHECK_CLOSE(pol1.getArea(), pol2.getArea(), EPSILON);

  }

BOOST_AUTO_TEST_SUITE_END()
