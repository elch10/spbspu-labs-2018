#include "triangle.hpp"
#include <algorithm>
#include <stdexcept>
#include <math.h>
#include "base-types.hpp"

betuganov::Triangle::Triangle(const point_t &p1, const point_t &p2, const point_t &p3) :
  a_(p1),
  b_(p2),
  c_(p3)
{
  if(getArea() < 0.0)
  {
    throw std::invalid_argument("Invalid triangle parameters!");
  }
}

double betuganov::Triangle::getArea() const
{
  double ab = getDistance(a_, b_);
  double ac = getDistance(a_, c_);
  double bc = getDistance(b_, c_);
  double s = (ab + ac + bc) / 2;
  return std::sqrt(s*(s - ab)*(s - ac)*(s - bc));
}

betuganov::rectangle_t betuganov::Triangle::getFrameRect() const
{
  using namespace std;
  double left = min(min(a_.x, b_.x), c_.x);
  double bot = min(min(a_.y, b_.y), c_.y);
  double h = fabs(max(max(a_.y, b_.y), c_.y) - bot);
  double w = fabs(max(max(a_.x, b_.x), c_.x) - left);
  return rectangle_t{ findCenter(), w, h };
}

void betuganov::Triangle::move(double dx, double dy)
{
  a_.x += dx;
  b_.x += dx;
  c_.x += dx;
  a_.y += dy;
  b_.y += dy;
  c_.y += dy;
}

void betuganov::Triangle::move(const point_t &p)
{
  move(p.x - findCenter().x, p.y - findCenter().y);
}

betuganov::point_t betuganov::Triangle::findCenter() const
{
  return point_t{ (a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3 };
}

double betuganov::Triangle::getDistance(const point_t &p1, const point_t &p2)
{
  return std::sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y));
}

void betuganov::Triangle::scale(const double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Scale coefficient must be > 0");
  }
  double CenterX = findCenter().x;
  double CenterY = findCenter().y;
  a_.x = (a_.x - CenterX) * ratio + CenterX;
  a_.y = (a_.y - CenterY) * ratio + CenterY;
  b_.x = (b_.x - CenterX) * ratio + CenterX;
  b_.y = (b_.y - CenterY) * ratio + CenterY;
  c_.x = (c_.x - CenterX) * ratio + CenterX;
  c_.y = (c_.y - CenterY) * ratio + CenterY;
}

void betuganov::Triangle::rotate(double angle)
{
    point_t centre = Triangle::getFrameRect().pos;
    angle = (angle * M_PI) / 180;
    const double sin_a = sin(angle);
    const double cos_a = cos(angle);
    a_ = { centre.x + (a_.x - centre.x) * cos_a - (a_.y - centre.y) * sin_a,
                centre.y + (a_.y - centre.y) * cos_a + (a_.x - centre.x) * sin_a };
    b_ = { centre.x + (b_.x - centre.x) * cos_a - (b_.y - centre.y) * sin_a,
                centre.y + (b_.y - centre.y) * cos_a + (b_.x - centre.x) * sin_a };
    c_ = { centre.x + (c_.x - centre.x) * cos_a - (c_.y - centre.y) * sin_a,
                centre.y + (c_.y - centre.y) * cos_a + (c_.x - centre.x) * sin_a };
}
