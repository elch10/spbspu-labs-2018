#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"
#include "base-types.hpp"

namespace betuganov
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &p1, const point_t &p2, const point_t &p3);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &p) override;
    void scale(double ratio) override;
    void rotate(double alpha) override;

  private:
    point_t a_, b_, c_;
    point_t findCenter() const;
    point_t center_;
    static double getDistance(const point_t &p1, const point_t &p2);
    double angle_;
    double alpha_;
  };
}
#endif
