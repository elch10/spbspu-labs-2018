#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace betuganov
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape(const std::shared_ptr<betuganov::Shape> shape_ptr);
    CompositeShape(const betuganov::CompositeShape & obj);
    CompositeShape(betuganov::CompositeShape && obj);

    CompositeShape &operator =(const betuganov::CompositeShape & obj);
    CompositeShape &operator =(betuganov::CompositeShape && obj);
    std::shared_ptr<Shape> operator [](const size_t index);

    void addShape(const std::shared_ptr <betuganov::Shape> shape_ptr);
    void deleteShape(const size_t index);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & point) override;
    void move(double dx, double dy) override;
    void scale(double ratio) override;
    void rotate(double alpha) override;

  private:
    std::unique_ptr <std::shared_ptr <betuganov::Shape>[]> shapes_;
    size_t size_;
    double alpha_;
  };
}
#endif
