#include <iostream>

#include "triangle.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "shape.hpp"

int main()
{
    // create Rectangle
    Rectangle rectangle({7.0, 5.0, { 4.0, 8.0 }});
    Shape *shape = &rectangle;
    // moving along OX by 4 and OY by 5
    shape->move(4, 5);
    // moving to the point (10;4)
    shape->move({10, 4});
    
    // create Circle
    Circle circle(7.0, { 4.0, 4.0 });
    shape = &circle;
    // moving along OX by 6 and OY by 3
    shape->move(6, 3);
    // moving to the point (2;7)
    shape->move({2, 7});
    
    // create Triangle
    Triangle triangle({ 4.0, 7.0 }, { 5.0, 5.0 }, { 8.0, 6.0 });
    shape = &triangle;
    // moving along OX by 5 and OY by 10
    shape->move(5, 10);
    // moving to the point (3;6)
    shape->move({3, 6});
    
    return 0;
}


