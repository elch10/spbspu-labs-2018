#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include "shape.hpp"
namespace chernyshev
{
class CompositeShape: public Shape
{
  public:
    CompositeShape();
    CompositeShape(const std::shared_ptr <Shape> & object);
    CompositeShape(const CompositeShape & object);
    CompositeShape(CompositeShape && object);
    ~CompositeShape();
    
    CompositeShape & operator=(const CompositeShape & object);
    CompositeShape & operator=(CompositeShape && object);
    
    Shape::ptr_type operator[](size_t index);
    
    void addShape(const Shape::ptr_type & shape);
    void deleteShape(size_t index);
    
    double getArea() const override;
    rectangle_t getFrameRect()const override;
    void move(double delta_x, double delta_y) override;
    void move(point_t newPoint) override;
    void scale(double size) override;
    size_t takeSize() const;
    virtual void rotate(const double angle) override;
    
  private:  
    size_t size_;
    std::unique_ptr<Shape::ptr_type []> shapes_;
    double angle_;
};
}
#endif /* COMPOSITE_SHAPE_HPP */
