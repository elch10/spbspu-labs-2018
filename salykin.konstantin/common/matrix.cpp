#include <memory>
#include <stdexcept>

#include "matrix.hpp"

salykin::Matrix::Matrix(const std::shared_ptr <Shape> newShape):
  list_(nullptr),
  layersNum_(0),
  layerSize_(0)
{
  if (!newShape)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  addShape(newShape);
}

salykin::Matrix::Matrix(const salykin::Matrix & matrixCopy):
  list_(new std::shared_ptr <salykin::Shape>[matrixCopy.layersNum_ * matrixCopy.layerSize_]),
  layersNum_(matrixCopy.layersNum_),
  layerSize_(matrixCopy.layerSize_)
{
  for (unsigned int i = 0; i < layersNum_ * layerSize_; ++i)
  {
    list_[i] = matrixCopy.list_[i];
  }
}

salykin::Matrix::Matrix(salykin::Matrix && matrixMove):
  list_(nullptr),
  layersNum_(matrixMove.layersNum_),
  layerSize_(matrixMove.layerSize_)
{
  list_.swap(matrixMove.list_);
  matrixMove.layersNum_ = 0;
  matrixMove.layerSize_ = 0;
}

salykin::Matrix::Matrix(const salykin::CompositeShape compShp):
  list_(nullptr),
  layersNum_(0),
  layerSize_(0)
{
  if (compShp.getSize() == 0)
  {
    throw std::invalid_argument("Empty CompositeShape");
  }
  for (unsigned int i = 0; i < compShp.getSize(); ++i)
  {
    addShape(compShp.getShape(i));
  }
}

salykin::Matrix::~Matrix()
{
  list_.reset();
  list_ = nullptr;
  layersNum_ = 0;
  layerSize_ = 0;
}

salykin::Matrix & salykin::Matrix::operator=(const salykin::Matrix & matrixCopy)
{
  if (this != & matrixCopy)
  {
    layersNum_ = matrixCopy.layersNum_;
    layerSize_ = matrixCopy.layerSize_;
    list_.reset(new std::shared_ptr <Shape>[layersNum_ * layerSize_]);
    for (unsigned int i = 0; i < layersNum_ * layerSize_; ++i)
    {
      list_[i] = matrixCopy.list_[i];
    }
  }
  return *this;
}

salykin::Matrix & salykin::Matrix::operator=(salykin::Matrix && matrixMove)
{
  if (this != & matrixMove)
  {
    layersNum_ = matrixMove.layersNum_;
    layerSize_ = matrixMove.layerSize_;
    list_.swap(matrixMove.list_);
    matrixMove.layersNum_ = 0;
    matrixMove.layerSize_ = 0;
  }
  return *this;
}

bool salykin::Matrix::operator==(const salykin::Matrix & matrix) const
{
  bool equal = false;
  if ((this->layersNum_ == matrix.layersNum_) && (this->layerSize_ == matrix.layerSize_))
  {
    equal = true;
    for (unsigned int i = 0; i < (layersNum_ * layerSize_); ++i)
    {
      if (!(this->list_[i] == matrix.list_[i]))
      {
        equal = false;
      }
    }
  }
  return equal;
}

bool salykin::Matrix::operator!=(const salykin::Matrix & matrix) const
{
  if (*this == matrix)
  {
    return false;
  }
  return true;
}

std::unique_ptr <std::shared_ptr <salykin::Shape>[]> salykin::Matrix::operator[](const unsigned int layerNum) const
{
  if (layerNum >= layersNum_)
  {
    throw std::out_of_range("Invalid layer number");
  }
  std::unique_ptr <std::shared_ptr <salykin::Shape>[] > newLayer(
      new std::shared_ptr <salykin::Shape>[layerSize_]);
  for (unsigned int i = 0; i < layerSize_; ++i)
  {
    newLayer[i] = list_[layerNum * layerSize_ + i];
  }
  return newLayer;
}

void salykin::Matrix::addShape(const std::shared_ptr <salykin::Shape> newShape)
{
  if (layersNum_ == 0)
  {
    ++layersNum_;
    ++layerSize_;
    std::unique_ptr <std::shared_ptr <salykin::Shape>[]> newList(
        new std::shared_ptr <salykin::Shape>[layersNum_ * layerSize_]);
    list_.swap(newList);
    list_[0] = newShape;
  }
  else
  {
    bool added = false;
    for (unsigned int i = 0; !added; ++i)
    {
      for (unsigned int j = 0; j < layerSize_; ++j)
      {
        if (!list_[i * layerSize_ + j])
        {
          list_[i * layerSize_ + j] = newShape;
          added = true;
          break;
        }
        else
        {
          if (overlapCheck(i * layerSize_ + j, newShape))
          {
            break;
          }
        }
        if (j == (layerSize_ - 1))
        {
          ++layerSize_;
          std::unique_ptr <std::shared_ptr <salykin::Shape>[]> newList(
              new std::shared_ptr <salykin::Shape>[layersNum_ * layerSize_]);
          for (unsigned int n = 0; n < layersNum_; ++n)
          {
            for (unsigned int m = 0; m < layerSize_ - 1; ++m)
            {
              newList[n * layerSize_ + m] = list_[n * (layerSize_ - 1) + m];
            }
            newList[(n + 1) * layerSize_ - 1] = nullptr; 
          }
          newList[(i + 1) * layerSize_ - 1] = newShape;
          list_.swap(newList);
          added = true;
          break;
        }
      }
      if ((i == layersNum_ - 1) && !added)
      {
        ++layersNum_;
        std::unique_ptr <std::shared_ptr <salykin::Shape>[]> newList(
            new std::shared_ptr <salykin::Shape>[layersNum_ * layerSize_]);
        for (unsigned int n = 0; n < ((layersNum_ - 1) * layerSize_); ++n)
        {
          newList[n] = list_[n];
        }
        for (unsigned int n = ((layersNum_ - 1) * layerSize_); n < (layersNum_ * layerSize_); ++n)
        {
          newList[n] = nullptr;
        }
        newList[(layersNum_ - 1) * layerSize_] = newShape;
        list_.swap(newList);
        added = true;
      }
    }
  }
}

unsigned int salykin::Matrix::getLayersNum() const
{
  return layersNum_;
}

unsigned int salykin::Matrix::getLayerSize(const unsigned int layerNum) const
{
  unsigned int a = 0;
  for (unsigned int i = layerNum; i < (layerNum * layerSize_); ++i)
  {
    if (list_ != nullptr)
    {
      ++a;
    }
  }
  return a;
}

unsigned int salykin::Matrix::getMaxLayerSize() const
{
  return layerSize_;
}

bool salykin::Matrix::overlapCheck(const unsigned int num, std::shared_ptr <salykin::Shape> shape) const
{
  salykin::rectangle_t shapeFrameRect1 = shape->getFrameRect();
  salykin::rectangle_t shapeFrameRect2 = list_[num]->getFrameRect();
  point_t newShapePoints[4] = {
      {shapeFrameRect1.pos.x - shapeFrameRect1.width / 2, shapeFrameRect1.pos.y + shapeFrameRect1.height / 2},
      {shapeFrameRect1.pos.x + shapeFrameRect1.width / 2, shapeFrameRect1.pos.y + shapeFrameRect1.height / 2},
      {shapeFrameRect1.pos.x + shapeFrameRect1.width / 2, shapeFrameRect1.pos.y - shapeFrameRect1.height / 2},
      {shapeFrameRect1.pos.x - shapeFrameRect1.width / 2, shapeFrameRect1.pos.y - shapeFrameRect1.height / 2}
  };
  point_t newMatrixPoints[4] = {
      {shapeFrameRect2.pos.x - shapeFrameRect2.width / 2, shapeFrameRect2.pos.y + shapeFrameRect2.height / 2},
      {shapeFrameRect2.pos.x + shapeFrameRect2.width / 2, shapeFrameRect2.pos.y + shapeFrameRect2.height / 2},
      {shapeFrameRect2.pos.x + shapeFrameRect2.width / 2, shapeFrameRect2.pos.y - shapeFrameRect2.height / 2},
      {shapeFrameRect2.pos.x - shapeFrameRect2.width / 2, shapeFrameRect2.pos.y - shapeFrameRect2.height / 2}
  };
  for (unsigned int i = 0; i < 4; ++i)
  {
    if (((newShapePoints[i].x >= newMatrixPoints[0].x) && (newShapePoints[i].x <= newMatrixPoints[2].x)
        && (newShapePoints[i].y >= newMatrixPoints[3].y) && (newShapePoints[i].y <= newMatrixPoints[1].y))
        || ((newShapePoints[i].x <= newMatrixPoints[0].x) && (newShapePoints[i].x >= newMatrixPoints[2].x)
        && (newShapePoints[i].y <= newMatrixPoints[0].y) && (newShapePoints[i].y >= newMatrixPoints[2].y)))
    {
      return true;
    }
  }
  return false;
}
