#include "composite-shape.hpp"

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <stdexcept>

using namespace kuznetsova;

CompositeShape::CompositeShape(std::shared_ptr <Shape> &newShape) :
  list_(nullptr),
  size_(0),
  angle_(0.0)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("New component is empty!");
  }
  addShape(newShape);
}

CompositeShape::CompositeShape(const CompositeShape &other) :
  list_(new std::shared_ptr<Shape >[other.size_]),
  size_(other.size_),
  angle_(other.angle_)
{
  for (int i = 0; i < size_; i++)
  {
    list_[i] = other.list_[i];
  }
}

CompositeShape::CompositeShape(CompositeShape &&other) :
  list_(std::move(other.list_)),
  size_(other.size_),
  angle_(other.angle_)
{
  other.size_ = 0;
  other.angle_ = 0.0;
  other.list_ = nullptr;
}

CompositeShape & CompositeShape::operator=(const CompositeShape &other)
{
  if (this != &other)
  {
    list_.reset(new std::shared_ptr<Shape>[other.size_]);
    size_ = other.size_;
    angle_ = other.angle_;
    for (int i = 0; i < size_; i++)
    {
      list_[i] = other.list_[i];
    }
  }
  return *this;
}

CompositeShape & CompositeShape::operator=(CompositeShape &&other)
{
  list_ = std::move(other.list_);
  size_ = other.size_;
  angle_ = other.angle_;
  other.size_ = 0;
  other.list_ = nullptr;
  other.angle_ = 0.0;
  return *this;
}

std::shared_ptr<Shape> CompositeShape::operator[](const int j) const
{
  if (j >= size_) 
  {
    throw std::out_of_range("Index out of range");
  }
  else
  {
    return list_[j];
  }
}

double CompositeShape::getArea() const
{
  double area = 0;

  for (int i = 0; i < size_; i++)
  {
    area += list_[i]->getArea();
  }
  return area;
}

rectangle_t CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    return { { 0.0, 0.0 }, 0.0, 0.0 };
    throw std::invalid_argument("Composite shape is emplty");
  }

  rectangle_t comp_rectangle = list_[0]->getFrameRect();
  double comp_min_x = comp_rectangle.pos.x - (comp_rectangle.width / 2);
  double comp_min_y = comp_rectangle.pos.y - (comp_rectangle.height / 2);
  double comp_max_x = comp_rectangle.pos.x + (comp_rectangle.width / 2);
  double comp_max_y = comp_rectangle.pos.y + (comp_rectangle.height / 2);

  for (int i = 1; i < size_; ++i)
  {
    rectangle_t compare_rect = list_[i]->getFrameRect();
    if (comp_min_x >(compare_rect.pos.x - (compare_rect.width / 2)))
    {
      comp_min_x = compare_rect.pos.x - (compare_rect.width / 2);
    }
    if (comp_min_y > (compare_rect.pos.y - (compare_rect.height / 2)))
    {
      comp_min_y = compare_rect.pos.y - (compare_rect.height / 2);
    }
    if (comp_max_x < (compare_rect.pos.x + (compare_rect.width / 2)))
    {
      comp_max_x = compare_rect.pos.x + (compare_rect.width / 2);
    }
    if (comp_max_y < (compare_rect.pos.y + (compare_rect.height / 2)))
    {
      comp_max_y = compare_rect.pos.y + (compare_rect.height / 2);
    }
  }

  comp_rectangle.width = comp_max_x - comp_min_x;
  comp_rectangle.height = comp_max_y - comp_min_y;
  comp_rectangle.pos.x = comp_min_x + (comp_rectangle.width / 2);
  comp_rectangle.pos.y = comp_min_y + (comp_rectangle.height / 2);

  return comp_rectangle;
}

  void CompositeShape::move(const double dx, const double dy)
  {
    for (int i = 0; i < size_; i++)
    {
      list_[i]->move(dx, dy);
    }
  }

  void CompositeShape::move(const kuznetsova::point_t &newcentre)
  {
    for (int i = 0; i < size_; i++) /*I don't actually use this function.
                                    It needs to be overriden as I inherit
                                    CSh from Shape, but in my case I decided
                                    to write another function for moving
                                    the shape as a whole*/
    {
      list_[i]->move(newcentre);
    }
  }

  void CompositeShape::moveUsingStruct(const kuznetsova::point_t &newcentre, const kuznetsova::CompositeShape &thisShape)
  {
    point_t centre_;
    rectangle_t frameRect = thisShape.getFrameRect();
    for (int i = 0; i < size_; i++)
    {
      centre_.x = newcentre.x - (frameRect.pos.x - list_[i]->getFrameRect().pos.x);
      centre_.y = newcentre.y - (frameRect.pos.y - list_[i]->getFrameRect().pos.y);
      list_[i]->move(centre_);
    }
  }

  void CompositeShape::scale(const double factor)
  {
    point_t comp_pos = getFrameRect().pos;
    for (int i = 0; i < size_; i++)
    {
      double dx = list_[i]->getFrameRect().pos.x - comp_pos.x; // here we apply to the structure field through the pointer - the value of the field is calculated for every element
      double dy = list_[i]->getFrameRect().pos.y - comp_pos.y; // here we apply to the structure field through the pointer - the value of the field is calculated for every element

      point_t newpos;
      newpos.x = comp_pos.x + factor * dx;
      newpos.y = comp_pos.y + factor * dy;
      list_[i]->move(newpos);
      list_[i]->scale(factor);
    }
  }

  void CompositeShape::rotate(const double angle)
  {
    angle_ += angle;
    if (angle_ >= 360.0)
    {
      angle_ = fmod(angle_, 360.0);
    }
    point_t centreCompShape = getFrameRect().pos;
    for (int i = 0; i < size_; ++i)
    {
      double delta_x = (centreCompShape.x - list_[i]->getFrameRect().pos.x);
      double delta_y = (centreCompShape.y - list_[i]->getFrameRect().pos.y);
      point_t newcentre;
      newcentre.x = centreCompShape.x + delta_x*cos(angle_*M_PI / 180) - delta_y * sin(angle_ * M_PI/ 180);
      newcentre.y = centreCompShape.y - delta_x * sin(angle_*M_PI / 180) + delta_y * cos(angle_ * M_PI / 180);
      list_[i]->move(newcentre);
      list_[i]->angle_ += angle;
    }
  }

void CompositeShape::addShape(std::shared_ptr <Shape> &newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("New component is empty");
  }
  for (int i = 0; i < size_; i++)
  {
    if (newShape == list_[i])
    {
      throw std::invalid_argument("This component already exists");
    }
  }
  std::unique_ptr <std::shared_ptr <Shape>[]> temp_array(new std::shared_ptr <Shape>[size_ + 1]);
  for (int i = 0; i < size_; i++)
  {
    temp_array[i] = list_[i];
  }
  temp_array[size_] = newShape;
  size_++;
  list_.swap(temp_array);
}

void CompositeShape::removeShape(const int index)
{
  if ((index >= size_) || (index < 0))
  {
    throw std::out_of_range("Invalid index");
  }
  if (size_ == 1)
  {
    list_.reset();
    size_ = 0;
  }
  else
  {
    std::unique_ptr < std::shared_ptr <Shape>[]> temp_array(new std::shared_ptr <Shape>[size_ - 1]);
    for (int i = 0; i < index; i++)
    {
      temp_array[i] = list_[i];
    }
    for (int i = index; i < size_ - 1; i++)
    {
      temp_array[i] = list_[i + 1];
    }
    list_.swap(temp_array);
    size_--;
  }
}

int CompositeShape::getSize() const
{
  return size_;
}

std::unique_ptr<kuznetsova::Shape> kuznetsova::CompositeShape::getCopy()
{
  return std::unique_ptr<kuznetsova::Shape>(new CompositeShape(*this));
}

void kuznetsova::CompositeShape::printInfo() const
{
  std::cout << "CompositeShape:";
  std::cout << "  Size: " << getSize() << ";\n" << "Angle: " 
    << angle_ << ";\n" <<  "Area: " << getArea() <<  ";"<< std::endl;
  std::cout << "  FrameRectangle:";
  const rectangle_t rectangle = getFrameRect();
  std::cout << "Centre: " << rectangle.pos.x << ", " << rectangle.pos.y << std::endl;
  std::cout << " Width: " << rectangle.width << ";\n";
  std::cout << "Height: " << rectangle.height << ";\n ";
}
