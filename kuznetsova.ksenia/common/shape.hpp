#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>

#include "base-types.hpp"

namespace kuznetsova
{
  class Shape
  {
  public:

    kuznetsova::point_t centre_;
    double angle_;

    Shape() = default;
    virtual ~Shape() = default;
    
    virtual double getArea() const = 0;
    virtual kuznetsova::rectangle_t getFrameRect() const = 0;
    virtual void move(const kuznetsova::point_t &newcentre) = 0;
    virtual void move(const double dx, const double dy) = 0;
    virtual void scale(const double factor) = 0;
    virtual void rotate(const double angle) = 0;
    virtual std::unique_ptr<kuznetsova::Shape> getCopy() = 0; /* we will need this function to realize
                                                                   replenishment in matrix class*/
    virtual void printInfo() const = 0;
  };
};
#endif
