#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace pichugina
{
  class Circle:
    public pichugina::Shape
  {
  public:
    Circle(const pichugina::point_t & centre, const double radius);
    double getArea() const noexcept override;
    double getAngle() const noexcept;
    void scale(const double factor) override;
    void rotate(const double a) noexcept override;
    pichugina::rectangle_t getFrameRect() const noexcept override;
    void move(const pichugina::point_t & new_centre) noexcept override;
    void move(const double dx, const double dy) noexcept override;
  private:
    pichugina::point_t centre_;
    double radius_;
    double angle_;
  };
}

#endif
