#include "rectangle.hpp"

#include <stdexcept>
#include <math.h>

pichugina::Rectangle::Rectangle(const pichugina::point_t & centre, const double width, const double height):
  centre_(centre),
  width_(width),
  height_(height),
  angle_(0)
  {
  if (width < 0.0)
  {
    throw std::invalid_argument("Invalid rectangle width");
  }
  if (height < 0.0)
  {
    throw std::invalid_argument("Invalid rectangle height");
  }
}

double pichugina::Rectangle::getArea() const noexcept
{
  return width_ * height_;
}

double pichugina::Rectangle::getAngle() const noexcept
{
  return angle_;
}

pichugina::rectangle_t pichugina::Rectangle::getFrameRect() const noexcept
{
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const double width = fabs(width_ * cosine) + fabs(height_ * sine);
  const double height =  fabs(height_ * cosine) + fabs(width_ * sine);
  return {centre_, width, height};

}


void pichugina::Rectangle::rotate(const double a) noexcept
{
  angle_ += a;
  if (fabs(angle_) >= 360)
  {
    angle_ = fmod(angle_, 360);
  }
}

void pichugina::Rectangle::move(const pichugina::point_t & pos) noexcept
{
  centre_ = pos;
}

void pichugina::Rectangle::move(const double dx, const double dy) noexcept
{
  centre_.x += dx;
  centre_.y += dy;
}

void pichugina::Rectangle::scale(const double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Invalid factor");
  }
  width_ *= factor;
  height_ *= factor;
}
