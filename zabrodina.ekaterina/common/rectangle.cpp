#include "rectangle.hpp"

#include <iostream>
#include <cmath>

zabrodina::Rectangle::Rectangle(const point_t &position, const double w, const double h):
  pos_(position),
  width_(w),
  height_(h)
{
  if (width_<=0)
  {
    throw std::invalid_argument("Error: width<=0");
  }
  if (height_<=0)
  {
    throw std::invalid_argument("Error: height<=0");
  }
  vertex_[0] = { pos_.x + width_ / 2, pos_.y + height_ / 2 };
  vertex_[1] = { pos_.x - width_ / 2, pos_.y + height_ / 2 };
  vertex_[2] = { pos_.x - width_ / 2, pos_.y - height_ / 2 };
  vertex_[3] = { pos_.x + width_ / 2, pos_.y - height_ / 2 };
}

double zabrodina::Rectangle::getArea() const
{
  return width_ * height_;
}

zabrodina::rectangle_t zabrodina::Rectangle::getFrameRect() const
{
  double right = vertex_[0].x;
  double left = vertex_[0].x;
  double top = vertex_[0].y;
  double bottom = vertex_[0].y;
  for (int i = 1; i < 4; i++)
  {
    if (vertex_[i].x > right)
    {
      right = vertex_[i].x;
    }
    if (vertex_[i].x < left)
    {
      left = vertex_[i].x;
    }
    if (vertex_[i].y > top)
    {
      top = vertex_[i].y;
    }
    if (vertex_[i].y < bottom)
    {
      bottom = vertex_[i].y;
    }
  }
  return {(top - bottom), (right - left), { ((right + left) / 2), ((top + bottom) / 2) } };
}

void zabrodina::Rectangle::move(const point_t &position)
{
  for (int i = 0; i < 4; i++)
  {
    vertex_[i].x += position.x - pos_.x;
    vertex_[i].y += position.y - pos_.y;
  }
 pos_ = position;
}

void zabrodina::Rectangle::move(const double ox, const double oy)
{
  pos_.x += ox;
  pos_.y += oy;
  for (int i = 0; i < 4; i++)
  {
    vertex_[i].x += ox;
    vertex_[i].y += oy;
  }
}

void zabrodina::Rectangle::printInf() const
{
  std::cout << "Area of a rectangle: " << zabrodina::Rectangle::getArea() << std::endl;
  std::cout<< "Height: " << height_ << " Width: " << width_ << std::endl;
  std::cout<< "Center: (" << pos_.x << "," << pos_.y << ")" <<std::endl;
  rectangle_t frame = getFrameRect();
  std::cout << "Frame Center ("<< frame.pos.x << "," << frame.pos.y << ")" << std::endl;
  std::cout << "Frame Width: " << frame.width << "   Frame Height: " << frame.height << std::endl;
}

void zabrodina::Rectangle::scale(const double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Error: coefficient <= 0");
  }
  else
  {
    height_ *= coefficient;
    width_  *= coefficient;
    vertex_[0] = { pos_.x + width_ / 2, pos_.y + height_ / 2 };
    vertex_[1] = { pos_.x - width_ / 2, pos_.y + height_ / 2 };
    vertex_[2] = { pos_.x - width_ / 2, pos_.y - height_ / 2 };
    vertex_[3] = { pos_.x + width_ / 2, pos_.y - height_ / 2 };
  }
}

void zabrodina::Rectangle::rotate(const double angle)
{
  double angle_cos = cos(angle * M_PI / 180);
  double angle_sin = sin(angle * M_PI / 180);
  point_t center = getFrameRect().pos;
  for (int i = 0; i < 4; i++)
  {
    vertex_[i] = {center.x + angle_cos * (vertex_[i].x - center.x) - angle_sin * (vertex_[i].y - center.y),
    center.y + angle_cos * (vertex_[i].y - center.y) + angle_sin * (vertex_[i].x - center.x)};
  }
}

std::string zabrodina::Rectangle::getName() const noexcept
{
  return "R";
}
