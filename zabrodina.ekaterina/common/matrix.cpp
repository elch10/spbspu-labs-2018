#include "matrix.hpp"
#include "composite-shape.hpp"

#include <iostream>
#include <cmath>
#include <stdexcept>
#include <iomanip>
#include <string>

zabrodina::Matrix::Matrix() :
lines_(0),
columns_(0)
{}

zabrodina::Matrix::Matrix(CompositeShape &compos):
matrix_(new std::shared_ptr<Shape> [1]),
lines_(1),
columns_(1)
{
  if (compos[1] == nullptr)
  {
    throw std::invalid_argument("Error: Object has not been created yet or already deleted");
  }
  for(int i=0; i< compos.count_; i++)
  {
    addShape(std::make_shared<zabrodina::CompositeShape> (compos[i]));
  }
}

zabrodina::Matrix::Matrix(const std::shared_ptr<Shape> &newShape):
matrix_(new std::shared_ptr<Shape> [1]),
lines_(1),
columns_(1)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Error: Object has not been created yet or already deleted");
  }
  matrix_[0] = newShape;
}

std::unique_ptr <std::shared_ptr<zabrodina::Shape>[]>::pointer zabrodina::Matrix::operator[](size_t index) const
{
  if (index > lines_)
  {
    throw std::invalid_argument("Error: Index out of range");
  }
  return matrix_.get() + index * columns_;
}

void zabrodina::Matrix::addShape(const std::shared_ptr<Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Error: Object has not been created yet or already deleted");
  }
  size_t i = lines_ * columns_;
  size_t current_line = 1;
  while (i > 0)
  {
    i--;
    if (checkOverlapOfShapes(matrix_[i], shape))
    {
      current_line = i / columns_ + 2;
    }
  }
  size_t lines = lines_;
  size_t columns = columns_;
  size_t free_columns = 0;
  if (current_line > lines_)
  {
    lines++;
    free_columns = columns_;
  }
  else
  {
    size_t j = (current_line - 1) * columns_;
    while (j < (current_line * columns_))
    {
      if (matrix_[j] == nullptr)
      {
        free_columns++;
      }
      j++;
    }
    if (free_columns == 0)
    {
      columns++;
      free_columns = 1;
    }
  }
  std::unique_ptr <std::shared_ptr<Shape>[]> matrix(new std::shared_ptr<Shape>[lines * columns]);
  for (size_t i = 0; i < lines; i++)
  {
    for (size_t j = 0; j < columns; j++)
    {
      if (i >= lines_ || j >= columns_)
      {
        matrix[i * columns + j] = nullptr;
        continue;
      }
      matrix[i * columns + j] = matrix_[i * columns_ + j];
    }
  }
  matrix[current_line * columns - free_columns] = shape;
  matrix_.swap(matrix);
  lines_ = lines;
  columns_ = columns;
}

void zabrodina::Matrix::addCompositeShape(CompositeShape &compos)
{
  for(int i=0; i< compos.count_; i++)
  {
    addShape(std::make_shared<zabrodina::CompositeShape> (compos[i]));
  }
}

bool zabrodina::Matrix::checkOverlapOfShapes(const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> &shape2) const
{
  if (shape1 == nullptr || shape2 == nullptr)
  {
    return false;
  }
  rectangle_t shape1_frame_rect = shape1->getFrameRect();
  rectangle_t shape2_frame_rect = shape2->getFrameRect();
  return ((std::abs(shape1_frame_rect.pos.x - shape2_frame_rect.pos.x)
       < ((shape1_frame_rect.width / 2) + (shape2_frame_rect.width / 2)))
        && ((std::abs(shape1_frame_rect.pos.y - shape2_frame_rect.pos.y)
           < ((shape1_frame_rect.height / 2) + (shape2_frame_rect.height / 2)))));
}

void zabrodina::Matrix::printInf() const
{
  for (size_t i = 0; i < lines_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      if (matrix_[i * columns_ + j] != nullptr)
      {
        std::cout << std::setw(3) << std::left << matrix_[i * columns_ + j]->getName();
      }
    }
    std::cout << std::endl;
  }
}
