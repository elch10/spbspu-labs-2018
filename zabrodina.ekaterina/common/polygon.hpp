#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <cstdlib>
#include <memory>
#include "shape.hpp"

namespace zabrodina
{
  class Polygon : public Shape
  {
  public:
    Polygon(std::initializer_list<point_t> vertexes);
    point_t operator[](size_t index) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &pos) override;
    void move(double Ox, double Oy) override;
    void printInf() const override;
    std::string getName() const noexcept override;
    void scale(double coefficient) override;
    void rotate(double angle) override;
  private:
    std::unique_ptr<point_t[]> polygonVertex_;
    size_t count_;
    point_t pos_;
    bool checkConvex() const;
  };
}

#endif
