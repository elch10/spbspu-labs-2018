#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

int main()
{
  zagorodnev::point_t p = {128.0, 256.0};
  std::shared_ptr<zagorodnev::Shape> rectPtr (new zagorodnev::Rectangle(p,24.0, 48.0));
  std::shared_ptr<zagorodnev::Shape> circlePtr(new zagorodnev::Circle(p,4.0));
  std::shared_ptr<zagorodnev::Shape> trianglePtr(new zagorodnev::Triangle({2.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
  zagorodnev::CompositeShape comp_shape(rectPtr);
  comp_shape.addShape(circlePtr);
  comp_shape.addShape(trianglePtr);
  std::cout<< comp_shape.getArea() << "\n";
  comp_shape.rotate(90.0);
  std::cout<< comp_shape.getArea() << "\n";
  comp_shape.move(34.0, 43.0);
  comp_shape.rotate(20.0);
  std::cout<< comp_shape.getArea() << "\n";
  comp_shape.deleteShape(1);

  zagorodnev::Matrix matr(rectPtr);
  matr.addShape(circlePtr);
  matr.addShape(trianglePtr);
  return 0;
}
