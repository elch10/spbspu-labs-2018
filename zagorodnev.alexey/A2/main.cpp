#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  zagorodnev::point_t p = {128.0, 256.0};
  std::shared_ptr<zagorodnev::Shape> rectanglePtr (new zagorodnev::Rectangle(p,24.0, 48.0));
  std::shared_ptr<zagorodnev::Shape> circlePtr(new zagorodnev::Circle(p,4.0));
  std::shared_ptr<zagorodnev::Shape> trianglePtr(new zagorodnev::Triangle(p, {4.0, 4.0}, {7.0, 5.0}));
  zagorodnev::CompositeShape comp_shape(rectanglePtr);
  comp_shape.addShape(circlePtr);
  comp_shape.addShape(trianglePtr);
  comp_shape.move({11.0, 12.0});
  comp_shape.move(34.0, 43.0);
  comp_shape.scale(5.0);
  comp_shape.deleteShape(1);

  return 0;
}
