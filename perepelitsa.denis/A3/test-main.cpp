#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 0.00001;


BOOST_AUTO_TEST_SUITE(CompositeShapeTests)
  
  BOOST_AUTO_TEST_CASE(Area)
  {
    std::shared_ptr<perepelitsa::Shape> rectPtr
    = std::make_shared<perepelitsa::Rectangle>(perepelitsa::Rectangle({128.0, 256.0},24.0, 48.0));
    perepelitsa::CompositeShape shape(rectPtr);

    std::shared_ptr<perepelitsa::Shape> circlePtr
    = std::make_shared<perepelitsa::Circle>(perepelitsa::Circle({10.0, 10.0},5.0));
    shape.addShape(circlePtr);

    std::shared_ptr<perepelitsa::Shape> trianglePtr
    = std::make_shared<perepelitsa::Triangle>(perepelitsa::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);

    double area = shape.getArea();
    shape.move(16.0, 32.0);
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getArea(), area, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Coords)
  {
    std::shared_ptr<perepelitsa::Shape> rectPtr
    = std::make_shared<perepelitsa::Rectangle>(perepelitsa::Rectangle({6.0, 4.0}, 6.0, 4.0));
    perepelitsa::CompositeShape shape(rectPtr);

    std::shared_ptr<perepelitsa::Shape> circlePtr
    = std::make_shared<perepelitsa::Circle>(perepelitsa::Circle({6.0, 4.0}, 40.0));
    shape.addShape(circlePtr);

    std::shared_ptr<perepelitsa::Shape> trianglePtr
    = std::make_shared<perepelitsa::Triangle>(perepelitsa::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);

    double posX = shape.getFrameRect().pos.x;
    double posY = shape.getFrameRect().pos.y;
    const double ratio = 1.7;
    shape.scale(ratio);

    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.x, posX, EPSILON);
    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.y, posY, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Scale)
  {
    const double ratio = 2.0;
    perepelitsa::point_t point = {6.0, 4.0};

    std::shared_ptr<perepelitsa::Shape> rectPtr (new perepelitsa::Rectangle(point, 6.0, 4.0));
    std::shared_ptr<perepelitsa::Shape> circlePtr (new perepelitsa::Circle(point, 40.0));
    std::shared_ptr<perepelitsa::Shape> trianglePtr (new perepelitsa::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));

    perepelitsa::CompositeShape shape(rectPtr);
    shape.addShape(circlePtr);
    shape.addShape(trianglePtr);

    double area = shape.getArea();
    double posX = shape.getFrameRect().pos.x;
    double posY = shape.getFrameRect().pos.y;

    shape.scale(ratio);

    BOOST_CHECK_CLOSE(shape.getArea(), area*ratio*ratio, EPSILON);
    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.x, posX, EPSILON);
    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.y, posY, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentConstructor)
  {
    std::shared_ptr<perepelitsa::Shape> rectPtr
    = std::make_shared<perepelitsa::Rectangle>(perepelitsa::Rectangle({128.0, 256.0},24.0, 48.0));
    perepelitsa::CompositeShape shape(rectPtr);

    std::shared_ptr<perepelitsa::Shape> circlePtr
    = std::make_shared<perepelitsa::Circle>(perepelitsa::Circle({10.0, 10.0}, 5.0));
    shape.addShape(circlePtr);

    std::shared_ptr<perepelitsa::Shape> trianglePtr
    = std::make_shared<perepelitsa::Triangle>(perepelitsa::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);

    BOOST_REQUIRE_THROW(perepelitsa::CompositeShape shape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentAddShape)
  {
    std::shared_ptr<perepelitsa::Shape> rectPtr
    = std::make_shared<perepelitsa::Rectangle>(perepelitsa::Rectangle({128.0, 256.0},24.0, 48.0));
    perepelitsa::CompositeShape shape(rectPtr);

    BOOST_REQUIRE_THROW(shape.addShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentScale)
  {
    perepelitsa::point_t point = { 6.0, 8.0 };
    std::shared_ptr<perepelitsa::Shape> rectPtr (new perepelitsa::Rectangle(point, 40.0, 50.0));
    perepelitsa::CompositeShape shape(rectPtr);
    BOOST_CHECK_THROW(shape.scale(-5.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
