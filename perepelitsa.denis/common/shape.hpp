#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "base-types.hpp"

namespace perepelitsa
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t &p) = 0;
    virtual void info() const = 0;
    virtual void scale(const double ratio) = 0;
    virtual void rotate(double degrees) = 0;
  };
}

#endif
