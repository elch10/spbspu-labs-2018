#include "triangle.hpp"
#include <algorithm>
#include <stdexcept>
#include <math.h>
#include "base-types.hpp"
#include <iostream>

perepelitsa::Triangle::Triangle(const point_t &p1, const point_t &p2, const point_t &p3) :
  a_(p1),
  b_(p2),
  c_(p3)
{
  if (getArea() < 0.0)
  {
    throw std::invalid_argument("Invalid triangle parameters!");
  }
}

double perepelitsa::Triangle::getArea() const
{
  double ab = getDistance(a_, b_);
  double ac = getDistance(a_, c_);
  double bc = getDistance(b_, c_);
  double s = (ab + ac + bc) / 2;
  return std::sqrt(s*(s - ab)*(s - ac)*(s - bc));
}

perepelitsa::rectangle_t perepelitsa::Triangle::getFrameRect() const
{
  using namespace std;
  double left = min(min(a_.x, b_.x), c_.x);
  double bot = min(min(a_.y, b_.y), c_.y);
  double h = fabs(max(max(a_.y, b_.y), c_.y) - bot);
  double w = fabs(max(max(a_.x, b_.x), c_.x) - left);
  return rectangle_t{ findCenter(), w, h };
}

void perepelitsa::Triangle::move(double dx, double dy)
{
  a_.x += dx;
  b_.x += dx;
  c_.x += dx;
  a_.y += dy;
  b_.y += dy;
  c_.y += dy;
}

void perepelitsa::Triangle::move(const point_t &p)
{
  move(p.x - findCenter().x, p.y - findCenter().y);
}

perepelitsa::point_t perepelitsa::Triangle::findCenter() const
{
  return point_t{ (a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3 };
}

double perepelitsa::Triangle::getDistance(const point_t &p1, const point_t &p2)
{
  return std::sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y));
}

void perepelitsa::Triangle::info() const
{
  std::cout << "A: x = " << a_.x << " y = " << a_.y << std::endl;
  std::cout << "B: x = " << b_.x << " y = " << b_.y << std::endl;
  std::cout << "C: x = " << c_.x << " y = " << c_.y << std::endl;
}

void perepelitsa::Triangle::scale(const double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Scale coefficient must be > 0");
  }
  double CenterX = findCenter().x;
  double CenterY = findCenter().y;
  a_.x = (a_.x - CenterX) * ratio + CenterX;
  a_.y = (a_.y - CenterY) * ratio + CenterY;
  b_.x = (b_.x - CenterX) * ratio + CenterX;
  b_.y = (b_.y - CenterY) * ratio + CenterY;
  c_.x = (c_.x - CenterX) * ratio + CenterX;
  c_.y = (c_.y - CenterY) * ratio + CenterY;
}

void perepelitsa::Triangle::rotate(double degrees)

{

  point_t centre = Triangle::getFrameRect().pos;

  degrees = (degrees * M_PI) / 180;

  const double sine = sin(degrees);

  const double cose = cos(degrees);

  a_ = { centre.x + (a_.x - centre.x) * cose - (a_.y - centre.y) * sine,

              centre.y + (a_.y - centre.y) * cose + (a_.x - centre.x) * sine };

  b_ = { centre.x + (b_.x - centre.x) * cose - (b_.y - centre.y) * sine,
              centre.y + (b_.y - centre.y) * cose + (b_.x - centre.x) * sine };

  c_ = { centre.x + (c_.x - centre.x) * cose - (c_.y - centre.y) * sine,

              centre.y + (c_.y - centre.y) * cose + (c_.x - centre.x) * sine };

}
