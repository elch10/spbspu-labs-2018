#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"
#include "base-types.hpp"

namespace perepelitsa
{
  class Triangle : 
    public Shape
  {
  public:
    Triangle(const point_t &p1, const point_t &p2, const point_t &p3);

    virtual double getArea() const override;
    virtual rectangle_t getFrameRect() const override;
    virtual void move(double dx, double dy) override;
    virtual void move(const point_t &p) override;
    void info() const override;
    void scale(const double ratio) override;
    void rotate(double degrees) override;

  private:
    point_t a_, b_, c_;
    point_t findCenter() const;
    static double getDistance(const point_t &p1, const point_t &p2);
    double angel_;
    double degrees_;
  };
}

#endif
