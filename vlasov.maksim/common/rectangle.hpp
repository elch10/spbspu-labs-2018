#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace vlasov {
  class Rectangle : public Shape {
    public:
    Rectangle(point_t position, double width, double height);

    double getArea() const override;

    rectangle_t getFrameRect() const override;

    void scale(double multiplier) override;

    void rotate(double angle) override;

    private:
    double width_;
    double height_;
    double angle_;
  };
}
#endif
