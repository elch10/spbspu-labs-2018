#ifndef A4_MATRIX_HPP
#define A4_MATRIX_HPP
#include <memory>
#include "shape.hpp"

namespace vlasov {
  class Matrix {
    public:
    Matrix();
    Matrix(const std::shared_ptr<Shape> shape);
    Matrix(const Matrix & matrix);
    Matrix(Matrix && rhs) noexcept;
    Matrix & operator=(const Matrix & rhs);
    Matrix & operator=(Matrix && rhs) noexcept;
    bool operator==(const Matrix & rhs) const;
    bool operator!=(const Matrix & rhs) const;
    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](const int index) const;
    void add(const std::shared_ptr<Shape> shape) noexcept;
    int getNumber() const noexcept;
    int getSize() const noexcept;

    private:
    std::unique_ptr<std::shared_ptr<Shape>[]> mShapes_;
    int layersNumber_;
    int layerSize_;
    bool intersect(const int index, const std::shared_ptr<Shape> & rhs) noexcept;
  };
}

#endif
