#include "circle.hpp"

#include <cmath>
#include <stdexcept>

mamaev::Circle::Circle(const double radius, const mamaev::point_t & pos):
  pos_(pos),
  radius_(radius)
{
  if (radius_ < 0.0)
  {
    throw std::invalid_argument("Invalid redius");
  }
}

double mamaev::Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

mamaev::rectangle_t mamaev::Circle::getFrameRect() const
{
  return {radius_ * 2 , radius_ * 2, pos_};
}

mamaev::point_t mamaev::Circle::getPos() const
{
  return pos_;
}

void mamaev::Circle::move(const point_t & pos)
{
  pos_ = pos;
}

void mamaev::Circle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void mamaev::Circle::scale(const double coefficient)
{
  if (coefficient < 0.0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  radius_ *= coefficient;
}

double mamaev::Circle::getRadius() const
{
  return radius_;
}
