#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"

using namespace mamaev;

const double EPS = 0.00001;

BOOST_AUTO_TEST_SUITE(Circle_Test)
  BOOST_AUTO_TEST_CASE(Area_Move_Test_Circle)
  {
    Circle circ(2.0, {7.6, 0.0});
    double oldArea = circ.getArea();
    circ.move(-1.0, 2.0);
    BOOST_CHECK_CLOSE_FRACTION(circ.getArea(), oldArea, EPS);
    circ.move({-1.0, 2.0});
    BOOST_CHECK_CLOSE_FRACTION(circ.getArea(), oldArea, EPS);
  }

  BOOST_AUTO_TEST_CASE(Radius_Move_Test_Circle)
  {
    Circle circ(2.0, {7.6, 0.0});
    double oldRadius = circ.getRadius();
    point_t oldPos = circ.getPos();
    circ.move(-1.0, 2.0);
    BOOST_CHECK_CLOSE_FRACTION(circ.getRadius(), oldRadius, EPS);
    BOOST_CHECK_CLOSE_FRACTION(circ.getPos().x, (oldPos.x - 1.0), EPS);
    BOOST_CHECK_CLOSE_FRACTION(circ.getPos().y, (oldPos.y + 2.0), EPS);
    circ.move({-1.0, 2.0});
    BOOST_CHECK_CLOSE_FRACTION(circ.getRadius(), oldRadius, EPS);
    BOOST_CHECK_CLOSE_FRACTION(circ.getPos().x, -1.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(circ.getPos().y, 2.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Area_Scale_Test_Circle)
  {
    Circle circ(2.0, {7.6, 0.0});
    double oldArea = circ.getArea();
    double coeff = 5.0;
    circ.scale(coeff);
    BOOST_CHECK_CLOSE_FRACTION(circ.getArea(), (oldArea * (coeff * coeff)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Radius_Scale_Test_Circle)
  {
    Circle circ(2.0, {7.6, 0.0});
    double oldRadius = circ.getRadius();
    double coeff = 10.0;
    circ.scale(coeff);
    BOOST_CHECK_CLOSE_FRACTION(circ.getRadius(), (oldRadius * coeff), EPS);
  }

  BOOST_AUTO_TEST_CASE(Inavalid_Argumend_Constructor_Test_Circle)
  {
    BOOST_CHECK_THROW(Circle(-1.0, {0.0, 0.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Argument_Scale_Test_Circle)
  {
    Circle circ(4.0, {0.0, 0.0});
    BOOST_CHECK_THROW(circ.scale(-1.0), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Rectangle_Tests)
  BOOST_AUTO_TEST_CASE(Area_Move_Test_Rectangle)
  {
    Rectangle rect(2.25, 9.7, {3.2, 3.4});
    double oldArea = rect.getArea();
    rect.move(10.0, 5.0);
    BOOST_CHECK_CLOSE_FRACTION(rect.getArea(), oldArea, EPS);
    rect.move({10.0, 5.0});
    BOOST_CHECK_CLOSE_FRACTION(rect.getArea(), oldArea, EPS);
  }

  BOOST_AUTO_TEST_CASE(Width_And_Height_Move_Test_Rectangle)
  {
    Rectangle rect(2.25, 9.7, {3.2, 3.4});
    double oldWidth = rect.getWidth();
    double oldHeight = rect.getHeight();
    rect.move(10.0, 24.0);
    BOOST_CHECK_CLOSE_FRACTION(rect.getWidth(), oldWidth, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rect.getHeight(), oldHeight, EPS);
    rect.move({10.0, 24.0});
    BOOST_CHECK_CLOSE_FRACTION(rect.getWidth(), oldWidth, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rect.getHeight(), oldHeight, EPS);
  }

  BOOST_AUTO_TEST_CASE(Area_Scale_Test_Rectangle)
  {
    Rectangle rect(2.25, 9.7, {3.2, 3.4});
    double oldArea = rect.getArea();
    double coeff = 10.0;
    rect.scale(coeff);
    BOOST_CHECK_CLOSE_FRACTION(rect.getArea(), (oldArea * (coeff * coeff)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Width_And_Height_Scale_Test_Rectangle)
  {
    Rectangle rect(2.25, 9.7, {3.2, 3.4});
    double oldWidth = rect.getWidth();
    double oldHeight = rect.getHeight();
    double coeff = 10.0;
    rect.scale(coeff);
    BOOST_CHECK_CLOSE_FRACTION(rect.getWidth(), (oldWidth * coeff), EPS);
    BOOST_CHECK_CLOSE_FRACTION(rect.getHeight(), (oldHeight * coeff), EPS);
  }

  BOOST_AUTO_TEST_CASE(Inavalid_Argumend_Constructor_Test_Rectangle)
  {
    BOOST_CHECK_THROW(Rectangle(-1, -1, {0.0, 0.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Argument_Scale_Test_Rectangle)
  {
    Rectangle rect(1, 1, {0.0, 0.0});
    BOOST_CHECK_THROW(rect.scale(-1.0), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()
