#ifndef base_types_hpp
#define base_types_hpp

struct point_t
{   
  double x,y;
};

struct rectangle_t
{
  double width,height;
  point_t pos;
};

#endif

