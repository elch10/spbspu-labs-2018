#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include <iostream>

void mainShape(Shape &sh, double mv1xx, double mv1yy, double mv2xx, double mv2yy)
{
  std::cout << "Area of figure: " << sh.getArea() << std::endl;
  std::cout << "Width: " << sh.getFrameRect().width << std::endl;
  std::cout << "Height: " << sh.getFrameRect().height << std::endl;
  std::cout << "x coordinate: " << sh.getFrameRect().pos.x << std::endl;
  std::cout << "y coodrinate: " << sh.getFrameRect().pos.y << std::endl;
  sh.move({ mv1xx, mv1yy });
  std::cout << "Move to point(x = " << mv1xx << ",y = " << mv1yy <<"): " << std::endl;
  std::cout << "x coordinate after moving to point: " << sh.getFrameRect().pos.x << std::endl;
  std::cout << "y coordinate after moving to point: " << sh.getFrameRect().pos.y << std::endl;
  sh.move(mv2xx, mv2yy);
  std::cout << "Move for axes(x = " << mv2xx << ",y = " << mv2yy << "): " << std::endl;
  std::cout << "x coordinate after moving for axes: " << sh.getFrameRect().pos.x << std::endl;
  std::cout << "y coordinate after moving for axes: " << sh.getFrameRect().pos.y << std::endl;
}

int main()
{
  Rectangle rect({ 8, 6,{ 6, 2 } });
  Circle circ({ 12, 5 }, 23);
  Triangle tria({ 2.0, 4.0 }, { 3.0, 3.0 }, { 9.0, 7.0 });
  const double mv1x = 243;
  const double mv1y = 462;
  const double mv2x = 345;
  const double mv2y = 123;
  std::cout << "Shape Rectangle" << std::endl;
  mainShape(rect,mv1x,mv1y,mv2x,mv2y);
  std::cout << std::endl;
  std::cout << "Shape Circle" << std::endl;
  mainShape(circ,mv1x,mv1y,mv2x,mv2y);
  std::cout << std::endl;
  std::cout <<"Shape Triangle" << std::endl;
  mainShape(tria,mv1x,mv1y,mv2x,mv2y);
 return 0;
}
