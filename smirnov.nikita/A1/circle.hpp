#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(const point_t &cr, const double rs);
  double getArea() const;
  rectangle_t getFrameRect() const;
  void move(const point_t &pt);
  void move(const double xx, const double yy);
private:
  point_t cr;
  double r;
};
