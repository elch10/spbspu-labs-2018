#include"matrix.hpp"
#include"shape.hpp"

#include <stdexcept>
#include <iostream>
#include <new>
#include <algorithm>


iliintsev::Matrix::Matrix(const std::shared_ptr<iliintsev::Shape> & shape):
  matrix_(nullptr),
  layerNum_(0),
  layerSize_(0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Empty is shape");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> newElement(new std::shared_ptr<Shape>[(layerNum_ + 1) * (layerSize_ + 1)]);
  newElement[0]=shape;
  matrix_=std::move(newElement);
  layerSize_++;
  layerNum_++;
}

iliintsev::Matrix::Matrix(const iliintsev::Matrix & matr):
  matrix_(new std::shared_ptr<Shape>[matr.layerNum_ * matr.layerSize_]),
  layerNum_(matr.layerNum_),
  layerSize_(matr.layerSize_)
{
  for(size_t i=0;i<layerNum_*layerSize_;++i)
  {
    matrix_[i]=matr.matrix_[i];
  }
}

iliintsev::Matrix::Matrix(iliintsev::Matrix && matr):
  matrix_(std::move(matr.matrix_)),
  layerNum_(matr.layerNum_),
  layerSize_(matr.layerSize_)
{
  matr.matrix_=nullptr;
  matr.layerNum_=0;
  matr.layerSize_=0;
}

iliintsev::Matrix::~Matrix()
{
  matrix_.reset();
  matrix_=nullptr;
  layerNum_=0;
  layerSize_=0;
}

iliintsev::Matrix & iliintsev::Matrix::operator = (const iliintsev::Matrix & matr)
{
  if (this == & matr)
  {
    return *this;
  }
  layerNum_ = matr.layerNum_;
  layerSize_ = matr.layerSize_;
  matrix_.reset(new std::shared_ptr<Shape> [matr.layerSize_ * matr.layerNum_] );
  for(size_t i=0;i<layerNum_*layerSize_;++i)
  {
    matrix_[i] = matr.matrix_[i];
  }
  return *this;
}

iliintsev::Matrix & iliintsev::Matrix::operator = (iliintsev::Matrix && matr)
{
  if (this == & matr)
  {
    return *this;
  }
  matrix_.reset(new std::shared_ptr<Shape>[matr.layerNum_* matr.layerSize_]);
  layerSize_ = matr.layerSize_;
  layerNum_ = matr.layerNum_;
  matrix_ = std::move(matr.matrix_);
  matr.matrix_ = nullptr;
  matr.layerNum_ = 0;
  matr.layerSize_= 0;
  return *this;
}

std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> iliintsev::Matrix::operator[](const size_t index) const
{
  if (layerNum_ == 0) 
  {
    throw std::out_of_range("Matrix is empty");
  }
  if (index > layerNum_ - 1) 
  {
    throw std::invalid_argument("Invalid layerCount index");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]>
    layer(new std::shared_ptr<Shape>[layerSize_]);
  for (size_t i = 0; i < layerSize_; ++i) {
    layer[i] = matrix_[index * layerSize_ + i];
  }
  return layer;
}

bool iliintsev::Matrix::checkOverLapping(size_t index, const std::shared_ptr< iliintsev::Shape > &shape) const noexcept
{
  rectangle_t nShapeFrameRect = shape->getFrameRect();
  rectangle_t mShapeFrameRect = matrix_[index]->getFrameRect();
  point_t newPoints[4] = 
  {
    { nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0 },
    { nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0 },
    { nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0 },
    { nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0 }
  };

  point_t matrixPoints[4] = 
  {
    { mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0 },
    { mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0 },
    { mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0 },
    { mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0 }
  };

  for (int i = 0; i < 4; ++i)
  {
    if (((newPoints[i].x >= matrixPoints[0].x) && (newPoints[i].x <= matrixPoints[2].x)
      && (newPoints[i].y >= matrixPoints[3].y) && (newPoints[i].y <= matrixPoints[1].y))
      || ((matrixPoints[i].x >= newPoints[0].x) && (matrixPoints[i].x <= newPoints[2].x)
        && (matrixPoints[i].y >= newPoints[3].y) && (matrixPoints[i].y <= newPoints[1].y)))
    {
      return true;
    }
  }
  return false;
}

void iliintsev::Matrix::add(const std::shared_ptr<iliintsev::Shape> & shape) noexcept
{
  bool addedShape = false;

  for (size_t i = 0; !addedShape; ++i)
  {
    for (size_t j = 0; j < layerSize_; ++j)
    {
      if (!matrix_[i * layerSize_ + j])
      {
        matrix_[i * layerSize_ + j] = shape;
        addedShape = true;
        break;
      }
      else
      {
        if (checkOverLapping(i * layerSize_ + j, shape))
        {
          break;
        }
      }

      if (j == (layerSize_ - 1))
      {
        layerSize_++;
        std::unique_ptr< std::shared_ptr<Shape>[] > newElements(new std::shared_ptr<Shape>[layerNum_ * layerSize_]);
        for (size_t n = 0; n < layerNum_; ++n)
        {
          for (size_t m = 0; m < layerSize_ - 1; ++m)
          {
            newElements[n * layerSize_ + m] = matrix_[n * (layerSize_ - 1) + m];
          }
          newElements[(n + 1) * layerSize_ - 1] = nullptr;
        }
        newElements[(i + 1) * layerSize_ - 1] = shape;
        matrix_.swap(newElements);
        addedShape = true;
        break;
      }
    }
    if ((i == (layerNum_ - 1)) && !addedShape)
    {
      layerNum_++;
      std::unique_ptr< std::shared_ptr<Shape >[] > newElements(new std::shared_ptr<Shape>[layerNum_ * layerSize_]);
      for (size_t n = 0; n < ((layerNum_ - 1) * layerSize_); ++n)
      {
        newElements[n] = matrix_[n];
      }
      for (size_t n = ((layerNum_ - 1) * layerSize_); n < (layerNum_ * layerSize_); ++n)
      {
        newElements[n] = nullptr;
      }
      newElements[(layerNum_ - 1) * layerSize_] = shape;
      matrix_.swap(newElements);
      addedShape = true;
    }
  }
}

size_t iliintsev::Matrix::getLayerNum() const noexcept
{
  return layerNum_;
}

size_t iliintsev::Matrix::getLayerSize() const noexcept
{
  return layerSize_;
}

void iliintsev::Matrix::add(const std::shared_ptr<iliintsev::CompositeShape> & shape, size_t size) noexcept
{
  for (size_t i = 0; i < size; ++i) 
  {
    this->add((*shape)[i]);
  }
}

void iliintsev::Matrix::printInfo() const noexcept
{
  for(size_t i=0;i<layerNum_;++i)
  {
    std::cout <<i+1<< "- Layer----------------------------------------" << std::endl;
    for(size_t j=0;j<layerSize_;++j)
    {
      if (matrix_[i*layerSize_ + j])
      {
        matrix_[i*layerSize_ + j]->printInfo();
      }
    }
  }
}
