#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle:
  public Shape
{
public:
  Rectangle(const rectangle_t & set_coordinate_size);

  double getArea() const noexcept override;
  rectangle_t getFrameRect() const noexcept override;
  void move(const point_t & new_center) noexcept override;
  void move(const double dx, const double dy) noexcept override;
  
private:
  rectangle_t coordinate_size_;
};

#endif
