#include "matrix.hpp"
#include "shape.hpp"
#include <memory>
#include <stdexcept>

namespace kudryakov
{
  Matrix::Matrix(const std::shared_ptr<Shape> shape) :
    array_(nullptr),
    layersNumber_(0),
    layerSize_(0)
  {
    if (shape == nullptr)
    {
      throw std::invalid_argument("Null pointer!!");
    }
    addShape(shape);
  }
  
  Matrix::Matrix(const Matrix& other) :
    array_(new std::shared_ptr<Shape>[other.layersNumber_ * other.layerSize_]),
    layersNumber_(other.layersNumber_),
    layerSize_(other.layerSize_)
  {
    for (int i = 0; i < layersNumber_ * layerSize_; ++i)
    {
      array_[i] = other.array_[i];
    }
  }
  
  Matrix::Matrix(Matrix&& other) :
    array_(nullptr),
    layersNumber_(other.layersNumber_),
    layerSize_(other.layerSize_)
  {
    array_.swap(other.array_);
    other.layersNumber_ = 0;
    other.layerSize_ = 0;
  }
  
  Matrix::Matrix(const CompositeShape& compositeShape) :
    array_(nullptr),
    layersNumber_(0),
    layerSize_(0)
  {
    if (compositeShape.getSize() == 0)
    {
      throw std::invalid_argument("Empty CompositeShape!!");
    }
    for (int i = 0; i < compositeShape.getSize(); ++i)
    {
      addShape(compositeShape[i]);
    }
  }
  
  Matrix& Matrix::operator=(const Matrix& other)
  {
    if (this != &other)
    {
      std::unique_ptr<std::shared_ptr<Shape>[]> new_parts_(new std::shared_ptr<Shape>[other.layersNumber_
        * other.layerSize_]);
      layersNumber_ = other.layersNumber_;
      layerSize_ = other.layerSize_;
      for (int i = 0; i < layersNumber_ * layerSize_; ++i)
      {
        new_parts_[i] = other.array_[i];
      }
      array_.swap(new_parts_);
    }
    return *this;
  }
  
  Matrix& Matrix::operator=(Matrix&& other)
  {
    if (this != &other)
    {
      layersNumber_ = other.layersNumber_;
      layerSize_ = other.layerSize_;
      array_.reset();
      array_.swap(other.array_);
      other.layersNumber_ = 0;
      other.layerSize_ = 0;
    }
    return *this;
  }
  
  bool Matrix::operator==(const Matrix& other) const
  {
    if ((this->layersNumber_ == other.layersNumber_) && (this->layerSize_ == other.layerSize_))
    {
      bool equal = true;
      for (int i = 0; i < layersNumber_ * layerSize_; ++i)
      {
        if (!(this->array_[i] == other.array_[i]))
        {
          equal = false;
        }
      }
      if (equal)
      {
        return true;
      }
    }
    return false;
  }
  
  bool Matrix::operator!=(const Matrix& other) const
  {
    return !(*this == other);
  }
  
  std::unique_ptr<std::shared_ptr<Shape>[]> Matrix::operator[](const int layerIndex) const
  {
    if ((layerIndex < 0) || (layerIndex >= layersNumber_))
    {
      throw std::out_of_range("Invalid layer number!!");
    }
    std::unique_ptr<std::shared_ptr<Shape>[]> new_layer(new std::shared_ptr<Shape>[layerSize_]);
    for (int i = 0; i < layerSize_; ++i)
    {
      new_layer[i] = array_[layerIndex * layerSize_ + i];
    }
    return new_layer;
  }
  
  void Matrix::addShape(const std::shared_ptr<Shape> new_shape)
  {
    if (layersNumber_ == 0)
    {
      std::unique_ptr<std::shared_ptr<Shape>[]> new_parts_(
        new std::shared_ptr<Shape>[(layersNumber_+1) * (layerSize_+1)]);
      ++layerSize_;
      ++layersNumber_;
      array_.swap(new_parts_);
      array_[0] = new_shape;
    }
    else
    {
      bool addedShape = false;
      for (int i = 0; !addedShape; ++i)
      {
        for (int j =0; j <layerSize_; ++j)
        {
          if (!array_[i * layerSize_ + j])
          {
            array_[i * layerSize_ + j] = new_shape;
            addedShape = true;
            break;
          }
          else
          {
            if (overLappingCheck(i * layerSize_ +j, new_shape))
            {
              break;
            }
          }
          if (j == (layerSize_ - 1))
          {
            std::unique_ptr<std::shared_ptr<Shape>[]> new_parts_(
              new std::shared_ptr<Shape>[layersNumber_ * (layerSize_+1)]);
            layerSize_++;
            for (int n = 0; n < layersNumber_; ++n)
            {
              for (int m = 0; m < layerSize_ - 1; ++m)
              {
                new_parts_[n * layerSize_ + m] = array_[n * (layerSize_ - 1) + m];
              }
              new_parts_[(n + 1) * layerSize_ - 1] = nullptr;
            }
            new_parts_[(i +1) * layerSize_ - 1] = new_shape;
            array_.swap(new_parts_);
            addedShape = true;
            break;
          }
        }
        if ((i == (layersNumber_ - 1)) && !addedShape)
        {
          std::unique_ptr<std::shared_ptr<Shape>[]> new_parts_(
            new std::shared_ptr<Shape>[(layersNumber_+1) * layerSize_]);
          layersNumber_++;
          for (int n = 0; n < ((layersNumber_ - 1) * layerSize_); ++n)
          {
            new_parts_[n] = array_[n];
          }
          for (int n = ((layersNumber_ - 1) * layerSize_); n < (layersNumber_ * layerSize_); ++n)
          {
            new_parts_[n] = nullptr;
          }
          new_parts_[(layersNumber_ - 1) * layerSize_] = new_shape;
          array_.swap(new_parts_);
          addedShape = true;
        }
      }
    }
  
  }
  
  int Matrix::getLayersNumber() const
  {
    return layersNumber_;
  }
  
  int Matrix::getLayerSize(const int layerNumber) const
  {
    int b = 0;
    for (int i = layerNumber; i < layerNumber * layerSize_; ++i)
      {
      if (array_ != nullptr)
        {
          ++b;
        }
      }
    return b;
  }
  
  int Matrix::getMaxLayerSize() const
  {
    return layerSize_;
  }
  
  bool Matrix::overLappingCheck(const int number, std::shared_ptr<Shape> new_shape) const
  {
    rectangle_t shapeFrameRect1 = new_shape->getFrameRect();
    rectangle_t shapeFrameRect2 = array_[number]->getFrameRect();
    
    point_t newShapePoints[4] = {
      {shapeFrameRect1.pos.x - shapeFrameRect1.width / 2, shapeFrameRect1.pos.y + shapeFrameRect1.height / 2},
      {shapeFrameRect1.pos.x + shapeFrameRect1.width / 2, shapeFrameRect1.pos.y + shapeFrameRect1.height / 2},
      {shapeFrameRect1.pos.x + shapeFrameRect1.width / 2, shapeFrameRect1.pos.y - shapeFrameRect1.height / 2},
      {shapeFrameRect1.pos.x - shapeFrameRect1.width / 2, shapeFrameRect1.pos.y - shapeFrameRect1.height / 2}
    };
    
    point_t newMatrixPoints[4] = {
      {shapeFrameRect2.pos.x - shapeFrameRect2.width / 2, shapeFrameRect2.pos.y + shapeFrameRect2.height / 2},
      {shapeFrameRect2.pos.x + shapeFrameRect2.width / 2, shapeFrameRect2.pos.y + shapeFrameRect2.height / 2},
      {shapeFrameRect2.pos.x + shapeFrameRect2.width / 2, shapeFrameRect2.pos.y - shapeFrameRect2.height / 2},
      {shapeFrameRect2.pos.x - shapeFrameRect2.width / 2, shapeFrameRect2.pos.y - shapeFrameRect2.height / 2}
    };
    
    for (int i = 0; i < 4; ++i)
    {
      if (((newShapePoints[i].x >= newMatrixPoints[0].x) && (newShapePoints[i].x <= newMatrixPoints[2].x)
           && (newShapePoints[i].y >= newMatrixPoints[3].y) && (newShapePoints[i].y <= newMatrixPoints[1].y))
          || ((newShapePoints[i].x <= newMatrixPoints[0].x) && (newShapePoints[i].x >= newMatrixPoints[2].x)
              && (newShapePoints[i].y <= newMatrixPoints[0].y) && (newShapePoints[i].y >= newMatrixPoints[2].y)))
      {
        return true;
      }
    }
    return false;
  }
}
