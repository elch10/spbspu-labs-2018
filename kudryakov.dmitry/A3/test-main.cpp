#define BOOST_TEST_MAIN

#include <stdexcept>
#include <math.h>
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double Epsilon = 0.00001;

BOOST_AUTO_TEST_SUITE(CompositeShapeSuite)
  
  BOOST_AUTO_TEST_CASE(CopyCosntructor)
  {
    kudryakov::Rectangle rectangle({-2,0}, 2, 2);
    kudryakov::Circle circle({2,0}, 1);
    
    std::shared_ptr <kudryakov::Shape> rectPtr = std::make_shared <kudryakov::Rectangle> (rectangle);
    std::shared_ptr <kudryakov::Shape> circPtr = std::make_shared <kudryakov::Circle> (circle);
  
    kudryakov::CompositeShape cs(circPtr);
    cs.addShape(rectPtr);
    
    kudryakov::CompositeShape cs2(cs);
    BOOST_CHECK_CLOSE(cs2.getArea(), cs.getArea(), Epsilon);
  }
  
  BOOST_AUTO_TEST_CASE(CopyOperator)
  {
    kudryakov::Rectangle rectangle({-2,0}, 2, 2);
    kudryakov::Circle circle({2,0}, 1);
  
    std::shared_ptr <kudryakov::Shape> rectPtr = std::make_shared <kudryakov::Rectangle> (rectangle);
    std::shared_ptr <kudryakov::Shape> circPtr = std::make_shared <kudryakov::Circle> (circle);
  
    kudryakov::CompositeShape cs(circPtr);
    cs.addShape(rectPtr);
    
    kudryakov::CompositeShape cs2;
    cs2 = cs;
    BOOST_CHECK_CLOSE(cs2.getArea(), cs.getArea(), Epsilon);
  }
  
  BOOST_AUTO_TEST_CASE(MoveAssignmentOperator)
  {
    kudryakov::Rectangle rectangle({-2,0}, 2, 2);
    kudryakov::Circle circle({2,0}, 1);
    
    std::shared_ptr <kudryakov::Shape> rectPtr = std::make_shared <kudryakov::Rectangle> (rectangle);
    std::shared_ptr <kudryakov::Shape> circPtr = std::make_shared <kudryakov::Circle> (circle);
    
    kudryakov::CompositeShape cs(circPtr);
    cs.addShape(rectPtr);
    
    double initCSArea = cs.getArea();
    
    kudryakov::CompositeShape cs2;
    cs2 = std::move(cs);
    BOOST_CHECK_CLOSE(cs2.getArea(), initCSArea, Epsilon);
    BOOST_CHECK_CLOSE(cs.getArea(), 0.0, Epsilon);
  }
  
  BOOST_AUTO_TEST_CASE(SquareBracketsOperator)
  {
    kudryakov::Rectangle rectangle({-2,0}, 2, 2);
    kudryakov::Circle circle({2,0}, 1);
    
    std::shared_ptr <kudryakov::Shape> rectPtr = std::make_shared <kudryakov::Rectangle> (rectangle);
    std::shared_ptr <kudryakov::Shape> circPtr = std::make_shared <kudryakov::Circle> (circle);
    
    kudryakov::CompositeShape cs(rectPtr);
    cs.addShape(circPtr);
    
    BOOST_CHECK_CLOSE(cs[0]->getArea(), 4.0, Epsilon);
    BOOST_CHECK_CLOSE(cs[1]->getArea(), M_PI, Epsilon);
    BOOST_CHECK_THROW(cs[2], std::out_of_range);
  }
  
  BOOST_AUTO_TEST_CASE(Move)
  {
    kudryakov::Rectangle rectangle({-2,0}, 2, 2);
    kudryakov::Circle circle({2,0}, 1);
    
    std::shared_ptr <kudryakov::Shape> rectPtr = std::make_shared <kudryakov::Rectangle> (rectangle);
    std::shared_ptr <kudryakov::Shape> circPtr = std::make_shared <kudryakov::Circle> (circle);
    
    kudryakov::CompositeShape cs(rectPtr);
    cs.addShape(circPtr);
    
    cs.move(1, 1);
    BOOST_CHECK_CLOSE(-1, cs[0]->getFrameRect().pos.x, Epsilon);
    BOOST_CHECK_CLOSE(1, cs[0]->getFrameRect().pos.y, Epsilon);
    BOOST_CHECK_CLOSE(3, cs[1]->getFrameRect().pos.x, Epsilon);
    BOOST_CHECK_CLOSE(1, cs[1]->getFrameRect().pos.y, Epsilon);
    
    BOOST_CHECK_CLOSE(2, cs[0]->getFrameRect().width, Epsilon);
    BOOST_CHECK_CLOSE(2, cs[0]->getFrameRect().height, Epsilon);
    BOOST_CHECK_CLOSE(4, cs[0]->getArea(), Epsilon);
    BOOST_CHECK_CLOSE(2, cs[1]->getFrameRect().width, Epsilon);
    BOOST_CHECK_CLOSE(2, cs[1]->getFrameRect().height, Epsilon);
    BOOST_CHECK_CLOSE(M_PI, cs[1]->getArea(), Epsilon);
    
    cs.move({0, 0});
    BOOST_CHECK_CLOSE(-2, cs[0]->getFrameRect().pos.x, Epsilon);
    BOOST_CHECK_CLOSE(0, cs[0]->getFrameRect().pos.y, Epsilon);
    BOOST_CHECK_CLOSE(2, cs[1]->getFrameRect().pos.x, Epsilon);
    BOOST_CHECK_CLOSE(0, cs[1]->getFrameRect().pos.y, Epsilon);
    
    BOOST_CHECK_CLOSE(2, cs[0]->getFrameRect().width, Epsilon);
    BOOST_CHECK_CLOSE(2, cs[0]->getFrameRect().height, Epsilon);
    BOOST_CHECK_CLOSE(4, cs[0]->getArea(), Epsilon);
    BOOST_CHECK_CLOSE(2, cs[1]->getFrameRect().width, Epsilon);
    BOOST_CHECK_CLOSE(2, cs[1]->getFrameRect().height, Epsilon);
    BOOST_CHECK_CLOSE(M_PI, cs[1]->getArea(), Epsilon);
  }
  
  BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
  {
    kudryakov::Rectangle rectangle({-2,0}, 2, 2);
    kudryakov::Circle circle({2,0}, 1);
    
    std::shared_ptr <kudryakov::Shape> rectPtr = std::make_shared <kudryakov::Rectangle> (rectangle);
    std::shared_ptr <kudryakov::Shape> circPtr = std::make_shared <kudryakov::Circle> (circle);
    
    kudryakov::CompositeShape cs(rectPtr);
    cs.addShape(circPtr);
    
    BOOST_CHECK_THROW(cs.scale(-2), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE(Scale)
  {
    kudryakov::Rectangle rectangle({-2,0}, 2, 2);
    kudryakov::Circle circle({2,0}, 1);
    
    std::shared_ptr <kudryakov::Shape> rectPtr = std::make_shared <kudryakov::Rectangle> (rectangle);
    std::shared_ptr <kudryakov::Shape> circPtr = std::make_shared <kudryakov::Circle> (circle);
    
    kudryakov::CompositeShape cs(rectPtr);
    cs.addShape(circPtr);
    
    double rectInitArea = rectangle.getArea();
    double circInitArea = circle.getArea();
    kudryakov::rectangle_t compositeShapeInitRectangle = cs.getFrameRect();
    
    cs.scale(2);
    BOOST_CHECK_CLOSE(rectInitArea*4, cs[0]->getArea(), Epsilon);
    BOOST_CHECK_CLOSE(circInitArea*4, cs[1]->getArea(), Epsilon);
    BOOST_CHECK_CLOSE(compositeShapeInitRectangle.height*2, cs.getFrameRect().height, Epsilon);
    BOOST_CHECK_CLOSE(compositeShapeInitRectangle.width*2, cs.getFrameRect().width, Epsilon);
    BOOST_CHECK_CLOSE(compositeShapeInitRectangle.pos.x, cs.getFrameRect().pos.x, Epsilon);
    BOOST_CHECK_CLOSE(compositeShapeInitRectangle.pos.y, cs.getFrameRect().pos.y, Epsilon);
  }

BOOST_AUTO_TEST_SUITE_END()
