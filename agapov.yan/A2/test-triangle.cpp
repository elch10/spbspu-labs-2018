#include <boost/test/unit_test.hpp>
#include "triangle.hpp"
#include <stdexcept>

using namespace agapov;

BOOST_AUTO_TEST_SUITE(Triang)

BOOST_AUTO_TEST_CASE(triangle_area)
{
  Triangle triang({2.0,2.0},{3.0,6.0},{4.0,1.0});
  BOOST_CHECK_EQUAL(triang.getArea(), 4.5);
}

BOOST_AUTO_TEST_CASE(triangle_area_minus)
{
  Triangle triang({-2.0,-2.0},{-3.0,-6.0},{-4.0,-1.0});
  BOOST_CHECK_EQUAL(triang.getArea(), 4.5);
}

BOOST_AUTO_TEST_CASE(triangle_rectangle)
{
  Triangle triang({0.0,0.0},{0.0,4.0},{4.0,0.0});
  rectangle_t chek = triang.getFrameRect();
  BOOST_CHECK_CLOSE(chek.height, 4.0 , 0.1 );
  BOOST_CHECK_CLOSE(chek.width, 4.0, 0.1 );
  BOOST_CHECK_CLOSE(chek.pos.x, 2.0, 0.1 );
  BOOST_CHECK_CLOSE(chek.pos.y, 2.0, 0.1 );
}

BOOST_AUTO_TEST_CASE(triangle_rectangle_new_A_B_C)
{
  Triangle triang({2.0,2.0},{3.0,6.0},{4.0,1.0});
  rectangle_t chek = triang.getFrameRect();
  BOOST_CHECK_CLOSE(chek.height, 5.0 , 0.1 );
  BOOST_CHECK_CLOSE(chek.width, 2.0, 0.1 );
  BOOST_CHECK_CLOSE(chek.pos.x, 3.0, 0.1 );
  BOOST_CHECK_CLOSE(chek.pos.y, 3.5, 0.1 );
}

BOOST_AUTO_TEST_CASE(triangle_rectangle_new_A_B_C_2)
{
  Triangle triang({2.0,2.0},{3.0,6.0},{3.0,1.0});
  rectangle_t chek = triang.getFrameRect();
  BOOST_CHECK_CLOSE(chek.height, 5.0, 0.1 );
  BOOST_CHECK_CLOSE(chek.width, 1.0, 0.1 );
  BOOST_CHECK_CLOSE(chek.pos.x, 2.5, 0.1 );
  BOOST_CHECK_CLOSE(chek.pos.y, 3.5, 0.1 );
}

BOOST_AUTO_TEST_CASE(triangle_move_dx)
{
  Triangle triangle({2.0,2.0},{3.0,6.0},{4.0,1.0});
  triangle.move(5.0,0.0);
  point_t checkA = triangle.getA();
  point_t checkB = triangle.getB();
  point_t checkC = triangle.getC();
  BOOST_CHECK_CLOSE(checkA.x, 7.0 , 0.01);
  BOOST_CHECK_CLOSE(checkB.x, 8.0 , 0.01);
  BOOST_CHECK_CLOSE(checkC.x, 9.0 , 0.01);
  BOOST_CHECK_CLOSE(checkA.y, 2.0 , 0.01);
  BOOST_CHECK_CLOSE(checkB.y, 6.0 , 0.01);
  BOOST_CHECK_CLOSE(checkC.y, 1.0 , 0.01);
}

BOOST_AUTO_TEST_CASE(triangle_move_dy)
{
  Triangle triangle({2.0,2.0},{3.0,6.0},{4.0,1.0});
  triangle.move(0.0,5.0);
  point_t checkA = triangle.getA();
  point_t checkB = triangle.getB();
  point_t checkC = triangle.getC();
  BOOST_CHECK_CLOSE(checkA.x, 2.0, 0.01);
  BOOST_CHECK_CLOSE(checkB.x, 3.0, 0.01);
  BOOST_CHECK_CLOSE(checkC.x, 4.0, 0.01);
  BOOST_CHECK_CLOSE(checkA.y, 7.0, 0.01);
  BOOST_CHECK_CLOSE(checkB.y, 11.0, 0.01);
  BOOST_CHECK_CLOSE(checkC.y, 6.0, 0.01);
}

BOOST_AUTO_TEST_CASE(triangle_move_dx_and_dy)
{
  Triangle triangle({2.0,2.0},{3.0,6.0},{4.0,1.0});
  triangle.move(6.0,6.0);
  point_t checkA = triangle.getA();
  point_t checkB = triangle.getB();
  point_t checkC = triangle.getC();
  BOOST_CHECK_CLOSE(checkA.x, 8.0, 0.01);
  BOOST_CHECK_CLOSE(checkB.x, 9.0, 0.01);
  BOOST_CHECK_CLOSE(checkC.x, 10.0, 0.01);
  BOOST_CHECK_CLOSE(checkA.y, 8.0, 0.01);
  BOOST_CHECK_CLOSE(checkB.y, 12.0, 0.01);
  BOOST_CHECK_CLOSE(checkC.y, 7.0, 0.01);
}

BOOST_AUTO_TEST_CASE(triangle_move_pos)
{
  Triangle triangle({2.0,2.0},{3.0,6.0},{4.0,1.0});
  triangle.move({6.0,6.0});
  point_t checkA = triangle.getA();
  point_t checkB = triangle.getB();
  point_t checkC = triangle.getC();
  BOOST_CHECK_CLOSE(checkA.x, 5.0, 0.1);
  BOOST_CHECK_CLOSE(checkB.x, 6.0, 0.1);
  BOOST_CHECK_CLOSE(checkC.x, 7.0, 0.1);
  BOOST_CHECK_CLOSE(checkA.y, 5.0, 0.1);
  BOOST_CHECK_CLOSE(checkB.y, 9.0, 0.1);
  BOOST_CHECK_CLOSE(checkC.y, 4.0, 0.1);
}

BOOST_AUTO_TEST_CASE(triangle_scale)
{
  Triangle triangle({5.0,0.0},{-5.0,0.0},{0.0,5.0});
  double checkArea = triangle.getArea();
  double coefficient = 2.0;
  triangle.scale(coefficient);
  BOOST_CHECK_CLOSE(coefficient*coefficient*checkArea, triangle.getArea(), 0.01);
}

BOOST_AUTO_TEST_CASE(triangle_scale_2)
{
  Triangle triangle({4.0,0.0},{-4.0,0.0},{0.0,4.0});
  double checkArea = triangle.getArea();
  double coefficient = 0.5;
  triangle.scale(coefficient);
  BOOST_CHECK_CLOSE(coefficient*coefficient*checkArea, triangle.getArea(), 0.01);
}

BOOST_AUTO_TEST_SUITE_END()
