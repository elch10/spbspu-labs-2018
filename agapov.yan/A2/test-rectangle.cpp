#include <boost/test/unit_test.hpp>
#include "rectangle.hpp"
#include <stdexcept>

using namespace agapov;

BOOST_AUTO_TEST_SUITE(Rectang)

BOOST_AUTO_TEST_CASE(rectangle_initialization_width)
{
  BOOST_CHECK_THROW(Rectangle rectang(-60.0,70.0,{0.0,0.0}), std::invalid_argument );
}

BOOST_AUTO_TEST_CASE(recctangle_initialization_height)
{
  BOOST_CHECK_THROW(Rectangle rectang(60.0,-70.0,{0.0,0.0}), std::invalid_argument );
}

BOOST_AUTO_TEST_CASE(rectangle_area)
{
  Rectangle rectang(60.0,70.0,{0.0,0.0});
  BOOST_CHECK_EQUAL(rectang.getArea(), 4200.0);
}

BOOST_AUTO_TEST_CASE(rectangle_area_new_dot)
{
  Rectangle rectang(60.0,70.0,{1.0,1.0});
  BOOST_CHECK_EQUAL(rectang.getArea(), 4200.0);
}

BOOST_AUTO_TEST_CASE(rectangle_area_swap_width_and_height)
{
  Rectangle rectang(70.0,60.0,{0.0,0.0});
  BOOST_CHECK_EQUAL(rectang.getArea(), 4200.0);
}

BOOST_AUTO_TEST_CASE(rectangle_rectangle)
{
  Rectangle rectang(70.0,60.0,{0.0,0.0});
  rectangle_t chek = rectang.getFrameRect();
  BOOST_CHECK_CLOSE(chek.height, 70.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.width, 60.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.x, 0.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.y, 0.0, 0.01 );
}

BOOST_AUTO_TEST_CASE(rectangle_rectangle_new_center)
{
  Rectangle rectang(70.0,60.0,{1.0,1.0});
  rectangle_t chek = rectang.getFrameRect();
  BOOST_CHECK_CLOSE(chek.height, 70.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.width, 60.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.x, 1.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.y, 1.0, 0.01 );
}

BOOST_AUTO_TEST_CASE(rectangle_rectangle_new_height)
{
  Rectangle rectang(80.0,60.0,{1.0,1.0});
  rectangle_t chek = rectang.getFrameRect();
  BOOST_CHECK_CLOSE(chek.height, 80.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.width, 60.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.x, 1.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.y, 1.0, 0.01 );
}

BOOST_AUTO_TEST_CASE(rectangle_rectangle_new_width)
{
  Rectangle rectang(70.0,90.0,{1.0,1.0});
  rectangle_t chek = rectang.getFrameRect();
  BOOST_CHECK_CLOSE(chek.height, 70.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.width, 90.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.x, 1.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.y, 1.0, 0.01 );
}

BOOST_AUTO_TEST_CASE(rectangle_move_dx)
{
  Rectangle rectangle(60.0,70.0,{1.0,1.0});
  rectangle.move(5.0,0.0);
  rectangle_t check;
  check.width = rectangle.getWidth();
  check.height = rectangle.getHeight();
  check.pos = rectangle.getCenter();
  BOOST_CHECK_CLOSE(check.width, 70.0, 0.01);
  BOOST_CHECK_CLOSE(check.height, 60.0, 0.01);
  BOOST_CHECK_CLOSE(check.pos.x, 6.0, 0.01);
}

BOOST_AUTO_TEST_CASE(rectangle_move_dy)
{
  Rectangle rectangle(60.0,70.0,{1.0,1.0});
  rectangle.move(0.0,5.0);
  rectangle_t check;
  check.width = rectangle.getWidth();
  check.height = rectangle.getHeight();
  check.pos = rectangle.getCenter();
  BOOST_CHECK_CLOSE(check.width, 70.0, 0.01);
  BOOST_CHECK_CLOSE(check.height, 60.0, 0.01);
  BOOST_CHECK_CLOSE(check.pos.y, 6.0, 0.01);
}

BOOST_AUTO_TEST_CASE(rectangle_move_dx_and_dy)
{
  Rectangle rectangle(60.0,70.0,{1,1});
  rectangle.move(5.0,5.0);
  rectangle_t check;
  check.width = rectangle.getWidth();
  check.height = rectangle.getHeight();
  check.pos = rectangle.getCenter();
  BOOST_CHECK_CLOSE(check.width, 70.0, 0.01);
  BOOST_CHECK_CLOSE(check.height, 60.0, 0.01);
  BOOST_CHECK_CLOSE(check.pos.y,6.0 , 0.01);
  BOOST_CHECK_CLOSE(check.pos.x,6.0 , 0.01);
}

BOOST_AUTO_TEST_CASE(rectangle_move_center)
{
  Rectangle rectangle(60.0,70.0,{1.0,1.0});
  rectangle.move({5.0,5.0});
  rectangle_t check;
  check.width = rectangle.getWidth();
  check.height = rectangle.getHeight();
  check.pos = rectangle.getCenter();
  BOOST_CHECK_CLOSE(check.width, 70.0, 0.01);
  BOOST_CHECK_CLOSE(check.height, 60.0, 0.01);
  BOOST_CHECK_CLOSE(check.pos.y, 5.0 , 0.01);
  BOOST_CHECK_CLOSE(check.pos.x, 5.0 , 0.01);
}

BOOST_AUTO_TEST_CASE(rectangle_scale)
{
  Rectangle rectangle(60.0,70.0,{0.0,0.0});
  double checkArea = rectangle.getArea();
  double coefficient = 0.5;
  bool condition = false;
  rectangle.scale(coefficient);
  if ((rectangle.getArea()/checkArea) == coefficient*coefficient)
  {
    condition = true;
  }
  BOOST_CHECK(condition);
}

BOOST_AUTO_TEST_CASE(rectangle_scale_2)
{
  Rectangle rectangle(60.0,70.0,{0,0});
  double checkArea = rectangle.getArea();
  double coefficient = 6.0;
  bool condition = false;
  rectangle.scale(coefficient);
  if ((rectangle.getArea()/checkArea) == coefficient*coefficient)
  {
    condition = true;
  }
  BOOST_CHECK(condition);
}

BOOST_AUTO_TEST_SUITE_END()
