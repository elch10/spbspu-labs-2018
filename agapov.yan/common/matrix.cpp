#include "matrix.hpp"
#include <memory>
#include <stdexcept>

using namespace agapov;

Matrix::Matrix(Shape *shape):
  rows_(1),
  columns_(1),
  matrix_(new Shape*[1])
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }

  matrix_[0] = shape;
}

Matrix::Matrix(const Matrix& matrix):
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  matrix_ = std::unique_ptr<Shape*[]>(new Shape*[rows_ * columns_]);
  for (size_t i = 0; i < rows_ * columns_; i++)
  {
    matrix_[i] = matrix.matrix_[i];
  }
}

Matrix::Matrix(Matrix&& matrix):
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  matrix_ = std::unique_ptr<Shape*[]>(new Shape*[rows_ * columns_]);
  for (size_t i = 0; i < rows_ * columns_; i++)
  {
    matrix_[i] = nullptr;
  }
  matrix_.swap(matrix.matrix_);
  matrix.rows_ = 0;
  matrix.columns_ = 0;
}

size_t Matrix::getRows() const
{
  return rows_;
}

size_t Matrix::getColumns() const
{
  return columns_;
}

void Matrix::addElem(Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }

  size_t i = rows_ * columns_;
  size_t desired_row = 1;

  while (i > 0)
  {
    i--;

    if (shapesIntersect(matrix_[i], shape))
    {
      desired_row = i / columns_ + 2;
    }
  }

  size_t rows_temp = rows_;
  size_t columns_temp = columns_;
  size_t free_columns = 0;

  if (desired_row > rows_)
  {
    rows_temp++;
    free_columns = columns_;
  }
  else
  {
    size_t j = (desired_row - 1) * columns_;

    while (j < (desired_row * columns_))
    {
      if (matrix_[j] == nullptr)
      {
        free_columns++;
      }

      j++;
    }
    
    if (free_columns == 0)
    {
      columns_temp++;
      free_columns = 1;
    }
  }

  std::unique_ptr<Shape*[]> matrix_temp(new Shape*[rows_temp * columns_temp]);
  
  for (size_t i = 0; i < rows_temp; i++)
  {
    for (size_t j = 0; j < columns_temp; j++)
    {
      if (i >= rows_ || j >= columns_)
      {
        matrix_temp[i * columns_temp + j] = nullptr;
        continue;
      }

      matrix_temp[i * columns_temp + j] = matrix_[i * columns_ + j];
    }
  }

  matrix_temp[desired_row * columns_temp - free_columns] = shape;
  
  matrix_.swap(matrix_temp);
  rows_ = rows_temp;
  columns_ = columns_temp;
}

Shape& Matrix::getElem(size_t columnm, size_t line)
{
  if((line >= rows_) || (columnm >=columns_))
  {
    throw std::out_of_range("Indx out of range");
  }
  return  *(matrix_[line * columns_ + columnm]);
}

bool Matrix::shapesIntersect(Shape *shape1, Shape *shape2) const
{
  rectangle_t rectangle1 = shape1->getFrameRect();
  rectangle_t rectangle2 = shape2->getFrameRect();

  point_t point1 = { rectangle1.pos.x - rectangle1.width / 2,
                     rectangle1.pos.y - rectangle1.height / 2 };
  point_t point2 = { rectangle1.pos.x + rectangle1.width / 2,
                     rectangle1.pos.y + rectangle1.height / 2 };

  point_t point3 = { rectangle2.pos.x - rectangle2.width / 2,
                     rectangle2.pos.y - rectangle2.height / 2 };
  point_t point4 = { rectangle2.pos.x + rectangle2.width / 2,
                     rectangle2.pos.y + rectangle2.height / 2 };

  if ((point1.x > point4.x) || (point2.x < point3.x)
  || (point1.y  > point4.y) || (point2.y < point3.y))
  {
    return false;
  }
  else
  {
    return true;
  }
}
