#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

using namespace agapov;

Circle::Circle(const double rad,const point_t &poss):
  radius_(rad),
  center_(poss)
{
  if( rad <= 0.0)
  {
    throw std::invalid_argument("Radius must be more than 0!");
  }
}

double Circle::getArea() const
{
  return M_PI*radius_*radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return { radius_ * 2.0, radius_ * 2.0, center_ };
}

void Circle::move(const point_t &poss)
{
  center_ = poss;
}

void Circle::move(const double Ox,const double Oy)
{
  center_.x += Ox;
  center_.y += Oy;
}

void Circle::info() const
{
  std::cout <<"Radius: " << radius_ <<" Center of Circle: x = " << center_.x <<" y = " << center_.y << std::endl;
}

void Circle::scale(const double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient must to be more than 0!" );
  }
  else
  {
    radius_ *= coefficient;
  }
}

double Circle::getRadius() const
{
  return radius_;
}

point_t Circle::getCenter() const
{
  return center_;
}

void Circle::rotate(const double)
{

}
