#include <boost/test/unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include <stdexcept>

using namespace agapov;

BOOST_AUTO_TEST_SUITE(Shape_)

  BOOST_AUTO_TEST_CASE(move_compositeshape_area)
  {
    Circle circ(1, {0.0, 0.0});
    Triangle triang({5.0, 5.0}, {0.0, 0.0}, {4.0, 4.0});
    Shape *circle = &circ;
    Shape *triangle = &triang;
    CompositeShape cs(triangle);
    cs.addShape(circle);
    double before_move = cs.getArea();
    cs.move(5, 5);
    BOOST_CHECK_CLOSE(cs.getArea(), before_move, 1.0);
  }

  BOOST_AUTO_TEST_CASE(move_compositeshape_area_2)
  {
    Circle circ(1.0, {0.0, 0.0});
    Triangle triang({5.0, 5.0}, {0.0, 0.0}, {4.0, 4.0});
    Shape *circle = &circ;
    Shape *triangle = &triang;
    CompositeShape cs(triangle);
    cs.addShape(circle);
    double before_move = cs.getArea();
    cs.move(-5.0, -5.0);
    BOOST_CHECK_CLOSE(cs.getArea(), before_move, 1.0);
  }

  BOOST_AUTO_TEST_CASE(compositeshape_scale)
  {
    Circle circ(1.0, {0.0, 0.0});
    Triangle triang({5.0, 5.0}, {0.0, 0.0}, {4.0, 4.0});
    Shape *circle = &circ;
    Shape *triangle = &triang;
    CompositeShape cs(triangle);
    cs.addShape(circle);
    double before_move = cs.getArea();
    cs.scale(2.0);
    double after_move = cs.getArea();
    BOOST_CHECK_CLOSE(4.0 * before_move, after_move, 1.0);
  }

  BOOST_AUTO_TEST_CASE(compositeshape_scale_2)
  {
    Circle circ(1, {0.0, 0.0});
    Triangle triang({5.0, 5.0}, {0.0, 0.0}, {4.0, 4.0});
    Shape *circle = &circ;
    Shape *triangle = &triang;
    CompositeShape cs(triangle);
    cs.addShape(circle);
    double before_move = cs.getArea();
    cs.scale(0.5);
    double after_move = cs.getArea();
    BOOST_CHECK_CLOSE(0.25 * before_move, after_move, 1.0);
  }

  BOOST_AUTO_TEST_CASE(compositeshape_rectangle)
  {
    Circle circ(1.0, {0.0, 0.0});
    Triangle triang({5.0, 5.0}, {0.0, 0.0}, {4.0, 4.0});
    Shape *circle = &circ;
    Shape *triangle = &triang;
    CompositeShape cs(triangle);
    cs.addShape(circle);
    rectangle_t rectang = cs.getFrameRect();
    BOOST_CHECK_CLOSE(rectang.height, 6.0, 1.0);
    BOOST_CHECK_CLOSE(rectang.width, 6.0, 1.0);
    BOOST_CHECK_CLOSE(rectang.pos.x, 2.0, 1.0);
    BOOST_CHECK_CLOSE(rectang.pos.y, 2.0, 1.0);
  }

  BOOST_AUTO_TEST_CASE(compositeshape_rectangle_2)
  {
    Circle circ(1.0, {0.0, 6.0});
    Triangle triang({5.0, 5.0}, {3.0, 0.0}, {4.0, 4.0});
    Shape *circle = &circ;
    Shape *triangle = &triang;
    CompositeShape cs(triangle);
    cs.addShape(circle);
    rectangle_t rectang = cs.getFrameRect();
    BOOST_CHECK_CLOSE(rectang.height, 7.0, 1.0);
    BOOST_CHECK_CLOSE(rectang.width, 6.0, 1.0);
    BOOST_CHECK_CLOSE(rectang.pos.x, 2.0, 1.0);
    BOOST_CHECK_CLOSE(rectang.pos.y, 3.5, 1.0);
  }

BOOST_AUTO_TEST_SUITE_END()
