#include "composite-shape.hpp"
#include <stdexcept>
#include <math.h>

using namespace orlov;

orlov::CompositeShape::CompositeShape(const std::shared_ptr<Shape> newshape):
  count_(0),
  mass_(nullptr)
{
  if (newshape == nullptr)
  {
    throw std::invalid_argument("Invalid shape");
  }

  addShape(newshape);
}

orlov::CompositeShape::CompositeShape():
  count_(0),
  mass_(nullptr)
{}

void orlov::CompositeShape::addShape(const std::shared_ptr<Shape> newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Invalid shape");
  }

  std::unique_ptr<std::shared_ptr <Shape> []> tempArr(new std::shared_ptr<Shape>[count_ + 1]);

  for (int i = 0; i < count_; i++)
  {
    tempArr[i] = mass_[i];
  }

  tempArr[count_] = newShape;
  mass_.swap(tempArr);
  count_++;
}

void orlov::CompositeShape::removeShape(const int index)
{
  if (index > count_ -1 || index < 0 || count_ == 1)
  {
    throw std::invalid_argument("Incorrect index");
  }

  count_ --;
  std::unique_ptr<std::shared_ptr<Shape> []> temp (new std::shared_ptr<Shape> [count_]);

  int i = 0;
  for (int a = 0; a < count_ + 1; a++)
  {
    if (a != index)
    {
      temp[i] = mass_[a];
      i++;
    }
  }

  mass_.swap(temp);
}

point_t orlov::CompositeShape::getPos() noexcept
{
  return getFrameRect().pos;
}

void orlov::CompositeShape::move(const double dx, const double dy) noexcept
{
  for (int i = 0; i < count_; i++)
  {
    mass_[i]->move(dx, dy);
  }
}

void orlov::CompositeShape::move(const point_t & point) noexcept
{
  double dx, dy;

  dx = point.x - getPos().x;
  dy = point.y - getPos().y;

  for (int i = 0; i < count_; i++)
  {
    mass_[i]->move(dx, dy);
  }
}

rectangle_t orlov::CompositeShape::getFrameRect() const noexcept
{
  double maxX = 0, minX = 0, maxY = 0, minY = 0;

  for (int i = 0; i < count_; i++)
  {
    double dx = mass_[i]->getFrameRect().pos.x + (mass_[i]->getFrameRect().width / 2);
    if (maxX < dx)
    {
      maxX = dx;
    }

    dx = mass_[i]->getFrameRect().pos.x - (mass_[i]->getFrameRect().width / 2);
    if (minX > dx)
    {
      minX = dx;
    }

    double dy = mass_[i]->getFrameRect().pos.y + (mass_[i]->getFrameRect().height / 2);
    if (maxY < dy)
    {
      maxY = dy;
    }

    dy = mass_[i]->getFrameRect().pos.y - (mass_[i]->getFrameRect().height / 2);
    if (minY > dy)
    {
      minY = dy;
    }
  }

  double width = maxX - minX;
  double height = maxY - minY;

  return rectangle_t {width, height, {(minX + (width / 2)), (minY + (height / 2))}};
}

double orlov::CompositeShape::getArea() const noexcept
{
  double temp = 0.0;

  for (int i = 0; i < count_; i++)
  {
    temp += mass_[i]->getArea();
  }

  return temp;
}

void orlov::CompositeShape::scale(const double factor)
{
  point_t temp;

  if (factor <= 0.0)
  {
    throw std::invalid_argument("Factor must be > 0");
  }

  for (int i = 0; i < count_; i++)
  {
    mass_[i]->scale(factor);

    temp.x = getPos().x + ((mass_[i]->getPos().x - getPos().x) * factor);
    temp.y = getPos().y + ((mass_[i]->getPos().y - getPos().y) * factor);

    mass_[i]->move(temp);
  }
}

void orlov::CompositeShape::rotate(const double angle)
{
  point_t centerComp = getFrameRect().pos;
  double tmpAngle = (angle * M_PI) / 180.0;
  for (int i = 0; i < count_; i++)
  {
    point_t centerCurrentShape = mass_[i]->getPos();
    double dx = (centerCurrentShape.x - centerComp.x) * cos(tmpAngle) - (centerCurrentShape.y - centerComp.y) * sin(tmpAngle);
    double dy = (centerCurrentShape.x - centerComp.x) * sin(tmpAngle) + (centerCurrentShape.y - centerComp.y) * cos(tmpAngle);
    mass_[i]->rotate(angle);
    mass_[i]->move({ centerComp.x + dx, centerComp.y + dy });
  }
}

int orlov::CompositeShape::getCount()
{
  return count_;
}
