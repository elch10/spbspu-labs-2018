#include <iostream>
#include <memory>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace anikin;

void showInfo(const Shape &shape)
{
  std::cout << "Frame's width: ";
  std::cout << shape.getFrameRect().width << std::endl;
  std::cout << "Frame's height: ";
  std::cout << shape.getFrameRect().height << std::endl;
  std::cout << "Frame's center (" << shape.getFrameRect().pos.x << " , "
  << shape.getFrameRect().pos.y << ")" << std::endl;
  std::cout << "Area: ";
  std::cout << shape.getArea() << std::endl << std::endl;
  std::cout << "Angle: ";
  std::cout << shape.getAngle() << std::endl << std::endl << std::endl;
 }

int main()
{
  try{
    Circle circle(30.0, {20.0, 2.0});
    Rectangle rectangle(40.0, 20.0, {60.0, 4.0});

    std::cout << "Circle: " << std::endl;
    showInfo(circle);

    std::cout << "Rectangle: " << std::endl;
    showInfo(rectangle);

    std::cout << "Rotate circle 90 degrees" << std::endl;
    circle.rotate(90.0);
    showInfo(circle);

    std::cout << "Rotate rectangle 45 degrees" << std::endl;
    rectangle.rotate(45.0);
    showInfo(rectangle);

    std::cout << "Rotate rectangle 45 degrees" << std::endl;
    rectangle.rotate(45.0);
    showInfo(rectangle);

    std::cout << "Rotate rectangle 45 degrees" << std::endl;
    rectangle.rotate(45.0);
    showInfo(rectangle);

    std::cout << "Rotate rectangle 45 degrees" << std::endl;
    rectangle.rotate(45.0);
    showInfo(rectangle);

    std::cout << "Composite shape:" << std::endl << std::endl;

    Rectangle compositeRectangle(80.0, 70.0, {200.0, 150.0});
    Circle compositeCircle(30.0, {100.0, 70.0});
    std::shared_ptr <Shape>pCompRectangle = std::make_shared <Rectangle>(compositeRectangle);
    std::shared_ptr <Shape>pCompCircle = std::make_shared <Circle>(compositeCircle);
    CompositeShape compositeShape(pCompRectangle);

    std::cout << "First shape (Rectangle): " << std::endl;
    showInfo(compositeRectangle);

    std::cout << "Second shape (Circle): " << std::endl;
    showInfo(compositeCircle);

    std::cout << "Composite shape with rectangle:" << std::endl << std::endl;
    showInfo(compositeShape);

    std::cout << "Circle shape was added:" << std::endl << std::endl;
    compositeShape.addShape(pCompCircle);
    showInfo(compositeShape);

    std::cout << "Rotate 45 degrees" << std::endl << std::endl;
    compositeShape.rotate(45.0);
    showInfo(compositeShape);

    std::cout << "Rotate 45 degrees" << std::endl << std::endl;
    compositeShape.rotate(45.0);
    showInfo(compositeShape);

    Circle matrixCircle{2.0, {0.0, 0.0}};
    Rectangle matrixRectangle{2.0, 2.0, {1.0, 0.0}};
    Rectangle matrixRectangle2{4.0, 4.0, {-3.0, 0.0}};
    Circle matrixCircle2{5.0, {3.0, 2.0}};

    std::shared_ptr<Shape> pMatrixCircle = std::make_shared<Circle>(matrixCircle);
    std::shared_ptr<Shape> pMatrixRectangle = std::make_shared<Rectangle>(matrixRectangle);
    std::shared_ptr<Shape> pMatrixRectangle2 = std::make_shared<Rectangle>(matrixRectangle2);
    std::shared_ptr<Shape> pMatrixCircle2 = std::make_shared<Circle>(matrixCircle2);

    Matrix matrix(pMatrixCircle);
    matrix.addShape(pMatrixRectangle);
    matrix.addShape(pMatrixRectangle2);
    matrix.addShape(pMatrixCircle2);

    std::cout << "Matrix:" << std::endl << std::endl;
    std::unique_ptr<std::shared_ptr<Shape>[]>layer1 = matrix[0];
    std::unique_ptr<std::shared_ptr<Shape>[]>layer2 = matrix[1];
    std::unique_ptr<std::shared_ptr<Shape>[]>layer3 = matrix[2];

    std::cout << "Circle:" << std::endl;
    showInfo(*layer1[0]);
    std::cout << "Rectangle:" << std::endl;
    showInfo(*layer2[0]);
    std::cout << "Rectangle2:" << std::endl;
    showInfo(*layer2[1]);
    std::cout << "Circle2:" << std::endl;
    showInfo(*layer3[0]);
  } catch(std::invalid_argument & e) {
    std::cerr << e.what() << std::endl;
  } catch(std::out_of_range & e) {
    std::cerr << e.what() << std::endl;
  } catch(...) {
    std::cerr << "Exception" << std::endl;
  }

  return 0;
}
