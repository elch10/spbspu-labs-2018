#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(const double &r, const point_t &p);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &placement) override;
  void move(const double x, const double y) override;
private:
  double radius_;
  point_t center_;
};

#endif
