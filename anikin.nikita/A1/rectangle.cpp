#include "rectangle.hpp"
#include <iostream>

Rectangle::Rectangle(const double &w, const double &h, const point_t &p) : width_(w), height_(h), center_(p)
{
  if (w <= 0)
  {
    std::cerr << "Invalid rectangle width" << std::endl;
  }
  if (h <= 0) 
  {
    std::cerr << "Invalid rectangle height" << std::endl;
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {width_, height_, center_};
}

void Rectangle::move(const point_t &placement)
{
  center_ = placement;
}

void Rectangle::move(const double x, const double y)
{
  center_.x += x;
  center_.y += y;
}
