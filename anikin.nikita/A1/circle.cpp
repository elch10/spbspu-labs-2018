#include <cmath>
#include "circle.hpp"
#include <iostream>

Circle::Circle(const double &r, const point_t &p) : radius_(r), center_(p)
{
  if (radius_ <= 0)
  {
    std::cerr << "Invalid radius" << std::endl;
  }
}

double Circle::getArea() const
{
  return M_PI * pow(radius_, 2);
}

rectangle_t Circle::getFrameRect() const
{
  return {2 * radius_, 2 * radius_, center_};
}

void Circle::move(const point_t &placement)
{
  center_ = placement;
}

void Circle::move(const double x, const double y)
{
  center_.x += x;
  center_.y += y;
}
