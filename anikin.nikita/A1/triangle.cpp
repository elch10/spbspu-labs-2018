#include "triangle.hpp"
#include <iostream>
#include <cmath>

const double delta = 0.00001;

Triangle::Triangle(const point_t &a, const point_t &b, const point_t &c) :
pA_(a), pB_(b), pC_(c), center_({(a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3})
{
  if (getArea() <= delta)
  {
    std::cerr << "Invalid points" << std::endl;
  }
}

double Triangle::getMin(const double &pA_, const double &pB_, const double &pC_) const
{
  double min = pA_;
  if (pB_ < min)
  {
    min = pB_;
  }
  if (pC_ < min)
  {
    min = pC_;
  }
  return min;
}

double Triangle::getMax(const double &pA_, const double &pB_, const double &pC_) const
{
  double max = pA_;
  if (pB_ > max)
  {
    max = pB_;
  }
  if (pC_ > max)
  {
    max = pC_;
  }
  return max;
}

double Triangle::getArea() const
{
  return ((std::abs(((pA_.x - pC_.x) * (pB_.y - pC_.y)) - ((pA_.y - pC_.y) * (pB_.x - pC_.x))) / 2));
}

rectangle_t Triangle::getFrameRect() const
{
  double max_x=getMax(pA_.x, pB_.x, pC_.x);
  double min_x=getMin(pA_.x, pB_.x, pC_.x);
  double max_y=getMax(pA_.y, pB_.y, pC_.y);
  double min_y=getMin(pA_.y, pB_.y, pC_.y);
  return rectangle_t{max_x - min_x, max_y - min_y, point_t {min_x + (max_x - min_x) / 2,min_y + (max_y - min_y) / 2}};
}

void Triangle::move(const point_t &placement)
{
  pA_.x += placement.x - center_.x;
  pB_.x += placement.x - center_.x;
  pC_.x += placement.x - center_.x;
  pA_.y += placement.y - center_.y;
  pB_.y += placement.y - center_.y;
  pC_.y += placement.y - center_.y;
  center_ = placement;
}

void Triangle::move(const double x, const double y)
{
  center_.x += x;
  pA_.x += x;
  pB_.x += x;
  pC_.x += x;
  center_.y += y;
  pA_.y += y;
  pB_.y += y;
  pC_.y += y;
}
