#define _USE_MATH_DEFINES 
#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <memory>
#include <cmath>
#include <algorithm>

using namespace anikin;


CompositeShape::CompositeShape(const std::shared_ptr <Shape>shape) :
  shapes_(nullptr),
  size_(0),
  angle_(0.0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Object can't be nullptr!");
  }
  addShape(shape);
}

CompositeShape::CompositeShape(const CompositeShape & compositeShape):
  shapes_(new std::shared_ptr <Shape>[compositeShape.size_]),
  size_(compositeShape.size_),
  angle_(compositeShape.angle_)
{
  for (int i =0; i < size_; ++i)
  {
    shapes_[i] = compositeShape.shapes_[i];
  }
}

CompositeShape::CompositeShape(CompositeShape && compositeShape):
  shapes_(nullptr),
  size_(compositeShape.size_),
  angle_(compositeShape.angle_)
{
  shapes_.swap(compositeShape.shapes_);
  compositeShape.deleteAllShapes();
}

CompositeShape::~CompositeShape()
{
  deleteAllShapes();
}

CompositeShape & CompositeShape::operator = (const CompositeShape & compositeShape)
{
  if (this != &compositeShape)
  {
    size_ = compositeShape.size_;
    angle_ = compositeShape.angle_;
    std::unique_ptr<std::shared_ptr<Shape>[]> newShapes(new std::shared_ptr <Shape> [compositeShape.size_]);
    for (int i = 0; i < size_; ++i)
    {
      newShapes[i] = compositeShape.shapes_[i];
    }
    shapes_.swap(newShapes);
  }
  return *this;
}

CompositeShape & CompositeShape::operator = (CompositeShape && compositeShape)
{
  if (this != &compositeShape)
  {
    size_ = compositeShape.size_;
    angle_ = compositeShape.angle_;
    shapes_.swap(compositeShape.shapes_);
    compositeShape.deleteAllShapes();
  }
  return *this;
}

bool CompositeShape::operator==(const CompositeShape & compositeShape) const
{
  if ((this->size_ != compositeShape.size_) || (this->angle_ != compositeShape.angle_))
  {
    return false;
  }
  for (int i = 0; i < size_; ++i)
  {
    if (this->shapes_[i] != compositeShape.shapes_[i])
    {
      return false;
    }
  }
  return true;
}

bool CompositeShape::operator!=(const CompositeShape & compositeShape) const
{
  if ((this->size_ != compositeShape.size_) || (this->angle_ != compositeShape.angle_))
  {
    return true;
  }
  for (int i = 0; i < size_; ++i)
  {
    if (this->shapes_[i] != compositeShape.shapes_[i])
    {
      return true;
    }
  }
  return false;
}

rectangle_t CompositeShape::getFrameRect() const noexcept
{
  if (size_ == 0)
  {
    return{0, 0, {0, 0}};
  }
  else
  {
    rectangle_t frameRect = shapes_[0]->getFrameRect();
    double left = frameRect.pos.x - frameRect.width / 2.0;
    double right = frameRect.pos.x + frameRect.width / 2.0;
    double bottom = frameRect.pos.y - frameRect.height / 2.0;
    double top = frameRect.pos.y + frameRect.height / 2.0;

    for (int i = 1; i < size_; ++i)
    {
      frameRect = shapes_[i]->getFrameRect();
      double left_ = frameRect.pos.x - frameRect.width / 2.0;
      double right_ = frameRect.pos.x + frameRect.width / 2.0;
      double bottom_ = frameRect.pos.y - frameRect.height / 2.0;
      double top_ = frameRect.pos.y + frameRect.height / 2.0;

      if (left_ < left)
      {
        left = left_;
      }
      if (right_ > right)
      {
        right = right_;
      }
      if (bottom_ < bottom)
      {
        bottom = bottom_;
      }
      if (top_ > top)
      {
        top = top_;
      }
    }
    return{(right - left), (top - bottom), {((left + right) / 2.0), ((top + bottom) / 2.0)}};
  }
}

double CompositeShape::getArea() const noexcept
{
  double area = 0;
  for (int i = 0; i < size_; ++i)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

double CompositeShape::getAngle() const noexcept
{
  return angle_;
}

void CompositeShape::move(const double dx, const double dy) noexcept
{
  for (int i = 0; i < size_; ++i)
  {
    shapes_[i]->move(dx, dy);
  }
}

void CompositeShape::move(const point_t & newPoint) noexcept
{
  move(newPoint.x - getFrameRect().pos.x, newPoint.y - getFrameRect().pos.y);
}

void CompositeShape::scale(const double coef)
{
  if (coef < 0.0)
  {
    throw std::invalid_argument("Invalide scale coefficient");
  }
  const point_t frame_center = getFrameRect().pos;
  for(int i = 0; i < size_; ++i)
  {
    shapes_[i]->move((coef - 1.0) * (shapes_[i]->getFrameRect().pos.x - frame_center.x),
      (coef - 1.0) * (shapes_[i]->getFrameRect().pos.y - frame_center.y));
    shapes_[i]->scale(coef);
  }
}

void CompositeShape::rotate(const double angle) noexcept
{
  angle_ += angle;
  const double sinus = sin(angle * M_PI / 180);
  const double cosinus = cos(angle * M_PI / 180);
  const point_t compositeCenter = getFrameRect().pos;
  for (int i = 0; i < size_; ++i)
  {
    const point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move({compositeCenter.x + (shapeCenter.x - compositeCenter.x) * cosinus - (shapeCenter.y - compositeCenter.y) * sinus,
      compositeCenter.y + (shapeCenter.y - compositeCenter.y) * cosinus + (shapeCenter.x - compositeCenter.x) * sinus});
    shapes_[i]->rotate(angle);
  }
}

void CompositeShape::addShape(const std::shared_ptr <Shape>shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Nullptr");
  }
  for (int i = 0; i < size_; ++i)
  {
    if (shape == shapes_[i])
    {
      throw std::invalid_argument("Shape is already added");
    }
  }
  std::unique_ptr <std::shared_ptr <Shape>[]> newShapes(new std::shared_ptr <Shape> [size_ +1]);
  for (int i = 0; i < size_; ++i)
  {
    newShapes[i] = shapes_[i];
  }
  newShapes[size_++] = shape;
  shapes_.swap(newShapes);
}

void CompositeShape::deleteShape(const int shapeNumber)
{
  if ((shapeNumber <= 0) || (shapeNumber >= size_))
  {
    throw std::invalid_argument("Invalide shape number");
  }
  if (size_ == 0)
  {
    throw std::invalid_argument("Empty composite shape");
  }
  else
  {
    std::unique_ptr <std::shared_ptr <Shape>[]> newShapes(new std::shared_ptr <Shape> [size_ - 1]);
    for (int i = 0; i < (shapeNumber - 1); ++i)
    {
      newShapes[i] = shapes_[i];
    }
    for (int i = shapeNumber; i < size_; ++i)
    {
      newShapes[i-1] = shapes_[i];
    }
    shapes_.swap(newShapes);
    --size_;
  }
}

void CompositeShape::deleteAllShapes() noexcept
{
  shapes_.reset();
  shapes_ = nullptr;
  size_ = 0;
  angle_ = 0.0;
}
