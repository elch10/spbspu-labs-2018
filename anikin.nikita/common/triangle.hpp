#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace anikin
{
  class Triangle : public anikin::Shape
  {
  public:
    Triangle(const anikin::point_t &a, const anikin::point_t &b, const anikin::point_t &c);

    double getArea() const noexcept override;
    double getAngle() const noexcept override;
    anikin::rectangle_t getFrameRect() const noexcept override;

    void move(const anikin::point_t &placement) noexcept override;
    void move(const double x, const double y) noexcept override;

    void scale(const double coef) override;
    void rotate(double angle) noexcept override;

  private:
    anikin::point_t pA_;
    anikin::point_t pB_;
    anikin::point_t pC_;
    anikin::point_t center_;

    double getMin(const double &A, const double &B, const double &C) const noexcept;
    double getMax(const double &A, const double &B, const double &C) const noexcept;
  };
}

#endif
