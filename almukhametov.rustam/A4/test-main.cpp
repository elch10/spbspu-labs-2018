#define BOOST_TEST_MODULE

#include <cmath>
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(Matrix_Tests)

  BOOST_AUTO_TEST_CASE(Constructor_Test)
  {
    BOOST_CHECK_THROW(almukhametov::Matrix matrix(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rows_Tests)
  {
    std::shared_ptr<almukhametov::Rectangle> rectangle1;
    rectangle1 = std::make_shared<almukhametov::Rectangle>(almukhametov::Rectangle(2.0,1.0,{0.0,0.0}));
    std::shared_ptr<almukhametov::Rectangle> rectangle2;
    rectangle2 = std::make_shared<almukhametov::Rectangle>(almukhametov::Rectangle(4.0,2.0,{0.0,0.0}));
    std::shared_ptr<almukhametov::Rectangle> rectangle3;
    rectangle3 = std::make_shared<almukhametov::Rectangle>(almukhametov::Rectangle(8.0,4.0,{0.0,0.0}));
    almukhametov::Matrix matrix;
    matrix.newShape(rectangle1);
    matrix.newShape(rectangle2);
    matrix.newShape(rectangle3);
    BOOST_REQUIRE_EQUAL(3, matrix.getRows());
  }

  BOOST_AUTO_TEST_CASE(Columns_Tests)
  {
    std::shared_ptr<almukhametov::Rectangle> rectangle1;
    rectangle1 = std::make_shared<almukhametov::Rectangle>(almukhametov::Rectangle(2.0,1.0,{0.0,0.0}));
    std::shared_ptr<almukhametov::Rectangle> rectangle2;
    rectangle2 = std::make_shared<almukhametov::Rectangle>(almukhametov::Rectangle(4.0,2.0,{100.0,200.0}));
    almukhametov::Matrix matrix;
    matrix.newShape(rectangle1);
    matrix.newShape(rectangle2);
    BOOST_REQUIRE_EQUAL(2, matrix.getColumns());
  }

BOOST_AUTO_TEST_SUITE_END()
