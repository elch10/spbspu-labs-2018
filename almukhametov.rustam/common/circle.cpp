#include "circle.hpp"
#include <cmath>
#include <stdexcept>

almukhametov::Circle::Circle(double r, point_t c):
  radius_(r),
  center_(c)
{
  if(r < 0.0)
  {
    throw std::invalid_argument("Radius must be >= 0");
  }
}

double almukhametov::Circle::getArea() const
{
  return M_PI*radius_*radius_;
}

almukhametov::rectangle_t almukhametov::Circle::getFrameRect() const
{
  return{2*radius_, 2*radius_, center_};
}

void almukhametov::Circle::move(point_t c)
{
  center_ = c;
}

void almukhametov::Circle::move(double dx, double dy)
{
  center_.y += dy;
  center_.x += dx;
}

void almukhametov::Circle::scale(double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Factor must be >= 0");
  }
  radius_ *= factor;
}

void almukhametov::Circle::rotate(double /*angle*/)
{

}
