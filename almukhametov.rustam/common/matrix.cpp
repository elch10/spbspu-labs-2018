#include <iostream>
#include <cmath>
#include "matrix.hpp"


almukhametov::Matrix::Matrix(const std::shared_ptr<Shape>& shape) :
  matrix_(new std::shared_ptr<Shape>[1]),
  columns_(1),
  rows_(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Wrong pointer");
  }
  matrix_[0] = shape;
}

almukhametov::Matrix::Matrix():
  matrix_(new std::shared_ptr<Shape>[1]),
  columns_(0),
  rows_(0)
{

}

almukhametov::Matrix &almukhametov::Matrix::operator = (const almukhametov::Matrix &matrix)
{
  if (this != &matrix)
  {
    this->columns_ = matrix.columns_;
    this->rows_ = matrix.rows_;
    std::unique_ptr<std::shared_ptr<Shape>[]> newMatrix(new std::shared_ptr<Shape>[rows_ * columns_]);
    for (int i = 0; i < rows_*columns_; i++)
    {
      newMatrix[i] = matrix.matrix_[i];
    }
    this->matrix_.swap(newMatrix);
  }
  return *this;
}

bool almukhametov::Matrix::checkIntersection(const std::shared_ptr<Shape>& shape1, const std::shared_ptr<Shape>& shape2)
{
  return ((fabs(shape1->getFrameRect().pos.x - shape2->getFrameRect().pos.x)
           < ((shape1->getFrameRect().height / 2) + (shape2->getFrameRect().height / 2)))
          && ((fabs(shape1->getFrameRect().pos.y - shape2->getFrameRect().pos.y)
               < ((shape1->getFrameRect().width / 2) + (shape2->getFrameRect().width / 2)))));
}

void almukhametov::Matrix::newShape(const std::shared_ptr<Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Wrong pointer");
  }

  for (int i = 0; i < columns_ * rows_; i++)
  {
    if(matrix_[i] == shape)
    {
      throw std::invalid_argument("This shape is already in matrix");
    }
  }

  if ((rows_==0) and (columns_==0))
  {
    columns_++;
    rows_++;
    matrix_[0] = shape;
    return;
  }

  int row = 1;
  int i = 0;
  while (i < rows_*columns_)
  {
    if (checkIntersection(matrix_[i], shape))
    {
      row = i / columns_ + 2;
    }
    i++;
  }

  int tempRows = rows_;
  int tempColumns = columns_;
  int freeColumns = 0;
  if (row > rows_)
  {
    tempRows++;
    freeColumns = columns_;
  }
  else
  {
    int j = (row - 1) * columns_;
    while (j < row*columns_)
    {
      if (matrix_[j] == nullptr)
      {
        freeColumns++;
      }
      j++;
    }
    if (freeColumns == 0)
    {
      tempColumns++;
      freeColumns = 1;
    }
  }

  std::unique_ptr <std::shared_ptr<Shape>[]> tempMatrix(new std::shared_ptr<Shape>[tempRows * tempColumns]);
  for (int i = 0; i < tempRows; i++)
  {
    for (int j = 0; j < tempColumns; j++)
    {
      if ((i >= rows_) or (j >= columns_))
      {
        tempMatrix[i * tempColumns + j] = nullptr;
        continue;
      }
      tempMatrix[i * tempColumns + j] = matrix_[i * columns_ + j];
    }
  }

  tempMatrix[row * tempColumns - freeColumns] = shape;
  matrix_.swap(tempMatrix);
  rows_ = tempRows;
  columns_ = tempColumns;
}

int almukhametov::Matrix::getRows() const
{
  return rows_;
}

int almukhametov::Matrix::getColumns() const
{
  return columns_;
}
