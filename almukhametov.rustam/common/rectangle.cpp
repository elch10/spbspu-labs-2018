#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

almukhametov::Rectangle::Rectangle(double h, double w, point_t c):
  height_(h),
  width_(w),
  center_(c)
{
  if (h < 0.0 or w < 0.0)
  {
    throw std::invalid_argument("Height and width must be >= 0");
  }
}

double almukhametov::Rectangle::getArea() const
{
  return height_*width_;
}

almukhametov::rectangle_t almukhametov::Rectangle::getFrameRect() const
{
  return {width_,height_,center_};
}

void almukhametov::Rectangle::move(point_t c)
{
  center_ = c;
}

void almukhametov::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void almukhametov::Rectangle::scale(double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Factor must be >= 0");
  }
  height_ *= factor;
  width_ *= factor;
}

void almukhametov::Rectangle::rotate(double angle)
{
  point_t center_ = Rectangle::getFrameRect().pos;
  angle = (angle * M_PI)/180;
  const double sinAngle = sin(angle);
  const double cosAngle = cos(angle);
  topLeft_ = {center_.x + (topLeft_.x - center_.x)*cosAngle - (topLeft_.y - center_.y)*sinAngle,
  center_.y + (topLeft_.x - center_.x)*sinAngle + (topLeft_.y - center_.y)*cosAngle};
}
