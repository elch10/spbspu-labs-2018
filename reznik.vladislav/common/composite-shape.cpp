#include <stdexcept>
#include <iostream>
#include <new>
#include <cmath>
#include <algorithm>
#include "composite-shape.hpp"

using uniqshared = std::unique_ptr<std::shared_ptr<reznik::Shape>[]>;
using shared = std::shared_ptr<reznik::Shape>;

reznik::CompositeShape::CompositeShape():
  arr_(new std::shared_ptr<reznik::Shape>[0]),
  count_(0),
  angle_(0)
{

}


reznik::CompositeShape::CompositeShape(const shared &shape):
  arr_(nullptr),
  count_(0),
  angle_(0.0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Empty");
  }
  addShape(shape);
}


reznik::CompositeShape::CompositeShape(const CompositeShape & copy) :
  arr_(nullptr),
  count_(0),
  angle_(0)
{
  uniqshared Copyarr_(new shared[copy.count_]);
  for (size_t i = 0; i < copy.count_; i++)
  {
    Copyarr_[i] = copy.arr_[i];
  }
  count_=copy.count_;
  angle_=copy.angle_;
  arr_=std::move(Copyarr_);
}

reznik::CompositeShape::CompositeShape(CompositeShape && other) :
  arr_(std::move(other.arr_)),
  count_(other.count_),
  angle_(other.angle_)
{
  other.arr_=(nullptr);
  other.count_=0;
  other.angle_=0;

}

reznik::CompositeShape & reznik::CompositeShape::operator=(CompositeShape && other)
{
  if (this == &other)
  {
    return *this;
  }

  count_ = other.count_;
  angle_ = other.angle_;
  std::swap(arr_,other.arr_);
  other.arr_ = nullptr;
  other.count_ = 0;
  other.angle_ = 0;

  return *this;
}

reznik::CompositeShape & reznik::CompositeShape::operator=(const CompositeShape & copy)
{
  if (this == &copy)
  {
    return *this;
  }

  arr_.reset(new shared[copy.count_]);
  count_ = copy.count_;
  angle_ = copy.angle_;
  for (size_t i = 0; i < count_; i++)
  {
    arr_[i] = copy.arr_[i];
  }
  return *this;
}

double reznik::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += arr_[i]->getArea();
  }
  return area;
}

double reznik::CompositeShape::getAngle() const
{
  return angle_;
}

int reznik::CompositeShape::getSize() const
{
  return count_;
}

shared & reznik::CompositeShape::operator [](size_t index)
{
  if(index>=count_)
  {
    throw std::out_of_range("Count is less than the index");
  }
  if(count_==0)
  {
    throw std::out_of_range("Composite shape is empty");
  }
  return arr_[index];
}




reznik::rectangle_t reznik::CompositeShape::getFrameRect() const
{
  rectangle_t rect = arr_[0]->getFrameRect();
  double minX = rect.pos.x - rect.width / 2;
  double maxX = rect.pos.x + rect.width / 2;
  double minY = rect.pos.y - rect.height / 2;
  double maxY = rect.pos.y + rect.height / 2;
  for (size_t i = 1; i < count_; i++)
  {
    rect = arr_[i]->getFrameRect();
    minX = std::min(minX, rect.pos.x - rect.width / 2);
    maxX = std::max(maxX, rect.pos.x + rect.width / 2);
    minY = std::min(minY, rect.pos.y - rect.height / 2);
    maxY = std::max(maxY, rect.pos.y + rect.height / 2);
  }
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const double width = (maxY - minY) * fabs(sine) + (maxX - minY) * fabs(cosine);
  const double height = (maxY - minY) * fabs(cosine) + (maxX - minY) * fabs(sine);

  return {width, height,{ (maxX + minX) / 2, ( maxY + minY) / 2 } };
}

void reznik::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; i++)
  {
    arr_[i]->move(dx, dy);
  }
}

void reznik::CompositeShape::move(const point_t & center)
{
  point_t pos = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    arr_[i]->move(center.x - pos.x, center.y - pos.y);
  }
}

void reznik::CompositeShape::scale(double k)
{
  if (k < 0)
  {
    throw std::invalid_argument("Error: negative argument scale!");
  }
  point_t tmp_centre = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    arr_[i]->move({ tmp_centre.x + (arr_[i]->getFrameRect().pos.x - tmp_centre.x) * k,
                   tmp_centre.y + (arr_[i]->getFrameRect().pos.y - tmp_centre.y) * k });
    arr_[i]->scale(k);
  }
}

void reznik::CompositeShape::rotate(const double a)
{
  angle_ += a;
  if (angle_ >= 360)
  {
    angle_ = fmod(angle_, 360);
  }
  const double sine = sin(a * M_PI / 180);
  const double cosine = cos(a * M_PI / 180);
  const reznik::point_t currentCentre = getFrameRect().pos;
  for (size_t i = 0; i < count_; ++i)
  {
    const reznik::point_t shapeCentre = arr_[i]->getFrameRect().pos;
    arr_[i]->move({(shapeCentre.x - currentCentre.x) * cosine + (shapeCentre.y - currentCentre.y) * sine + currentCentre.x,
      (shapeCentre.y - currentCentre.y) * cosine + (shapeCentre.x - currentCentre.x) * sine + currentCentre.y});
    arr_[i]->rotate(a);
  }
}


void reznik::CompositeShape::addShape(const std::shared_ptr<reznik::Shape> shape)
{
  uniqshared newEllement(new shared[count_ + 1]);
  for (size_t i = 0; i < count_; i++)
  {
    newEllement[i] = arr_[i];
  }
  newEllement[count_++] = shape;
  arr_.swap(newEllement);
}

void reznik::CompositeShape::deleteShape(size_t index)
{
  if (count_ == 0)
  {
    throw std::out_of_range("CompositeShape is empty ");
  }
  if (index >= count_)
  {
    throw std::out_of_range("Count less than index ");
  }
  for (size_t i = index; i < count_ - 1; i++)
  {
    arr_[i] = arr_[i + 1];
  }
  arr_[count_ - 1] = nullptr;
  count_--;
}

void reznik::CompositeShape::clear()
{
  count_ = 0;
  arr_ = nullptr;
  angle_ = 0.0;
}

void reznik::CompositeShape::printInfo() const
{
  std::cout<<"------CompositeShape----------"<<std::endl;
  for(size_t i=0; i<count_;++i)
  {
    arr_[i]->printInfo();
  }
  std::cout<<"-----------------------------"<<std::endl;
}


