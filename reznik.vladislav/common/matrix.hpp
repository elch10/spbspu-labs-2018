#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"
#include "composite-shape.hpp"

namespace reznik
{
  using uniqshared = std::unique_ptr<std::shared_ptr<reznik::Shape>[]>;
  using shared = std::shared_ptr<reznik::Shape>;
  class Matrix
  {
  public:

    Matrix(const std::shared_ptr<reznik::Shape> &shape);
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix);

    ~Matrix();

    Matrix & operator=(const Matrix & matrix);
    Matrix & operator=(Matrix && matrix);
    bool operator==(const Matrix & new_matrix) const;
    bool operator!=(const Matrix & new_matrix) const;

    uniqshared operator[](const size_t index) const;

    void addElement(const shared &shape);
    void addComposite(const std::shared_ptr<reznik::CompositeShape> &shape, size_t size);
    size_t getLayerNumber() const;
    size_t getLayerSize() const;
    void printInfo() const;

  private:
    uniqshared matrix_;
    size_t layerNumber_;
    size_t layerSize_;
    bool checkOverLapping(size_t index, const shared &shape) const;
  };
}

#endif
