#include "circle.hpp"
#include <iostream>
#include <cmath>

using namespace volohov;

volohov::Circle::Circle(const volohov::point_t & center, const double r):
  center_(center),
  r_(r),
  angle_(0.0)
{
  if (r_ < 0.0)
  {
    throw std::invalid_argument("Wait. Invalid radius of circle.");
  }
}

double volohov::Circle::getArea() const
{
  return M_PI * r_ * r_;
}

volohov::rectangle_t volohov::Circle::getFrameRect() const
{
  return {center_, 2 * r_, 2 * r_};
}

void volohov::Circle::move(const volohov::point_t & newCenter)
{
  center_ = newCenter;
}

void volohov::Circle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void volohov::Circle::scale(const double coeff)
{
  if (coeff < 0.0)
  {
    throw std::invalid_argument("Wait. Invalid coefficient.");
  }
  r_ *= coeff;
}

void volohov::Circle::rotate(const double /*angle*/)
{
  
}
