#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <stdexcept>
#include <cmath>
#include <memory>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace volohov;

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(TestCircle)

BOOST_AUTO_TEST_CASE(MoveByAccuracy)
{
  volohov::Circle circle({3.0, 4.0}, 1.0);
  circle.move(1.0, 18.0);
  BOOST_CHECK_EQUAL(circle.getFrameRect().height / 2, 1.0);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, 4.0, ACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, 22.0, ACCURACY);
  BOOST_CHECK_CLOSE(circle.getArea(), M_PI, ACCURACY);
}

BOOST_AUTO_TEST_CASE(TestMoveToPoint)
{
  volohov::Circle circle({3.0, 4.0}, 1.0);
  circle.move({1.0, 18.0});
  BOOST_CHECK_EQUAL(circle.getFrameRect().height / 2, 1.0);
  BOOST_CHECK_EQUAL(circle.getFrameRect().pos.x, 1.0);
  BOOST_CHECK_EQUAL(circle.getFrameRect().pos.y, 18.0);
  BOOST_CHECK_CLOSE(circle.getArea(), M_PI, ACCURACY);
}

BOOST_AUTO_TEST_CASE(ScaleTest)
{
  volohov::Circle circle({15.32, 1.23}, 4.3);
  circle.scale(7.0);
  BOOST_CHECK_CLOSE(circle.getArea(), 4.3 * 4.3 * M_PI * 49.0, ACCURACY);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestConstructor)
{
  BOOST_CHECK_THROW(volohov::Circle({1.32, 1.23}, -1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(RectangleTests)

BOOST_AUTO_TEST_CASE(MoveByAccuracyTest)
{
  volohov::Rectangle rectangle({19.0, 8.0}, 2.335, 10.1);
  rectangle.move(1.0, 12.0);
  BOOST_CHECK_EQUAL(rectangle.getFrameRect().height, 10.1);
  BOOST_CHECK_EQUAL(rectangle.getFrameRect().width, 2.335);
  BOOST_CHECK_CLOSE(rectangle.getArea(), 2.335 * 10.1, ACCURACY);
}

BOOST_AUTO_TEST_CASE(MoveToPosTest)
{
  volohov::Rectangle rectangle({19.64, 8.35}, 7.0, 10.79);
  rectangle.move({5.27, 18.9});
  BOOST_CHECK_EQUAL(rectangle.getFrameRect().height, 10.79);
  BOOST_CHECK_EQUAL(rectangle.getFrameRect().width, 7.0);
  BOOST_CHECK_CLOSE(rectangle.getArea(), 7.0 * 10.79, ACCURACY);
}

BOOST_AUTO_TEST_CASE(ScaleTest)
{
  volohov::Rectangle rectangle({19.64, 8.35}, 7.0, 10.7);
  rectangle.scale(2.0);
  BOOST_CHECK_CLOSE(rectangle.getArea(), 7.0 * 10.7 * 2.0 * 2.0, ACCURACY);
  BOOST_CHECK_EQUAL(rectangle.getFrameRect().pos.x, 19.64);
  BOOST_CHECK_EQUAL(rectangle.getFrameRect().pos.y, 8.35);
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorTest)
{
  BOOST_CHECK_THROW(volohov::Rectangle({15.32, 16.23}, -3.26, 7.15), std::invalid_argument);
  BOOST_CHECK_THROW(volohov::Rectangle({15.32, 16.23}, 3.26, -7.15), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(RotateTest)
{
  volohov::Rectangle rectangle({2.9, 4.5}, 2.3, 7.9);
  rectangle.rotate(90);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 7.9, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 2.3, ACCURACY);
  BOOST_CHECK_EQUAL(rectangle.getFrameRect().pos.x, 2.9);
  BOOST_CHECK_EQUAL(rectangle.getFrameRect().pos.y, 4.5);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

BOOST_AUTO_TEST_CASE(MoveByAccuracyTest)
{
  volohov::Circle circle({5.0, 3.0}, 1.0);
  volohov::Rectangle rectangle({1.0, 0.5} , 2.0, 1.0);
  std::shared_ptr< volohov::Shape > circlePtr = std::make_shared< volohov::Circle >(circle);
  std::shared_ptr< volohov::Shape > rectanglePtr = std::make_shared< volohov::Rectangle >(rectangle);
  volohov::CompositeShape compositeShape(circlePtr);
  compositeShape.addShape(rectanglePtr);
  compositeShape.move(1.2, 2.7);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), 2 + M_PI, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, 6.0, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, 4.0, ACCURACY);
}

BOOST_AUTO_TEST_CASE(MoveToPointTest)
{
  volohov::Circle circle({5.0, 3.0}, 1.0);
  volohov::Rectangle rectangle({1.0, 0.5}, 2.0, 1.0);
  std::shared_ptr< volohov::Shape > circlePtr = std::make_shared< volohov::Circle >(circle);
  std::shared_ptr< volohov::Shape > rectanglePtr = std::make_shared< volohov::Rectangle >(rectangle);
  volohov::CompositeShape compositeShape(circlePtr);
  compositeShape.addShape(rectanglePtr);
  compositeShape.move({1.2, 2.7});
  BOOST_CHECK_CLOSE(compositeShape.getArea(), 2 + M_PI, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, 6.0, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, 4.0, ACCURACY);
}

BOOST_AUTO_TEST_CASE(ScaleTest)
{
  volohov::Circle circle({5.0, 3.0}, 1.0);
  volohov::Rectangle rectangle({1.0, 0.5}, 2.0, 1.0);
  std::shared_ptr< volohov::Shape > circlePtr = std::make_shared< volohov::Circle >(circle);
  std::shared_ptr< volohov::Shape > rectanglePtr = std::make_shared< volohov::Rectangle >(rectangle);
  volohov::CompositeShape compositeShape(circlePtr);
  compositeShape.addShape(rectanglePtr);
  compositeShape.scale(2);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), 8 + 4 * M_PI, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, 12.0, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, 8.0, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, 3, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, 2.0, ACCURACY);
  BOOST_CHECK_THROW(compositeShape.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(RemoveShapeTest)
{
  volohov::Circle circle({3.6, 5.0}, 4.3);
  volohov::Rectangle rectangle({2.0, 2.67}, 7.0, 10.79);
  std::shared_ptr< volohov::Shape > circlePtr = std::make_shared< volohov::Circle >(circle);
  std::shared_ptr< volohov::Shape > rectanglePtr = std::make_shared< volohov::Rectangle >(rectangle);
  volohov::CompositeShape compositeShape(circlePtr);
  compositeShape.addShape(rectanglePtr);
  compositeShape.removeShape(0);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, 2.0, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, 2.67, ACCURACY);
  BOOST_CHECK_EQUAL(compositeShape.getArea(), rectangle.getArea());
  BOOST_CHECK_EQUAL(compositeShape.getCount(), 1);
  BOOST_CHECK_THROW(compositeShape.removeShape(2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(DeleteShapesTest)
{
  volohov::Circle circle({5.0, 6.32}, 4.3);
  volohov::Rectangle rectangle({19.64, 8.35}, 7.0, 10.79);
  std::shared_ptr< volohov::Shape > circlePtr = std::make_shared< volohov::Circle >(circle);
  std::shared_ptr< volohov::Shape > rectanglePtr = std::make_shared< volohov::Rectangle >(rectangle);
  volohov::CompositeShape compositeShape(circlePtr);
  compositeShape.addShape(rectanglePtr);
  compositeShape.removeAll();
  BOOST_CHECK_EQUAL(compositeShape.getCount(), 0);
}

BOOST_AUTO_TEST_CASE(InvalidIdenticalShapeTest)
{
  volohov::Circle circle({15.32, 1.23}, 4.3);
  std::shared_ptr< volohov::Shape > circlePtr = std::make_shared< volohov::Circle >(circle);
  volohov::CompositeShape compositeShape(circlePtr);
  BOOST_CHECK_THROW(compositeShape.addShape(circlePtr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(RotateTest)
{
  volohov::Circle circle({5.0, 3.0}, 1.0);
  volohov::Rectangle rectangle({1.0, 0.5}, 2.0, 1.0);
  std::shared_ptr< volohov::Shape > circlePtr = std::make_shared< volohov::Circle >(circle);
  std::shared_ptr< volohov::Shape > rectanglePtr = std::make_shared< volohov::Rectangle >(rectangle);
  volohov::CompositeShape compositeShape(circlePtr);
  compositeShape.addShape(rectanglePtr);
  compositeShape.rotate(90);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, 4.0, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, 6.0, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTests)

BOOST_AUTO_TEST_CASE(MatrixSplitting)
{
  Circle circleM({-2.0, -2.0}, 2.0);
  volohov::Rectangle rectangleM1({-2.0, 0.0}, 2.0, 2.0);
  volohov::Rectangle rectangleM2({1.0, 1.0}, 6.0, 3.0);
  volohov::Rectangle rectangleM3({3.0, 1.0}, 2.0, 4.0);
  volohov::Rectangle rectangleM4({3.0, 3.0}, 4.0, 4.0);
  std::shared_ptr< volohov::Shape > circlePtrM = std::make_shared< Circle >(circleM);
  std::shared_ptr< volohov::Shape > rectanglePtrM1 = std::make_shared< volohov::Rectangle >(rectangleM1);
  std::shared_ptr< volohov::Shape > rectanglePtrM2 = std::make_shared< volohov::Rectangle >(rectangleM2);
  std::shared_ptr< volohov::Shape > rectanglePtrM3 = std::make_shared< volohov::Rectangle >(rectangleM3);
  std::shared_ptr< volohov::Shape > rectanglePtrM4 = std::make_shared< volohov::Rectangle >(rectangleM4);
  volohov::CompositeShape shapes(circlePtrM);
  shapes.addShape(rectanglePtrM1);
  shapes.addShape(rectanglePtrM2);
  shapes.addShape(rectanglePtrM3);
  shapes.addShape(rectanglePtrM4);
  volohov::Matrix matrix(shapes);
  BOOST_CHECK_CLOSE(matrix[0][0]->getFrameRect().pos.x, -2.0, ACCURACY);
  BOOST_CHECK_CLOSE(matrix[0][1]->getFrameRect().pos.x, 3.0, ACCURACY);
  BOOST_CHECK_CLOSE(matrix[1][0]->getFrameRect().pos.x, -2.0, ACCURACY);
  BOOST_CHECK_CLOSE(matrix[1][1]->getFrameRect().pos.x, 3.0, ACCURACY);
  BOOST_CHECK_CLOSE(matrix[2][0]->getFrameRect().pos.x, 1.0, ACCURACY);
  BOOST_CHECK_CLOSE(matrix[0][0]->getFrameRect().pos.y, -2.0, ACCURACY);
  BOOST_CHECK_CLOSE(matrix[0][1]->getFrameRect().pos.y, 1.0, ACCURACY);
  BOOST_CHECK_CLOSE(matrix[1][0]->getFrameRect().pos.y, 0.0, ACCURACY);
  BOOST_CHECK_CLOSE(matrix[1][1]->getFrameRect().pos.y, 3.0, ACCURACY);
  BOOST_CHECK_CLOSE(matrix[2][0]->getFrameRect().pos.y, 1.0, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
