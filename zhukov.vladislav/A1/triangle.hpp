#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t& A, const point_t& B, const point_t& C);
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(const double dx, const double dy) override;
  void move(const point_t& teleport) override;
  point_t getCenter() const;
  point_t getRadVects(const char vertex) const;
  double max_x(bool need) const;
  double max_y(bool need) const;

private:
  point_t center_;
  point_t vectors_[3];
};

#endif
