#include "triangle.hpp"
#include <cmath>
#include <stdexcept>

Triangle::Triangle(const point_t& A, const point_t& B, const point_t& C) :
  center_ { (A.x + B.x + C.x) / 3, (A.y + B.y + C.y) / 3 },
  vectors_ { { A.x - center_.x , A.y - center_.y } , { B.x - center_.x , B.y - center_.y }, 
             { C.x - center_.x , C.y - center_.y } }
{
  if((C.x - A.x) * (B.y - A.y) == (B.x - A.x) * (C.y - A.y))
  {
    throw std::invalid_argument("This is a line or a point");
  }
}

double Triangle::getArea() const
{
  point_t A = { vectors_[0].x + center_.x , vectors_[0].y + center_.y };
  point_t B = { vectors_[1].x + center_.x , vectors_[1].y + center_.y };
  point_t C = { vectors_[2].x + center_.x , vectors_[2].y + center_.y };
  return std::abs((A.x - C.x) * (B.y - C.y) - (B.x - C.x) * (A.y - C.y)) / 2;
}

rectangle_t Triangle::getFrameRect() const
{
  rectangle_t rect;
  rect.pos.x = (max_x(true) + max_x(false) + 2 * center_.x) / 2;
  rect.pos.y = (max_y(true) + max_y(false) + 2 * center_.y) / 2;
  rect.height = max_y(true) - max_y(false);
  rect.width  = max_x(true) - max_x(false);
  return rect;
}

void Triangle::move(const double dx, const double dy)
{
  center_.x += dx , center_.y += dy;
}

void Triangle::move(const point_t& teleport)
{
  move(teleport.x - center_.x, teleport.y - center_.y);
}

point_t Triangle::getCenter() const
{
  return center_;
}

point_t Triangle::getRadVects(const char vertex) const
{
  switch(vertex)
  {
    case 'A' : return vectors_[0]; break;
    case 'B' : return vectors_[1]; break;
    case 'C' : return vectors_[2]; break;
    default  : return { 0. , 0. }; break;
  }
}

double Triangle::max_x(bool need) const
{
  if(need == true)
  {
    return fmax(fmax(vectors_[0].x, vectors_[1].x), vectors_[2].x);
  }
  else
  {
    return fmin(fmin(vectors_[0].x, vectors_[1].x), vectors_[2].x);  
  }
}

double Triangle::max_y(bool need) const
{
  if(need == true)
  {
    return fmax(fmax(vectors_[0].y, vectors_[1].y), vectors_[2].y);
  }
  else
  {
    return fmin(fmin(vectors_[0].y, vectors_[1].y), vectors_[2].y);
  }
}