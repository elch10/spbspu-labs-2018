#include "rectangle.hpp"
#include <iostream>
#include<cmath>

kostylev::Rectangle::Rectangle(const point_t & centre, const double width, const double height):
  centre_(centre),
  width_(width),
  height_(height),
  angle_(0.0)
{
  if ((height < 0.0) || (width < 0.0))
  {
    throw std::invalid_argument("Error.Invalid height or width of rectangle.");
  }
  corners_[0] = {centre_.x - width_ /2, centre_.y - height_ / 2}; //left bottom
  corners_[1] = {centre_.x - width_ /2, centre_.y + height_ / 2}; //left top
  corners_[2] = {centre_.x + width_ /2, centre_.y + height_ / 2}; //right top
  corners_[3] = {centre_.x + width_ /2, centre_.y - height_ / 2}; //right bottom
}

double kostylev::Rectangle::getArea() const
{
  return width_ * height_;
}

kostylev::rectangle_t kostylev::Rectangle::getFrameRect() const
{
  double maxY = std::max(std::max(corners_[0].y, corners_[1].y),
    std::max(corners_[2].y, corners_[3].y));
  double maxX = std::max(std::max(corners_[0].x, corners_[1].x),
    std::max(corners_[2].x, corners_[3].x));
  double minY = std::min(std::min(corners_[0].y, corners_[1].y),
    std::max(corners_[2].y, corners_[3].y));
  double minX = std::min(std::min(corners_[0].x, corners_[1].x),
    std::max(corners_[2].x, corners_[3].x));
  return {{(minX + maxX) / 2, (minY + maxY) / 2 }, (maxX - minX), (maxY - minY) };
}

void kostylev::Rectangle::move(const point_t & point)
{
  for (size_t i = 0; i < 4; i++)
  {
    corners_[i].x += point.x - centre_.x;
    corners_[i].y += point.y - centre_.y;
  }
  centre_ = point;
}

void kostylev::Rectangle::move(const double dx, const double dy)
{
  centre_ = { centre_.x + dx, centre_.y + dy };
  for (size_t i = 0; i < 4; i++)
  {
    corners_[i].x += dx;
    corners_[i].y += dy;
  }
}

void kostylev::Rectangle::scale(const double coef)
{
  if (coef < 0.0)
  {
    throw std::invalid_argument("Error.Invalid parameter of scaling.");
  }
  for (size_t i = 0; i < 4; i++)
  {
    corners_[i].x = (corners_[i].x - centre_.x) * coef + centre_.x;
    corners_[i].y = (corners_[i].y - centre_.y) * coef + centre_.y;
  }
  width_ *= coef;
  height_ *= coef;
}


void kostylev::Rectangle::rotate(const double alpha)
{
  double msin = sin(alpha * 3.14 / 180);
  double mcos = cos(alpha * 3.14 / 180);
  kostylev::point_t position = getFrameRect().pos;
  for (size_t i =0; i < 4; i++)
  {
    corners_[i] = {position.x + (corners_[i].x - position.x) * mcos - (corners_[i].y - position.y) * msin,
      position.y + (corners_[i].y - position.y) * mcos + (corners_[i].x - position.x) * msin};
  }
}
