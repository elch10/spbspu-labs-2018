#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace kostylev
{
  class Matrix
  {
  public:
    Matrix(const std::shared_ptr<Shape> & shapeElement);
    Matrix(const Matrix & object);
    Matrix(Matrix && object);

    Matrix & operator =(const Matrix & object);
    Matrix & operator =(Matrix && object);

    std::unique_ptr<std::shared_ptr<Shape>[]>::pointer operator [](const size_t index);
    void addShape(const std::shared_ptr<Shape> & shapeElement);
    size_t getLayers() const;

  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> matrix_;
    size_t rows_, columns_;
    bool checkOverlap(const std::shared_ptr<Shape> & shape1, const std::shared_ptr<Shape> & shape2) const;
  };
}

#endif
