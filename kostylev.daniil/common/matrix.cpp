#include "matrix.hpp"
#include <iostream>
#include <cmath>

kostylev::Matrix::Matrix(const std::shared_ptr<Shape> & shapeElement):
  matrix_(new std::shared_ptr<Shape>[1]),
  rows_(0),
  columns_(0)
{
  if (!shapeElement)
  {
    matrix_.reset();
    throw std::invalid_argument("Error. Object does not exist.");
  }
  matrix_[0] = shapeElement;
  ++rows_;
  ++columns_;
}

kostylev::Matrix::Matrix(const Matrix & object):
  matrix_(new std::shared_ptr<Shape>[object.rows_ * object.columns_]),
  rows_(object.rows_),
  columns_(object.columns_)
{
  for (size_t i = 0; i < (rows_ * columns_); ++i)
  {
    matrix_[i] = object.matrix_[i];
  }
}

kostylev::Matrix::Matrix(Matrix && object):
  matrix_(std::move(object.matrix_)),
  rows_(std::move(object.rows_)),
  columns_(std::move(object.columns_))
{
  object.matrix_.reset();
  object.rows_ = 0;
  object.columns_ = 0;
}

kostylev::Matrix & kostylev::Matrix::operator =(const Matrix & object)
{
  if (this != &object)
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> new_arr(new std::shared_ptr<Shape>[rows_ * columns_]);
    rows_ = object.rows_;
    columns_ = object.columns_;
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      new_arr[i] = object.matrix_[i];
    }
    matrix_.swap(new_arr);
  }
  return *this;
}

kostylev::Matrix & kostylev::Matrix::operator =(Matrix && object)
{
  if (this != &object)
  {
    matrix_ = std::move(object.matrix_);
    rows_ = std::move(object.rows_);
    columns_ = std::move(object.columns_);
    object.matrix_.reset();
    object.rows_ = 0;
    object.columns_ = 0;
  }
  return *this;
}

std::unique_ptr<std::shared_ptr<kostylev::Shape>[]>::pointer kostylev::Matrix::operator [](const size_t index)
{
  if (index > rows_)
  {
    throw std::out_of_range("Error. Index is out of range.");
  }
  return (matrix_.get() + index * columns_);
}

void kostylev::Matrix::addShape(const std::shared_ptr<Shape> & shapeElement)
{
  if (!shapeElement)
  {
    throw std::invalid_argument("Error. Object does not exist.");
  }
  size_t i = rows_ * columns_;
  size_t desired_row = 1;
  while (i > 0)
  {
    --i;
    if (checkOverlap(matrix_[i], shapeElement))
    {
      desired_row = i / columns_ + 2;
    }
  }
  size_t rows_temp = rows_;
  size_t columns_temp = columns_;
  size_t free_columns = 0;
  if (desired_row > rows_)
  {
    ++rows_temp;
    free_columns = columns_;
  }
  else
  {
    size_t j = (desired_row - 1) * columns_;
    while (j < (desired_row * columns_))
    {
      if (!matrix_[j])
      {
        ++free_columns;
      }
      ++j;
    }
    if (free_columns == 0)
    {
      ++columns_temp;
      free_columns = 1;
    }
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> new_arr(new std::shared_ptr<Shape>[rows_temp * columns_temp]);
  for (size_t i = 0; i < rows_temp; ++i)
  {
    for (size_t j = 0; j < columns_temp; ++j)
    {
      if ((i >= rows_) || (j >= columns_))
      {
        new_arr[i * columns_temp + j].reset();
        continue;
      }
      new_arr[i * columns_temp + j] = matrix_[i * columns_ + j];
    }
  }
  new_arr[desired_row * columns_temp - free_columns] = shapeElement;
  matrix_.swap(new_arr);
  rows_ = rows_temp;
  columns_ = columns_temp;
}

size_t kostylev::Matrix::getLayers() const
{
  return rows_;
}

bool kostylev::Matrix::checkOverlap(const std::shared_ptr<Shape> & shape1, const std::shared_ptr<Shape> & shape2) const
{
  if ((!shape1) || (!shape2))
  {
    return false;
  }
  rectangle_t shapeFrameRect1 = shape1 -> getFrameRect();
  rectangle_t shapeFrameRect2 = shape2 -> getFrameRect();
  return ((abs(shapeFrameRect1.pos.x - shapeFrameRect2.pos.x)
    < ((shapeFrameRect1.width / 2) + (shapeFrameRect2.width / 2)))
      && ((abs(shapeFrameRect1.pos.y - shapeFrameRect2.pos.y)
        < ((shapeFrameRect1.height / 2) + (shapeFrameRect2.height / 2)))));
}
