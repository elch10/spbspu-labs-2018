#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

using namespace ryabov;

void checkRectForErrs(double width, double height)
{
  if (width <= 0 || height <= 0) {
    throw std::invalid_argument("Error: Invalid argument: Width and height must be > 0.");
  }
}

Rectangle::Rectangle(double width, double height, const point_t &pos) :
  width_(width),
  height_(height),
  frame_({width, height, pos})
{
  checkRectForErrs(width, height);
  generateVerticesCoords(width, height, pos);
}

Rectangle::Rectangle(const rectangle_t &frame) :
  width_(frame.width),
  height_(frame.height),
  frame_(frame)
{
  checkRectForErrs(frame.width, frame.height);
  generateVerticesCoords(frame.width, frame.height, frame.pos);
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return frame_;
}

point_t Rectangle::move(const point_t &coords)
{
  frame_.pos = coords;
  return coords;
}

point_t Rectangle::move(double dx, double dy)
{
  frame_.pos.x += dx;
  frame_.pos.y += dy;
  return frame_.pos;
}

void Rectangle::scale(double factor)
{
  if (factor <= 0) {
    throw std::invalid_argument("Error: Invalid argument: factor must be > 0.");
  } else {
    double centerX = frame_.pos.x;
    double centerY = frame_.pos.y;
    frame_.width *= factor;
    frame_.height *= factor;
    width_ *= factor;
    height_ *= factor;

    for (unsigned int i = 0; i < vertices_.size(); i++) {
      vertices_[i].x = centerX + factor * (vertices_[i].x  - centerX);
      vertices_[i].y = centerY + factor * (vertices_[i].y  - centerY);
    }
  } 
}

void Rectangle::rotate(double deg)
{
  double rad = deg * (M_PI / 180);
  double centerX = frame_.pos.x;
  double centerY = frame_.pos.y;
  double topX = centerX;
  double topY = centerY;
  double bottomX = centerX;
  double bottomY = centerY;

  for (unsigned int i = 0; i < vertices_.size(); i++) {
    double x = vertices_[i].x;
    double y = vertices_[i].y;
    vertices_[i].x = centerX + (x - centerX) * cos(rad) - (y - centerY) * sin(rad);
    vertices_[i].y = centerY + (x - centerX) * sin(rad) + (y - centerY) * cos(rad);

    if (vertices_[i].x > topX) {
      topX = vertices_[i].x;
    } else if (vertices_[i].x < bottomX) {
      bottomX = vertices_[i].x;
    }
    if (vertices_[i].y > topY) {
      topY = vertices_[i].y;
    } else if (vertices_[i].y < bottomY) {
      bottomY = vertices_[i].y;
    }
  }

  frame_.width = topX - bottomX;
  frame_.height = topY - bottomY;
}

void Rectangle::generateVerticesCoords(double width, double height, point_t currentPos)
{
  vertices_[0] = { currentPos.x - width / 2, currentPos.y - height / 2 };
  vertices_[1] = { currentPos.x - width / 2, currentPos.y + height / 2 };
  vertices_[2] = { currentPos.x + width / 2, currentPos.y + height / 2 };
  vertices_[3] = { currentPos.x + width / 2, currentPos.y - height / 2 };
}
