#include "shapeSpliter.hpp"
#include <stdexcept>

using namespace ryabov;

ShapeSpliter::ShapeSpliter() :
  layers_(),
  rows_(1),
  cols_(1)
{}

void ShapeSpliter::addShape(Shape *shape)
{
  if (!shape) {
    throw std::invalid_argument("Error: Invalid argument: shape can't be null.");
  }

  int correctLayer = 0;
  int correctCol = 0;

  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      if (shape == layers_(i, j)){
        throw std::invalid_argument("Error: Invalid argument: shape is duplicate.");
      }
      if (isOverlapingShapes(layers_(i, j), shape)) {
        correctLayer = i + 1;
        break;
      }
    }
  }

  if (correctLayer > rows_ - 1) {
    rows_ += 1;
    layers_.resize(rows_, cols_);
  } else {
    for (int j = 0; j < cols_; j++) {
      if (!layers_(correctLayer, j)) {
        correctCol = j;
        break;
      } else {
        correctCol = j + 1;
      }
    }

    if (correctCol > cols_ - 1) {
      cols_ += 1;
      layers_.resize(rows_, cols_);
    }
  }

  layers_.setElement(correctLayer, correctCol, shape);
}

void ShapeSpliter::showLayers() const
{
  layers_.showMatrix();
}

Shape** ShapeSpliter::getLayer(int num)
{
  if (num < 0 || num > rows_ - 1) {
    throw std::invalid_argument("Error: Invalid argument: invalid number.");
  }

  int shapesCount = 0;
  int i = 0;
  for (int j = 0; j < cols_; j++) {
    if (layers_(num, j))
      shapesCount += 1;
  }

  Shape** layer = new Shape * [shapesCount];
  for (int j = 0; j < cols_; j++) {
    if (layers_(num, j))
      layer[i++] = layers_(num, j);
  }

  return layer;
}

int ShapeSpliter::getLayersCount()
{
  if (!layers_(0, 0))
    return 0;
  return rows_;
}

bool ShapeSpliter::isOverlapingShapes(Shape *shape1, Shape *shape2) const
{
  if (shape1 && shape2) {
    rectangle_t shape1Rect = shape1->getFrameRect();
    rectangle_t shape2Rect = shape2->getFrameRect();
    double shape1Left = shape1Rect.pos.x - (shape1Rect.width / 2),
           shape1Right = shape1Rect.pos.x + (shape1Rect.width / 2),
           shape1Bottom = shape1Rect.pos.y - (shape1Rect.height / 2),
           shape1Top = shape1Rect.pos.y + (shape1Rect.height / 2);
    double shape2Left = shape2Rect.pos.x - (shape2Rect.width / 2),
           shape2Right = shape2Rect.pos.x + (shape2Rect.width / 2),
           shape2Bottom = shape2Rect.pos.y - (shape2Rect.height / 2),
           shape2Top = shape2Rect.pos.y + (shape2Rect.height / 2);
    
    if (
      shape1Left < shape2Right && shape1Right > shape2Left &&
      shape1Bottom < shape2Top && shape1Top > shape2Bottom
    ) {
      return true;
    }
  }
  return false;
}
