#include "composite-shape.hpp"

#include <stdexcept>

luzhbinin::CompositeShape::CompositeShape() :
figures_(nullptr),
size_(0)
{
};

luzhbinin::CompositeShape::CompositeShape(const std::shared_ptr<Shape> & object) :
  figures_(new std::shared_ptr<Shape>[1]),
  size_(0)
{
  if (object == nullptr)
  {
    figures_.reset();
    throw std::invalid_argument("Invalid pointer");
  }
  figures_[0] = object;
  size_++;
}

luzhbinin::CompositeShape::CompositeShape(const CompositeShape & object) :
  figures_(new std::shared_ptr<Shape>[object.size_]),
  size_(object.size_)
{
  for (std::size_t i = 0; i < object.size_; i++)
  {
    figures_[i] = object.figures_[i];
  }
}

luzhbinin::CompositeShape::CompositeShape(CompositeShape && object) :
  figures_(std::move(object.figures_)),
  size_(object.size_)
{
  object.size_ = 0;
  object.figures_.reset();
}

std::shared_ptr<luzhbinin::Shape> luzhbinin::CompositeShape::operator [] (const std::size_t index) const
{
  if ((index >= size_))
  {
    throw std::out_of_range("Index is out of range");
  }
  return figures_[index];
}

luzhbinin::CompositeShape & luzhbinin::CompositeShape::operator = (const luzhbinin::CompositeShape & object)
{
  if (this == &object)
  {
    return *this;
  }
  std::unique_ptr <std::shared_ptr<luzhbinin::Shape>[]>
    newArray(new std::shared_ptr<luzhbinin::Shape>[size_]);
  for (std::size_t i = 0; i < size_; i++)
  {
    newArray[i] = object.figures_[i];
  }
  figures_.swap(newArray);
  return *this;
}

luzhbinin::CompositeShape & luzhbinin::CompositeShape::operator = (luzhbinin::CompositeShape && object)
{
  size_ = object.size_;
  figures_ = std::move(object.figures_);
  object.size_ = 0;
  return *this;
}

void luzhbinin::CompositeShape::addShape(const std::shared_ptr<Shape> & object)
{
  if (object == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> newArray(new std::shared_ptr<Shape>[size_ + 1]);
  for (std::size_t i = 0; i < size_; i++)
  {
    newArray[i] = figures_[i];
  }
  newArray[size_] = object;
  size_++;
  figures_.swap(newArray);
}

void luzhbinin::CompositeShape::delShape(const std::size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("Index is out of array size");
  }
  if ((size_ == 1) && (index == 0))
  {
    figures_.reset();
    size_ = 0;
    return;
  }
  std::unique_ptr<std::shared_ptr <Shape >[]> newArray(new std::shared_ptr<Shape>[size_ - 1]);
  for (std::size_t i = 0; i < index; i++)
  {
    newArray[i] = figures_[i];
  }
  for (std::size_t i = index; i < size_ - 1; ++i)
  {
    newArray[i] = figures_[i + 1];
  }
  figures_.swap(newArray);
  size_--;
}

std::size_t luzhbinin::CompositeShape::getSize() const
{
  return size_;
}

double luzhbinin::CompositeShape::getArea() const
{
  double area = 0.0;
  for (std::size_t i = 0; i < size_; i++)
  {
    area += figures_[i]->getArea();
  }
  return area;
}

luzhbinin::rectangle_t luzhbinin::CompositeShape::getFrameRect() const
{
  if (size_ <= 0)
  {
    return { { 0.0 , 0.0 }, 0.0 , 0.0 };
  }

  luzhbinin::rectangle_t Rectan = figures_[0]->getFrameRect();
  double minX = Rectan.pos.x - Rectan.width / 2;
  double maxX = Rectan.pos.x + Rectan.width / 2;
  double maxY = Rectan.pos.y + Rectan.height / 2;
  double minY = Rectan.pos.y - Rectan.height / 2;

  for (std::size_t i = 1; i < size_; i++)
  {
    Rectan = figures_[i]->getFrameRect();
    if ((Rectan.pos.x - Rectan.width / 2) < minX)
    {
      minX = Rectan.pos.x - Rectan.width / 2;
    }
    if ((Rectan.pos.x + Rectan.width / 2) > maxX)
    {
      maxX = Rectan.pos.x + Rectan.width / 2;
    }
    if ((Rectan.pos.y + Rectan.height / 2) > maxY)
    {
      maxY = Rectan.pos.y + Rectan.height / 2;
    }
    if ((Rectan.pos.y - Rectan.height / 2) < minY)
    {
      minY = Rectan.pos.y - Rectan.height / 2;
    }
  }
  return { { (minX + (maxX - minX) / 2), (minY + (maxY - minY) / 2) }, (maxX - minX), (maxY - minY) };
}

void luzhbinin::CompositeShape::move(const point_t & to)
{
  double dx = to.x - getFrameRect().pos.x;
  double dy = to.y - getFrameRect().pos.y;
  for (std::size_t i = 0; i < size_; i++)
  {
    figures_[i]->move(dx, dy);
  }
}

void luzhbinin::CompositeShape::move(double dx, double dy)
{
  for (std::size_t i = 0; i < size_; i++)
  {
    figures_[i]->move(dx, dy);
  }
}

void luzhbinin::CompositeShape::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Invalid shape ratio");
  }
  //luzhbinin::point_t initPos = getFrameRect().pos;
  for (std::size_t i = 0; i < size_; i++)
  {
    figures_[i]->scale(ratio);
    figures_[i]->move((figures_[i]->getFrameRect().pos.x - getFrameRect().pos.x) * ratio,
      (figures_[i]->getFrameRect().pos.y - getFrameRect().pos.y) * ratio);
  }
}
