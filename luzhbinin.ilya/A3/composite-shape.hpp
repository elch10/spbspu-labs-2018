#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace luzhbinin
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const std::shared_ptr<Shape> & object);
    CompositeShape(const CompositeShape & object);
    CompositeShape(CompositeShape && object);
    ~CompositeShape() = default;

    std::shared_ptr<Shape> operator [] (const std::size_t index) const;
    
    CompositeShape & operator = (const CompositeShape & object);
    CompositeShape & operator = (CompositeShape && object);
    
    void addShape(const std::shared_ptr<Shape> & object);
    void delShape(const std::size_t index);
    std::size_t getSize() const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & to) override;
    void move(double dx, double dy) override;
    void scale(double ratio) override;
    
  private:
    std::unique_ptr< std::shared_ptr<Shape>[]> figures_;
    std::size_t size_;
  };
}

#endif
