#define BOOST_TEST_MODULE MyTests

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include <cmath>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(CompositeShape)

BOOST_AUTO_TEST_CASE(MovingToPoint)
{
  std::shared_ptr<luzhbinin::Shape> rectangle =
    std::make_shared<luzhbinin::Rectangle>(luzhbinin::Rectangle({ { 25.0, 25.0 }, 5.0, 10.0 }));
  std::shared_ptr<luzhbinin::Shape> circle =
    std::make_shared<luzhbinin::Circle>(luzhbinin::Circle({ 50.0, 50.0 }, 10.0));
  luzhbinin::CompositeShape testShape;
  testShape.addShape(rectangle);
  testShape.addShape(circle);

  const luzhbinin::rectangle_t rectan = testShape.getFrameRect();
  const double area = testShape.getArea();

  testShape.move({ 0.0, 0.0 });

  BOOST_CHECK_EQUAL(area, testShape.getArea());
  BOOST_CHECK_EQUAL(rectan.width, testShape.getFrameRect().width);
  BOOST_CHECK_EQUAL(rectan.height, testShape.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(RelativeMoving)
{
  std::shared_ptr<luzhbinin::Shape> rectangle =
    std::make_shared<luzhbinin::Rectangle>(luzhbinin::Rectangle({ { 25.0, 25.0 }, 5.0, 10.0 }));
  std::shared_ptr<luzhbinin::Shape> circle =
    std::make_shared<luzhbinin::Circle>(luzhbinin::Circle({ 50.0, 50.0 }, 10.0));
  luzhbinin::CompositeShape testShape;
  testShape.addShape(rectangle);
  testShape.addShape(circle);

  const luzhbinin::rectangle_t rectan = testShape.getFrameRect();
  const double area = testShape.getArea();

  testShape.move( 10.0, 10.0 );

  BOOST_CHECK_EQUAL(area, testShape.getArea());
  BOOST_CHECK_EQUAL(rectan.width, testShape.getFrameRect().width);
  BOOST_CHECK_EQUAL(rectan.height, testShape.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(Scaling)
{
  std::shared_ptr<luzhbinin::Shape> rectangle =
    std::make_shared<luzhbinin::Rectangle>(luzhbinin::Rectangle({ { 25.0, 25.0 }, 5.0, 10.0 }));
  std::shared_ptr<luzhbinin::Shape> circle =
    std::make_shared<luzhbinin::Circle>(luzhbinin::Circle({ 50.0, 50.0 }, 10.0));
  luzhbinin::CompositeShape testShape;
  testShape.addShape(rectangle);
  testShape.addShape(circle);

  const double area = testShape.getArea();
  testShape.scale(5.0);

  BOOST_CHECK_CLOSE(5.0 * 5.0 * area, testShape.getArea(), 0.001);
}

BOOST_AUTO_TEST_CASE(InvalidScaling)
{
  std::shared_ptr<luzhbinin::Shape> rectangle =
    std::make_shared<luzhbinin::Rectangle>(luzhbinin::Rectangle({ { 25.0, 25.0 }, 5.0, 10.0 }));
  std::shared_ptr<luzhbinin::Shape> circle =
    std::make_shared<luzhbinin::Circle>(luzhbinin::Circle({ 50.0, 50.0 }, 10.0));
  luzhbinin::CompositeShape testShape;
  testShape.addShape(rectangle);
  testShape.addShape(circle);
  
  BOOST_CHECK_THROW(testShape.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestAddShape)
{
  std::shared_ptr<luzhbinin::Shape> rectangle =
    std::make_shared<luzhbinin::Rectangle>(luzhbinin::Rectangle({ { 25.0, 25.0 }, 5.0, 10.0 }));
  std::shared_ptr<luzhbinin::Shape> circle =
    std::make_shared<luzhbinin::Circle>(luzhbinin::Circle({ 50.0, 50.0 }, 10.0));
  luzhbinin::CompositeShape testShape;
  testShape.addShape(rectangle);
  testShape.addShape(circle);
  BOOST_CHECK_EQUAL(testShape.getSize(), 2);
}

BOOST_AUTO_TEST_CASE(TestDelShape)
{
  std::shared_ptr<luzhbinin::Shape> rectangle =
    std::make_shared<luzhbinin::Rectangle>(luzhbinin::Rectangle({ { 25.0, 25.0 }, 5.0, 10.0 }));
  std::shared_ptr<luzhbinin::Shape> circle =
    std::make_shared<luzhbinin::Circle>(luzhbinin::Circle({ 50.0, 50.0 }, 10.0));
  luzhbinin::CompositeShape testShape;
  testShape.addShape(rectangle);
  testShape.addShape(circle);
  testShape.delShape(0);
  BOOST_CHECK_EQUAL(testShape.getSize(), 1);
}

BOOST_AUTO_TEST_CASE(CommonArea)
{
  const luzhbinin::rectangle_t rectan = { {25.0, 25.0}, 5.0, 10.0 };
  const double radius = 5.0;
  std::shared_ptr<luzhbinin::Shape> rectangle =
    std::make_shared<luzhbinin::Rectangle>(luzhbinin::Rectangle(rectan));
  std::shared_ptr<luzhbinin::Shape> circle =
    std::make_shared<luzhbinin::Circle>(luzhbinin::Circle({ rectan.pos.x, rectan.pos.y }, radius));
  luzhbinin::CompositeShape testShape;
  testShape.addShape(rectangle);
  testShape.addShape(circle);
  BOOST_CHECK_CLOSE(testShape.getArea(), rectan.height * rectan.width + M_PI * radius * radius, 0.001);
}

BOOST_AUTO_TEST_SUITE_END()
