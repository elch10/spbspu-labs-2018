#define BOOST_TEST_MODULE MyTests
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(RectangleSuite)

BOOST_AUTO_TEST_CASE(MovingToPoint)
{
  luzhbinin::Rectangle rectangle{ { { 25.0, 25.0 }, 5.0, 10.0 } };
  luzhbinin::rectangle_t rectBeforeMove = rectangle.getFrameRect();
  double areaBeforeMove = rectangle.getArea();

  rectangle.move({ 0.0, 0.0 });

  BOOST_CHECK_EQUAL(areaBeforeMove, rectangle.getArea());
  BOOST_CHECK_EQUAL(rectBeforeMove.width, rectangle.getFrameRect().width);
  BOOST_CHECK_EQUAL(rectBeforeMove.height, rectangle.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(RelativeMoving)
{
  luzhbinin::Rectangle rectangle{ { { 25.0, 25.0 }, 5.0, 10.0 } };
  luzhbinin::rectangle_t rectBeforeMove = rectangle.getFrameRect();
  double areaBeforeMove = rectangle.getArea();

  rectangle.move( 10.0, 10.0 );

  BOOST_CHECK_EQUAL(areaBeforeMove, rectangle.getArea());
  BOOST_CHECK_EQUAL(rectBeforeMove.width, rectangle.getFrameRect().width);
  BOOST_CHECK_EQUAL(rectBeforeMove.height, rectangle.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(Scaling)
{
  luzhbinin::Rectangle rectangle{ { { 25.0, 25.0 }, 5.0, 10.0 } };
  double areaBeforeScale = rectangle.getArea();
  const double ratio = 2.0;

  rectangle.scale(ratio);

  BOOST_CHECK_CLOSE(ratio * ratio * areaBeforeScale, rectangle.getArea(), 0.001);
}

BOOST_AUTO_TEST_CASE(InvalidRectangleParametr)
{
  BOOST_CHECK_THROW( luzhbinin::Rectangle( { { 25.0, 25.0 }, 5.0, -10.0 } ), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
{
  luzhbinin::Rectangle rectangle{ { { 25.0, 25.0 }, 5.0, 10.0 } };
  BOOST_CHECK_THROW(rectangle.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CircleSuite)

BOOST_AUTO_TEST_CASE(MovingToPoint)
{
  luzhbinin::Circle circle{ { 50.0, 50.0 }, 10.0 };
  luzhbinin::rectangle_t rectBeforeMove = circle.getFrameRect();
  double areaBeforeMove = circle.getArea();

  circle.move({ 0.0, 0.0 });

  BOOST_CHECK_EQUAL(areaBeforeMove, circle.getArea());
  BOOST_CHECK_EQUAL(rectBeforeMove.width, circle.getFrameRect().width);
  BOOST_CHECK_EQUAL(rectBeforeMove.height, circle.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(RelativeMoving)
{
  luzhbinin::Circle circle{ { 50.0, 50.0 }, 10.0 };
  luzhbinin::rectangle_t rectBeforeMove = circle.getFrameRect();
  double areaBeforeMove = circle.getArea();

  circle.move(-10.0, -10.0);

  BOOST_CHECK_EQUAL(areaBeforeMove, circle.getArea());
  BOOST_CHECK_EQUAL(rectBeforeMove.width, circle.getFrameRect().width);
  BOOST_CHECK_EQUAL(rectBeforeMove.height, circle.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(Scaling)
{
  luzhbinin::Circle circle{ { 50.0, 50.0 }, 10.0 };
  double areaBeforeScale = circle.getArea();
  const double ratio = 2.0;

  circle.scale(ratio);

  BOOST_CHECK_CLOSE(ratio * ratio * areaBeforeScale, circle.getArea(), 0.001);
}

BOOST_AUTO_TEST_CASE(InvalidcircleParametr)
{
  BOOST_CHECK_THROW(luzhbinin::Circle( { 50.0, 50.0 }, -10.0 ); , std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
{
  luzhbinin::Circle circle{ { 50.0, 50.0 }, 10.0 };
  BOOST_CHECK_THROW(circle.scale(-5.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
