#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace stashevskii;

void print(const CompositeShape &shapes, const char *name)
{
  const rectangle_t obj = shapes.getFrameRect();
  std::cout << name <<std::endl;
  std::cout << "Width: " << obj.width << " Height: " << obj.height<< std::endl;
  std::cout << "X: " << obj.pos.x << " Y: " << obj.pos.y << std::endl;
  std::cout << "Area:" << shapes.getArea() << std::endl;
  std::cout << std::endl;
}


int main()
{
  CompositeShape objShapes;
  objShapes.addShape(std::shared_ptr< Shape > (new Rectangle {47, 10, {47, 50}}));
  objShapes.addShape(std::shared_ptr< Shape > (new Circle {10, {47, 50}}));
  print(objShapes, "objShapes");

  CompositeShape objShapes2;
  objShapes2.addShape(std::shared_ptr< Shape > (new Rectangle {105, 150, {47, 55}}));
  objShapes2.addShape(std::shared_ptr< Shape > (new Circle {105, {47, 55}}));
  print(objShapes2, "objShapes2");

  CompositeShape objShapes3(objShapes);
  objShapes2 = std::move(objShapes);
  objShapes3.deleteShape(1);
  print(objShapes3, "objShapes3");

  objShapes3.deleteAll();
  print(objShapes3, "objShapes3 deleted");

  Circle testCircle1 (10.0, { -10.0, 0.0 });
  Circle testCircle2 (30.0, { 40.0, 30.0 });
  Rectangle testRectangle1 (30.0, 40.0, { 20.0, 30.0 });
  Rectangle testRectangle2 (30.0, 40.0, { 30.0, 00.0 });
  
  std::shared_ptr < Shape > circlePtrM1 = std::make_shared < Circle > (testCircle1);
  std::shared_ptr < Shape > circlePtrM2 = std::make_shared < Circle > (testCircle2);
  std::shared_ptr < Shape > rectanglePtrM1 = std::make_shared < Rectangle > (testRectangle1);
  std::shared_ptr < Shape > rectanglePtrM2 = std::make_shared < Rectangle > (testRectangle2);

  Matrix testMatrix(circlePtrM1);
  testMatrix.addShape(rectanglePtrM1);
  testMatrix.addShape(rectanglePtrM2);
  testMatrix.addShape(circlePtrM2);
  std::unique_ptr < std::shared_ptr < Shape > [] > layer0 = testMatrix[0];
  std::unique_ptr < std::shared_ptr < Shape > [] > layer1 = testMatrix[1];
  std::unique_ptr < std::shared_ptr < Shape > [] > layer2 = testMatrix[2];

  return 0;
}
