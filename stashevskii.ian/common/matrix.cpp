#include "matrix.hpp"

#include <memory>
#include <iostream>

using namespace stashevskii;

Matrix::Matrix(const std::shared_ptr < Shape > &shape ):
  shapes_(nullptr),
  amount_(0),
  size_(0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }
  addShape(shape);
}

Matrix::Matrix(const Matrix & matrix):
  shapes_(new std::shared_ptr < Shape >[matrix.amount_ * matrix.size_]),
  amount_(matrix.amount_),
  size_(matrix.size_)
{
  for (int i = 0; i < matrix.amount_ * matrix.size_; ++i)
  {
    shapes_[i] = matrix.shapes_[i];
  }
}

Matrix::Matrix(Matrix && matrix):
  shapes_(nullptr),
  amount_(matrix.amount_),
  size_(matrix.size_)
{
  shapes_.swap(matrix.shapes_);
  matrix.amount_ = 0;
  matrix.size_ = 0;
}

Matrix::~Matrix()
{
  shapes_.reset();
  shapes_ = nullptr;
  amount_ = 0;
  size_ = 0;
}

Matrix & Matrix::operator =(const Matrix & matrix)
{
  if (this != & matrix)
  {
    amount_ = matrix.amount_;
    size_ = matrix.size_;
    std::unique_ptr < std::shared_ptr < Shape >[] > 
        newList (new std::shared_ptr < Shape >[matrix.amount_ * matrix.size_]);
    for (int i = 0; i < matrix.amount_ * matrix.size_; ++i)
    {
      newList[i] = matrix.shapes_[i];
    }
    shapes_.swap(newList);
  }
  return *this;
}

Matrix & Matrix::operator =(Matrix && matrix)
{
  if (this != & matrix)
  {
    amount_ = matrix.amount_;
    size_ = matrix.size_;
    shapes_.reset();
    shapes_.swap(matrix.shapes_);
    matrix.amount_ = 0;
    matrix.size_ = 0;
  }
  return *this;
}

std::unique_ptr < std::shared_ptr < Shape >[] > Matrix::operator [](const int layerIndex) const
{
  if (amount_ == 0)
  {
    throw std::out_of_range("empty matrix");
  }
  if ((layerIndex < 0) || (layerIndex > amount_ - 1))
  {
    throw std::invalid_argument("invalid layer index");
  }
  std::unique_ptr < std::shared_ptr < Shape >[] > 
      layer (new std::shared_ptr < Shape >[size_]);
  for (int i = 0; i < size_; ++i)
  {
    layer[i] = shapes_[layerIndex * size_ + i];
  }
  return layer;
}

int Matrix::getLayersNumber() const
{
  return amount_;
}

int Matrix::getLayerSize() const
{
  return size_;
}

void Matrix::addShape(const std::shared_ptr < Shape > new_shape)
{
  if (amount_ == 0)
  {
    ++amount_;
    ++size_;
    std::unique_ptr < std::shared_ptr < Shape >[] > 
        newList (new std::shared_ptr < Shape >[amount_ * size_]);
    shapes_.swap(newList);
    shapes_[0] = new_shape;
  }
  else
  {
    bool addedShape = false;
    for (int i = 0; !addedShape; ++i)
    {
      for (int j = 0; j < size_; ++j)
      {
        if (shapes_[i * size_ + j] == nullptr)
        {
          shapes_[i * size_ + j] = new_shape;
          addedShape = true;
          break;
        }
        else
        {
          if (checkOverlapping(i * size_ + j, new_shape))
          {
            break;
          }
        }
        if (j == (size_ - 1))
        {
          ++size_;
          std::unique_ptr < std::shared_ptr < Shape >[] > 
              newList (new std::shared_ptr < Shape >[amount_ * size_]);
          for (int n = 0; n < amount_; ++n)
          {
            for (int m = 0; m < size_ - 1; ++m)
            {
              newList[n * size_ + m] = shapes_[n * (size_ - 1) + m];
            }
            newList[(n + 1) * size_ - 1] = nullptr;
          }
          newList[(i + 1) * size_ - 1] = new_shape;
          shapes_.swap(newList);
          addedShape = true;
          break;
        }
      }
      if ((i == (amount_ - 1)) && !(addedShape))
      {
        ++amount_;
        std::unique_ptr < std::shared_ptr < Shape >[] > 
            newList (new std::shared_ptr < Shape >[amount_ * size_]);
        for (int n = 0; n < (amount_ - 1) * size_; ++n)
        {
          newList[n] = shapes_[n];
        }
        for (int n = (amount_ - 1) * size_; n < amount_ * size_; ++n)
        {
          newList[n] = nullptr;
        }
        newList[(amount_ - 1) * size_] = new_shape;
        shapes_.swap(newList);
        addedShape = true;
      }
    }
  }
}

bool Matrix::checkOverlapping(const int index, std::shared_ptr < Shape > new_shape) const
{
  rectangle_t newShapeFrameRect = new_shape->getFrameRect();
  rectangle_t matrixShapeFrameRect = shapes_[index]->getFrameRect();

  point_t newShapePoints[4] = 
  {
    { newShapeFrameRect.pos.x - newShapeFrameRect.width / 2.0, 
        newShapeFrameRect.pos.y + newShapeFrameRect.height / 2.0 },
    { newShapeFrameRect.pos.x + newShapeFrameRect.width / 2.0, 
        newShapeFrameRect.pos.y + newShapeFrameRect.height / 2.0 },
    { newShapeFrameRect.pos.x + newShapeFrameRect.width / 2.0, 
        newShapeFrameRect.pos.y - newShapeFrameRect.height / 2.0 },
    { newShapeFrameRect.pos.x - newShapeFrameRect.width / 2.0, 
        newShapeFrameRect.pos.y - newShapeFrameRect.height / 2.0 },
  };

  point_t matrixShapePoints[4] = 
  {
    { matrixShapeFrameRect.pos.x - matrixShapeFrameRect.width / 2.0, 
        matrixShapeFrameRect.pos.y + matrixShapeFrameRect.height / 2.0 },
    { matrixShapeFrameRect.pos.x + matrixShapeFrameRect.width / 2.0, 
        matrixShapeFrameRect.pos.y + matrixShapeFrameRect.height / 2.0 },
    { matrixShapeFrameRect.pos.x + matrixShapeFrameRect.width / 2.0, 
        matrixShapeFrameRect.pos.y - matrixShapeFrameRect.height / 2.0 },
    { matrixShapeFrameRect.pos.x - matrixShapeFrameRect.width / 2.0, 
        matrixShapeFrameRect.pos.y - matrixShapeFrameRect.height / 2.0 },
  };

  for (int i = 0; i < 4; ++i)
  {
    if (((newShapePoints[i].x >= matrixShapePoints[0].x) && (newShapePoints[i].x <= matrixShapePoints[2].x)
           && (newShapePoints[i].y >= matrixShapePoints[3].y) && (newShapePoints[i].y <= newShapePoints[1].y))
           || ((matrixShapePoints[i].x >= newShapePoints[0].x) && (matrixShapePoints[i].x <= newShapePoints[2].x)
           && (matrixShapePoints[i].y >= newShapePoints[3].y) && (matrixShapePoints[i].y <= newShapePoints[1].y)))
    {
      return true;
    }
  }
  return false;
}
