#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

#include <memory>

namespace stashevskii
{
  class Matrix
  {
  public:
    Matrix(const std::shared_ptr < Shape > &shape);
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix);
    ~Matrix();

    Matrix & operator =(const Matrix & matrix);
    Matrix & operator =(Matrix && matrix);
    std::unique_ptr < std::shared_ptr < Shape > [] > operator [](const int layerIndex) const;

    int getLayersNumber() const;
    int getLayerSize() const;
    void addShape(const std::shared_ptr < Shape > new_shape);

  private:
    std::unique_ptr < std::shared_ptr < Shape > [] > shapes_;
    int amount_;
    int size_;

    bool checkOverlapping(const int index, std::shared_ptr < Shape > new_shape) const;
  };
}

#endif
