#define BOOST_TEST_MAIN

#include <cmath>
#include <boost/test/included/unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"


BOOST_AUTO_TEST_SUITE(Tests_Composite_Shape)
  const double accuracy = 0.01;

  BOOST_AUTO_TEST_CASE(Test_Get_Frame_Rect)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 10, accuracy);
  }

  BOOST_AUTO_TEST_CASE(Test_Move_By_X_Y)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    comp.move(10, 20);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 20, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 30, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getArea(), circ.getArea() + rect.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(Test_Move_To_Point)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    comp.move({20, 10});
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 20, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getArea(), circ.getArea() + rect.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(Test_Information_Move)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    sorvenkov::CompositeShape comp1 = std::move(comp);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().height, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().width, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.x, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.y, 10, accuracy);
  }

   BOOST_AUTO_TEST_CASE(Test_Information_Operator_Move)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    sorvenkov::CompositeShape comp1(rectPtr);
    comp = std::move(comp1);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, 20, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, 0, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 30, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 20, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().height, 0, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().width, 0, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.x, 0, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.y, 0, accuracy);
  }

  BOOST_AUTO_TEST_CASE(Test_Information_Copy)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    sorvenkov::CompositeShape comp1 = comp;
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().height, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().width, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.x, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.y, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, comp1.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, comp1.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, comp1.getFrameRect().pos.x, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, comp1.getFrameRect().pos.y, accuracy);
  }

  BOOST_AUTO_TEST_CASE(Test_Change_Area_Scale)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    comp.scale(5);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, 200, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, 200, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getShape(0)->getFrameRect().pos.x, -40, accuracy); 
    BOOST_CHECK_CLOSE_FRACTION(comp.getShape(0)->getFrameRect().pos.y, -40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getShape(1)->getFrameRect().pos.x, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getShape(1)->getFrameRect().pos.y, 60, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getArea(), (circ.getArea() + rect.getArea()) * 5 * 5, accuracy);
  }

 BOOST_AUTO_TEST_CASE(Test_Information_Operator_Copy)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    sorvenkov::CompositeShape comp1(rectPtr);
    comp = comp1;
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, 20, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, 0, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 30, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 20, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, comp1.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, comp1.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, comp1.getFrameRect().pos.x, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, comp1.getFrameRect().pos.y, accuracy);
  }

  BOOST_AUTO_TEST_CASE(Test_Remove)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    comp.remove(0);
    BOOST_CHECK_CLOSE_FRACTION(comp.getArea(), 800, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 30, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 20, accuracy);
    BOOST_REQUIRE_EQUAL(comp.getSize(), 1);
  }

  BOOST_AUTO_TEST_CASE(Test_Invalid_Scale)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    BOOST_CHECK_THROW(comp.scale(-5), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Test_Invalid_Removing)
  {
    sorvenkov::Circle circ(10, {0, 0});
    sorvenkov::Rectangle rect (40,20,{10,20});
    std::shared_ptr <sorvenkov::Shape> circPtr = std::make_shared <sorvenkov::Circle> (circ);
    std::shared_ptr <sorvenkov::Shape> rectPtr = std::make_shared <sorvenkov::Rectangle> (rect);
    sorvenkov::CompositeShape comp(circPtr);
    comp.add(rectPtr);
    BOOST_CHECK_THROW(comp.remove(-5), std::out_of_range);
  }


BOOST_AUTO_TEST_SUITE_END()
