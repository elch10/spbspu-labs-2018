#include <iostream>
#include <string>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using namespace sorvenkov;

void OutInf(Shape& shp)
{
  std::cout << "Width:  " << shp.getFrameRect().width << std::endl
  << "Height: " << shp.getFrameRect().height << std::endl
  << "Area:   " << shp.getArea() << std::endl
  << "Pos(x): " << shp.getFrameRect().pos.x << std::endl
  << "Pos(y): " << shp.getFrameRect().pos.y << std::endl
  << "_____________________ " << std::endl;
}

int main()
{
  sorvenkov::Rectangle rect(4.0, 5.0, {2.0, 3.0});
  sorvenkov::Circle circ(4.0, {2.0, 3.0});
  sorvenkov::Triangle tri({0.0, 0.0}, {2.0, 4.0}, {4.0, 0.0});

  std::shared_ptr<sorvenkov::Shape> circ_ptr = std::make_shared<sorvenkov::Circle>(circ);
  std::shared_ptr<sorvenkov::Shape> rect_ptr = std::make_shared<sorvenkov::Rectangle>(rect);
  std::shared_ptr<sorvenkov::Shape> tri_ptr = std::make_shared<sorvenkov::Triangle>(tri);

  sorvenkov::CompositeShape compos(circ_ptr);
    
  compos.add(rect_ptr);
  compos.add(circ_ptr);
  OutInf(compos);

  std::cout << "Scaling" << std::endl;
  
  compos.scale(4.0);
  OutInf(compos);

  std::cout << "Move by dx, dy" << std::endl;
  
  compos.move(2.0, 2.0);
  OutInf(compos);

  std::cout << "Move to point" << std::endl;
  
  compos.move({2.0, 2.0});
  OutInf(compos);

  std::cout << "Delete circle" << std::endl;
  
  compos.remove(2.0);
  OutInf(compos);

  return 0;
}
