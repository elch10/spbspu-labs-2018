#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "shape.hpp"
#include "rectangle.hpp"

namespace sorvenkov
{
  class CompositeShape : public Shape
  {
  public:
    explicit CompositeShape(std::shared_ptr<sorvenkov::Shape> shape);
    CompositeShape(const CompositeShape &copy);
    CompositeShape(CompositeShape &&copy) noexcept;
    ~CompositeShape() override = default;
    CompositeShape &operator=(const CompositeShape &shape);
    CompositeShape &operator=(CompositeShape &&copy) noexcept;
    std::shared_ptr<sorvenkov::Shape> &operator[](size_t index) const;

    double getArea() const override;
    point_t getCenter() const;
    rectangle_t getFrameRect() const override;
    void move(const point_t &center) override;
    void move(double dx, double dy) override;
    std::shared_ptr < sorvenkov::Shape > getShape(int const size_t) const;
    void scale(double ratio) override;
    void rotate(const double angle) override;
    int getSize() const noexcept;
    double getAngle() const noexcept;
    void add(std::shared_ptr<sorvenkov::Shape> shape);
    void remove(size_t i);
    size_t size() const;


  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> subpart_;
    size_t size_;
    double angle_;
  };
}
#endif
