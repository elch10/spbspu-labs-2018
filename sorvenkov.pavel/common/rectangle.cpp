#include "rectangle.hpp"

#include <iostream>
#include <cmath>
#include <math.h>

using namespace sorvenkov;

Rectangle::Rectangle(const double width, const double height, const point_t& pos) :
  width_(width),
  height_(height),
  center_(pos),
  angle_(0)
{
  if (width < 0.0 || height < 0.0) 
  {
    throw std::invalid_argument("Width and height of the rectangle must be greater than zero");
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

sorvenkov::rectangle_t sorvenkov::Rectangle::getFrameRect() const
{
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const double width = fabs(width_ * cosine) + fabs(height_ * sine);
  const double height =  fabs(height_ * cosine) + fabs(width_ * sine);
  return {width, height, center_};
}

point_t Rectangle::getCenter() const
{
  return center_;
}

void Rectangle::move(const point_t& pos)
{
  center_ = pos;
}

void Rectangle::move(double Ox, double Oy)
{
  center_.x += Ox;
  center_.y += Oy;
}
void Rectangle::scale(double ratio)
{
  if (ratio < 0) 
  {
    throw std::invalid_argument("Scale ratio must be greater than zero");
  }
  else
  {
  width_ *= ratio;
  height_ *= ratio;
  }
}

double sorvenkov::Rectangle::getAngle() const noexcept
{
  return angle_;
}

void sorvenkov::Rectangle::rotate(const double ang)
{
  angle_ += ang;
  if (fabs(angle_) >= 360)
  {
    angle_ = fmod(angle_, 360);
  }
}
