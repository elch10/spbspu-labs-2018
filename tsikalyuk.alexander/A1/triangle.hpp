#ifndef triangle_hpp
#define triangle_hpp
#include "shape.hpp"
#include "base-types.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t &point1, const point_t &point2, const point_t &point3);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const double dX,const double dY) override;
  void move(const point_t point) override;
  void dataoutput() const override; 
private:
  point_t dot1_;
  point_t dot2_;
  point_t dot3_;
  point_t pos_;
};

#endif
