#include <iostream>
#include "shape.hpp"
#include "base-types.hpp"
#include "rectangle.hpp"

Rectangle::Rectangle(double w, double h, double xrect, double yrect):
  rctngl_{w,h,{xrect,yrect}}
{}

double Rectangle::getArea() const 
{
  return rctngl_.height*rctngl_.width;
}

rectangle_t Rectangle::getFrameRect() const  
{
  return {rctngl_.height , rctngl_.width , rctngl_.pos.x , rctngl_.pos.y};
}

void Rectangle::move(const double dX,const double dY) 
{
  rctngl_.pos.x += dX;
  rctngl_.pos.y += dY;
}

void Rectangle::move(const point_t point)
{
  rctngl_.pos.x = point.x;
  rctngl_.pos.y = point.y;
}

void Rectangle::dataoutput() const 
{
  std::cout<<"area_of_rectangle_="<<getArea()<<std::endl;
  std::cout<<"framerect_width_height_X_Y_="<<getFrameRect().width<<" "<<getFrameRect().height<<" ";
  std::cout<<getFrameRect().pos.x<<" "<<getFrameRect().pos.y<<std::endl;
  std::cout<<"pos_ition_X_Y_"<<rctngl_.pos.x<<" "<<rctngl_.pos.y<<std::endl;
}
