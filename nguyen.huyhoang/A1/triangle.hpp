#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

class Triangle: public Shape
{
  public:
    Triangle(point_t poA, point_t poB, point_t poC);
    point_t getCenter() const override;
    void move(const point_t& c) override;
    void move(const double& dx, const double& dy) override;
    void getLengths () const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void getShapeName() override;
  private:
    point_t a_;
    point_t b_;
    point_t c_;
    point_t g_;
    double ab_;
    double bc_;
    double ca_;
};
#endif
