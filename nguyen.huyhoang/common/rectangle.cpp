#include "rectangle.hpp"
#include <iostream>
#include <cmath>

nguyen::Rectangle::Rectangle(point_t pos, double width, double lenght):
  wid_(width),
  hei_(lenght),
  cen_(pos),
  angle_(0)
{
  if (wid_ <= 0) 
  {
    throw std::invalid_argument("Invalid rectangle width");
  }
  if (hei_ <= 0) 
  {
    throw std::invalid_argument("Invalid rectangle height");
  }
}

double nguyen::Rectangle::getArea() const
{
  return wid_*hei_;
}

nguyen::point_t nguyen::Rectangle::getCenter() const
{
  return cen_;
}

double nguyen::Rectangle::getHeight() const
{
  return hei_;
}

double nguyen::Rectangle::getWidth() const
{
  return wid_;
}

nguyen::rectangle_t nguyen::Rectangle::getFrameRect() const
{
  const double ang = fmod(angle_, 90);
  const double sinan = sin(ang*M_PI/180);
  const double cosan = cos(ang*M_PI/180);
  const double width = wid_*cosan + hei_*sinan;
  const double height = hei_*cosan + wid_*sinan;
  return {cen_, width, height};
}

void nguyen::Rectangle::move(const point_t& c)
{
  cen_ = c;
}

void nguyen::Rectangle::move(const double& dx, const double& dy)
{
  cen_.x += dx;
  cen_.y += dy;
}

void nguyen::Rectangle::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("Invalid scale of the rectangle");
  }
  wid_ *= k;
  hei_ *= k;
}

void nguyen::Rectangle::rotate(double deg)
{
  angle_ += deg;
  angle_ = fmod(angle_, 360);
}

void nguyen::Rectangle::getInfoShape()
{
  std::cout << "This shape is Rectangle:" << std::endl;
  std::cout << "The center of the rectangle is: (" << cen_.x
      << ";" << cen_.y << ")" << std::endl;
  std::cout << "The width of the rectangle is " << wid_ << std::endl;
  std::cout << "The height of the rectangle is " << hei_ << std::endl;
  std::cout << "The area of the rectangle is " << this->getArea() << std::endl;
  rectangle_t fram = this->getFrameRect();
  std::cout << "The frame of the rectangle is: Center (" << fram.pos.x
      << ";" << fram.pos.y << ") and Width " << fram.width
          << " and Height " << fram.height << std::endl;
  std::cout << "The current angle is:" << angle_ << std::endl;
}
