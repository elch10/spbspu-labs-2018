#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

nguyen::CompositeShape::CompositeShape(const std::shared_ptr<Shape>& obj):
  arr_(new std::shared_ptr<Shape> [1]),
  size_(1),
  angle_(0)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Invalid array of object");
  }
  arr_[0] = obj;
}

nguyen::CompositeShape::CompositeShape(const CompositeShape& arobj):
  arr_((arobj.size_!=0) ? new Shape::ptr_type[arobj.size_] : nullptr),
  size_(arobj.size_)
{
  for (int i=0; i<size_; ++i)
  {
    arr_[i]=arobj.arr_[i];
  }
}

nguyen::CompositeShape::~CompositeShape()
{
  arr_.reset();
  size_=0;
  arr_ = nullptr;
}

double nguyen::CompositeShape::getArea() const
{
  double sumArea = 0;
  for (int i=0; i<size_; ++i)
  {
    sumArea += arr_[i]->getArea();
  }
  return sumArea;
}

nguyen::rectangle_t nguyen::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    return {{0,0},0,0};
  }
  else
  {
    rectangle_t rect = arr_[0]->getFrameRect();
    double left = rect.pos.x - rect.width/2;
    double right = rect.pos.x + rect.width/2;
    double beneath = rect.pos.y + rect.height/2;
    double under = rect.pos.y - rect.height/2;
    for (int i=1; i<size_; ++i)
    {
      rect = arr_[i]->getFrameRect();
      if ((rect.pos.x - rect.width/2) < left)
      {
        left = rect.pos.x - rect.width/2;
      }
      if ((rect.pos.x + rect.width/2) > right)
      {
        right = rect.pos.x + rect.width/2;
      }
      if ((rect.pos.y - rect.height/2) < under)
      {
        under = rect.pos.y - rect.height/2;
      }
      if ((rect.pos.y + rect.height/2) > beneath)
      {
        beneath = rect.pos.y + rect.height/2;
      }
    }
    point_t cenR;
    cenR.x = (right + left)/2;
    cenR.y = (beneath + under)/2;
    return {cenR, right - left, beneath - under};
  }
}
 
void nguyen::CompositeShape::move(const point_t& c)
{
  const point_t curr = this->getCenter();
  this->move(c.x-curr.x, c.y-curr.y);
}
 
void nguyen::CompositeShape::move(const double& dx, const double& dy)
{
  for (int i=0; i<size_; ++i)
  {
    arr_[i]->move(dx,dy);
  }
}

void nguyen::CompositeShape::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("Invalid scale");
  }
  const point_t curr = this->getCenter();
  for (int i=0; i<size_; ++i)
  {
    const point_t thiscen = arr_[i]->getCenter();
    arr_[i]->move({curr.x+k*(thiscen.x-curr.x), curr.y+k*(thiscen.y-curr.y)});  
    arr_[i]->scale(k);
  }
}

void nguyen::CompositeShape::addElem(const std::shared_ptr<Shape>& obj)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Wrong object pointer");
  }
  for (int i=0; i<size_; ++i)
  {
    if (arr_[i] == obj)
    {
      throw std::invalid_argument("The object is already exist");
    }
  }
  std::unique_ptr<std::shared_ptr<Shape> []> newShape(new std::shared_ptr<Shape> [size_ +1]);
  for (int i=0; i<size_; ++i)
  {
    newShape[i] = arr_[i];
  }
  newShape[size_++] = obj;
  arr_.swap(newShape);
}

void nguyen::CompositeShape::delElem(const int& index)
{
  if (size_ == 0)
  {
    throw std::out_of_range("There is already no objects in the array");
  }
  if ((index >= size_)|| (index < 0))
  {
    throw std::out_of_range("Invalid index");
  }
  if ((size_ == 1) && (index == 0))
  {
    arr_.reset();
    arr_ = nullptr;
    size_ = 0;
    return;
  }
  std::unique_ptr<std::shared_ptr<Shape> []> newShape(new std::shared_ptr<Shape> [size_ - 1]);
  for (int i=0; i<index; ++i)
  {
    newShape[i] = arr_[i];
  }
  for (int i = index; i<size_-1; ++i)
  {
    newShape[i] = arr_[i+1];
  }
  arr_.swap(newShape);
  size_--;
}

int nguyen::CompositeShape::getSize() const
{
  return size_;
}

nguyen::point_t nguyen::CompositeShape::getCenter() const
{
  return this->getFrameRect().pos;
}

void nguyen::CompositeShape::rotate(double deg)
{
  angle_ += deg;
  angle_ = fmod(angle_, 360);
  point_t curr = this->getFrameRect().pos;
  const double sindeg = sin(deg*M_PI/180);
  const double cosdeg = cos(deg*M_PI/180);
  for (int i=0; i<size_; ++i)
  {
    arr_[i]->rotate(deg);
    const point_t shapos = arr_[i]->getCenter();
    arr_[i]->move({curr.x+(shapos.x-curr.x)*cosdeg-(shapos.y-curr.y)*sindeg, curr.y
        +(shapos.x -curr.x)*sindeg+(shapos.y -curr.y)*cosdeg});
  }
}

void nguyen::CompositeShape::getInfoShape()
{
  std::cout << "For this composite of shapes:" << std::endl;
  std::cout << "There is(are) " << this->getSize() << " shape(s) in this composite." << std::endl;
  std::cout << "The area of the composite is:" << this->getArea() << std::endl;
  const rectangle_t fram = this->getFrameRect();
  std::cout << "The frame of the composite is: Center (" << fram.pos.x
      << ";" << fram.pos.y << ") and Width " << fram.width
          << " and Height " << fram.height << std::endl;
  std::cout << "------For more information------" << std::endl;
  for (int i=0; i<size_; ++i)
  {
    std::cout << "---The shape " << i+1 << "---" << std::endl; 
    arr_[i]->getInfoShape();
  }
}
