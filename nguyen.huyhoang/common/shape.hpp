#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace nguyen
{
class Shape
{
  public:
    using ptr_type = std::shared_ptr<Shape>;
    virtual ~Shape() = default; 
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual point_t getCenter() const = 0;
    virtual void scale(double k) = 0;
    virtual void rotate(double deg) = 0;
    //Move to certain point
    virtual void move(const point_t& c) = 0;
    //Move the distance dx,dy
    virtual void move(const double& dx, const double& dy) = 0;
    virtual void getInfoShape() = 0;
};
}
#endif
