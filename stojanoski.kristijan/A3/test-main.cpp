#define BOOST_TEST_MODULE LAB_A3
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <math.h>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(simple_shapes)
BOOST_AUTO_TEST_CASE(rectangle_move_dxy)
{
  stojanoski::Rectangle r({ -9, 27 }, 15, 29);
  const double width_before = r.getFrameRect().width;
  const double height_before = r.getFrameRect().height;
  const double area_before = r.getArea();
  r.move(6.66, 32.38);
  BOOST_CHECK_CLOSE(width_before, r.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, r.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, r.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(rectangle_move_point)
{
  stojanoski::Rectangle r({ -91, 127.4 }, 15.8, 1.29);
  const double width_before = r.getFrameRect().width;
  const double height_before = r.getFrameRect().height;
  const double area_before = r.getArea();
  r.move({ 1.81, -57.11 });
  BOOST_CHECK_CLOSE(width_before, r.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, r.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, r.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(rectangle_scale)
{
  stojanoski::Rectangle r({ 13,18 }, 8, 5);
  const double area_before = r.getArea();
  const double k = 1.8;
  r.scale(k);
  BOOST_CHECK_CLOSE(area_before * k * k, r.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(rectangle_invalid_constructor)
{
  BOOST_CHECK_THROW(stojanoski::Rectangle({ 1, 1 }, -1, 0), std::invalid_argument);
  BOOST_CHECK_THROW(stojanoski::Rectangle({ 1, 1 }, 1, -2), std::invalid_argument);
  BOOST_CHECK_THROW(stojanoski::Rectangle({ 1, 1 }, -1, 2), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(rectangle_invalid_scale)
{
  stojanoski::Rectangle r({ 1, 5 }, 10, 12);
  BOOST_CHECK_THROW(r.scale(-3), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(circle_move_dxy)
{
  stojanoski::Circle c({ -9, 27 }, 20);
  const double width_before = c.getFrameRect().width;
  const double height_before = c.getFrameRect().height;
  const double area_before = c.getArea();
  c.move(5.51, 0);
  BOOST_CHECK_CLOSE(width_before, c.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, c.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, c.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(circle_move_point)
{
  stojanoski::Circle c({ -91, 127.4 }, 2);
  const double width_before = c.getFrameRect().width;
  const double height_before = c.getFrameRect().height;
  const double area_before = c.getArea();
  c.move({ 1.81, -57.11 });
  BOOST_CHECK_CLOSE(width_before, c.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, c.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, c.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(circle_invalid_constructor)
{
  BOOST_CHECK_THROW(stojanoski::Circle({ 1, 1 }, -10), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(circle_invalid_scale)
{
  stojanoski::Circle c({ 1, 5 }, 5);
  BOOST_CHECK_THROW(c.scale(-2), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(triangle_move_dxy)
{
  stojanoski::Triangle t({ -9, 27 }, { 1,-1 }, { 15,81 });
  const double width_before = t.getFrameRect().width;
  const double height_before = t.getFrameRect().height;
  const double area_before = t.getArea();
  t.move(-3.316, 1.257);
  BOOST_CHECK_CLOSE(width_before, t.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, t.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, t.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(triangle_move_point)
{
  stojanoski::Triangle t({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  const double width_before = t.getFrameRect().width;
  const double height_before = t.getFrameRect().height;
  const double area_before = t.getArea();
  t.move({ 45.8, -3.38 });
  BOOST_CHECK_CLOSE(width_before, t.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, t.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, t.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(triangle_scale)
{
  stojanoski::Triangle t({ 13,18 }, { 5, 22 }, { 12, 2 });
  const double area_before = t.getArea();
  const double k = 1.82;
  t.scale(k);
  BOOST_CHECK_CLOSE(area_before * k * k, t.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(triangle_invalid_constructor)
{
  BOOST_CHECK_THROW(stojanoski::Triangle({ 1, 1 }, { 1, 1 }, { 5, 0 }), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(triangle_invalid_scale)
{
  stojanoski::Triangle t({ 1, 5 }, { 5, 1 }, { 0, 0 });
  BOOST_CHECK_THROW(t.scale(-15), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(compositeShape_matching_shape)
{
  stojanoski::Circle c({ 11,4 }, 8);
  stojanoski::CompositeShape cs;
  cs.addShape(c);
  BOOST_CHECK_CLOSE(cs.getArea(), c.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().height, c.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().width, c.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.x, 11, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.y, 4, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_1)
{
  stojanoski::Circle circ({ 61, 18 }, 7.23);
  stojanoski::Triangle triang({ -61.5, 2 }, { 71, 0 }, { 14.4, 3.14 });
  stojanoski::Rectangle rect({ 1, 5 }, 10, 12);

  stojanoski::CompositeShape compshape;
  compshape.addShape(circ);
  compshape.addShape(triang);
  compshape.addShape(rect);
  BOOST_CHECK_CLOSE(compshape.getArea(), circ.getArea() + triang.getArea() + rect.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_2)
{
  stojanoski::Circle c1({ -91, 127.4 }, 2);
  stojanoski::Triangle t1({ -9, 27 }, { 1,-1 }, { 15,81 });
  stojanoski::CompositeShape cs1;
  cs1.addShape(c1);
  cs1.addShape(t1);

  stojanoski::Rectangle r1({ 13,18 }, 8, 5);
  stojanoski::Rectangle r2({ 1, 5 }, 10, 12);
  stojanoski::Triangle t2({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  stojanoski::CompositeShape cs2;
  cs2.addShape(r1);
  cs2.addShape(r2);
  cs2.addShape(t2);

  stojanoski::CompositeShape cs3;
  cs3.addShape(cs1);
  cs3.addShape(cs2);
  BOOST_CHECK_CLOSE(cs3.getArea(), cs1.getArea() + cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs3.getArea(), c1.getArea() + t1.getArea() + r1.getArea()
    + r2.getArea() + t2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_3)
{
  stojanoski::Circle c({ -191, 127.4 }, 22);
  stojanoski::Rectangle r({ 13,128 }, 84, 35);
  stojanoski::Triangle t({ -11, -12.7 }, { -5, -121 }, { 48.44, 291.666 });
  stojanoski::CompositeShape cs;
  cs.addShape(c);
  cs.addShape(r);
  cs.addShape(t);
  BOOST_CHECK_CLOSE(c.getArea(), cs[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(r.getArea(), cs[1].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(t.getArea(), cs[2].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_4)
{
  stojanoski::Circle c({ -191, 127.4 }, 22);
  stojanoski::Rectangle r({ 13,128 }, 84, 35);
  stojanoski::CompositeShape cs1;
  cs1.addShape(c);
  cs1.addShape(r);

  stojanoski::Triangle t({ -11, -12.7 }, { -5, -121 }, { 48.44, 291.666 });
  stojanoski::CompositeShape cs2;
  cs2.addShape(cs1);
  cs2.addShape(t);

  BOOST_CHECK_CLOSE(cs1.getArea(), cs2[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(t.getArea(), cs2[1].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_dxy)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Triangle({ -9, 27 }, { 1,-1 }, { 15,81 }));
  cs.addShape(stojanoski::Circle({ 7,19 }, 6));
  const double width_before = cs.getFrameRect().width;
  const double height_before = cs.getFrameRect().height;
  const double area_before = cs.getArea();
  cs.move(-3.316, 1.257);
  BOOST_CHECK_CLOSE(width_before, cs.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, cs.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, cs.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_point)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));
  const double width_before = cs.getFrameRect().width;
  const double height_before = cs.getFrameRect().height;
  const double area_before = cs.getArea();
  cs.move({ 45.8, -3.38 });
  BOOST_CHECK_CLOSE(width_before, cs.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, cs.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, cs.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_assignment)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  stojanoski::CompositeShape cs2;
  cs2.addShape(stojanoski::Rectangle({ 13,18 }, 8, 5));
  cs2 = cs1;
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().height, cs2.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().width, cs2.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().pos.x, cs2.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().pos.y, cs2.getFrameRect().pos.y, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_self_assignment)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  stojanoski::CompositeShape cs2;
  cs2.addShape(cs1[0]);
  cs2 = cs2;
  BOOST_CHECK(cs2.size() == cs1.size());
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_multiple_assignment)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  stojanoski::CompositeShape cs2;
  cs2.addShape(stojanoski::Rectangle({ -1, 5 }, 4, 8));
  stojanoski::CompositeShape cs3;
  cs3.addShape(stojanoski::Triangle({ 1,5 }, { 8,1 }, { 2,7 }));

  cs1 = cs2 = cs3;

  BOOST_CHECK(cs1.size() == 1);
  BOOST_CHECK(cs2.size() == 1);
  BOOST_CHECK(cs3.size() == 1);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs3.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_complex_assignment)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  stojanoski::CompositeShape cs2;
  cs2.addShape(stojanoski::Rectangle({ -1, 5 }, 4, 8));
  stojanoski::CompositeShape cs3;
  cs3.addShape(cs1);

  cs1 = cs2 = cs3;

  BOOST_CHECK(cs1.size() == 1);
  BOOST_CHECK(cs2.size() == 1);
  BOOST_CHECK(cs3.size() == 1);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs3.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_constructor)
{
  stojanoski::Circle c({ 1,2 }, 3);
  stojanoski::CompositeShape cs1;
  cs1.addShape(c);
  stojanoski::CompositeShape cs2(std::move(cs1));
  BOOST_CHECK(cs1.size() == 0);
  BOOST_CHECK_CLOSE(cs2.getArea(), c.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_operator)
{
  stojanoski::Circle c({ 1,2 }, 3);
  stojanoski::Rectangle r({ 1,1 }, 4, 3);
  stojanoski::CompositeShape cs1;
  cs1.addShape(c);
  stojanoski::CompositeShape cs2;
  cs2.addShape(r);

  cs2 = std::move(cs1);

  BOOST_CHECK(cs1.size() == 0);
  BOOST_CHECK_CLOSE(cs2.getArea(), c.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_copy_shape)
{
  stojanoski::Rectangle r({ 11.89, -54.1 }, 61.1, 84.8);
  stojanoski::CompositeShape cs;
  cs.addShape(r);

  r.move(-74, 61);
  BOOST_CHECK(std::fabs(r.getFrameRect().pos.x - cs.getFrameRect().pos.x) > EPSILON);
  BOOST_CHECK(std::fabs(r.getFrameRect().pos.y - cs.getFrameRect().pos.y) > EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale)
{
  stojanoski::Rectangle r1({ 0.5, 0.5 }, 1, 1);
  stojanoski::Rectangle r2({ 3, 3 }, 2, 2);
  stojanoski::CompositeShape cs1;
  cs1.addShape(r1);
  cs1.addShape(r2);

  cs1.scale(0.5);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().pos.x, 2, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().pos.y, 2, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().width, 2, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().height, 2, EPSILON);

  BOOST_CHECK_CLOSE(cs1[0].getFrameRect().pos.x, 1.25, EPSILON);
  BOOST_CHECK_CLOSE(cs1[0].getFrameRect().pos.y, 1.25, EPSILON);
  BOOST_CHECK_CLOSE(cs1[0].getFrameRect().width, 0.5, EPSILON);
  BOOST_CHECK_CLOSE(cs1[0].getFrameRect().height, 0.5, EPSILON);

  BOOST_CHECK_CLOSE(cs1[1].getFrameRect().pos.x, 2.5, EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getFrameRect().pos.y, 2.5, EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getFrameRect().width, 1, EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getFrameRect().height, 1, EPSILON);

}
BOOST_AUTO_TEST_CASE(compositeShape_scale_area_1)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 13,18 }, 8));
  cs.addShape(stojanoski::Triangle({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 }));
  cs.addShape(stojanoski::Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));
  const double area_before = cs.getArea();
  const double k = 0.77;
  cs.scale(k);
  BOOST_CHECK_CLOSE(area_before * k * k, cs.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale_area_2)
{
  stojanoski::Circle c({ 13,18 }, 8);
  stojanoski::Triangle t({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 });

  stojanoski::CompositeShape cs;
  cs.addShape(c);
  cs.addShape(t);

  const double k = 1.337;
  cs.scale(k);
  c.scale(k);
  t.scale(k);
  BOOST_CHECK_CLOSE(c.getArea() + t.getArea(), cs.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale_area_3)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  cs1.addShape(stojanoski::Triangle({ -9, 27 }, { 1,-1 }, { 15,81 }));

  stojanoski::CompositeShape cs2;
  cs2.addShape(stojanoski::Rectangle({ 13,18 }, 8, 5));
  cs2.addShape(stojanoski::Rectangle({ 1, 5 }, 10, 12));
  cs2.addShape(stojanoski::Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));

  stojanoski::CompositeShape cs3(cs1);
  cs3.addShape(cs2);

  const double k = 2.017;
  cs1.scale(k);
  cs2.scale(k);
  cs3.scale(k);

  stojanoski::CompositeShape cs4(cs1);
  cs4.addShape(cs2);

  BOOST_CHECK_CLOSE(cs3.getArea(), cs1.getArea() + cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs4.getArea(), cs3.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale_center)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 13,18 }, 8));
  cs.addShape(stojanoski::Triangle({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 }));
  const stojanoski::point_t center_before = cs.getFrameRect().pos;
  const double k = 1.37;
  cs.scale(k);
  BOOST_CHECK_CLOSE(center_before.x, cs.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(center_before.y, cs.getFrameRect().pos.y, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_remove_test_1)
{
  stojanoski::Triangle t1({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 });
  stojanoski::Triangle t2({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  stojanoski::CompositeShape cs1;
  cs1.addShape(t1);
  cs1.addShape(t2);

  BOOST_CHECK_CLOSE(cs1[0].getArea(), t1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getArea(), t2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), t1.getArea() + t2.getArea(), EPSILON);

  cs1.removeShape(1);

  BOOST_CHECK(cs1.size() == 1);
  BOOST_CHECK_CLOSE(cs1[0].getArea(), t1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), t1.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_remove_test_2)
{
  stojanoski::Triangle t1({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 });
  stojanoski::Triangle t2({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  stojanoski::CompositeShape cs1;
  cs1.addShape(t1);
  cs1.addShape(t2);

  BOOST_CHECK_CLOSE(cs1[0].getArea(), t1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getArea(), t2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), t1.getArea() + t2.getArea(), EPSILON);

  cs1.removeShape(0);

  BOOST_CHECK(cs1.size() == 1);
  BOOST_CHECK_CLOSE(cs1[0].getArea(), t2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), t2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_invalid_scale_1)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 1, 2 }, 3));
  BOOST_CHECK_THROW(cs.scale(-1), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(compositeShape_invalid_scale_2)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 1, 2 }, 3));
  BOOST_CHECK_THROW(cs.scale(-2), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(compositeShape_operator_out_of_range)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 1,2 }, 3));
  BOOST_CHECK_THROW(cs[5], std::out_of_range);
}
BOOST_AUTO_TEST_CASE(compositeShape_removeShape_out_of_range)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 1,2 }, 3));
  BOOST_CHECK_THROW(cs.removeShape(3), std::out_of_range);
}
BOOST_AUTO_TEST_CASE(compositeShape_empty_getArea)
{
  stojanoski::CompositeShape cs;
  BOOST_CHECK_CLOSE(cs.getArea(), 0, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_empty_getFrameRect)
{
  stojanoski::CompositeShape cs;
  BOOST_CHECK_CLOSE(cs.getFrameRect().height, 0, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().width, 0, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.x, 0, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.y, 0, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_copy_constructor)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ 1,2 }, 3));
  cs1.addShape(stojanoski::Rectangle({ 4,5 }, 6, 7));

  stojanoski::CompositeShape cs2(cs1);

  BOOST_CHECK(cs2.size() == cs1.size());
  BOOST_CHECK_CLOSE(cs2[0].getArea(), cs1[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs2[1].getArea(), cs1[1].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_const_copy_1)
{
  stojanoski::Circle c({ 5,10 }, 15);
  const double initialArea = c.getArea();

  stojanoski::CompositeShape cs;
  cs.addShape(c);

  cs.scale(3);

  BOOST_CHECK_CLOSE(c.getArea(), initialArea, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_const_copy_2)
{
  stojanoski::Circle c({ 5,10 }, 15);
  const double initialArea = c.getArea();

  stojanoski::CompositeShape cs;
  cs.addShape(c);

  cs[0].scale(3);

  BOOST_CHECK_CLOSE(c.getArea(), initialArea, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()
