#ifndef CIRCLE_H
#define CIRCLE_H
#include "shape.hpp"

namespace stojanoski
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &center, double radius);

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(double dx, double dy) noexcept override;
    void move(const point_t &p) noexcept override;
    void scale(double coefficient) override;
    void rotate(double) noexcept;

    std::unique_ptr<Shape> getCopy() const override;

  private:
    point_t center_;
    double radius_;
  };
}
#endif
