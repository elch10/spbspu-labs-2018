#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "polygon.hpp"




void testOfShape(dudina::Shape & cObject)
{
  std::cout << "Shape test:" << std::endl;
  cObject.printInformation();
  std::cout << "Area of figure:" << cObject.getArea() << std::endl;
  std::cout << "Rectangle frame:" << std::endl;
  std::cout << " Center: {" << cObject.getFrameRect().pos.x << ";" << cObject.getFrameRect().pos.y << "}" << std::endl;
  std::cout << " Width:" << cObject.getFrameRect().width << std::endl;
  std::cout << " Height:" << cObject.getFrameRect().height << std::endl;

  cObject.move({ 5,2 });
  std::cout << "After move to {5,2}:" << std::endl;
  std::cout << "Rectangle frame:" << std::endl;
  std::cout << " Center: {" << cObject.getFrameRect().pos.x << ";" << cObject.getFrameRect().pos.y << "}" << std::endl;
  std::cout << " Width:" << cObject.getFrameRect().width << std::endl;
  std::cout << " Height:" << cObject.getFrameRect().height << std::endl;

  cObject.move(5, 2);
  std::cout << "After move on x=5 and y=2:" << std::endl;
  std::cout << "Rectangle frame:" << std::endl;
  std::cout << " Center: {" << cObject.getFrameRect().pos.x << ";" << cObject.getFrameRect().pos.y << "}" << std::endl;
  std::cout << " Width:" << cObject.getFrameRect().width << std::endl;
  std::cout << " Height:" << cObject.getFrameRect().height << std::endl;

  cObject.scale(2);
  std::cout << "After scale:" << std::endl;
  std::cout << "Area after scale:" << cObject.getArea() << std::endl;
  std::cout << "Rectangle frame:" << std::endl;
  std::cout << " Center: {" << cObject.getFrameRect().pos.x << ";" << cObject.getFrameRect().pos.y << "}" << std::endl;
  std::cout << " Width:" << cObject.getFrameRect().width << std::endl;
  std::cout << " Height:" << cObject.getFrameRect().height << std::endl;


};


int main()
{
  try
  {
    dudina::Rectangle rect({ 2.0, 3.0 }, 4.0, 5.0);
    dudina::Circle circl({ 2.0, 3.0 }, 4.0);
    dudina::Triangle triangl({ 0.0, 0.0 }, { 2.0, 4.0 }, { 4.0, 0.0 });
    dudina::Polygon polyg{ { -1.0,-4.0 },{ 1.0,3.0 },{ 4.0,5.0 },{ -2.0,4.0 },{ -4.0,-3.0 } };

    std::cout << "Rectangle" << std::endl;
    testOfShape(rect);
    std::cout << "Circle" << std::endl;
    testOfShape(circl);
    std::cout << "Triangle" << std::endl;
    testOfShape(triangl);
    std::cout << "Polygon" << std::endl;
    testOfShape(polyg);

    std::cout << "Composite Shape" << std::endl;
    std::shared_ptr<dudina::Shape> rectPtr = std::make_shared<dudina::Rectangle>(rect);
    std::shared_ptr<dudina::Shape> cirPtr = std::make_shared<dudina::Circle>(circl);
    std::shared_ptr<dudina::Shape> trianPtr = std::make_shared<dudina::Triangle>(triangl);
    dudina::CompositeShape List(rectPtr);
    List.printInformation();

  
    List.addShape(cirPtr);
    List.addShape(trianPtr);

    List.printInformation();

    List.move(5, 2);
    List.printInformation();

    List.move({ 5, 2 });
    List.printInformation();

    List.scale(2.0);
    List.printInformation();

    List.rotate(45.0);
    List.printInformation();

    size_t size = List.getSize();

    dudina::Circle circlM({ 1.0, 5.0 }, 6.0);
    dudina::Rectangle rectM1( { 1.0, 1.0 }, 6.0, 3.0 );
    dudina::Rectangle rectM2( { 3.0, 1.0 }, 2.0, 4.0 );

    std::shared_ptr< dudina::Shape > circlPtrM = std::make_shared< dudina::Circle >(circlM); 
    std::shared_ptr< dudina::Shape > rectPtrM1 = std::make_shared< dudina::Rectangle >(rectM1);
    std::shared_ptr< dudina::Shape > rectPtrM2 = std::make_shared< dudina::Rectangle >(rectM2);

    dudina::Matrix matrix(circlPtrM);

    matrix.addShape(rectPtrM1);
    matrix.addShape(rectPtrM2);

    std::shared_ptr<dudina::CompositeShape> arr = std::make_shared<dudina::CompositeShape>(List);

    matrix.addComposShape(arr, size);

    std::cout << "Matrix :" << std::endl;
    matrix.printInformation();


  }
  catch (const std::invalid_argument & e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}

