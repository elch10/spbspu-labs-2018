#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include  <cmath>
#include "rectangle.hpp"
#include "triangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "polygon.hpp"

const double accuracy = 0.00001;

BOOST_AUTO_TEST_SUITE(RECTANGLE_TESTS)

 BOOST_AUTO_TEST_CASE(TEST_CONSTRUCTOR)
 {
   BOOST_CHECK_THROW(dudina::Rectangle rectangle({ 1, 1 }, -5.0, 1.0), std::invalid_argument);
   BOOST_CHECK_THROW(dudina::Rectangle rectangle({ 1, 1 }, 1.0, -10.0), std::invalid_argument);
 }

 BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_TO_POINT)
 {
   dudina::Rectangle rectangle({ 1, 1 }, 1.0, 1.0);
   double rectanWidthBeforeMove = rectangle.getFrameRect().width;
   double rectanHeightBeforeMove = rectangle.getFrameRect().height;
   double rectanAreaBeforeMove = rectangle.getArea();

   rectangle.move({ 2, 2 });
   BOOST_CHECK_CLOSE(rectanWidthBeforeMove, rectangle.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(rectanHeightBeforeMove, rectangle.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(rectanAreaBeforeMove, rectangle.getArea(), accuracy);

 }

 BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_BY_dx_and_dy)
 {
   dudina::Rectangle rectangle({ 1.0, 1.0 }, 1.0, 1.0);
   double rectWidthBeforeMove = rectangle.getFrameRect().width;
   double rectHeightBeforeMove = rectangle.getFrameRect().height;
   double rectAreaBeforeMove = rectangle.getArea();

   rectangle.move(2, 2);
   BOOST_CHECK_CLOSE(rectWidthBeforeMove, rectangle.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(rectHeightBeforeMove, rectangle.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(rectAreaBeforeMove, rectangle.getArea(), accuracy);

 }

 BOOST_AUTO_TEST_CASE(TEST_SCALE)
 {
   dudina::Rectangle rectangle({ 1.0, 1.0 }, 1.0, 1.0);
   double recAreaBeforeScale = rectangle.getArea();
   double recWidthBeforeScale = rectangle.getFrameRect().width;
   double recHeightBeforeScale = rectangle.getFrameRect().height;
   double coeffi = 2.0;

   rectangle.scale(coeffi);

   BOOST_CHECK_CLOSE(coeffi * coeffi * recAreaBeforeScale, rectangle.getArea(), accuracy);
   BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, coeffi * recWidthBeforeScale, accuracy);
   BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, coeffi * recHeightBeforeScale, accuracy);
   BOOST_CHECK_CLOSE(rectangle.getArea(), coeffi * coeffi * recAreaBeforeScale, accuracy);
   BOOST_CHECK_THROW(rectangle.scale(-2), std::invalid_argument);
 }


BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(CIRCLE_TESTS)

 BOOST_AUTO_TEST_CASE(TEST_CONSTRUCTOR)
 {
   BOOST_CHECK_THROW(dudina::Circle circle({ 1, 1 }, -10.0), std::invalid_argument);
 }

 BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_TO_POINT)
 {
   dudina::Circle circle({ -1.0, 1.0 }, 5.0);
   double circleWidthBeforeMove = circle.getFrameRect().width;
   double circleHeightBeforeMove = circle.getFrameRect().height;
   double circleAreaBeforeMove = circle.getArea();

   circle.move({ 2.0, 2.0 });

   BOOST_CHECK_CLOSE(circleWidthBeforeMove, circle.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(circleHeightBeforeMove, circle.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(circleAreaBeforeMove, circle.getArea(), accuracy);

 }

 BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_BY_dx_and_dy)
 {
   dudina::Circle circle({ -1.0, 1.0 }, 5.0);
   double circlWidthBeforeMove = circle.getFrameRect().width;
   double circlHeightBeforeMove = circle.getFrameRect().height;
   double circlAreaBeforeMove = circle.getArea();

   circle.move(2.0, 2.0);

   BOOST_CHECK_CLOSE(circlWidthBeforeMove, circle.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(circlHeightBeforeMove, circle.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(circlAreaBeforeMove, circle.getArea(), accuracy);

 }

 BOOST_AUTO_TEST_CASE(TEST_SCALE)
 {
   dudina::Circle circle({ -1.0, -1.0 }, 2.0);

   double circleWidthBeforeScale = circle.getFrameRect().width;
   double circleHeightBeforeScale = circle.getFrameRect().height;
   double circleAreaBeforeScale = circle.getArea();
   double coef = 3.0;

   circle.scale(coef);

   BOOST_CHECK_CLOSE(circle.getFrameRect().width, coef * circleWidthBeforeScale, accuracy);
   BOOST_CHECK_CLOSE(circle.getFrameRect().height, coef * circleHeightBeforeScale, accuracy);
   BOOST_CHECK_CLOSE(circle.getArea(), coef * coef * circleAreaBeforeScale, accuracy);
   BOOST_CHECK_THROW(circle.scale(-3.0), std::invalid_argument);
 }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TRIANGLE_TESTS)

 BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_TO_POINT)
 {
   dudina::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
   double trianglWidthBeforeMove = triangle.getFrameRect().width;
   double trianglHeightBeforeMove = triangle.getFrameRect().height;
   double trianglAreaBeforeMove = triangle.getArea();

   triangle.move({ 5.0, 5.0 });

   BOOST_CHECK_CLOSE(trianglWidthBeforeMove, triangle.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(trianglHeightBeforeMove, triangle.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(trianglAreaBeforeMove, triangle.getArea(), accuracy);

 }

 BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_BY_dx_and_dy)
 { 
   dudina::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
   double triangleWidthBeforeMove = triangle.getFrameRect().width;
   double triangleHeightBeforeMove = triangle.getFrameRect().height;
   double triangleAreaBeforeMove = triangle.getArea();

   triangle.move(1.0, 1.0);

   BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), accuracy);

 }

 BOOST_AUTO_TEST_CASE(TEST_SCALE)
 {
   dudina::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
   double  coefficient_ = 2.0;
   double trianglWidthBeforeScale = triangle.getFrameRect().width;
   double trianglHeightBeforeScale = triangle.getFrameRect().height;
   double trianglAreaBeforeScale = triangle.getArea();

   triangle.scale(coefficient_);

   BOOST_CHECK_CLOSE(coefficient_ * coefficient_ * trianglAreaBeforeScale, triangle.getArea(), accuracy);
   BOOST_CHECK_CLOSE(triangle.getFrameRect().width, coefficient_ * trianglWidthBeforeScale, accuracy);
   BOOST_CHECK_CLOSE(triangle.getFrameRect().height, coefficient_ * trianglHeightBeforeScale, accuracy);
   BOOST_CHECK_THROW(triangle.scale(-2), std::invalid_argument);
 }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(COMPOSITESHAPE_TESTS)

 BOOST_AUTO_TEST_CASE(InvalidArgumentOfConstructor)
 {
   std::shared_ptr<dudina::Shape> circlePtr = nullptr;

   BOOST_CHECK_THROW(dudina::CompositeShape compShape(circlePtr), std::invalid_argument);
 }

 BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_TO_POINT)
 {
   dudina::Circle circle({ 1.0, 10.0 }, 5.0);
   dudina::Rectangle rectangle({ -2.0, 3.0 }, 12.0, 8.0);
   std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
   std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
   dudina::CompositeShape compShape(circlePtr);
   compShape.addShape(rectanglePtr);

   double composShapeWidthBeforeMove = compShape.getFrameRect().width;
   double composShapeHeightBeforeMove = compShape.getFrameRect().height;
   double composShapeAreaBeforeMove = compShape.getArea();

   compShape.move({ 5.0, 5.0 });

   BOOST_CHECK_CLOSE(composShapeWidthBeforeMove, compShape.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(composShapeHeightBeforeMove, compShape.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(composShapeAreaBeforeMove, compShape.getArea(), accuracy);
 }

 BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_BY_dx_and_dy)
 {
   dudina::Circle circle({ 1.0, 10.0 }, 5.0);
   dudina::Rectangle rectangle({ -2.0, 3.0 }, 12.0, 8.0);
   std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
   std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
   dudina::CompositeShape compShape(circlePtr);
   compShape.addShape(rectanglePtr);

   double composShapeWidthBeforeMove = compShape.getFrameRect().width;
   double composShapeHeightBeforeMove = compShape.getFrameRect().height;
   double composShapeAreaBeforeMove = compShape.getArea();

   compShape.move(5.0, 5.0);

   BOOST_CHECK_CLOSE(composShapeWidthBeforeMove, compShape.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(composShapeHeightBeforeMove, compShape.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(composShapeAreaBeforeMove, compShape.getArea(), accuracy);
 }

 BOOST_AUTO_TEST_CASE(TEST_SCALE)
 {
   dudina::Circle circle({ 1.0, 1.0 }, 5.0);
   dudina::Rectangle rectangle({ 5.0, 3.0 }, 2.0, 2.0);
   std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
   std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
   dudina::CompositeShape compShape(circlePtr);
   compShape.addShape(rectanglePtr);

   dudina::point_t CenterBeforeSacling = compShape.getFrameRect().pos;
   double composShapeAreaBeforeScale = compShape.getArea();
   double coeffic = 2.0;

   compShape.scale(coeffic);

   BOOST_CHECK_CLOSE(coeffic * coeffic * composShapeAreaBeforeScale, compShape.getArea(), accuracy);
   BOOST_CHECK_CLOSE(CenterBeforeSacling.x, compShape.getFrameRect().pos.x, accuracy);
   BOOST_CHECK_CLOSE(CenterBeforeSacling.y, compShape.getFrameRect().pos.y, accuracy);
   BOOST_CHECK_THROW(compShape.scale(-3.0), std::invalid_argument);
 }

 BOOST_AUTO_TEST_CASE(REMOVE_TEST)
 {
   dudina::Circle circle({ 3.0, 5.0 }, 5.0);
   dudina::Rectangle rectangle({ -6.0, -3.0 }, 12.0, 8.0);
   std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
   std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
   dudina::CompositeShape compShape(rectanglePtr);
   compShape.addShape(circlePtr);

   compShape.deleteShape(1);

   BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.x, rectangle.getFrameRect().pos.x, accuracy);
   BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.y, rectangle.getFrameRect().pos.y, accuracy);
   BOOST_CHECK_CLOSE(compShape.getArea(), rectangle.getArea(), accuracy);
   BOOST_CHECK_EQUAL(compShape.getAmount(), 1);
 }



 BOOST_AUTO_TEST_CASE(INVALID_ARGUMENT_OF_ELEMENT)
 {
   dudina::Rectangle rectangle({ -2.0, 2.0 }, 13.0, 4.0);
   std::shared_ptr <dudina::Shape> circlePtr = nullptr;
   std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
   dudina::CompositeShape compShape(rectanglePtr);

   BOOST_CHECK_THROW(compShape.addShape(circlePtr), std::invalid_argument);
 }


 BOOST_AUTO_TEST_CASE(OUT_OF_RANGE_TEST)
 {
   dudina::Circle circle({ 7.0, 0.0 }, 5.0);
   std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
   dudina::CompositeShape compShape(circlePtr);

   BOOST_CHECK_THROW(compShape.deleteShape(2), std::out_of_range);
   BOOST_CHECK_THROW(compShape.deleteShape(-1), std::out_of_range);
 }


BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(ROTATE_TESTS)

 BOOST_AUTO_TEST_CASE(ROTATE_RECTANGLE)
 {
   dudina::Rectangle rect({ { 0.0, 0.0 }, 3.0, 3.0 });
   dudina::rectangle_t frameRectBefore = rect.getFrameRect();

   double areaR = rect.getArea();
   double degrees = 45.0;

   rect.rotate(degrees);
   dudina::rectangle_t frameRect = rect.getFrameRect();
   BOOST_CHECK_CLOSE(frameRect.width, frameRectBefore.width * sqrt(2), accuracy);
   BOOST_CHECK_CLOSE(frameRect.height, frameRectBefore.height * sqrt(2), accuracy);
   BOOST_CHECK_CLOSE(frameRect.pos.x, frameRectBefore.pos.x, accuracy);
   BOOST_CHECK_CLOSE(frameRect.pos.y, frameRectBefore.pos.y, accuracy);
   BOOST_CHECK_CLOSE(areaR, rect.getArea(), accuracy);
 }

 BOOST_AUTO_TEST_CASE(ROTATE_COMPSHAPE)
 {
   dudina::Rectangle rect({ 1.0,4.0 }, 6.0, 2.0);
   std::shared_ptr<dudina::Shape> rectanglePtr =
    std::make_shared<dudina::Rectangle>(rect);
   dudina::CompositeShape List(rectanglePtr);
   dudina::Rectangle rect1({ 5.0,8.0 }, 6.0, 2.0);
   std::shared_ptr<dudina::Shape> rectPtr =
     std::make_shared<dudina::Rectangle>(rect1);
   List.addShape(rectPtr);

   dudina::rectangle_t frameRectBefore = List.getFrameRect();

   double areaC = List.getArea();
   double degrees = 90.0;

   List.rotate(degrees);

   BOOST_CHECK_CLOSE(frameRectBefore.width, List.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(frameRectBefore.height, List.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(frameRectBefore.pos.x, List.getFrameRect().pos.x, accuracy);
   BOOST_CHECK_CLOSE(frameRectBefore.pos.y, List.getFrameRect().pos.y, accuracy);
   BOOST_CHECK_CLOSE(areaC, List.getArea(), accuracy);

}

 BOOST_AUTO_TEST_CASE(ROTATE_TRIANGLE)
 {
   dudina::Triangle trian({ -3.0, 2.0 }, { -6.0, 4.5 }, { -4.0, -2.4 });
   dudina::rectangle_t frameRectBefore = trian.getFrameRect();

   double areaT = trian.getArea();
   double degrees = 360.0;

   trian.rotate(degrees);

   dudina::rectangle_t frameRect = trian.getFrameRect();

   BOOST_CHECK_CLOSE(frameRect.pos.x, frameRectBefore.pos.x, accuracy);
   BOOST_CHECK_CLOSE(frameRect.pos.y, frameRectBefore.pos.y, accuracy);
   BOOST_CHECK_CLOSE(frameRect.width, frameRectBefore.width, accuracy);
   BOOST_CHECK_CLOSE(frameRect.height, frameRectBefore.height, accuracy);
   BOOST_CHECK_CLOSE(areaT, trian.getArea(), accuracy);
 }

 BOOST_AUTO_TEST_CASE(Rotate_polygon)
 {
   dudina::Polygon polyg{ { -2.0,-2.0 },{ -2.0,2.0 },{ 2.0,2.0 },{ 2.0,-2.0 } };

   dudina::rectangle_t frameRectBefore = polyg.getFrameRect();
   double areaP = polyg.getArea();
   double degrees = 90.0;

   polyg.rotate(degrees);
   dudina::rectangle_t frameRect = polyg.getFrameRect();
   BOOST_CHECK_CLOSE(frameRect.pos.x, frameRectBefore.pos.x, accuracy);
   BOOST_CHECK_CLOSE(frameRect.pos.y, frameRectBefore.pos.y, accuracy);
   BOOST_CHECK_CLOSE(frameRect.width, frameRectBefore.width, accuracy);
   BOOST_CHECK_CLOSE(frameRect.height, frameRectBefore.height, accuracy);
   BOOST_CHECK_CLOSE(areaP, polyg.getArea(), accuracy);
 }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(POLYGON_TESTS)

 BOOST_AUTO_TEST_CASE(MOVE_ON_X_Y)
 {
   dudina::Polygon polyg{ { -4.0,0.0 },{ -1.0,4.0 },{ 2.0,0.0 },{ -3.0,-3.0 } };

   const dudina::point_t centerBefore({ polyg.getFrameRect().pos.x, polyg.getFrameRect().pos.y });
   const double areaBefore = polyg.getArea();
   const double widthBefore = polyg.getFrameRect().width;
   const double heightBefore = polyg.getFrameRect().height;

   polyg.move(2.0, 4.0);
   BOOST_CHECK_CLOSE(centerBefore.x, polyg.getFrameRect().pos.x - 2.0, accuracy);
   BOOST_CHECK_CLOSE(centerBefore.y, polyg.getFrameRect().pos.y - 4.0, accuracy);
   BOOST_CHECK_CLOSE(widthBefore, polyg.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(heightBefore, polyg.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(areaBefore, polyg.getArea(), accuracy);
 }

 BOOST_AUTO_TEST_CASE(MOVE_TO_X_Y)
 {
   dudina::Polygon polyg{ { -4.0,0.0 },{ -1.0,4.0 },{ 2.0,0.0 },{ -3.0,-3.0 } };
   const double areaBefore = polyg.getArea();
   const double widthBefore = polyg.getFrameRect().width;
   const double heightBefore = polyg.getFrameRect().height;
   polyg.move({ -2.0, 2.1 });
   BOOST_CHECK_CLOSE(widthBefore, polyg.getFrameRect().width, accuracy);
   BOOST_CHECK_CLOSE(heightBefore, polyg.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(areaBefore, polyg.getArea(), accuracy);
 }

 BOOST_AUTO_TEST_CASE(POLYG_SCALE)
 {
   dudina::Polygon polyg{ { -3.0,-3.0 },{ -3.0,3.0 },{ 3.0,3.0 },{ 3.0,-3.0 } };

   const double areaBefore = polyg.getArea();
   const double widthBefore = polyg.getFrameRect().width;
   const double heightBefore = polyg.getFrameRect().height;
   const double coeff = 4.0;
   polyg.scale(coeff);

   BOOST_CHECK_CLOSE(areaBefore * coeff *coeff, polyg.getArea(), accuracy);
   BOOST_CHECK_CLOSE(heightBefore * coeff, polyg.getFrameRect().height, accuracy);
   BOOST_CHECK_CLOSE(widthBefore * coeff, polyg.getFrameRect().width, accuracy);
 }

 BOOST_AUTO_TEST_CASE(WRONG_SCALE_COEFF)
 {
   dudina::Polygon polyg{ { -5.0,0.0 },{ -2.0,5.0 },{ 3.0,1.0 },{ -4.0,-4.0 } };
   BOOST_CHECK_THROW(polyg.scale(0.0), std::invalid_argument);
 }

 BOOST_AUTO_TEST_CASE(WRONG_VERTEXES)
 {
   BOOST_CHECK_THROW(dudina::Polygon({ { -5.0,0.0 },{ -2.0,4.0 } }), std::invalid_argument);
 }

 BOOST_AUTO_TEST_CASE(WRONG_AREA)
 {
   BOOST_CHECK_THROW(dudina::Polygon({ { -4.0,0.0 },{ -1.0,0.0 },{ 2.0,0.0 } }), std::invalid_argument);
 }

 BOOST_AUTO_TEST_CASE(CONCAVE)
 {
   BOOST_CHECK_THROW(dudina::Polygon({ { -4.0,-4.0 },{ 0.0,-2.0 },{ 4.0,-4.0 },{ 0.0,3.0 } }), std::invalid_argument);
 }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MATRIX_TESTS)

 BOOST_AUTO_TEST_CASE(NEW_SHAPES)
 {
   std::shared_ptr<dudina::Shape> circle1 = std::make_shared<dudina::Circle>(dudina::Circle({ 3.0, 0.0 }, 1.0));
   std::shared_ptr<dudina::Shape> circle2 = std::make_shared<dudina::Circle>(dudina::Circle({ 0.0, 0.0 }, 1.0));
   std::shared_ptr<dudina::Shape> circle3 = std::make_shared<dudina::Circle>(dudina::Circle({ -3.0, 0.0 }, 1.0));

   dudina::Matrix matrix(circle1);
   matrix.addShape(circle2);
   matrix.addShape(circle3);

   BOOST_CHECK(matrix[0][0] == circle1);
   BOOST_CHECK(matrix[0][1] == circle2);
   BOOST_CHECK(matrix[0][2] == circle3);

 }

 BOOST_AUTO_TEST_CASE(NEW_LAYERS)
 {
   std::shared_ptr<dudina::Shape> rect1 = std::make_shared<dudina::Rectangle>(dudina::Rectangle({ -2.0, 2.0 }, 2.0, 2.0));
   std::shared_ptr<dudina::Shape> rect2 = std::make_shared<dudina::Rectangle>(dudina::Rectangle({ 0.0, 0.0 }, 3.0, 3.0));

   dudina::Matrix matrix(rect1);
   matrix.addShape(rect2);

   BOOST_CHECK(matrix[0][0] == rect1);
   BOOST_CHECK(matrix[1][0] == rect2);
 }

 BOOST_AUTO_TEST_CASE(COPY_CONSTRUCTOR)
 {
   std::shared_ptr<dudina::Shape> rect = std::make_shared<dudina::Rectangle>(dudina::Rectangle({ 4.0, 5.0 }, 6.0, 8.0));

   dudina::Matrix matrix(rect);
   dudina::Matrix matrix2(matrix);

   BOOST_CHECK(matrix[0][0] == matrix2[0][0]);
 }

 BOOST_AUTO_TEST_CASE(COPY_OPERATOR)
 {
   std::shared_ptr<dudina::Shape> circle1 = std::make_shared<dudina::Circle>(dudina::Circle({ 0.0, 0.0 }, 6.0));
   std::shared_ptr<dudina::Shape> circle2 = std::make_shared<dudina::Circle>(dudina::Circle({ 0.0, 0.0 }, 2.0));

   dudina::Matrix matrix(circle1);
   dudina::Matrix matrix2(circle2);

   matrix2 = matrix;

   BOOST_CHECK(matrix[0][0] == matrix2[0][0]);
 }

 BOOST_AUTO_TEST_CASE(MOVE_CONSTRUCTOR)
 {
   std::shared_ptr<dudina::Shape> rect = std::make_shared<dudina::Rectangle>(dudina::Rectangle({ 4.0, 5.0 }, 6.0, 8.0));

   dudina::Matrix matrix(rect);
   dudina::Matrix matrix2(matrix);
   dudina::Matrix matrix3(std::move(matrix));

   BOOST_CHECK(matrix2[0][0] == matrix3[0][0]);
 }

 BOOST_AUTO_TEST_CASE(MOVE_OPERATOR)
 {
   std::shared_ptr<dudina::Shape> rect = std::make_shared<dudina::Rectangle>(dudina::Rectangle({ 4.0, 5.0 }, 6.0, 8.0));
   std::shared_ptr<dudina::Shape> rect2 = std::make_shared<dudina::Rectangle>(dudina::Rectangle({ 5.0, 4.0 }, 8.0, 6.0));

   dudina::Matrix matrix(rect);
   dudina::Matrix matrix2(matrix);
   dudina::Matrix matrix3(rect2);
   matrix3 = std::move(matrix);

   BOOST_CHECK(matrix2[0][0] == matrix3[0][0]);
 }

 BOOST_AUTO_TEST_CASE(ROTATE_MATRIX)
 {
   dudina::Rectangle rectangle1({ 0.0, 0.0 }, 4.0, 2.0);
   dudina::Rectangle rectangle2({ 3.0, 0.0 }, 4.0, 2.0);

   std::shared_ptr<dudina::Shape> rect1 = std::make_shared<dudina::Rectangle>(rectangle1);
   std::shared_ptr<dudina::Shape> rect2 = std::make_shared<dudina::Rectangle>(rectangle2);

   dudina::Matrix matrix(rect1);
   matrix.addShape(rect2);

   BOOST_CHECK(matrix[0][0] == rect1);
   BOOST_CHECK(matrix[0][1] == rect2);

   rectangle1.rotate(45.0);
   rectangle2.rotate(45.0);

   std::shared_ptr<dudina::Shape> newrect1 = std::make_shared<dudina::Rectangle>(rectangle1);
   std::shared_ptr<dudina::Shape> newrect2 = std::make_shared<dudina::Rectangle>(rectangle2);

   dudina::Matrix matrix2(newrect1);
   matrix2.addShape(newrect2);

   BOOST_CHECK(matrix2.getLayerNumber() == 2);
   BOOST_CHECK(matrix2[0][0] == newrect1);
   BOOST_CHECK(matrix2[1][0] == newrect2);
 }

BOOST_AUTO_TEST_SUITE_END()


