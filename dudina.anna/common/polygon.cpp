
#include <stdexcept>
#include <cmath>
#include <iostream>
#include "base-types.hpp"
#include "polygon.hpp"
#include <iomanip>


dudina::Polygon::Polygon(std::initializer_list<point_t> vertexes)
{
  if (vertexes.size()<3)
  {
    throw std::invalid_argument("Polygon must have >2 vertexes!");
  }

  size_ = vertexes.size();
  vertexes_ = std::unique_ptr<point_t[]>(new point_t[size_]);
  int i = 0;

  for (std::initializer_list<point_t>::const_iterator vertex = vertexes.begin(); vertex != vertexes.end(); ++vertex)
  {
    vertexes_[i] = *vertex;
    i++;
  }

  if (getArea() <= 0.0)
  {
    vertexes_.reset();
    throw std::invalid_argument("Area must be >0!");
  }

  if (!Convex())
  {
    vertexes_.reset();
    throw std::invalid_argument("Polygon isn't convex");

  }

  center_ = { 0.0, 0.0 };
  for (size_t i = 0; i < size_; i++)
  {
    center_.x += vertexes_[i].x;
    center_.y += vertexes_[i].y;
  }

  center_.x = center_.x / size_;
  center_.y = center_.y / size_;

}

dudina::point_t  dudina::Polygon::operator[](size_t index) const
{
  if (index >= size_) 
  {
    throw std::out_of_range("Index is out of range!");
  }

  return vertexes_[index];
}

double dudina::Polygon::getArea() const
{
  double area = 0;

  for (size_t i = 0; i < size_ - 1; i++) 
  {
    area += ((vertexes_[i].x + vertexes_[i + 1].x) * (vertexes_[i].y - vertexes_[i + 1].y));
  }

  area += ((vertexes_[size_ - 1].x + vertexes_[0].x) * (vertexes_[size_ - 1].y - vertexes_[0].y));

  return abs(area) / 2;

}

dudina::rectangle_t dudina::Polygon::getFrameRect() const
{
  double minX = vertexes_[0].x;
  double maxX = vertexes_[0].x;
  double minY = vertexes_[0].y;
  double maxY = vertexes_[0].y;

  for (size_t i = 1; i < size_; i++) 
  {
    if (vertexes_[i].x < minX) 
    {
      minX = vertexes_[i].x;
    }

    if (vertexes_[i].x > maxX) 
    {
      maxX = vertexes_[i].x;
    }

    if (vertexes_[i].y < minY) 
    {
      minY = vertexes_[i].y;
    }

    if (vertexes_[i].x > maxY) 
    {
      maxY = vertexes_[i].y;
    }
  }

  return { maxY - minY, maxX - minX, { minX + (maxX - minX) / 2, minY + (maxY - minY) / 2 } };
}

void dudina::Polygon::move(const dudina::point_t & pos)
{
  move(pos.x - getCenter().x, pos.y - getCenter().y);
}

void dudina::Polygon::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; ++i) 
  {
    vertexes_[i].x += dx;
    vertexes_[i].y += dy;
  }
  center_.x += dx;
  center_.y += dy;
}

void dudina::Polygon::printInformation()
{
  for (size_t i = 0; i < size_; ++i) 
  {
    std::cout << "Vertex[" << i << "] : {" << vertexes_[i].x << ", " << vertexes_[i].y << "}" << std::endl;
  }
  std::cout << "Center : " << center_.x << " " << center_.y << std::endl;
  std::cout << "Area of polygon: " << getArea() << std::endl;
  rectangle_t frame = getFrameRect();
  std::cout << "RectFr Center: (" << frame.pos.x << "," << frame.pos.y << ")" << std::endl;
  std::cout << "Frame Width: " << frame.width << std::endl;
  std::cout << "Frame Height: " << frame.height << std::endl;


}

void dudina::Polygon::scale(double coeff)
{
  if (coeff <= 0)
  {
    throw std::invalid_argument("Coefficient must be > 0!");
  }

  for (size_t i = 0; i < size_; ++i) 
  {
    vertexes_[i].x = center_.x + coeff * (vertexes_[i].x - center_.x);
    vertexes_[i].y = center_.y + coeff * (vertexes_[i].y - center_.y);
  }
}

void dudina::Polygon::rotate(double degrees)
{
  double angle = degrees * M_PI / 180;
  const double sinOfAngle = sin(angle);
  const double cosOfAngle = cos(angle);

  point_t center = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++) 
  {
    vertexes_[i] = { center.x + cosOfAngle * (vertexes_[i].x - center.x) - sinOfAngle * (vertexes_[i].y - center.y),
      center.y + cosOfAngle * (vertexes_[i].y - center.y) + sinOfAngle * (vertexes_[i].x - center.x) };
  }

}


dudina::point_t dudina::Polygon::getCenter() const
{
  double sum_x = 0;
  double sum_y = 0;

  for (size_t i = 0; i < size_; i++) 
  {
    sum_x += vertexes_[i].x;
    sum_y += vertexes_[i].y;
  }
  return { sum_x / size_, sum_y / size_ };

}

std::string dudina::Polygon::getName() const 
{
  return "P";
}


bool dudina::Polygon::Convex() const
{
  for (size_t i = 0; i < size_ - 1; i++) 
  {
    double line_pos1 = (vertexes_[i + 1].y - vertexes_[i].y) * (vertexes_[0].x - vertexes_[i].x)
      - (vertexes_[i + 1].x - vertexes_[i].x) * (vertexes_[0].y - vertexes_[i].y);

    double line_pos2 = 0;

    for (size_t j = 1; j < size_; j++) 
    {
      line_pos2 = (vertexes_[i + 1].y - vertexes_[i].y) * (vertexes_[j].x - vertexes_[i].x)
        - (vertexes_[i + 1].x - vertexes_[i].x) * (vertexes_[j].y - vertexes_[i].y);

      if (line_pos2 * line_pos1 >= 0) 
      {
        line_pos1 = line_pos2;
      }

      else 
      {
        return false;
      }
    }
  }
  return true;

}
