#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <memory>
#include "shape.hpp"

namespace dudina
{
  class Polygon : 
    public Shape
  {
  public:

    Polygon(std::initializer_list<point_t> vertexes);
    point_t operator[](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(double dx, double dy) override;

    void printInformation() override;

    void scale(double coeff) override;
    void rotate(double degrees) override;

    point_t getCenter() const ;
    std::string getName() const override;


  private:
    size_t size_;
    std::unique_ptr<point_t[]> vertexes_;

    point_t center_;
    bool Convex() const;
  };
}
#endif
