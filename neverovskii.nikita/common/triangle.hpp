#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"
namespace neverovskii
{
class Triangle : public Shape
{
public:
  Triangle(const point_t &A, const point_t &B, const point_t &C);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &center) override;
  void move(double dx, double dy) override;
  void scale(double cfc) override;
  void show() const override;
private:
  point_t A_;
  point_t B_;
  point_t C_;
  point_t center_;
};
}
#endif
