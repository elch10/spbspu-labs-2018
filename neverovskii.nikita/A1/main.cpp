#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

void ShowShape(const Shape &shape)
{
  shape.show();
}

int main()
{
  Rectangle rectangle(6.0, 4.0, {4.0, 1.0});
  std::cout << " " << std::endl;
  std::cout << "Rectangle:" << std::endl;
  std::cout << " " << std::endl;
  ShowShape(rectangle);
  std::cout << " " << std::endl;
  std::cout << "Move to the certain point: " << std::endl;
  std::cout << " " << std::endl;
  rectangle.move({4.10, 5.20});
  ShowShape(rectangle);
  std::cout << " " << std::endl;
  std::cout << "Move by X and Y: " << std::endl;
  std::cout << " " << std::endl;
  rectangle.move(1.80, 6.00);
  ShowShape(rectangle);

  Circle circle(2.0, {4.0, 5.0});
  std::cout << " " << std::endl;
  std::cout << "Circle:" << std::endl;
  std::cout << " " << std::endl;
  ShowShape(circle);
  std::cout << " " << std::endl;
  std::cout << "Move to the certain point: " << std::endl;
  std::cout << " " << std::endl;
  rectangle.move({3.50, 8.00});
  ShowShape(circle);
  std::cout << " " << std::endl;
  std::cout << "Move by X and Y: " << std::endl;
  std::cout << " " << std::endl;
  rectangle.move(7.30, 7.40);
  ShowShape(circle);

  Triangle triangle({7.0, 4.0}, {6.0, 8.0}, {9.0, 11.0});
  std::cout << " " << std::endl;
  std::cout << "Triangle:" << std::endl;
  std::cout << " " << std::endl;
  ShowShape(triangle);
  std::cout << " " << std::endl;
  std::cout << "Move to the certain point: " << std::endl;
  std::cout << " " << std::endl;
  triangle.move({2.00, 9.00});
  ShowShape(triangle);
  std::cout << " " << std::endl;
  std::cout << "Move by X and Y: " << std::endl;
  std::cout << " " << std::endl;
  triangle.move(14.10, 5.50);
  ShowShape(triangle);
  return 0;
}
