#include <iostream>
#include <cmath>
#include "circle.hpp"

Circle::Circle(double rad, const point_t &center):
  radius_(rad),
  center_(center)
{
  if (rad <= 0.0) {
    std::cerr << "Radius can't be less than zero!" << std::endl;
  }
  radius_ = rad;
  center_ = center;
}

rectangle_t Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

void Circle::move(const point_t &center)
{
  center_ = center;
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::show() const
{
  std::cout << "Center of the circle ( " << center_.x << " ; " << center_.y << " )" << std::endl;
  std::cout << "Width of the circle:  " << getFrameRect().width << std::endl;
  std::cout << "Height of the circle: " << getFrameRect().height << std::endl;
  std::cout << "Area of the circle: " << getArea() << std::endl;
  std::cout << " " << std::endl;
}
