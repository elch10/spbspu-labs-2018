#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

using namespace drozdov;

drozdov::Rectangle::Rectangle(const rectangle_t & rect):
rec_(rect),
currentAngle_(0.0)
{
  if (rect.height <= 0.0)
  {
    throw std::invalid_argument("Height must be > 0");
  }
  if (rect.width <= 0.0)
  {
    throw std::invalid_argument("Width must be > 0");
  }
}

double drozdov::Rectangle::getArea() const noexcept
{
  return (rec_.width * rec_.height);
}

point_t drozdov::Rectangle::getPos() noexcept
{
  return rec_.pos;
}

rectangle_t drozdov::Rectangle::getFrameRect() const noexcept
{
  return rec_;
}

void drozdov::Rectangle::move(const point_t & point) noexcept
{
  rec_.pos = point;
}

void drozdov::Rectangle::move(const double dx, const double dy) noexcept
{
  rec_.pos.x += dx;
  rec_.pos.y += dy;
}

void drozdov::Rectangle::scale(const double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("factor must be > 0");
  }

  rec_.height *= factor;
  rec_.width *= factor;
}

void drozdov::Rectangle::rotate(const double angle)
{
  currentAngle_ = std::fmod(currentAngle_ + angle, 360.0);
}

double drozdov::Rectangle::getAngle()
{
  return currentAngle_;
}
