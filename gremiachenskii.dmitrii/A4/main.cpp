#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace gremiachenskii;
using namespace std;

int main()
{
  try
  {
    std::shared_ptr<Circle> circ1 = std::make_shared<Circle>( Circle( {5.0, 5.0}, 60 ) );
    std::shared_ptr<Rectangle> rect1 = std::make_shared<Rectangle>( Rectangle( {10.0, 10.0}, 50.0, 10.0 ) );
    std::shared_ptr<Rectangle> rect2 = std::make_shared<Rectangle>( Rectangle( {-100.0, -100.0}, 10.0, 10.0 ) );

    CompositeShape cs(circ1);
    cs.addElement(rect1);
    cs.addElement(rect2);

    cout << "(" << cs.getFrameRect().pos.x << ", " << cs.getFrameRect().pos.y << ")" << endl;

    cs.move(20.0, 20.0);
    cout << "Shape moved\ndx = 20\ndy = 20" << std::endl;
    cout << "(" << cs.getFrameRect().pos.x << ", " << cs.getFrameRect().pos.y << ")" << endl;

    cs.move({ 15.0, 15.0 });
    cs.move(20.0, 20.0);
    cout << "Shape moved to (15, 15)" << std::endl;
    cout << "(" << cs.getFrameRect().pos.x << ", " << cs.getFrameRect().pos.y << ")" << endl;

    cout << "Height = " << cs.getFrameRect().height << "\nWidth = " << cs.getFrameRect().width << endl;
    cs.scale(0.5);
    cout << "Shape scales 0.5" << endl;
    cout << "Height = " << cs.getFrameRect().height << "\nWidth = " << cs.getFrameRect().width << endl;

    cs.rotate(90);
    cout << "(" << cs.getFrameRect().pos.x << ", " << cs.getFrameRect().pos.y << ")" << endl;
    cout << "Height = " << cs.getFrameRect().height << "\nWidth = " << cs.getFrameRect().width << endl;

    Matrix matrix(circ1);
    matrix.addElement(rect1);
    matrix.addElement(rect2);
    cout << "There are " << matrix.getLayersCount() << " layers" << endl;
  }
  catch(std::invalid_argument & exception)
  {
    cerr << exception.what() << endl;
  }
  catch(std::out_of_range & exception)
  {
    cerr << exception.what() << endl;
  }
}
