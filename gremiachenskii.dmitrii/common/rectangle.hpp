#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace gremiachenskii
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t & center, double height, double width);
    Rectangle(const rectangle_t & rect);
    void move(const point_t & pos) noexcept override;
    void move(double dx, double dy) noexcept override;
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void scale(double ratio) override;
    void rotate(double alpha) noexcept override;

  private:
    rectangle_t rect_;
    point_t corners_[4];
    double angle_;
  };
}

#endif // RECTANGLE_HPP
