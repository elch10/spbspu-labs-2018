#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

using namespace gremiachenskii;

Rectangle::Rectangle(const point_t & center, double height, double width):
  rect_{ center, height, width },
  angle_(0)
{
  if (height < 0.0 || width < 0.0)
  {
    throw std::invalid_argument("ERROR: Negative rectangle height or width");
  }
  corners_[0] = { center.x - width / 2, center.y + height / 2 };
  corners_[1] = { center.x + width / 2, center.y + height / 2 };
  corners_[2] = { center.x + width / 2, center.y - height / 2 };
  corners_[3] = { center.x - width / 2, center.y - height / 2 };
}

Rectangle::Rectangle(const rectangle_t & rect):
  rect_(rect),
  angle_(0)
{
  if (rect.height < 0.0 || rect.width < 0.0)
  {
    throw std::invalid_argument("ERROR: Negative rectangle height or width");
  }
  corners_[0] = { rect_.pos.x - rect_.width / 2, rect_.pos.y + rect_.height / 2 };
  corners_[1] = { rect_.pos.x + rect_.width / 2, rect_.pos.y + rect_.height / 2 };
  corners_[2] = { rect_.pos.x + rect_.width / 2, rect_.pos.y - rect_.height / 2 };
  corners_[3] = { rect_.pos.x - rect_.width / 2, rect_.pos.y - rect_.height / 2 };
}

void Rectangle::move(const point_t & pos) noexcept
{
  rect_.pos = pos;

  for (unsigned short int i = 0; i < 4; i++)
  {
    corners_[i].x += (pos.x - rect_.pos.x);
    corners_[i].y += (pos.y - rect_.pos.y);
  }
}

void Rectangle::move(double dx, double dy) noexcept
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;

  for (unsigned short int i = 0; i < 4; i++)
  {
    corners_[i].x += dx;
    corners_[i].y += dy;
  }
}

double Rectangle::getArea() const noexcept
{
  return rect_.height * rect_.width;
}

rectangle_t Rectangle::getFrameRect() const noexcept
{
  double maxX = std::max(std::max(corners_[0].x, corners_[1].x), std::max(corners_[2].x, corners_[3].x));
  double maxY = std::max(std::max(corners_[0].y, corners_[1].y), std::max(corners_[2].y, corners_[3].y));
  double minX = std::min(std::min(corners_[0].x, corners_[1].x), std::min(corners_[2].x, corners_[3].x));
  double minY = std::min(std::min(corners_[0].y, corners_[1].y), std::min(corners_[2].y, corners_[3].y));

  return { {(maxX + minX) / 2, (maxY + minY) / 2}, maxY - minY, maxX - minX };
}

void Rectangle::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("ERROR: Negative scale ratio");
  }

  rect_.height *= ratio;
  rect_.width *= ratio;

  for (unsigned short int i = 0; i < 4; i++)
  {
    corners_[i].x = (corners_[i].x - rect_.pos.x) * ratio + rect_.pos.x;
    corners_[i].y = (corners_[i].y - rect_.pos.y) * ratio + rect_.pos.y;
  }
}

void Rectangle::rotate(double alpha) noexcept
{
  angle_ += alpha;

  double alphaInRadians = alpha * 3.14 / 180; 
  double cosAlpha = cos(alphaInRadians);
  double sinAlpha = sin(alphaInRadians);

  point_t pos = rect_.pos;
  for (unsigned short int i = 0; i < 4; i++)
  {
    double X = pos.x + (corners_[i].x - pos.x) * cosAlpha
               - (corners_[i].y - pos.y) * sinAlpha;
    double Y = pos.y + (corners_[i].y - pos.y) * cosAlpha
               + (corners_[i].x - pos.x) * sinAlpha;
    corners_[i].x = X;
    corners_[i].y = Y;
  }
}
