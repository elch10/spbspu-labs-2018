#include <cstdlib>
#include <iostream>
#include "circle.hpp"
#include "base-types.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using std::cout;
using std::endl;
using namespace neizhko;

void test(Shape& obj)
{
  obj.printInfo();
  point_t cc;
  cc.x = 250;
  cc.y = 250;
  obj.move(cc);
  obj.move(55, 55);
  obj.scale(2);
  obj.printInfo();
}

int main()
{
  try
  {
    std::shared_ptr<Rectangle> rec = std::make_shared<Rectangle>(1,1,0,0);
    std::shared_ptr<Rectangle> rec2 = std::make_shared<Rectangle>(5,11,2,8);
    std::shared_ptr<Circle> cir = std::make_shared<Circle>(1,6,8);
    Matrix matrix;
    matrix.addShape(rec);
    matrix.addShape(cir);
    matrix.addShape(rec2);
    matrix.printInfo();
  }
  catch (const std::exception& e)
  {
    cout << e.what();
    return 1;
  }
  return 0;
}
