#include "rectangle.hpp"

Rectangle::Rectangle(const double width, const double height, const point_t& center) : 
  rect_{ width, height, center }
{
  if ((height <= 0.0)
  || (width <= 0.0))
  {
    throw std::invalid_argument("Invalid width / height");
  }
}

double Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

rectangle_t Rectangle::getFrameRect() const
{
  return rect_;
}

void Rectangle::move(const point_t& r)
{
  rect_.pos = r;
}

void Rectangle::move(const double dx, const double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}
