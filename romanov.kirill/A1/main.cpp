#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

int main()
{
  std::unique_ptr <Shape> circle(new Circle(1, {0, 0}));
  std::cout << "circle \n";
  std::cout << circle->getFrameRect();
  circle->move(2, 2);
  std::cout << circle->getFrameRect();
  circle->move({ 3, 3 });
  std::cout << circle->getFrameRect();
  std::unique_ptr <Shape> rectangle(new Rectangle(2, 2, { 0, 0 }));
  std::cout << "rect \n";
  std::cout << rectangle->getFrameRect();
  rectangle->move(2, 2);
  std::cout << rectangle->getFrameRect();
  rectangle->move({ 3, 3 });
  std::cout << rectangle->getFrameRect();
  std::unique_ptr <Shape> triangle(new Triangle({ 0.0, 0.0,  0.0, 3.0,  3.0, 0.0 }));
  std::cout << "trio \n";
  std::cout << triangle->getFrameRect();
  triangle->move(2, 2);
  std::cout << triangle->getFrameRect();
  triangle->move({ 2, 2 });
  std::cout << triangle->getFrameRect();
  return 0;
}
