#include "circle.hpp"
#include <cmath>
#define _USE_MATH_DEFINES

Circle::Circle(double r, const point_t& center) :
  r_(r),
  center_(center)
{
  if (r <= 0.0)
  {
    throw std::invalid_argument("r < 0");
  }
}

double Circle::getArea() const
{
  return pow(r_, 2) * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return rectangle_t{ r_ * 2, r_ * 2, center_ };
}

void Circle::move(const point_t& r)
{
  center_ = r;
}

void Circle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}
