#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace romanov
{
  class Circle : public Shape
  {
  public:
    Circle(double r, const point_t& center);
    rectangle_t getFrameRect() const noexcept override;
    double getArea() const noexcept override;
    void move(const point_t& r) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double scaleK) override;
    void getInform(std::ostream &out) const override;
    void rotate(const double angle) noexcept override;
  private:
    double r_;
    point_t center_;
  };
}
#endif
