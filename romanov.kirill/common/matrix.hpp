#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"

namespace romanov {
  class Matrix
  {
  public:
    Matrix(const std::shared_ptr<Shape> &shape);

    ~Matrix() = default;

    Matrix(const Matrix &Matrix);
    Matrix(Matrix &&Matrix);

    Matrix &operator=(const Matrix &Matrix);
    Matrix &operator=(Matrix &&Matrix);

    std::shared_ptr<Shape> operator ()(const double &line, const double &collumn) const;

    void addShape(const std::shared_ptr<Shape> &shape);

    bool Intersection(const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> &shape2);

    void getInform(std::ostream &out) const;

    friend std::ostream &operator<<(std::ostream &out, const Matrix &matrix);

  private:
    std::unique_ptr <std::shared_ptr<Shape>[]> matrix_array;
    size_t NumCol;
    size_t NumLine;
  };
}

#endif
