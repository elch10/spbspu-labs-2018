#include <cmath>
#include <stdexcept>
#include <iostream>
#define _USE_MATH_DEFINES
#include "circle.hpp"

romanov::Circle::Circle(double r, const point_t& center) :
  r_(r),
  center_(center)
{
  if (r < 0.0)
  {
    throw std::invalid_argument("r < 0");
  }
}

double romanov::Circle::getArea() const noexcept
{
  return pow(r_, 2) * M_PI;
}

romanov::rectangle_t romanov::Circle::getFrameRect() const noexcept
{
  return rectangle_t{ r_ * 2, r_ * 2, center_ };
}

void romanov::Circle::move(const point_t& r) noexcept
{
  center_ = r;
}

void romanov::Circle::move(const double dx, const double dy) noexcept
{
  center_.x += dx;
  center_.y += dy;
}

void romanov::Circle::scale(const double scaleK)
{
  r_ *= scaleK;
}

void romanov::Circle::getInform(std::ostream &out) const
{
    const rectangle_t rect = getFrameRect();
    out << "Information for the circle:\n"
    << "Area:" << getArea() << "\n"
    << "Frame:\n"
    << "width=" << rect.width << "\n"
    << "height=" << rect.height << "\n"
    << "x=" << rect.pos.x << "\n"
    << "y=" << rect.pos.y << "\n";
}

void romanov::Circle::rotate(const double angle) noexcept
{
  const double angleConverted = angle * M_PI / 180; //Convertion from degrees to radians.
  (void)angleConverted;
}
