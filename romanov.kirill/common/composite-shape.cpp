#include <iostream>
#include <cmath>
#include <stdexcept>
#define _USE_MATH_DEFINES
#include "composite-shape.hpp"

romanov::CompositeShape::CompositeShape():
  m_array(nullptr),
  m_size(0)
{
}

romanov::CompositeShape::CompositeShape(const std::shared_ptr<romanov::Shape> &shape) :
  m_array(new std::shared_ptr<Shape>[1]),
  m_size(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("There is no shape");
  }

  m_array[0] = shape;
}

romanov::CompositeShape::CompositeShape(const CompositeShape &CompShape) :
  m_array(new std::shared_ptr<Shape>[CompShape.m_size]),
  m_size(CompShape.m_size)
{
  for (size_t i = 0; i < CompShape.m_size; i++)
  m_array[i] = CompShape.m_array[i];
}

romanov::CompositeShape::CompositeShape(CompositeShape &&CompShape) :
  m_array(nullptr),
  m_size(CompShape.m_size)
{
  m_array.swap(CompShape.m_array);
  CompShape.m_size = 0;
}

std::shared_ptr<romanov::Shape> romanov::CompositeShape::operator[](size_t n) const
{
  if (n >= m_size)
  {
    throw std::out_of_range("out of range");
  }

  return (m_array[n]);
}

romanov::CompositeShape &romanov::CompositeShape::operator=(const CompositeShape &CompShape)
{
  if (this != &CompShape)
  {
    CompositeShape comp_shape(CompShape);
    this->m_array.swap(comp_shape.m_array);
    this->m_size = CompShape.m_size;
  }
  return *this;
}

romanov::CompositeShape &romanov::CompositeShape::operator=(CompositeShape &&CompShape)
{
  this->m_array.swap(CompShape.m_array);
  this->m_size = CompShape.m_size;
  CompShape.m_array.reset();
  CompShape.m_size = 0;
  return *this;
}

void romanov::CompositeShape::addShape(const std::shared_ptr<romanov::Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("There is not shape");
  }

  std::unique_ptr < std::shared_ptr<Shape>[] > new_array(new std::shared_ptr<Shape>[m_size + 1]);

  new_array[m_size] = shape;

  for (size_t i = 0; i < m_size; i++)
  {
    new_array[i] = m_array[i];
  }

  m_array.swap(new_array);

  m_size += 1;
}

void romanov::CompositeShape::removeShape(size_t n)
{
  if (m_size == 0)
  {
    throw std::out_of_range("CompositeShape is empty");
  }
  else
  {
    if ((n > m_size) || (n < 1))
    {
      throw std::out_of_range("Out of range");
    }
  }
  --n; //Because array starts from zero, but shape counter (to display) from 1
  if (m_size == 1)
  {
    resetShape();
  }
  else
  {
    std::unique_ptr<std::shared_ptr<Shape>[]>tempShape(new std::shared_ptr<Shape>[m_size - 1]);
    if (n != 0)
    {
      for (size_t i = 0; i < n; i++)
      {
        tempShape[i] = m_array[i];
      }
    }
    for (size_t i = n; i<m_size-1; i++)
    {
      tempShape[i] = m_array[i+1];
    }
    m_array.swap(tempShape);
    m_size--;
  }
}

void romanov::CompositeShape::resetShape()
{
  m_array.reset();
  m_array = nullptr;
  m_size = 0;
}

double romanov::CompositeShape::getArea() const noexcept
{
  double m_area = 0.0;

  for (size_t i = 0; i < m_size; i++)
  {
    m_area += m_array[i]->getArea();
  }

  return m_area;
}

romanov::rectangle_t romanov::CompositeShape::getFrameRect() const noexcept
{
  rectangle_t tempFRect = m_array[0]->getFrameRect();
  point_t top_right = { tempFRect.pos.x + tempFRect.width / 2, tempFRect.pos.y + tempFRect.height / 2 };
  point_t bottom_left = { tempFRect.pos.x - tempFRect.width / 2, tempFRect.pos.y - tempFRect.height / 2 };

  for (size_t i = 1; i < m_size; i++)
  {
    rectangle_t tempRect = m_array[i]->getFrameRect();
    point_t top_right_i = {tempRect.pos.x + tempRect.width / 2, tempRect.pos.y + tempRect.height / 2 };
    point_t bottom_left_i = { tempRect.pos.x - tempRect.width / 2, tempRect.pos.y - tempRect.height / 2 };

    if (top_right_i.x > top_right.x)
    {
      top_right.x = top_right_i.x;
    }

    if (top_right_i.y > top_right.y)
    {
      top_right.y = top_right_i.y;
    }

    if (bottom_left_i.x < bottom_left.x)
    {
      bottom_left.x = bottom_left_i.x;
    }

    if (bottom_left_i.y < bottom_left.y)
    {
      bottom_left.y = bottom_left_i.y;
    }
  }

  double height = top_right.y - bottom_left.y;
  double width = top_right.x - bottom_left.x;
  point_t pos = { (top_right.x + bottom_left.x) / 2 , (top_right.y + bottom_left.y) / 2 };

  return{ width, height, pos };
}

void romanov::CompositeShape::move(double dx, double dy) noexcept
{
  for (size_t i = 0; i < m_size; i++)
  {
    m_array[i]->move(dx, dy);
  }
}

void romanov::CompositeShape::move(const point_t &newpos) noexcept
{
  double dx = (newpos.x - getFrameRect().pos.x);
  double dy = (newpos.y - getFrameRect().pos.y);

  for (size_t i = 0; i < m_size; i++)
  {
    m_array[i]->move(dx, dy);
  }
}

void romanov::CompositeShape::scale(double ScaleK)
{
  if (ScaleK < 0)
  {
    throw std::invalid_argument("Incorrect coefficient");
  }
  point_t tempCenter = getFrameRect().pos;
  for (size_t i = 0; i < m_size; i++)
  {
    double dx = (m_array[i]->getFrameRect().pos.x - tempCenter.x) * (ScaleK - 1);
    double dy = (m_array[i]->getFrameRect().pos.y - tempCenter.y) * (ScaleK - 1);

    m_array[i]->move(dx, dy);
    m_array[i]->scale(ScaleK);
  }
}

void romanov::CompositeShape::getInform(std::ostream &out) const
{
  for (size_t i = 0; i < m_size; i++)
  {
    out << "Inform for shape number " << (i + 1) << ":\n";
    m_array[i]->getInform(out);
  }
}

void romanov::CompositeShape::rotate(const double angle) noexcept
{
  const point_t currPos = getFrameRect().pos;
  const double angleConverted = angle * M_PI / 180; //Convertion from degrees to radians.
  for (size_t i = 0; i < m_size; i++)
  {
    point_t tempFRect = m_array[i]->getFrameRect().pos;
    double r = sqrt(pow(tempFRect.x - currPos.x, 2) + pow(tempFRect.y - currPos.y, 2));
    m_array[i]->move({ currPos.x + r * cos(angleConverted), currPos.y + r * sin(angleConverted) });
    m_array[i]->rotate(angle);
  }
}
