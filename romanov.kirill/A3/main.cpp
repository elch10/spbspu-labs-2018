#include <memory>
#include <iostream>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

using namespace romanov;
using shared = std::shared_ptr <romanov::Shape>;

void ShowComp(CompositeShape& form)
{
  std::cout << "Composite center:" << std::endl
  << "x=" << form.getFrameRect().pos.x << std::endl
  << "y=" << form.getFrameRect().pos.y << std::endl
  << "Width: " << form.getFrameRect().width << std::endl
  << "Height: " << form.getFrameRect().height << std::endl
  << std::endl;
}

int main()
{
  Rectangle testRect( 5, 10,{ 10, 10 } );
  Circle testCircle(5, { 15, 15 });
  Triangle testTriangle({ 0.0, 0.0,  0.0, 10.0,  10.0, 0.0 });
  shared rectptr = std::make_shared <Rectangle>(testRect);
  shared circptr = std::make_shared <Circle>(testCircle);
  shared triptr = std::make_shared <Triangle>(testTriangle);
  CompositeShape Composite(rectptr);
  Composite.addShape(circptr);
  Composite.addShape(triptr);
  std::cout << "Figures:" << std::endl;
  ShowComp(Composite);
  Composite.getInform(std::cout);
  Composite.move(5, 5);
  std::cout << std::endl << "Coordinates increased for (5, 5):" << std::endl;
  ShowComp(Composite);
  Composite.getInform(std::cout);
  Composite.move({ 5, 5 });
  std::cout << std::endl << "Coordinates changed to (5, 5):" << std::endl;
  ShowComp(Composite);
  Composite.getInform(std::cout);
  Composite.scale(2);
  std::cout << std::endl << "Scaled x2:" << std::endl;
  ShowComp(Composite);
  Composite.getInform(std::cout);
  Composite.removeShape(3);
  std::cout << std::endl << "Removed shape 3 (triangle)" <<std::endl;
  ShowComp(Composite);
  Composite.getInform(std::cout);
  return 0;
}
