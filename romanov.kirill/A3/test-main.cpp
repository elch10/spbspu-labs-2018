#define BOOST_TEST_MAIN
#include <cmath>
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#define _USE_MATH_DEFINES
#include "composite-shape.hpp"

using namespace romanov;
using shared = std::shared_ptr <romanov::Shape>;

BOOST_AUTO_TEST_CASE(InvalidParametrs)
{
  shared rect = std::make_shared<class Rectangle>(Rectangle(3, 2, { 0, 0 }));
  shared circ = std::make_shared<class Circle>(Circle(3, { 1, 3 }));
  CompositeShape composite(rect);
  composite.addShape(circ);
  CompositeShape EmptyComposite;
  BOOST_CHECK_THROW(EmptyComposite.addShape(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(EmptyComposite.removeShape(1), std::out_of_range);
  BOOST_CHECK_THROW(composite.removeShape(3), std::out_of_range);
  BOOST_CHECK_THROW(composite.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(EmptyComposite[2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(CompositeShapeArea)
{
  shared rect = std::make_shared<class Rectangle>(Rectangle(3, 2, { 0, 0 }));
  shared circ = std::make_shared<class Circle>(Circle(3, { 1, 3 }));
  CompositeShape composite(rect);
  composite.addShape(circ);
  BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), 3 * 3 * M_PI + 3 * 2, 1e-10);
}

BOOST_AUTO_TEST_CASE(CompositeGetFrameRect)
{
  shared rect = std::make_shared<class Rectangle>(Rectangle(30, 20, { 10, 15 }));
  shared circ = std::make_shared<class Circle>(Circle(15, { 0, 10 }));
  CompositeShape composite(rect);
  composite.addShape(circ);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite.getFrameRect().height, 30);
}

BOOST_AUTO_TEST_CASE(CompositeMoveDxDy)
{
  shared rect = std::make_shared<class Rectangle>(Rectangle(30, 20, { 10, 15 }));
  shared circ = std::make_shared<class Circle>(Circle(15, { 0, 10 }));
  CompositeShape composite(rect);
  composite.addShape(circ);
  composite.move(15, 15);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, 20);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, 25);
  BOOST_CHECK_EQUAL(composite.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite.getFrameRect().height, 30);
}

BOOST_AUTO_TEST_CASE(CompositeMoveTo)
{
  shared rect = std::make_shared<class Rectangle>(Rectangle(30, 20, { 10, 15 }));
  shared circ = std::make_shared<class Circle>(Circle(15, { 0, 10 }));
  CompositeShape composite(rect);
  composite.addShape(circ);
  composite.move({ 15, 15 });
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, 15);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, 15);
  BOOST_CHECK_EQUAL(composite.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite.getFrameRect().height, 30);
}

BOOST_AUTO_TEST_CASE(CompositeScale)
{
  shared rect = std::make_shared<class Rectangle>(Rectangle(30, 20, { 10, 15 }));
  shared circ = std::make_shared<class Circle>(Circle(15, { 0, 10 }));
  CompositeShape composite(rect);
  composite.addShape(circ);
  const double area = composite.getArea();
  const rectangle_t frame = composite.getFrameRect();
  const double k = 0.5;
  composite.scale(k);
  BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area * k * k, 1e-10);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, frame.pos.x);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, frame.pos.y);
  BOOST_CHECK_EQUAL(composite.getFrameRect().width, frame.width * k);
  BOOST_CHECK_EQUAL(composite.getFrameRect().height, frame.height * k);
}

BOOST_AUTO_TEST_CASE(CompositeScale2)
{
  shared rect = std::make_shared<class Rectangle>(Rectangle(30, 20, { 10, 15 }));
  shared rect1 = std::make_shared<class Rectangle>(Rectangle(20, 20, { 0, -15 }));
  shared circ = std::make_shared<class Circle>(Circle(15, { 0, 10 }));
  CompositeShape composite(rect);
  composite.addShape(rect1);
  composite.addShape(circ);
  const double area = composite.getArea();
  const rectangle_t frame = composite.getFrameRect();
  const double k = 2.0;
  composite.scale(2);
  BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), area * k * k, 1e-10);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, frame.pos.x);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, frame.pos.y);
  BOOST_CHECK_EQUAL(composite.getFrameRect().width, frame.width * k);
  BOOST_CHECK_EQUAL(composite.getFrameRect().height, frame.height * k);
}

BOOST_AUTO_TEST_CASE(MoveConstructor)
{
  shared rect = std::make_shared<class Rectangle>(Rectangle(30, 20, { 10, 15 }));
  shared circ = std::make_shared<class Circle>(Circle(15, { 0, 10 }));
  CompositeShape composite(rect);
  composite.addShape(circ);
  CompositeShape composite1(std::move(composite));
  BOOST_CHECK_EQUAL(composite1.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite1.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite1.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite1.getFrameRect().height, 30);
}

BOOST_AUTO_TEST_CASE(CopyConstructor)
{
  shared rect = std::make_shared<class Rectangle>(Rectangle(30, 20, { 10, 15 }));
  shared circ = std::make_shared<class Circle>(Circle(15, { 0, 10 }));
  CompositeShape composite(circ);
  composite.addShape(rect);
  CompositeShape composite1(rect);
  composite1 = composite;
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite.getFrameRect().height, 30);
  BOOST_CHECK_EQUAL(composite1.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite1.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite1.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite1.getFrameRect().height, 30);
}
