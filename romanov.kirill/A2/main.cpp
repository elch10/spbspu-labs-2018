#include <memory>
#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

using namespace romanov;

void printinfo(Shape& form)
{
  std::cout << "Area: " << form.getArea() << std::endl
  << "X: " << form.getFrameRect().pos.x << ", " << "Y: " << form.getFrameRect().pos.y << std::endl
  << "Width: " << form.getFrameRect().width << ", " << "Height: " << form.getFrameRect().height << std::endl
  << std::endl;
}


int main()
{
  Circle circle(2, { 0,0 });
  std::cout << "Circle \n";
  printinfo(circle);
  circle.scale(2);
  printinfo(circle);
  Rectangle rectangle(2, 4, { 0,0 });
  std::cout << "Rectangle \n";
  printinfo(rectangle);
  rectangle.scale(2);
  printinfo(rectangle);
  Triangle triangle({ 0.0, 0.0,  0.0, 1.0,  1.0, 0.0 });
  std::cout << "Triangle \n";
  printinfo(triangle);
  triangle.scale(2);
  printinfo(triangle);

  return 0;
}
