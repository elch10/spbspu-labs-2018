#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "polygon.hpp"

int main()
{
  try
  {
     std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({ 5.0, 5.0 }, 5.0, 7.0));
     std::shared_ptr<vladykin::Shape> circ = std::make_shared<vladykin::Circle>(vladykin::Circle({ -10.0, 2.0 }, 5.0));
     std::shared_ptr<vladykin::Shape> tria = std::make_shared<vladykin::Triangle>(vladykin::Triangle({ 5.0, 5.0 }, { -2.0 , 3.0 }, {3.0, 0.0}));
     vladykin::CompositeShape compShape(rect);
     compShape.addShape(circ);
     compShape.addShape(tria);
     std::cout << "Composite shape area is " << compShape.getArea() << std::endl;
     compShape.showSize();
     std::cout << "Width of frame rectangle is " << compShape.getFrameRect().width << std::endl;
     std::cout << "Height of frame rectangle is " << compShape.getFrameRect().height << std::endl << std::endl;
     compShape.scale(2.0);
     std::cout << "After scaling: " << std::endl;
     std::cout << "Composite shape area is " << compShape.getArea() << std::endl << std::endl;
     compShape.move({ 10.0, 10.0 });
     std::cout << "The center of the Composite Shape was moved." << std::endl;
     compShape.showPos();
     std::cout << "Width of frame rectangle is " << compShape.getFrameRect().width << std::endl;
     std::cout << "Height of frame rectangle is " << compShape.getFrameRect().height << std::endl << std::endl;
     compShape.move(-10.0, -10.0);
     std::cout << "The center of the Composite Shape was moved." << std::endl;
     compShape.showPos();
     std::cout << "Width of frame rectangle is " << compShape.getFrameRect().width << std::endl;
     std::cout << "Height of frame rectangle is " << compShape.getFrameRect().height << std::endl << std::endl;
     compShape.deleteShape(0);
     std::cout << "The first figure was deleted. " << std::endl;
     std::cout << "Composite shape area is " << compShape.getArea() << std::endl;
     compShape.showSize();
     std::cout << "Width of frame rectangle is " << compShape.getFrameRect().width << std::endl;
     std::cout << "Height of frame rectangle is " << compShape.getFrameRect().height << std::endl << std::endl;
     compShape.rotate(45.0);
     std::cout << "Rotation " << std::endl;
     std::cout << "Width of frame rectangle is " << compShape.getFrameRect().width << std::endl;
     std::cout << "Height of frame rectangle is " << compShape.getFrameRect().height << std::endl << std::endl;
     std::cout << "Matrix: " << std::endl;
     std::shared_ptr<vladykin::Shape> rect2 = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({ -10.0,10.0 }, 10.0, 5.0));
     std::shared_ptr<vladykin::Shape> circ2 = std::make_shared<vladykin::Circle>(vladykin::Circle({ 10.0, 10.0 }, 6.0));
     vladykin::Matrix matrix(rect2);
     matrix.addShape(circ2);
     matrix.addCompShape(compShape);
     std::cout << "In this matrix there are " << matrix.getRows() << " layers" << std::endl;
     std::cout << "In this matrix there are " << matrix.getColumns() << " columns" << std::endl << std::endl;
     matrix.showSize();
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}
