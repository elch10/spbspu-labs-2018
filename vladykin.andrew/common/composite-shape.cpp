#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <cmath>

vladykin::CompositeShape::CompositeShape() :
  shapes_(nullptr),
  size_(0)
{
}

vladykin::CompositeShape::CompositeShape(const std::shared_ptr<Shape> &obj) :
  shapes_(new std::shared_ptr<Shape>[1]),
  size_(1)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Empty pointer");
  }
  shapes_[0] = obj;
}

vladykin::CompositeShape::CompositeShape(const CompositeShape &obj) :
  shapes_(new std::shared_ptr<Shape>[obj.size_]),
  size_(obj.size_)
{
  for (size_t i = 0; i < obj.size_; i++)
  {
    shapes_[i] = obj.shapes_[i];
  }
}

vladykin::CompositeShape::CompositeShape(CompositeShape &&obj) :
  size_(obj.size_)
{
  shapes_ = std::move(obj.shapes_);
  obj.size_ = 0;
  obj.shapes_ = nullptr;
}

vladykin::CompositeShape &vladykin::CompositeShape::operator=(const CompositeShape & obj)
{
  if (this != &obj)
  {
    size_ = obj.size_;
    std::unique_ptr<std::shared_ptr<Shape>[]> shapeCopy(new std::shared_ptr<Shape>[size_]);
    for (size_t i = 0; i < size_; i++)
    {
      shapeCopy[i] = obj.shapes_[i];
    }
    shapes_.swap(shapeCopy);
  }
  return *this;
}

vladykin::CompositeShape &vladykin::CompositeShape::operator=(CompositeShape && obj)
{
  if (this != &obj)
  {
    shapes_ = std::move(obj.shapes_);
    size_ = obj.size_;
    obj.size_ = 0;
  }
  return *this;
}

std::shared_ptr<vladykin::Shape> &vladykin::CompositeShape::operator[](const size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Index is out range");
  }
  return shapes_[index];
}

void vladykin::CompositeShape::addShape(const std::shared_ptr<Shape> &obj)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> newShapes((new std::shared_ptr<Shape>[size_ + 1]));
  for (size_t i = 0; i < size_; i++)
  {
    newShapes[i] = shapes_[i];
  }
  newShapes[size_] = obj;
  size_++;
  shapes_.swap(newShapes);
}

void vladykin::CompositeShape::deleteShape(const size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("Index is out range");
  }
  if (size_ == 0 )
  {
    throw std::invalid_argument("Object is empty");
  }
  for (size_t i = index; i < size_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[size_ - 1] = nullptr;
  size_--;
}

double vladykin::CompositeShape::getArea() const
{
  double area = 0.0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

vladykin::rectangle_t vladykin::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    return { 0.0 , 0.0,{ 0.0 , 0.0 } };
  }
  vladykin::rectangle_t frameRect = shapes_[0]->getFrameRect();
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  for (size_t i = 1; i < size_; i++)
  {
    frameRect = shapes_[i]->getFrameRect();
    if ((frameRect.pos.x - frameRect.width / 2) < minX)
    {
      minX = frameRect.pos.x - frameRect.width / 2;
    }
    if ((frameRect.pos.x + frameRect.width / 2) > maxX)
    {
      maxX = frameRect.pos.x + frameRect.width / 2;
    }
   if ((frameRect.pos.y + frameRect.height / 2) > maxY)
   {
     maxY = frameRect.pos.y + frameRect.height / 2;
   }
   if ((frameRect.pos.y - frameRect.height / 2) < minY)
   {
     minY = frameRect.pos.y - frameRect.height / 2;
   }
  }
  return { (maxX - minX), (maxY - minY),{ (minX + (maxX - minX) / 2), (minY + (maxY - minY) / 2) } };
}

void vladykin::CompositeShape::move(const point_t & newPos)
{
  move(newPos.x - getFrameRect().pos.x, newPos.y - getFrameRect().pos.y);
}

void vladykin::CompositeShape::move(const double dx, const double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void vladykin::CompositeShape::scale(const double coeff)
{
  if (coeff < 0.0)
  {
    throw std::invalid_argument("Coefficient < 0");
  }
  vladykin::point_t compShapeCenter = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    vladykin::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move({(compShapeCenter.x + coeff * (shapeCenter.x - compShapeCenter.x)), 
(compShapeCenter.y + coeff * (shapeCenter.y - compShapeCenter.y))});
    shapes_[i]->scale(coeff);
  }
}

void vladykin::CompositeShape::showSize() const
{
  std::cout << "Composite Shape size is " << size_ << std::endl;
}

void vladykin::CompositeShape::showPos() const
{
  std::cout << "Coordinates of the center of  Composite Shape ";
  std::cout << "X = " << getFrameRect().pos.x << " Y = " << getFrameRect().pos.y << std::endl;
}

void vladykin::CompositeShape::rotate(double angle)
{
  double cosAngle = cos(angle * M_PI / 180);
  double sinAngle = sin(angle * M_PI / 180);

  point_t center = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    point_t shape_center = shapes_[i]->getFrameRect().pos;

    shapes_[i]->move({ center.x + cosAngle * (shape_center.x - center.x) - sinAngle * (shape_center.y - center.y),
      center.y + cosAngle * (shape_center.y - center.y) + sinAngle * (shape_center.x - center.x) });
    shapes_[i]->rotate(angle);
  }
}

size_t vladykin::CompositeShape::getSize() const
{
  return size_;
}
