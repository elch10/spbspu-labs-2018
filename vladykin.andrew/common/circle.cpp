#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <iostream>
#include <cmath>

vladykin::Circle::Circle(const point_t &pos, const double radius) :
  pos_(pos),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("Radius <= 0");
  }
}

double vladykin::Circle::getArea() const
{
  return (radius_ * radius_ * M_PI);
}

vladykin::rectangle_t vladykin::Circle::getFrameRect() const
{
  return { radius_ * 2, radius_ * 2, pos_ };
}

void vladykin::Circle::showSize() const
{
  std::cout << "Radius of the circle: " << radius_ << std::endl;
}

void vladykin::Circle::showPos() const
{
  std::cout << "Coordinates of the center of the circle ";
  std::cout << "X = " << pos_.x << " Y = " << pos_.y << std::endl;
}

void vladykin::Circle::move(const point_t &pos)
{
  pos_ = pos;
}

void vladykin::Circle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void vladykin::Circle::scale(const double coeff)
{
  if (coeff < 0.0)
  {
    throw std::invalid_argument("Coefficient < 0");
  }
  radius_ *= coeff;
}

void vladykin::Circle::rotate(const double /*angle*/)
{
}
