#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <memory>
#include "shape.hpp"

namespace vladykin
{
  class Polygon : public Shape
  {
  public:
    Polygon(std::initializer_list<point_t> vertices);
    point_t operator[](size_t index) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &pos) override;
    void move(double dx, double dy) override;
    void showSize() const override;
    void showPos() const override;
    void scale(double coeff) override;
    void rotate(double angle) override;
    bool checkConvex() const;
  private:
    std::unique_ptr<point_t[]> vertices_;
    size_t count_;
    point_t pos_;
  };
}
#endif
