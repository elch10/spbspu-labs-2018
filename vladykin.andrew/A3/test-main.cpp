#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double tolerance = 0.000001;

BOOST_AUTO_TEST_SUITE(rectangleTests)

  BOOST_AUTO_TEST_CASE(Constructor)
  {
    BOOST_CHECK_THROW(vladykin::Rectangle rectangle({0.0, 0.0}, -1.0, 2.0), std::invalid_argument);
    BOOST_CHECK_THROW(vladykin::Rectangle rectangle({ 0.0, 0.0 }, 1.0, -2.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(moveToPoint)
  {
    vladykin::Rectangle rectangle({0.0, 0.0}, 10.0, 20.0);
    rectangle.move({4.0, 4.0});
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, 4.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, 4.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 10.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 20.0,tolerance);
    BOOST_CHECK_CLOSE(rectangle.getArea(), 200.0, tolerance);
  }
 
  BOOST_AUTO_TEST_CASE(moveXY)
  {
    vladykin::Rectangle rectangle({0.0, 0.0}, 10.0, 20.0);
    rectangle.move(4.0, 4.0);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, 4.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, 4.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 10.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 20.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getArea(), 200.0, tolerance);
  }

  BOOST_AUTO_TEST_CASE(scale)
  {
    vladykin::Rectangle rectangle({0.0, 0.0}, 10.0, 20.0);
    rectangle.scale(2.0);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 20.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 40.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getArea(), 800.0, tolerance);
    BOOST_CHECK_THROW(rectangle.scale(-1.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(circleTests)

  BOOST_AUTO_TEST_CASE(Constructor)
  {
    BOOST_CHECK_THROW(vladykin::Circle circle({0.0, 0.0}, -5.0),std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(moveToPoint)
  {
    vladykin::Circle circle({0.0, 0.0}, 5.0);
    double area = circle.getArea();
    circle.move({1.0, 1.0});
    BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, 1.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, 1.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getFrameRect().width / 2.0, 5.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getArea(), area, tolerance);
  }

  BOOST_AUTO_TEST_CASE(moveXY)
  {
    vladykin::Circle circle({0.0, 0.0}, 5.0);
    double area = circle.getArea();
    circle.move(5.0, 5.0);
    BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, 5.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, 5.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getFrameRect().width / 2.0, 5.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getArea(), area, tolerance);
  }

  BOOST_AUTO_TEST_CASE(scale)
  {
    vladykin::Circle circle({0.0, 0.0}, 5.0);
    double area = circle.getArea();
    double coeff = 2.0;
    circle.scale(coeff);
    BOOST_CHECK_CLOSE(circle.getFrameRect().width / 2.0, 10.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getArea(), coeff*coeff*area, tolerance);
    BOOST_CHECK_THROW(circle.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(triangleTests)

  BOOST_AUTO_TEST_CASE(Constructor)
  {
    BOOST_CHECK_THROW(vladykin::Triangle triangle({1.0, 2}, {1.0, 3}, {1.0, 4}), std::invalid_argument);
    BOOST_CHECK_THROW(vladykin::Triangle triangle({2, 1.0}, {3.0, 1.0}, {4.0, 1.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(moveToPoint)
  {
    vladykin::Triangle triangle({0.0, -3.0}, {0.0, 3.0}, {3.0, 0.0});
    const double area = triangle.getArea();
    vladykin::rectangle_t rect(triangle.getFrameRect());
    triangle.move({1.0, 1.0});
    BOOST_CHECK_CLOSE(triangle.getFrameRect().pos.x, rect.pos.x, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().pos.y, rect.pos.y, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, rect.width, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, rect.height, tolerance);
    BOOST_CHECK_CLOSE(triangle.getArea(), area, tolerance);
  }


  BOOST_AUTO_TEST_CASE(moveXY)
  {
    vladykin::Triangle triangle({ 0.0, -3.0 }, { 0.0, 3.0 }, { 3.0, 0.0 });
    const double area = triangle.getArea();
    vladykin::rectangle_t rect(triangle.getFrameRect());
    triangle.move(3.0, 3.0);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().pos.x, rect.pos.x, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().pos.y, rect.pos.y, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, rect.width, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, rect.height, tolerance);
    BOOST_CHECK_CLOSE(triangle.getArea(), area, tolerance);
  }


  BOOST_AUTO_TEST_CASE(scale)
  {
    vladykin::Triangle triangle({ 0.0, -3.0 }, { 0.0, 3.0 }, { 3.0, 0.0 });
    const double area = triangle.getArea();
    vladykin::rectangle_t rect(triangle.getFrameRect());
    const double coeff = 2.0;
    triangle.scale(coeff);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, rect.width * coeff, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, rect.height * coeff, tolerance);
    BOOST_CHECK_CLOSE(triangle.getArea(), coeff*coeff*area, tolerance);
    BOOST_CHECK_THROW(triangle.scale(-3.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(compositeShapeTests)

  BOOST_AUTO_TEST_CASE(Constructor)
  {
    std::shared_ptr<vladykin::Shape> emptyShape = nullptr;
    BOOST_CHECK_THROW(vladykin::CompositeShape compShape(emptyShape), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(copyConstructor)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({1.0, 6.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    vladykin::CompositeShape compShape2(compShape);
    for (size_t i = 0; i < 1; i++)
    {
      vladykin::rectangle_t rect1 = compShape[i]->getFrameRect();
      vladykin::rectangle_t rect2 = compShape2[i]->getFrameRect();
      BOOST_CHECK_CLOSE(rect1.height, rect2.height, tolerance);
      BOOST_CHECK_CLOSE(rect1.width, rect2.width, tolerance);
      BOOST_CHECK_CLOSE(rect1.pos.x, rect2.pos.x, tolerance);
      BOOST_CHECK_CLOSE(rect1.pos.y, rect2.pos.y, tolerance);
    }
  }

  BOOST_AUTO_TEST_CASE(copyOperator)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({1.0, 6.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    std::shared_ptr<vladykin::Shape> rect2 = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({1.0, 5.0}, 7.0, 8.0));
    vladykin::CompositeShape compShape2(rect2);
    compShape2.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({6.0, 6.0}, 8.0)));
    compShape = compShape2;
    for (size_t i = 0; i < 1; i++)
    {
      vladykin::rectangle_t rectangle = compShape[i]->getFrameRect();
      vladykin::rectangle_t rectangle2 = compShape2[i]->getFrameRect();
      BOOST_CHECK_CLOSE(rectangle.height, rectangle2.height, tolerance);
      BOOST_CHECK_CLOSE(rectangle.width, rectangle2.width, tolerance);
      BOOST_CHECK_CLOSE(rectangle.pos.x, rectangle2.pos.x, tolerance);
      BOOST_CHECK_CLOSE(rectangle.pos.y, rectangle2.pos.y, tolerance);
    }
  }

  BOOST_AUTO_TEST_CASE(moveConstructor)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({1.0, 6.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    vladykin::CompositeShape compShape2(compShape);
    vladykin::CompositeShape compShape3(std::move(compShape));
    for (size_t i = 0; i < 1; i++)
    {
      vladykin::rectangle_t rect2 = compShape2[i]->getFrameRect();
      vladykin::rectangle_t rect3 = compShape3[i]->getFrameRect();
      BOOST_CHECK_CLOSE(rect2.height, rect3.height, tolerance);
      BOOST_CHECK_CLOSE(rect2.width, rect3.width, tolerance);
      BOOST_CHECK_CLOSE(rect2.pos.x, rect3.pos.x, tolerance);
      BOOST_CHECK_CLOSE(rect2.pos.y, rect3.pos.y, tolerance);
   }
  }

  BOOST_AUTO_TEST_CASE(moveOperator)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({1.0, 6.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    vladykin::CompositeShape compShape2(compShape);
    std::shared_ptr<vladykin::Shape> rect2 = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({2.0, 5.0}, 7.0, 7.0));
    vladykin::CompositeShape compShape3(rect2);
    compShape3.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({6.0, 6.0}, 5.0)));
    compShape3 = std::move(compShape);
    for (size_t i = 0; i < 1; i++)
    {
      vladykin::rectangle_t rectangle2 = compShape2[i]->getFrameRect();
      vladykin::rectangle_t rectangle3 = compShape3[i]->getFrameRect();
      BOOST_CHECK_CLOSE(rectangle2.height, rectangle3.height, tolerance);
      BOOST_CHECK_CLOSE(rectangle2.width, rectangle3.width, tolerance);
      BOOST_CHECK_CLOSE(rectangle2.pos.x, rectangle3.pos.x, tolerance);
      BOOST_CHECK_CLOSE(rectangle2.pos.y, rectangle3.pos.y, tolerance);
    }
  }

  BOOST_AUTO_TEST_CASE(Index)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({1.0, 6.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    BOOST_CHECK_THROW(compShape[3], std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(moveToPoint)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({1.0, 6.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    vladykin::rectangle_t rectangle1 = compShape.getFrameRect();
    const double area1 = compShape.getArea();
    compShape.move({7.0,7.0});
    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.x, 7.0, tolerance);
    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.y, 7.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle1.height, compShape.getFrameRect().height, tolerance);
    BOOST_CHECK_CLOSE(rectangle1.width, compShape.getFrameRect().width, tolerance);
    BOOST_CHECK_CLOSE(area1, compShape.getArea(), tolerance);
  }

  BOOST_AUTO_TEST_CASE(moveXY)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({1.0, 6.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    vladykin::rectangle_t rectangle1 = compShape.getFrameRect();
    const double area1 = compShape.getArea();
    compShape.move(7.0,7.0);
    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.x, rectangle1.pos.x + 7.0, tolerance);
    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.y, rectangle1.pos.y + 7.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle1.height, compShape.getFrameRect().height, tolerance);
    BOOST_CHECK_CLOSE(rectangle1.width, compShape.getFrameRect().width, tolerance);
    BOOST_CHECK_CLOSE(area1, compShape.getArea(), tolerance);
  }

  BOOST_AUTO_TEST_CASE(scale)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({1.0, 6.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    vladykin::rectangle_t rectangle1 = compShape.getFrameRect();
    const double area1 = compShape.getArea();
    compShape.scale(2.0);
    BOOST_CHECK_CLOSE(area1 * 4.0, compShape.getArea(), tolerance);
    BOOST_CHECK_CLOSE(rectangle1.height * 2, compShape.getFrameRect().height, tolerance);
    BOOST_CHECK_CLOSE(rectangle1.width * 2, compShape.getFrameRect().width, tolerance);
    BOOST_CHECK_CLOSE(rectangle1.pos.x, compShape.getFrameRect().pos.x, tolerance);
    BOOST_CHECK_CLOSE(rectangle1.pos.y, compShape.getFrameRect().pos.y, tolerance);
  }

  BOOST_AUTO_TEST_CASE(invalidScale)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({5.0, 5.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    BOOST_CHECK_THROW(compShape.scale(-2.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(invalidAdd)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({5.0, 5.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    BOOST_CHECK_THROW(compShape.addShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(addShape)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({5.0, 5.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    BOOST_CHECK_EQUAL(compShape.getSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(invalidDelete)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({5.0, 5.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    BOOST_CHECK_THROW(compShape.deleteShape(5), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(deleteShape)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({5.0, 5.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({5.0, 5.0}, 7.0)));
    compShape.addShape(std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({3.0, 3.0}, 7.0, 7.0)));
    compShape.addShape(std::make_shared<vladykin::Circle>(vladykin::Circle({3.0, 7.0}, 10.0)));
    compShape.deleteShape(1);
    BOOST_CHECK_EQUAL(compShape.getSize(), 3);
  }

  BOOST_AUTO_TEST_CASE(getArea)
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({5.0, 5.0}, 10.0, 8.0));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({8.0, 8.0}, 5.0, 3.0)));
    BOOST_CHECK_CLOSE(compShape.getArea(), 95.0, tolerance);
  }

BOOST_AUTO_TEST_SUITE_END()
