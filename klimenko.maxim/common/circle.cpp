#include "circle.hpp"

#include <stdexcept>
#include <cmath>

using namespace klimenko;

klimenko::Circle::Circle(const double r, const point_t & point):
centre_(point),
radius_(r)
{
  if (r <= 0.0)
  {
    throw std::invalid_argument("Radius must be > 0");
  }
}

double klimenko::Circle::getArea() const noexcept
{
  return (M_PI * pow(radius_, 2));
}

point_t klimenko::Circle::getPos() noexcept
{
  return centre_;
}

rectangle_t klimenko::Circle::getFrameRect() const noexcept
{
  return rectangle_t({ (radius_ * 2.0), (radius_ * 2.0), { centre_.x, centre_.y } });
}

void klimenko::Circle::move(const point_t & point) noexcept
{
  centre_ = point;
}

void klimenko::Circle::move(const double dx, const double dy) noexcept
{
  centre_.x += dx;
  centre_.y += dy;
}

void klimenko::Circle::scale(const double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("factor must be > 0");
  }

  radius_ *= factor;
}

void klimenko::Circle::rotate(const double)
{
}
