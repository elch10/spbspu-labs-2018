#include "matrix.hpp"
#include <iostream>
#include <cmath>

using namespace klimenko;

Matrix::Matrix():
  shapes_(nullptr),
  line_(0),
  column_(0)
{
}

Matrix::Matrix(const Matrix & rhs):
  shapes_(new Shape *[rhs.line_ * rhs.column_]),
  line_(rhs.line_),
  column_(rhs.column_)
{
  for (int i = 0; i < (line_ * column_); i++)
  {
    shapes_[i] = rhs.shapes_[i];
  }
}

Matrix::Matrix(Matrix && rhs):
  shapes_(nullptr),
  line_(rhs.line_),
  column_(rhs.column_)
{
  shapes_.swap(rhs.shapes_);
  rhs.line_ = 0;
  rhs.column_ = 0;
}

Matrix &Matrix::operator=(Matrix & rhs)
{
  if (this != &rhs)
  {
    line_ = rhs.line_;
    column_ = rhs.column_;
    std::unique_ptr <Shape *[]> tmpArr (new Shape *[rhs.line_ * rhs.column_]);
    for (int i=0; i < (line_*column_); i++)
    {
      tmpArr[i] = rhs.shapes_[i];
    }
    shapes_.swap(tmpArr);
  }
  return *this;
}

Matrix &Matrix::operator=(Matrix && rhs)
{
  if (this != &rhs)
  {
    shapes_.swap(rhs.shapes_);
    line_ = rhs.line_;
    column_ = rhs.column_;
    rhs.shapes_ = nullptr;
    rhs.line_ = 0;
    rhs.column_ = 0;
  }
  return *this;
}

std::unique_ptr<Shape *[]> Matrix::operator[](int index) const
{
  if (index >= line_)
  {
    throw std::invalid_argument("Invalid index.");
  }
  std::unique_ptr<Shape *[]> tmpArr (new Shape *[column_]);
  for (int i = 0; i < column_; i++)
  {
    tmpArr[i] = shapes_[index * column_ + i];
  }
  return tmpArr;
}

bool Matrix::checkOverlapping(const Shape *shape1,const Shape *shape2)
{
  rectangle_t FrameRect1 = shape1->getFrameRect();
  rectangle_t FrameRect2 = shape2->getFrameRect();
  double dx = std::abs(FrameRect1.pos.x - FrameRect2.pos.x) - (FrameRect1.width+FrameRect2.width) / 2.0;
  double dy = std::abs(FrameRect1.pos.y - FrameRect2.pos.y) - (FrameRect1.height + FrameRect2.height) / 2.0;
  if ((dx < 0) && (dy < 0))
  {
    return true;
  }
  else
  {
    return false;
  }
}

void Matrix::add(Shape *newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("No new shape.");
  }
  if (line_ == 0)
  {
    line_ = 1;
    column_ = 1;
    std::unique_ptr <Shape *[]> tmpArr (new Shape *[1]);
    shapes_.swap(tmpArr);
    shapes_[0] = newShape;
  }
  else
  {
    bool added = false;
    for (int i = 0; added == false; i++)
    {
      for (int j = 0; j < column_; j++)
      {
        if (shapes_[i * column_ + j] == nullptr)
        {
          shapes_[i * column_ + j] = newShape;
          added = true;
          break;
        }
        else
        {
          if (checkOverlapping(shapes_[i * column_ + j],newShape))
          {
            break;
          }
        }
        if (j == (column_ - 1))
        {
          column_++;
          std::unique_ptr <Shape *[]> tmpArr (new Shape *[line_ * column_]);
          for (int i1 = 0; i1 < line_; i1++)
          {
            for (int j1 = 0; j1 < (column_ - 1); j1++)
            {
              tmpArr[i1 * column_ + j1] = shapes_[i1 * (column_ - 1) + j1];
            }
            tmpArr[(i1 + 1) * column_ - 1] = nullptr;
          }
          tmpArr[(i + 1) * column_ - 1] = newShape;
          shapes_.swap(tmpArr);
          added = true;
          break;
        }
      }
      if ((i == (line_ - 1)) && (added == false))
      {
        line_++;
        std::unique_ptr <Shape *[]> tmpArr (new Shape *[line_ * column_]);
        for (int i1 = 0; i1 < ((line_ - 1) * column_); i1++)
        {
          tmpArr[i1] = shapes_[i1];
        }
        for (int i1 = ((line_ - 1) * column_); i1 < (line_ * column_); i1++)
        {
          tmpArr[i1] = nullptr;
        }
        tmpArr[(line_ - 1) * column_] = newShape;
        shapes_.swap(tmpArr);
        added = true;
      }
    }
  }
}

void Matrix::printCurrentInfo() const
{
  std::cout << "\nMatrix size (columns * lines): " << column_ << " * " << line_ << std::endl;
}
