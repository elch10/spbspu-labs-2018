#include <iostream>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"

using namespace aleksovski;

int main()
{
  point_t p = {5.3, 8.1};
  Rectangle rect1 {5.3, 6.2, p};
  Circle circ1 {4.1, {7.8, 6.4}} ;
  CompositeShape comshape;
  comshape.add(rect1);
  comshape.add(circ1);
  double s = comshape.getArea();
  std::cout << "CompositeShape square=" << s << std::endl;
  double k = 0.5;
  std::cout << "Cooficient of scaling=" << k << std::endl;
  comshape.scale(k);
  comshape.printf();
  s = comshape.getArea();
  std::cout << "CompositeShape square=" << s << std::endl;
  std::cout << "Movement to point (" << p.x <<";" << p.y << ")" << std::endl;
  comshape.move(p);
  comshape.printf();
  double dx = 7.0, dy = 9.0;
  std::cout << "X-movement=" << dx << ";" << "Y-movement=" << dy << std::endl;
  comshape.move(dx, dy);
  comshape.printf();
  
  std::cout << std::endl;
  
  return 0;
}
