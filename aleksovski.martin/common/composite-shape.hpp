#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"
#include "matrix.hpp"

namespace aleksovski
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(CompositeShape &);
    CompositeShape(CompositeShape &&);
    Shape* operator[](size_t number) const;
    CompositeShape &operator = (CompositeShape &);
    CompositeShape &operator = (CompositeShape &&);
    void add(Shape & figure);
    void removeElement(size_t number);
    void addtoMatrix(Matrix & obj);
    void printf() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t & p)override;
    void scale(double k) override;
    size_t getSize() const;
    point_t getCentre() const;
    void rotate(double alpha) override;
  private:
    size_t m_size;
    std::unique_ptr <Shape *[]> m_massive;
  };
}
#endif // COMPOSITESHAPE_HPP
