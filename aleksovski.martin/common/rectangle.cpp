#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

using namespace aleksovski;

Rectangle::Rectangle(double width, double height, const point_t & point) :
  Shape(point),
  m_width(width),
  m_height(height),
  m_angle(0)
{
  if ((m_width < 0.0) || (m_height < 0.0))
  {
    throw std::invalid_argument("Incorrect value");
  }
}

double Rectangle::getWidth() const
{
  return m_width;
}

double Rectangle::getHeight() const
{
  return m_height;
}

double Rectangle::getArea() const
{
  return(m_width*m_height);
}

point_t Rectangle::getCentre() const
{
  return m_centre;
}

rectangle_t Rectangle::getFrameRect() const
{
  rectangle_t rect {std::fabs(m_width * std::cos(m_angle) + m_height * std::cos(M_PI / 2 - m_angle)),
      std::fabs(m_height * std::cos(m_angle) + m_width * std::cos(M_PI / 2 - m_angle)), m_centre};
 
  return rect;
}

void Rectangle::move(double dx, double dy)
{
  m_centre.x += dx;
  m_centre.y += dy;
}

void Rectangle::move(const point_t & p)
{
  m_centre.x = p.x;
  m_centre.y = p.y;
}

void Rectangle::scale(double k)
{ 
  if (k < 0.0)
  {
    throw std::invalid_argument("Incorrect value");
  }
  m_width = m_width * k;
  m_height = m_height * k;
}

void Rectangle::rotate(double alpha)
{
  m_angle = alpha;
}

void Rectangle::printf() const
{
  std::cout << "Width of rectangle = " << m_width << std::endl;
  std::cout << "Hight of rectangle = " << m_height << std::endl;
  std::cout << "Centre rectangle(" << m_centre.x << ";" << m_centre.y << ")" << std::endl;
  rectangle_t rect = getFrameRect();
  std::cout << "Centre of FrameRectangle (" << rect.pos.x << ";" << rect.pos.y << ")" << std::endl;
  std::cout << "Width and height of FrameRectangle (" << rect.width << ";" << rect.height << ")" << std::endl;
} 
