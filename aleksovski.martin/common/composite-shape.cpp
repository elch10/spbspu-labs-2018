#include <iostream>
#include <stdexcept>
#include <memory>
#include <cmath>
#include "matrix.hpp"
#include "composite-shape.hpp"

using namespace aleksovski;

CompositeShape::CompositeShape():
   Shape ({0,0}),
   m_size (0),
   m_massive (new Shape* [1])
  {}

CompositeShape::CompositeShape (CompositeShape && rhs):
  Shape ({0,0}),
  m_size(0),
  m_massive(nullptr)
  {
    *this = std::move(rhs);
  }

CompositeShape::CompositeShape(CompositeShape  & rhs):
  Shape ({0,0}),
  m_size (rhs.m_size),
  m_massive(new Shape *[rhs.m_size])
{
  m_massive.swap(rhs.m_massive);
}

void CompositeShape::add(Shape& figure)
{
  if (m_size == 0)
  {
    m_massive[m_size] = & figure;
  }
  else
  {
    std::unique_ptr <Shape *[]> temp_array (new Shape * [m_size+1]);
    for(size_t i = 0; i < m_size; i++)
    {
      temp_array[i] = m_massive[i];
    }
    temp_array[m_size] = & figure;
    m_massive.swap(temp_array);
  }
  m_size++;
}

double CompositeShape::getArea() const
{
  double s = 0;
  for (size_t i = 0; i < m_size; i++)
  {
    s = s + m_massive[i]->getArea();
  }
  return s;
}

void CompositeShape::move(double dx, double dy)
{
  rectangle_t rect = getFrameRect();
  double x = 0;
  double y = 0;
  for (size_t i = 0; i < m_size; i++)
  {
    x = rect.pos.x-m_massive[i]->getFrameRect().pos.x+dx;
    y = rect.pos.y-m_massive[i]->getFrameRect().pos.y+dy;
    m_massive[i]->move(x, y);
  }
}

void CompositeShape::move(const point_t  & p)
{
  rectangle_t rect = getFrameRect();
  double x =0 ;
  double y = 0;
  for (size_t i = 0; i < m_size; i++)
  {
    x = rect.pos.x-m_massive[i]->getFrameRect().pos.x + p.x;
    y = rect.pos.y-m_massive[i]->getFrameRect().pos.y + p.y;
    m_massive[i]->move(x, y);
   }
}

rectangle_t CompositeShape::getFrameRect() const
{
  rectangle_t rect = m_massive[0]->getFrameRect();
  if (m_size > 1)
  {
    point_t top{rect.pos.x, rect.pos.y + (rect.height / 2)};
    point_t bot{rect.pos.x, rect.pos.y - (rect.height / 2)};
    point_t right{rect.pos.x + (rect.width / 2), rect.pos.y};
    point_t left{rect.pos.x - (rect.width / 2), rect.pos.y};
    for (size_t i = 1; i < m_size; i++)
    {
      rect = m_massive[i]->getFrameRect();
      if ((top.y < rect.pos.y + rect.height / 2) & (top.x >= rect.pos.x + (rect.width / 2)) & (top.x <= rect.pos.y - (rect.width / 2)))
      {
        top.y = rect.pos.y + (rect.height / 2);
        top.x = rect.pos.x;
      }
      if ((bot.y > rect.pos.y - rect.height / 2) & (bot.x >= rect.pos.x + (rect.width / 2)) & (bot.x <= rect.pos.y - (rect.width / 2)))
      {
        bot.y = rect.pos.y - (rect.height / 2);
        bot.x = rect.pos.x;
      }
      if ((right.x > rect.pos.x + rect.width / 2) & (right.y <= rect.pos.y + (rect.height / 2)) & (right.y >= rect.pos.y - (rect.height / 2)))
      {
        right.x = (rect.pos.x + rect.width / 2);
        right.y = rect.pos.y;
      }
      if ((left.x < rect.pos.x - rect.width / 2) & (left.y < rect.pos.y + (rect.height / 2)) & (left.y > rect.pos.y - (rect.height / 2)))
      {
        left.x = rect.pos.x - (rect.width / 2);
        left.y = rect.pos.y;
      }
    }
  rect.height = top.y - bot.y;
  rect.width = right.x - left.x;
  rect.pos = {rect.width / 2, rect.height / 2};
  }
  return rect;
}

void CompositeShape::scale(double k) 
{
  if (k < 0.0)
  {
    throw std::invalid_argument("Incorrect value");
  }
  for (size_t i = 0; i < m_size; i++)
  {
    m_massive[i]->scale(k);
  }
}

size_t CompositeShape::getSize() const
{
  return m_size;
}

point_t CompositeShape::getCentre() const
{
  return m_centre;
}

void CompositeShape::rotate(double alpha)
{
  double gip = 0;
  double beta = 0;
  for (size_t i = 0; i < m_size; i++)
  {
    gip = sqrt( m_massive[i]->m_centre.x * m_massive[i]->m_centre.x + m_massive[i]->m_centre.x * m_massive[i]->m_centre.y);
    beta = std::atan(m_massive[i]->m_centre.y / m_massive[i]->m_centre.x);
    m_massive[i]->rotate(alpha);
    if ((m_massive[i]->m_centre.x > 0) & (m_massive[i]->m_centre.y > 0))
    {
      m_massive[i]->m_centre.x = std::cos(alpha + beta) * gip;
      m_massive[i]->m_centre.y = std::sin(alpha + beta) * gip;
    }
    else if ((m_massive[i]->m_centre.x < 0) & (m_massive[i]->m_centre.y < 0))
    {
      m_massive[i]->m_centre.x = std::cos(alpha + beta + M_PI) * gip;
      m_massive[i]->m_centre.y = std::sin(alpha + beta + M_PI) * gip;
    }
    else if ((m_massive[i]->m_centre.x > 0) & (m_massive[i]->m_centre.y < 0))
    {
      m_massive[i]->m_centre.x = std::cos(beta - alpha) * gip;
      m_massive[i]->m_centre.y = std::sin(beta - alpha) * gip;
    }
    else
    {
      m_massive[i]->m_centre.x = std::cos(beta - alpha + M_PI) * gip;
      m_massive[i]->m_centre.y = std::sin(beta - alpha + M_PI) * gip;
    }
  }
}

void CompositeShape::removeElement(size_t number)
{
  if (number > m_size)
  {
    throw std::out_of_range("Out of range");
  }
  m_massive[number] = nullptr;
  std::unique_ptr <Shape *[]> temp_array (new Shape * [m_size-1]);
  for (size_t i = 0; i < number; i++)
  {
    temp_array[i] = m_massive[i];
  }
  for (size_t i = number; i < (m_size - 1); i++)
  {
    temp_array[i] = m_massive[i+1];
  }
  m_massive.swap(temp_array);
  m_size--;
}

void CompositeShape::addtoMatrix(Matrix & obj)
{
  obj.setNumberOfFigures(m_size);
  obj.addFigure(m_massive);
  obj.getNumberLayers();
}

CompositeShape & CompositeShape::operator = ( CompositeShape & rhs)
{
  if (this != & rhs)
  {
    m_size = rhs.m_size;
    m_massive.swap(rhs.m_massive);
  }
  return *this;
}

CompositeShape & CompositeShape::operator = (CompositeShape && rhs)
{
  if (this != & rhs)
  {
    m_size = rhs.m_size;
    m_massive.swap(rhs.m_massive);
    rhs.m_massive = nullptr;
    rhs.m_size = 0;
  }
  return *this;
}

Shape* CompositeShape::operator[](size_t number) const
{
  if (number > m_size)
  {
    throw std::out_of_range("Incorrect value");
  }
  return m_massive[number];
}

void CompositeShape::printf() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "CompositeShape" << std::endl;
  std::cout << "Size = " << m_size << std::endl;
  std::cout << "frameRect centre (" << rect.pos.x << ";" << rect.pos.y << ")" << std::endl;
  std::cout << "width and height of FrameRectangle (" << rect.width << ";" << rect.height << ")" << std::endl;
}
