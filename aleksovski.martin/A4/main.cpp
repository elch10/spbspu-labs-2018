#include <iostream>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace aleksovski;

int main()
{
  point_t p = {7.3, 5.1};
  Rectangle rect1 {5.3, 6.2, p};
  Circle circ1 {4.8, {7.3, 5.4}} ;
  CompositeShape comshape;
  comshape.add(rect1);
  comshape.add(circ1);
  comshape.printf();
  double alpha = M_PI * 10 / 180;
  std::cout << "Rotate" << std::endl;
  comshape.rotate(alpha);
  comshape.printf();
  Matrix layer {comshape.getSize()};
  comshape.addtoMatrix(layer);
  comshape.printf();

  return 0;
}
