#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  using namespace lyalyuk;
  std::shared_ptr<Shape> rect( new Rectangle({1,1,{0,0}}));
  std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>(Rectangle({2, 2,{1, 1}}));
  std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({3, 3, {2, 2}}));
  std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(7, {0, 0}));

  CompositeShape cs;
  cs.add(rect);
  CompositeShape cs2;
  cs2.add(rect1);
  CompositeShape cs3;
  cs3.add(rect2);

  cs.printInfo();
  cs2.printInfo();
  cs3.printInfo();
  cs2.move({1,2});
  cs3.add(cir);
  cs3.move(12,12);
  cs3.remove(cs3.getSize() - 1);
  cs.printInfo();
  return 0;
}

