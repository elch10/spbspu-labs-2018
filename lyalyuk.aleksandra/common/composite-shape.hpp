#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace lyalyuk {
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& compositeShape);
    CompositeShape(CompositeShape&& compositeShape);

    const std::shared_ptr<Shape> operator [](size_t index) const;
    CompositeShape& operator=(const CompositeShape & compositeShape);
    CompositeShape& operator=(CompositeShape&& compositeShape);

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t &point) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double coefficient) override;
    void printInfo() const noexcept override;

    size_t getSize() const noexcept;
    void add(const std::shared_ptr<Shape> &shape);
    void remove(const size_t index);

  private:
    using array_type = std::unique_ptr<std::shared_ptr<Shape>[]>;
    array_type shapes_;
    size_t size_;
  };
}

#endif
