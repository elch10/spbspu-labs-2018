#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>
#include <functional>
#include <numeric>

lyalyuk::CompositeShape::CompositeShape() :
  shapes_(nullptr),
  size_(0)
{
}

lyalyuk::CompositeShape::CompositeShape(const CompositeShape &compositeShape) :
  shapes_(new std::shared_ptr<Shape>[compositeShape.size_]),
  size_(compositeShape.size_)
{
  std::copy(compositeShape.shapes_.get(), compositeShape.shapes_.get() + size_, shapes_.get());
}

lyalyuk::CompositeShape::CompositeShape(CompositeShape&& compositeShape) :
  shapes_(std::move(compositeShape.shapes_)),
  size_(std::move(compositeShape.size_))
{
  compositeShape.size_ = 0;
}

const std::shared_ptr<lyalyuk::Shape> lyalyuk::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("out of range");
  }
  return shapes_[index];
}

lyalyuk::CompositeShape &lyalyuk::CompositeShape::operator=(const CompositeShape& compositeShape)
{
  if (this != &compositeShape)
  {
    size_ = compositeShape.size_;
    shapes_.reset();
    shapes_ = std::unique_ptr<std::shared_ptr<Shape>[]>(new std::shared_ptr<Shape>[compositeShape.size_]);
    std::copy(compositeShape.shapes_.get(), compositeShape.shapes_.get() + size_, shapes_.get());
  }
  return *this;
}

lyalyuk::CompositeShape &lyalyuk::CompositeShape::operator=(CompositeShape&& compositeShape)
{
  if (this != &compositeShape) {
    shapes_.reset();
    size_ = compositeShape.size_;
    shapes_.swap(compositeShape.shapes_);
    compositeShape.size_= 0;
  }
  return *this;
}

double lyalyuk::CompositeShape::getArea() const noexcept
{
  return std::accumulate(shapes_.get(), shapes_.get() + size_, 0.0, [] (double area, const std::shared_ptr<Shape>& shape) {
    return area + shape->getArea();
  });
}

lyalyuk::rectangle_t lyalyuk::CompositeShape::getFrameRect() const noexcept
{
  if (size_ == 0)
  {
    return {0, 0, {0, 0}};
  }
  rectangle_t rect = shapes_[0]->getFrameRect();
  point_t left = {rect.pos.x - rect.width / 2, rect.pos.y - rect.height / 2};
  point_t right = {rect.pos.x + rect.width / 2, rect.pos.y + rect.height / 2};

  for (size_t i = 1; i < size_; i++)
  {
    rect = shapes_[i]->getFrameRect();
    if (rect.pos.y + rect.height/2 > right.y)
    {
      right.y = rect.pos.y + rect.height/2;
    }
    if (rect.pos.y - rect.height/2 < left.y)
    {
      left.y = rect.pos.y - rect.height/2;
    }

    if (rect.pos.x + rect.width/2 > right.x)
    {
      right.x = rect.pos.x + rect.width/2;
    }
    if (rect.pos.x - rect.width/2 < left.x)
    {
      left.x = rect.pos.x - rect.width/2;
    }
  }
  return {right.x - left.x, right.y - left.y, { left.x + (right.x - left.x) / 2, left.y + (right.y - left.y) / 2}};
}

size_t lyalyuk::CompositeShape::getSize() const noexcept
{
  return size_;
}

void lyalyuk::CompositeShape::add(const std::shared_ptr<Shape> &shape)
{
  if (shape == nullptr && shape.get() == this)
  {
    throw std::invalid_argument("wrong parameter");
  }
  for (size_t i = 0; i < size_; i++)
  {
    if (shapes_[i] == shape)
    {
      throw std::invalid_argument("shape already in array");
    }
  }
  array_type tempArray(new std::shared_ptr<Shape>[size_ + 1]);
  tempArray[size_] = shape;
  std::copy(shapes_.get(), shapes_.get() + size_, tempArray.get());
  shapes_.swap(tempArray);
  size_++;
}

void lyalyuk::CompositeShape::remove(const size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("out of range");
  }
  size_--;
  array_type tempArray(new std::shared_ptr<Shape>[size_]);
  std::copy(shapes_.get(), shapes_.get() + index, tempArray.get());
  std::copy(shapes_.get() + index, shapes_.get() + size_, tempArray.get());
  shapes_.swap(tempArray);
}

void lyalyuk::CompositeShape::move(const point_t &point) noexcept
{
  const auto center = getFrameRect().pos;
  const auto dx = point.x - center.x;
  const auto dy = point.y - center.y;
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void lyalyuk::CompositeShape::move(const double dx, const double dy) noexcept
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void lyalyuk::CompositeShape::scale(const double coefficient)
{
  const auto center = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    const auto shapeCenter = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move((coefficient - 1) * (shapeCenter.x - center.x),
                     (coefficient - 1) * (shapeCenter.y - center.y));
    shapes_[i]->scale(coefficient);
  }
}

void lyalyuk::CompositeShape::printInfo() const noexcept
{
  rectangle_t rect = getFrameRect();

  std::cout <<"Composite Shape :\n----------------------\nsize = "<<size_<< "\nArea = "
    << getArea() <<"\nFrameRect : \nWidth = "<<rect.width <<"\nHeight = "
    << rect.height <<"\nCenter = (" <<rect.pos.x <<";"
    << rect.pos.y << ")" <<"\n=========\n" << "ARRAY INFO:\n";

  if (size_ == 0)
  {
    std::cout <<"Empty\n";
    return;
  }
  for (size_t i = 0; i <size_ ; i++)
  {
    std::cout << "Shape number " << i << ", ";
    shapes_[i]->printInfo();
    std::cout<<"\n";
  }
  std::cout << "\n";
}
