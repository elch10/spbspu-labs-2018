#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double EPSILON = 0.00001;

using namespace std;
using namespace subtselnaya;

BOOST_AUTO_TEST_SUITE(RectangleSuite)

BOOST_AUTO_TEST_CASE(MoveToPoint)
{
  Rectangle rect (3.5, 4.8, {4.3, 3.4});
  rectangle_t startRect = rect.getFrameRect();
  double startArea = rect.getArea();

  rect.move ({25, 10});
  BOOST_CHECK_EQUAL(startArea, rect.getArea());
  BOOST_CHECK_EQUAL(startRect.width, rect.getFrameRect().width);
  BOOST_CHECK_EQUAL(startRect.height, rect.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(MoveByAxis)
{
  Rectangle rect (3.5, 4.8, {4.3, 3.4});
  rectangle_t startRect = rect.getFrameRect();
  double startArea = rect.getArea();

  rect.move (15,5);
  BOOST_CHECK_EQUAL(startArea, rect.getArea());
  BOOST_CHECK_EQUAL(startRect.width, rect.getFrameRect().width);
  BOOST_CHECK_EQUAL(startRect.height, rect.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(Scaling)
{
  Rectangle rect (3.5, 4.8, {4.3, 3.4});
  double startArea = rect.getArea();
  const double value = 3.3;

  rect.scale(value);
  BOOST_CHECK_CLOSE(value * value * startArea, rect.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
{
  Rectangle rect (3.5, 4.8, { 4.3, 3.4 });
  BOOST_CHECK_THROW (rect.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(CircleSuite)

BOOST_AUTO_TEST_CASE(MoveToPoint)
{
  Circle circ ({5.9, 7.1}, 3.0);
  rectangle_t startRect = circ.getFrameRect();
  double startArea = circ.getArea();

  circ.move ({20,10});
  BOOST_CHECK_EQUAL(startArea, circ.getArea());
  BOOST_CHECK_EQUAL(startRect.width, circ.getFrameRect().width);
  BOOST_CHECK_EQUAL(startRect.height, circ.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(MoveByAxis)
{
  Circle circ ({5.9, 7.1}, 3.0);
  rectangle_t startRect = circ.getFrameRect();
  double startArea = circ.getArea();

  circ.move (10,5);
  BOOST_CHECK_EQUAL(startArea, circ.getArea());
  BOOST_CHECK_EQUAL(startRect.width, circ.getFrameRect().width);
  BOOST_CHECK_EQUAL(startRect.height, circ.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(Scaling)
{
  Circle circ ({5.9, 7.1}, 3.0);
  double startArea = circ.getArea();
  const double value = 3.3;

  circ.scale(value);
  BOOST_CHECK_CLOSE(value * value * startArea, circ.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
{
  Circle circ ({5.9, 7.1}, 3.0);
  BOOST_CHECK_THROW(circ.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeSuite)

    BOOST_AUTO_TEST_CASE(InvarienceOfAreaTest)
    {
        Rectangle rect (1.0, 1.0, {4.0, 4.0});
        Circle circ ({2.0 , 2.0}, 1.0);
        CompositeShape::ptr_type rectPtr = make_shared < Rectangle > (rect);
        CompositeShape::ptr_type circPtr = make_shared < Circle > (circ);
        CompositeShape composhape;
        composhape.addShape(rectPtr);
        composhape.addShape(circPtr);
        double area = composhape.getArea();
        composhape.move(3.0, 3.0);
        BOOST_CHECK_CLOSE_FRACTION(composhape.getArea(), area, EPSILON);
    }

    BOOST_AUTO_TEST_CASE(InvarienceOfCoordsTest)
    {
        Rectangle rect (1.0, 1.0, {4.0, 4.0});
        Circle circ ({2.0 , 2.0}, 1.0);

        CompositeShape::ptr_type rectPtr = make_shared < Rectangle > (rect);
        CompositeShape::ptr_type circPtr = make_shared < Circle > (circ);
        CompositeShape composhape;
        composhape.addShape(rectPtr);
        composhape.addShape(circPtr);

        double posX = composhape.getFrameRect().pos.x,
               posY = composhape.getFrameRect().pos.y;

        composhape.scale(2.0);
        BOOST_CHECK_CLOSE_FRACTION(composhape.getFrameRect().pos.x, posX, EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(composhape.getFrameRect().pos.y, posY, EPSILON);
    }

    BOOST_AUTO_TEST_CASE(Scaling)
    {
        Rectangle rect (1.0, 1.0, {4.0, 4.0});
        Circle circ ({2.0 , 2.0}, 1.0);
        CompositeShape::ptr_type rectPtr = make_shared < Rectangle > (rect);
        CompositeShape::ptr_type circPtr = make_shared < Circle > (circ);
        CompositeShape composhape;
        composhape.addShape(rectPtr);
        composhape.addShape(circPtr);

        double area = composhape.getArea(),
               value = 2.0;

        composhape.scale(value);
        BOOST_CHECK_CLOSE_FRACTION(composhape.getArea(), area * value * value, EPSILON);
    }


    BOOST_AUTO_TEST_CASE(InvalidAddShapeArgument)
    {
        Rectangle rect (1.0, 1.0, {4.0, 4.0});
        CompositeShape::ptr_type rectPtr = make_shared < Rectangle > (rect);
        CompositeShape composhape;
        composhape.addShape(rectPtr);

        BOOST_REQUIRE_THROW(composhape.addShape(nullptr), invalid_argument);
    }

    BOOST_AUTO_TEST_CASE(InvalidScalingArgument)
    {
        Rectangle rect (1.0, 1.0, {4.0, 4.0});
        Circle circ ({2.0 , 2.0}, 1.0);
        CompositeShape::ptr_type rectPtr = make_shared < Rectangle > (rect);
        CompositeShape::ptr_type circPtr = make_shared < Circle > (circ);
        CompositeShape composhape;
        composhape.addShape(rectPtr);
        composhape.addShape(circPtr);

        BOOST_REQUIRE_THROW(composhape.scale(-1.0), invalid_argument);
    }

    BOOST_AUTO_TEST_CASE(OutOfRangeRemoveShape)
    {
        Rectangle rect (1.0, 1.0, {4.0, 4.0});
        CompositeShape::ptr_type rectPtr = make_shared < Rectangle > (rect);
        CompositeShape composhape;
        composhape.addShape(rectPtr);
        BOOST_REQUIRE_THROW(composhape.removeShape(1), out_of_range);
    }

    BOOST_AUTO_TEST_CASE(CompositeShapeArrayOperator)
    {
        Rectangle rect (1.0, 1.0, {4.0, 4.0});
        CompositeShape::ptr_type rectPtr = make_shared < Rectangle > (rect);
        CompositeShape composhape;
        composhape.addShape(rectPtr);

        BOOST_CHECK_EQUAL(composhape[0], rectPtr);
        BOOST_REQUIRE_THROW(composhape[1], out_of_range);
    }

    BOOST_AUTO_TEST_CASE(General)
    {
        Rectangle rect (1.0, 1.0, {4.0, 4.0});
        CompositeShape::ptr_type rectPtr = make_shared < Rectangle > (rect);
        CompositeShape composhape;
        composhape.addShape(rectPtr);

        rectangle_t shapeFrameRect = composhape.getFrameRect(),
                    rectFrameRect = rectPtr -> getFrameRect();

        BOOST_CHECK_CLOSE_FRACTION(shapeFrameRect.pos.x, rectFrameRect.pos.x, EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(shapeFrameRect.pos.y, rectFrameRect.pos.y, EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(shapeFrameRect.width, rectFrameRect.width, EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(shapeFrameRect.height, rectFrameRect.height, EPSILON);
    }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTests)

  BOOST_AUTO_TEST_CASE(MatrixCorrectnessTest)
  {
    Circle testCircleM1 ({ -10.0, 0.0 }, 10.0);
    Circle testCircleM2 ({ 40.0, 30.0 }, 20.0);
    Rectangle testRectangleM1 (20.0, 40.0,{ 20.0, 30.0 });
    Rectangle testRectangleM2 (20.0, 40.0,{ 30.0, 0.0 });

    Matrix::ptr_type circlePtrM1 = make_shared < Circle > (testCircleM1);
    Matrix::ptr_type circlePtrM2 = make_shared < Circle > (testCircleM2);
    Matrix::ptr_type rectanglePtrM1 = make_shared < Rectangle > (testRectangleM1);
    Matrix::ptr_type rectanglePtrM2 = make_shared < Rectangle > (testRectangleM2);

    Matrix testMatrix(circlePtrM1);

    testMatrix.addShape(rectanglePtrM1);
    testMatrix.addShape(rectanglePtrM2);
    testMatrix.addShape(circlePtrM2);

    unique_ptr < Matrix::ptr_type[] > layer0 = testMatrix[0];
    unique_ptr < Matrix::ptr_type[] > layer1 = testMatrix[1];
    unique_ptr < Matrix::ptr_type[] > layer2 = testMatrix[2];

    BOOST_CHECK(layer0[0] == circlePtrM1);
    BOOST_CHECK(layer0[1] == rectanglePtrM1);
    BOOST_CHECK(layer1[0] == rectanglePtrM2);
    BOOST_CHECK(layer1[1] == nullptr);
    BOOST_CHECK(layer2[0] == circlePtrM2);
    BOOST_CHECK(layer2[1] == nullptr);

    BOOST_CHECK_CLOSE_FRACTION(layer0[0] -> getFrameRect().pos.x, -10.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer0[1] -> getFrameRect().pos.x, 20.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer1[0] -> getFrameRect().pos.x, 30.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer2[0] -> getFrameRect().pos.x, 40.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer0[0] -> getFrameRect().pos.y, 0.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer0[1] -> getFrameRect().pos.y, 30.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer1[0] -> getFrameRect().pos.y, 0.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer2[0] -> getFrameRect().pos.y, 30.0, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    Circle testCircleM ({ -10.0, 0.0 }, 10.0);
    Rectangle testRectangleM1 (20.0, 40.0,{ 20.0, 30.0 });
    Rectangle testRectangleM2 (20.0, 40.0,{ 30.0, 0.0 });

    Matrix::ptr_type circlePtrM = make_shared < Circle > (testCircleM);
    Matrix::ptr_type rectanglePtrM1 = make_shared < Rectangle > (testRectangleM1);
    Matrix::ptr_type rectanglePtrM2 = make_shared < Rectangle > (testRectangleM2);

    Matrix testMatrix1(circlePtrM);

    testMatrix1.addShape(rectanglePtrM1);
    testMatrix1.addShape(rectanglePtrM2);

    Matrix testMatrix2(testMatrix1);

    unique_ptr < Matrix::ptr_type[] > layer0 = testMatrix2[0];
    unique_ptr < Matrix::ptr_type[] > layer1 = testMatrix2[1];

    BOOST_CHECK(layer0[0] == circlePtrM);
    BOOST_CHECK(layer0[1] == rectanglePtrM1);
    BOOST_CHECK(layer1[0] == rectanglePtrM2);

    BOOST_REQUIRE_EQUAL(testMatrix2.getLayersNumber(), 2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    Circle testCircleM ({ -10.0, 0.0 }, 10.0);
    Rectangle testRectangleM1 (20.0, 40.0,{ 20.0, 30.0 });
    Rectangle testRectangleM2 (20.0, 40.0, { 30.0, 0.0 });

    Matrix::ptr_type circlePtrM = make_shared < Circle > (testCircleM);
    Matrix::ptr_type rectanglePtrM1 = make_shared < Rectangle > (testRectangleM1);
    Matrix::ptr_type rectanglePtrM2 = make_shared < Rectangle > (testRectangleM2);

    Matrix testMatrix1(circlePtrM);
    testMatrix1.addShape(rectanglePtrM1);
    testMatrix1.addShape(rectanglePtrM2);

    Matrix testMatrix2(std::move(testMatrix1));
    unique_ptr < Matrix::ptr_type[] > layer0 = testMatrix2[0];
    unique_ptr < Matrix::ptr_type[] > layer1 = testMatrix2[1];

    BOOST_CHECK(layer0[0] == circlePtrM);
    BOOST_CHECK(layer0[1] == rectanglePtrM1);
    BOOST_CHECK(layer1[0] == rectanglePtrM2);

    BOOST_REQUIRE_EQUAL(testMatrix1.getLayersNumber(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix1.getLayerSize(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayersNumber(), 2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    Circle testCircleM1 ({ -10.0, 0.0 }, 10.0);
    Circle testCircleM2 ({ 40.0, 30.0 }, 20.0);
    Rectangle testRectangleM1 (20.0, 40.0,{ 20.0, 30.0 });
    Rectangle testRectangleM2 (20.0, 40.0,{ 30.0, 0.0 });

    Matrix::ptr_type circlePtrM1 = make_shared < Circle > (testCircleM1);
    Matrix::ptr_type circlePtrM2 = make_shared < Circle > (testCircleM2);
    Matrix::ptr_type rectanglePtrM1 = make_shared < Rectangle > (testRectangleM1);
    Matrix::ptr_type rectanglePtrM2 = make_shared < Rectangle > (testRectangleM2);

    Matrix testMatrix1(circlePtrM1);
    testMatrix1.addShape(rectanglePtrM1);
    testMatrix1.addShape(rectanglePtrM2);

    Matrix testMatrix2(circlePtrM2);
    testMatrix2 = testMatrix1;
    unique_ptr < Matrix::ptr_type[] > layer0 = testMatrix2[0];
    unique_ptr < Matrix::ptr_type[] > layer1 = testMatrix2[1];

    BOOST_CHECK(layer0[0] == circlePtrM1);
    BOOST_CHECK(layer0[1] == rectanglePtrM1);
    BOOST_CHECK(layer1[0] == rectanglePtrM2);

    BOOST_REQUIRE_EQUAL(testMatrix2.getLayersNumber(), 2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    Circle testCircleM1 ({ -10.0, 0.0 }, 10.0);
    Circle testCircleM2 ({ 40.0, 30.0 }, 20.0);
    Rectangle testRectangleM1 (20.0, 40.0,{ 20.0, 30.0 });
    Rectangle testRectangleM2 (20.0, 40.0,{ 30.0, 0.0 });

    Matrix::ptr_type circlePtrM1 = make_shared < Circle > (testCircleM1);
    Matrix::ptr_type circlePtrM2 = make_shared < Circle > (testCircleM2);
    Matrix::ptr_type rectanglePtrM1 = make_shared < Rectangle > (testRectangleM1);
    Matrix::ptr_type rectanglePtrM2 = make_shared < Rectangle > (testRectangleM2);

    Matrix testMatrix1(circlePtrM1);
    testMatrix1.addShape(rectanglePtrM1);
    testMatrix1.addShape(rectanglePtrM2);

    Matrix testMatrix2(circlePtrM2);
    testMatrix2 = move(testMatrix1);
    unique_ptr < Matrix::ptr_type[] > layer0 = testMatrix2[0];
    unique_ptr < Matrix::ptr_type[] > layer1 = testMatrix2[1];

    BOOST_CHECK(layer0[0] == circlePtrM1);
    BOOST_CHECK(layer0[1] == rectanglePtrM1);
    BOOST_CHECK(layer1[0] == rectanglePtrM2);

    BOOST_REQUIRE_EQUAL(testMatrix1.getLayersNumber(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix1.getLayerSize(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayersNumber(), 2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(EqualOperatorTest)
  {
    Circle testCircleM ({ -10.0, 0.0 }, 10.0);
    Rectangle testRectangleM (20.0, 40.0,{ 20.0, 30.0 });

    Matrix::ptr_type circlePtrM = make_shared < Circle > (testCircleM);
    Matrix::ptr_type rectanglePtrM = make_shared < Rectangle > (testRectangleM);

    Matrix testMatrix1(circlePtrM);
    testMatrix1.addShape(rectanglePtrM);

    Matrix testMatrix2(circlePtrM);
    testMatrix2.addShape(rectanglePtrM);

    bool equality = (testMatrix1 == testMatrix2);
    BOOST_CHECK(equality == true);
  }

  BOOST_AUTO_TEST_CASE(NonequalOperatorTest)
  {
    Circle testCircleM ({ -10.0, 0.0 }, 10.0);
    Rectangle testRectangleM (20.0, 40.0,{ 20.0, 30.0 });
    Matrix::ptr_type circlePtrM = make_shared < Circle > (testCircleM);
    Matrix::ptr_type rectanglePtrM = make_shared < Rectangle > (testRectangleM);

    Matrix testMatrix1(circlePtrM);
    Matrix testMatrix2(rectanglePtrM);

    bool nonequality = (testMatrix1 != testMatrix2);
    BOOST_CHECK(nonequality == true);
  }

  BOOST_AUTO_TEST_CASE(CreateMatrixWithNullptrTest)
  {
    BOOST_CHECK_THROW(Matrix testMatrix(nullptr), invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(GetLayerFromEmptyMatrixTest)
  {
    Circle testCircle ({ 100.0, 0.0 }, 10.0);
    Matrix::ptr_type circlePtr = make_shared < Circle > (testCircle);

    Matrix testMatrix(circlePtr);
    testMatrix.~Matrix();

    BOOST_CHECK_THROW(testMatrix[0], out_of_range);
  }

  BOOST_AUTO_TEST_CASE(GetLayerWithIncorrectIndexTest)
  {
    Circle testCircle ({ 100.0, 0.0 }, 10.0);
    Matrix::ptr_type circlePtr = make_shared < Circle > (testCircle);
    Matrix testMatrix(circlePtr);
    BOOST_CHECK_THROW(testMatrix[-1], invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

