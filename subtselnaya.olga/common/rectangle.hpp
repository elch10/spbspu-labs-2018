#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "base-types.hpp"
#include "shape.hpp"

namespace subtselnaya
{
  class Rectangle : public Shape {
  public:
    Rectangle (const double width, const double height, const point_t & center);

    double getArea() const noexcept;
    rectangle_t getFrameRect() const noexcept;
    void move(const point_t & resPoint) noexcept;
    void move(const double dx, const double dy) noexcept;
    void scale(const double value);
    void rotate(const double angle) noexcept;
    point_t getPos() const noexcept;

  private:
    rectangle_t rect_;
    double angle_;
  };
}
#endif // RECTANGLE_HPP

