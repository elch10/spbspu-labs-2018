#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

using namespace subtselnaya;

Rectangle::Rectangle(const double width, const double height, const point_t & center):
  rect_({width, height, center})
{
  if ((width < 0.0) || (height < 0.0)) {
    throw std::invalid_argument("Wrong parameters");
  }
}

double Rectangle::getArea() const noexcept
{
  return rect_.width * rect_.height;
}

rectangle_t Rectangle::getFrameRect() const noexcept
{
  return rect_;
}

void Rectangle::move(const point_t & resPoint) noexcept
{
  rect_.pos = resPoint;
}

void Rectangle::move(const double dx, const double dy) noexcept
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void Rectangle::scale(const double value)
{
  if (value < 0.0) {
    throw std::invalid_argument("Wrong value of scaling");
  }
  rect_.height *= value;
  rect_.width *= value;
}

void Rectangle::rotate(const double angle) noexcept
{
  angle_ = std::fmod(angle_ + angle, 360.0);
}

point_t Rectangle::getPos() const noexcept
{
  
  return getFrameRect().pos;

}

