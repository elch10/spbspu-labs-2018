#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "base-types.hpp"
#include "shape.hpp"

namespace subtselnaya
{
  class Circle : public Shape {
  public:
    Circle (const point_t & center, const double radius);

    double getArea() const noexcept;
    rectangle_t getFrameRect() const noexcept;
    void move(const point_t & resPoint) noexcept;
    void move(const double dx, const double dy) noexcept;
    void scale(const double value);
    void rotate(const double angle) noexcept;
    point_t getPos() const noexcept;

  private:
    point_t center_;
    double radius_;
    double angle_;
  };
}

#endif // CIRCLE_HPP

