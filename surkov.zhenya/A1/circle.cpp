#include "circle.hpp"
#include <math.h>

#include <stdexcept>

Circle::Circle(const point_t &centre,const double rad) : 
  rad_(rad), 
  centre_(centre)
{
  if (rad_ <= 0.0)
  {
    throw std::invalid_argument("Invalid circle parameters!");
  }
}

double Circle::getArea() const
{
  return M_PI * pow(rad_, 2);
}

rectangle_t  Circle::getFrameRect() const
{
  rectangle_t rect;
  rect.pos = centre_;
  rect.width = 2 * rad_;
  rect.height = 2 * rad_;
  return rect;
}

void Circle::move(const point_t &point)
{
  centre_ = point;
}

void Circle::move(const double x_offset,const double y_offset)
{
  centre_.x += x_offset;
  centre_.y += y_offset;
}
