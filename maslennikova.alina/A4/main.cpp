#include <iostream>

#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/composite-shape.hpp"
#include "../common/matrix.hpp"


int main()
{
    
    std::shared_ptr<maslennikova::Shape> rectPtr1
      = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    maslennikova::CompositeShape shape(rectPtr1);
    
    std::shared_ptr<maslennikova::Shape> rectPtr2
      = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {150.0, 256.0}}));
    shape.addShape(rectPtr2);
    
    std::shared_ptr<maslennikova::Shape> circlePtr
      = std::make_shared<maslennikova::Circle>(maslennikova::Circle(10.0, {130.0, 256.0}));
    shape.addShape(circlePtr);
        
    maslennikova::rectangle_t frameRect = shape.getFrameRect();
    std::cout<< shape.getArea() << "\n";
    shape.rotate(90.0);
    frameRect = shape.getFrameRect();
    std::cout<< shape.getArea() << "\n";
    
    maslennikova::MatrixShape matrix_shape(rectPtr1);
    matrix_shape.addShape(rectPtr2);
    matrix_shape.addShape(circlePtr);
    
    shape.delShape(0);
    
    return 0;
}
