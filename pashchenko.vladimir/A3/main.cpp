#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using namespace pashchenko;

int main()
{
  pashchenko::point_t pointRect = { 45.6, 13.0 };
  pashchenko::point_t pointCircle = { 10.5, -3.0 };

  pashchenko::point_t pointT1 = { 1, 2 };
  pashchenko::point_t pointT2 = { 7.5, 6 };
  pashchenko::point_t pointT3 = { 10.5, 5 };

  pashchenko::Rectangle rect(pointRect, 20, 5.5);
  pashchenko::Triangle triangle(pointT1, pointT2, pointT3);
  pashchenko::Circle circle(pointCircle, 4.5);

  std::shared_ptr<Shape> rectC = std::make_shared<Rectangle>(Rectangle(pointRect, 20, 5.5));
  std::shared_ptr<Shape> triangleC = std::make_shared<Triangle>(Triangle(pointT1, pointT2, pointT3));
  std::shared_ptr<Shape> circleC = std::make_shared<Circle>(Circle(pointCircle, 4.5));

  std::cout << "Current area of rect: " << rect.getArea() << std::endl;
  std::cout << "Current area of triangle: " << triangle.getArea() << std::endl;
  std::cout << "Current area of circle: " << circle.getArea() << std::endl;

  pashchenko::CompositeShape exampleCS;
  exampleCS.addElement(rectC);
  exampleCS.addElement(triangleC);

  std::cout << "Now we have: " << exampleCS.getSize() << " shapes with common area: " << exampleCS.getArea() << std::endl;

  exampleCS.addElement(circleC);
  std::cout << "Now we have: " << exampleCS.getSize() << " shapes with common area: " << exampleCS.getArea() << std::endl;

  exampleCS.removeElement(0);
  exampleCS.removeElement(0);
  std::cout << "Now we have: " << exampleCS.getSize() << " shapes with common area: " << exampleCS.getArea() << std::endl;

  exampleCS.removeElement(0);
  std::cout << "Now we have: " << exampleCS.getSize() << " shapes with common area: " << exampleCS.getArea() << std::endl;

  system("pause");
  return 0;
}
