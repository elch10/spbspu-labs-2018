#include <iostream>
#include <math.h>
#include "triangle.hpp"

pashchenko::Triangle::Triangle(const point_t fPoint, const point_t sPoint, const point_t tPoint) :
  vertices_{ fPoint, sPoint, tPoint }
{
}

void pashchenko::Triangle::move(const point_t & newP) noexcept
{
  const point_t centre = getFrameRect().pos;

  for (int i = 0; i < 3; ++i)
  {
    vertices_[i].x += newP.x - centre.x;
    vertices_[i].y += newP.y - centre.y;
  }
}

void pashchenko::Triangle::move(const double &nX, const double &nY) noexcept
{
  for (int i = 0; i < 3; ++i)
    vertices_[i].x += nX;
  for (int i = 0; i < 3; ++i)
    vertices_[i].y += nY;
}

double pashchenko::Triangle::getArea() const noexcept
{
  return ((fabs(((vertices_[0].x - vertices_[2].x) * (vertices_[1].y - vertices_[2].y)) -
    ((vertices_[0].y - vertices_[2].y) * (vertices_[1].x - vertices_[2].x)))) / 2);
}

pashchenko::rectangle_t pashchenko::Triangle::getFrameRect() const noexcept
{
  double top = vertices_[0].y;
  double bottom = vertices_[0].y;
  double left = vertices_[0].x;
  double right = vertices_[0].x;

  for (int i = 1; i < 3; ++i)
  {
    if (vertices_[i].x < left)
      left = vertices_[i].x;
    if (vertices_[i].x > right)
      right = vertices_[i].x;
    if (vertices_[i].y < bottom)
      bottom = vertices_[i].y;
    if (vertices_[i].y > top)
      top = vertices_[i].y;
  }

  double centreX = 0;
  double centreY = 0;

  for (int i = 0; i < 3; ++i)
    centreX += vertices_[i].x;
  for (int i = 0; i < 3; ++i)
    centreY += vertices_[i].y;

  return { { centreX / 3, centreY / 3 }, right - left, top - bottom };
}

pashchenko::point_t pashchenko::Triangle::getPosition() const noexcept
{
  return getFrameRect().pos;
}

void pashchenko::Triangle::scale(const double coefficient)
{
  if (coefficient <= 0)
    throw std::invalid_argument("Coefficient is less than or equal to zero");
  const point_t centre = getFrameRect().pos;

  for (int i = 0; i < 3; ++i)
  {
    vertices_[i] = { centre.x + (vertices_[i].x - centre.x) * coefficient, centre.y + (vertices_[i].y - centre.y) * coefficient };
  }
}

void pashchenko::Triangle::rotate(const double newAngle) noexcept
{
  const point_t centre = getPosition();
  const double cosAngle = cos(newAngle);
  const double sinAngle = sin(newAngle);

  for (int i = 0; i < 3; ++i)
  {
    vertices_[i] = { centre.x + (vertices_[i].x - centre.x) * cosAngle -
      (vertices_[i].y - centre.y) * sinAngle, centre.y + (vertices_[i].y - centre.y) * cosAngle +
      (vertices_[i].x - centre.x) * sinAngle };
  }
}
