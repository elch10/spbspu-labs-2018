#ifndef TRIANGLE_HPP_FILE
#define TRIANGLE_HPP_FILE

#include "shape.hpp"

namespace pashchenko {
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t fPoint, const point_t sPoint, const point_t tPoint);

    virtual void move(const point_t & newP) noexcept;
    virtual void move(const double & nX, const double & nY) noexcept;
    virtual void scale(const double coefficient);
    virtual void rotate(const double newAngle) noexcept;

    virtual double getArea() const noexcept;
    virtual rectangle_t getFrameRect() const noexcept;
    virtual point_t getPosition() const noexcept;

  private:
    point_t vertices_[3];
    point_t currPos_;
  };
}

#endif
