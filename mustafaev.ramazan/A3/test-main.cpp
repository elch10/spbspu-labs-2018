#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

  BOOST_AUTO_TEST_CASE(InvarienceOfArea)
  {
    double E = 0.0001;
    
    std::shared_ptr<mustafaev::Shape> rectPtr
      = std::make_shared<mustafaev::Rectangle>(mustafaev::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    mustafaev::CompositeShape shape(rectPtr);
    
    std::shared_ptr<mustafaev::Shape> circlePtr
      = std::make_shared<mustafaev::Circle>(mustafaev::Circle(5.0, {10.0, 10.0}));
    shape.addShape(circlePtr);
    
    std::shared_ptr<mustafaev::Shape> trianglePtr
      = std::make_shared<mustafaev::Triangle>(mustafaev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    
    double area = shape.getArea();
    shape.move(16.0, 32.0);
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getArea(), area, E);

  }

  BOOST_AUTO_TEST_CASE(InvarienceOfCoords)
  {
    double E = 0.1;
    
    std::shared_ptr<mustafaev::Shape> rectPtr
      = std::make_shared<mustafaev::Rectangle>(mustafaev::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    mustafaev::CompositeShape shape(rectPtr);
    
    std::shared_ptr<mustafaev::Shape> circlePtr
      = std::make_shared<mustafaev::Circle>(mustafaev::Circle(5.0, {10.0, 10.0}));
    shape.addShape(circlePtr);
    
    std::shared_ptr<mustafaev::Shape> trianglePtr
    = std::make_shared<mustafaev::Triangle>(mustafaev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    
    double posX = shape.getFrameRect().pos.x;
    double posY = shape.getFrameRect().pos.y;
    shape.scale(2.0);
    
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getFrameRect().pos.x, posX, E);
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getFrameRect().pos.y, posY, E);

  }

  BOOST_AUTO_TEST_CASE(Scale)
  {
    double E = 0.0001;
    
    std::shared_ptr<mustafaev::Shape> rectPtr
    = std::make_shared<mustafaev::Rectangle>(mustafaev::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    mustafaev::CompositeShape shape(rectPtr);
    
    std::shared_ptr<mustafaev::Shape> circlePtr
    = std::make_shared<mustafaev::Circle>(mustafaev::Circle(5.0, {10.0, 10.0}));
    shape.addShape(circlePtr);
    
    std::shared_ptr<mustafaev::Shape> trianglePtr
    = std::make_shared<mustafaev::Triangle>(mustafaev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    
    double area = shape.getArea();
    shape.scale(2.0);
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getArea(), area * pow(2.0, 2.0), E);

  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentConstructor)
  {
    
    BOOST_REQUIRE_THROW(mustafaev::CompositeShape shape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentAddShape)
  {
    std::shared_ptr<mustafaev::Shape> rectPtr
      = std::make_shared<mustafaev::Rectangle>(mustafaev::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    mustafaev::CompositeShape shape(rectPtr);
    
    BOOST_REQUIRE_THROW(shape.addShape(nullptr), std::invalid_argument);

  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentScale)
  {
    std::shared_ptr<mustafaev::Shape> rectPtr
      = std::make_shared<mustafaev::Rectangle>(mustafaev::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    mustafaev::CompositeShape shape(rectPtr);
    
    std::shared_ptr<mustafaev::Shape> circlePtr
      = std::make_shared<mustafaev::Circle>(mustafaev::Circle(5.0, {10.0, 10.0}));
    shape.addShape(circlePtr);
    
    std::shared_ptr<mustafaev::Shape> trianglePtr
      = std::make_shared<mustafaev::Triangle>(mustafaev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    
    BOOST_CHECK_THROW(shape.scale(-1.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(GeneralAccordance)
  {
    double E = 0.0001;
    
    std::shared_ptr<mustafaev::Shape> rectPtr
      = std::make_shared<mustafaev::Rectangle>(mustafaev::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    mustafaev::CompositeShape shape(rectPtr);
    mustafaev::rectangle_t shapeFrameRect = shape.getFrameRect();
    mustafaev::rectangle_t rectFrameRect = rectPtr -> getFrameRect();
    
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.x, rectFrameRect.pos.x, E);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.y, rectFrameRect.pos.y, E);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.width, rectFrameRect.width, E);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.height, rectFrameRect.height, E);
      
  }

BOOST_AUTO_TEST_SUITE_END()


