#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

int main()
{
  mustafaev::Rectangle rectangle({5.0, 7.0, {2.0, 5.0}});
  mustafaev::Shape *shape = &rectangle;
  std::cout << "rectangle:" << std::endl;
  std::cout << "area before scaling: " << shape -> getArea() << std::endl;

  shape -> scale(3.0);
  std::cout<< "area after scaling: " << shape -> getArea() << std::endl;

  mustafaev::Circle circle(7.0, {4.0, 4.0});
  shape = &circle;
  std::cout << "circle:" << std::endl;
  std::cout << "area before scaling: " << shape -> getArea() << std::endl;

  shape -> scale(2.0);
  std::cout << "area after scaling: " << shape -> getArea() << std::endl;
  std::cout << std::endl;

  mustafaev::Triangle triangle({4.0, 5.0}, {6.0, 8.0}, {7.0, 5.0});
  shape = &triangle;
  std::cout << "triangle:" << std::endl;
  std::cout << "area before scaling: " << shape -> getArea() << std::endl;

  shape -> scale(4.0);
  std::cout << "area after scaling: " << shape -> getArea() << std::endl;
  std::cout << std::endl;
  return 0;
}
