#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

int main()
{
    
    std::shared_ptr<mustafaev::Shape> rectPtr1
      = std::make_shared<mustafaev::Rectangle>(mustafaev::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    mustafaev::CompositeShape shape(rectPtr1);
    
    std::shared_ptr<mustafaev::Shape> rectPtr2
      = std::make_shared<mustafaev::Rectangle>(mustafaev::Rectangle({24.0, 48.0, {150.0, 256.0}}));
    shape.addShape(rectPtr2);
    
    std::shared_ptr<mustafaev::Shape> circlePtr
      = std::make_shared<mustafaev::Circle>(mustafaev::Circle(10.0, {130.0, 256.0}));
    shape.addShape(circlePtr);
    
    std::shared_ptr<mustafaev::Shape> trianglePtr
      = std::make_shared<mustafaev::Triangle>(mustafaev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    
    mustafaev::rectangle_t frameRect = shape.getFrameRect();
    std::cout<< shape.getArea() << "\n";
    shape.rotate(90.0);
    frameRect = shape.getFrameRect();
    std::cout<< shape.getArea() << "\n";
    
    mustafaev::MatrixShape matrix_shape(rectPtr1);
    matrix_shape.addShape(rectPtr2);
    matrix_shape.addShape(circlePtr);
    matrix_shape.addShape(trianglePtr);
    
    shape.delShape(0);
    
    return 0;
}

