#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include <math.h>
#include <memory>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using UP = std::unique_ptr < std::shared_ptr <volkov::Shape>[] >;
using SP = std::shared_ptr <volkov::Shape>;

const double epsilon = 0.0001;

BOOST_AUTO_TEST_SUITE(CircleTests)

  BOOST_AUTO_TEST_CASE(MoveToXY)
  {
    volkov::Circle circ({0,0},1);
    circ.move(1, 2);
    BOOST_CHECK_CLOSE(circ.getFrameRect().pos.x, 1, epsilon); 
    BOOST_CHECK_CLOSE(circ.getFrameRect().pos.y, 2, epsilon);
    BOOST_CHECK_CLOSE(circ.getFrameRect().height / 2 , 1, epsilon);
    BOOST_CHECK_CLOSE_FRACTION(circ.getArea(), M_PI, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    volkov::Circle circ({0,0},1);
    circ.move({2, 1});
    BOOST_CHECK_CLOSE(circ.getFrameRect().pos.x, 2, epsilon);
    BOOST_CHECK_CLOSE(circ.getFrameRect().pos.y, 1, epsilon);
    BOOST_CHECK_CLOSE(circ.getFrameRect().height / 2 , 1, epsilon);
    BOOST_CHECK_CLOSE_FRACTION(circ.getArea(), M_PI, epsilon);
  }

  BOOST_AUTO_TEST_CASE(ChangeAreaScale)
  {
    volkov::Circle circ({0,0},1);
    circ.scale(2);
    BOOST_CHECK_CLOSE_FRACTION(circ.getArea(), 12.5663, epsilon);
  }

  BOOST_AUTO_TEST_CASE(Rotation)
  {
    volkov::Circle circ({0,0},1);
    circ.rotate(90);
    BOOST_CHECK_CLOSE(circ.getAngle(), 90, epsilon);
  }

  BOOST_AUTO_TEST_CASE(InvalidCircle)
  {
    BOOST_CHECK_THROW(volkov::Circle({0,0},-10), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidCircleScale)
  {
    volkov::Circle circ ({0,0},1);
    BOOST_CHECK_THROW(circ.scale(-5), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(RectangleTests)

  BOOST_AUTO_TEST_CASE(MoveToXY)
  {
    volkov::Rectangle rect({1,2}, 4, 2);
    rect.move(1, 2);
    BOOST_CHECK_CLOSE(rect.getFrameRect().pos.x, 2, epsilon);
    BOOST_CHECK_CLOSE(rect.getFrameRect().pos.y, 4, epsilon);
    BOOST_CHECK_CLOSE(rect.getFrameRect().height , 2, epsilon);
    BOOST_CHECK_CLOSE(rect.getFrameRect().width , 4, epsilon);
    BOOST_CHECK_CLOSE(rect.getArea(), 4 * 2 , epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    volkov::Rectangle rect({1,2}, 4, 2);
    rect.move({2, 1});
    BOOST_CHECK_CLOSE(rect.getFrameRect().pos.x, 2, epsilon);
    BOOST_CHECK_CLOSE(rect.getFrameRect().pos.y, 1, epsilon);
    BOOST_CHECK_CLOSE(rect.getFrameRect().height , 2, epsilon);
    BOOST_CHECK_CLOSE(rect.getFrameRect().width , 4, epsilon);
    BOOST_CHECK_CLOSE(rect.getArea(), 4 * 2 , epsilon);
  }

  BOOST_AUTO_TEST_CASE(ChangeAreaScale)
  {
    volkov::Rectangle rect({1,2}, 4, 2);
    rect.scale(2);
    BOOST_CHECK_CLOSE(rect.getArea(), 4 * 2 * 2 * 2 , epsilon);
  }

  BOOST_AUTO_TEST_CASE(Rotation)
  {
    volkov::Rectangle rect({1,2}, 4, 2);
    rect.rotate(45);
    BOOST_CHECK_CLOSE(rect.getFrameRect().pos.x, 1, epsilon);
    BOOST_CHECK_CLOSE(rect.getFrameRect().pos.y, 2, epsilon);
    BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().height , 4.2426, epsilon);
    BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().width , 4.2426, epsilon);
    BOOST_CHECK_CLOSE(rect.getAngle(), 45, epsilon);
  }

  BOOST_AUTO_TEST_CASE(InvalidRectangleHeight)
  {
    BOOST_CHECK_THROW(volkov::Rectangle({0,0}, -1, 2), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidRectangleWigth)
  {
    BOOST_CHECK_THROW(volkov::Rectangle({0,0}, 1, -2), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidRectangleScale)
  {
    volkov::Rectangle rect ({0,0}, 1, 2);
    BOOST_CHECK_THROW(rect.scale(-5), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

  BOOST_AUTO_TEST_CASE(GetFrameRect)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 4, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 4, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 1, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 1, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveToXY)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    composite_shape.move(10, 20);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 4, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 4, epsilon);
    BOOST_CHECK_CLOSE_FRACTION(composite_shape.getFrameRect().pos.x, 11, epsilon);
    BOOST_CHECK_CLOSE_FRACTION(composite_shape.getFrameRect().pos.y, 21, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getArea(), circ.getArea() + rect.getArea(), epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    composite_shape.move({2, 1});
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 4, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 4, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 1, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getArea(), circ.getArea() + rect.getArea(), epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveInformationConstructor)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    volkov::CompositeShape compositeshape1 = std::move(composite_shape);
    BOOST_CHECK_CLOSE(compositeshape1.getFrameRect().height, 4, epsilon);
    BOOST_CHECK_CLOSE(compositeshape1.getFrameRect().width, 4, epsilon);
    BOOST_CHECK_CLOSE(compositeshape1.getFrameRect().pos.x, 1, epsilon);
    BOOST_CHECK_CLOSE(compositeshape1.getFrameRect().pos.y, 1, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveInformationOperator)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    volkov::CompositeShape composite_shape1(rectPtr);
    composite_shape = std::move(composite_shape1);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 4, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 1, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape1.getFrameRect().height, 0, epsilon);
    BOOST_CHECK_CLOSE(composite_shape1.getFrameRect().width, 0, epsilon);
    BOOST_CHECK_CLOSE(composite_shape1.getFrameRect().pos.x, 0, epsilon);
    BOOST_CHECK_CLOSE(composite_shape1.getFrameRect().pos.y, 0, epsilon);
  }

  BOOST_AUTO_TEST_CASE(CopyInformationConstructor)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    volkov::CompositeShape composite_shape1 = composite_shape;
    BOOST_CHECK_CLOSE(composite_shape1.getFrameRect().height, 4, epsilon);
    BOOST_CHECK_CLOSE(composite_shape1.getFrameRect().width, 4, epsilon);
    BOOST_CHECK_CLOSE(composite_shape1.getFrameRect().pos.x, 1, epsilon);
    BOOST_CHECK_CLOSE(composite_shape1.getFrameRect().pos.y, 1, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, composite_shape1.getFrameRect().height, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, composite_shape1.getFrameRect().width, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, composite_shape1.getFrameRect().pos.x, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, composite_shape1.getFrameRect().pos.y, epsilon);
  }

BOOST_AUTO_TEST_CASE(CopyInformationOperator)
  {
    SP circPtr = std::make_shared <volkov::Circle> (volkov::Circle({0, 0}, 1));
    SP rectPtr = std::make_shared <volkov::Rectangle> (volkov::Rectangle({1, 2}, 4, 2));
    volkov::CompositeShape composite_shape(rectPtr);
    volkov::CompositeShape composite_shape_n(circPtr);
    composite_shape = composite_shape_n;
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 0, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 0, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, composite_shape_n.getFrameRect().height, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, composite_shape_n.getFrameRect().width, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, composite_shape_n.getFrameRect().pos.x, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, composite_shape_n.getFrameRect().pos.y, epsilon);
  }

  BOOST_AUTO_TEST_CASE(ChangeAreaScale)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    composite_shape.scale(5);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 1, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 1, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 20, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 20, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getShape(0)->getFrameRect().pos.x, -4, epsilon); 
    BOOST_CHECK_CLOSE(composite_shape.getShape(0)->getFrameRect().pos.y, -4, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getShape(1)->getFrameRect().pos.x, 1, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getShape(1)->getFrameRect().pos.y, 6, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getArea(), (circ.getArea() + rect.getArea()) * 5 * 5, epsilon);
  }

  BOOST_AUTO_TEST_CASE(Rotation)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({4, 0}, 2, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    composite_shape.rotate(270);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 0, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 6, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getShape(0)->getFrameRect().pos.x, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getShape(0)->getFrameRect().pos.y, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getShape(1)->getFrameRect().pos.x, 2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getShape(1)->getFrameRect().pos.y, -2, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getArea(), (circ.getArea() + rect.getArea()), epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getAngle(), 270, epsilon);
  }

  BOOST_AUTO_TEST_CASE(RemoveShape)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    composite_shape.removeShape(1);
    BOOST_CHECK_CLOSE(composite_shape.getArea(), rect.getArea(), epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, rect.getFrameRect().pos.x, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, rect.getFrameRect().pos.y, epsilon);
    BOOST_REQUIRE_EQUAL(composite_shape.getSize(), 1);
  }

  BOOST_AUTO_TEST_CASE(DeleteShapes)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    composite_shape.deleteShapes();
    BOOST_REQUIRE_EQUAL(composite_shape.getSize(), 0);
    BOOST_CHECK_CLOSE(composite_shape.getArea(), 0, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 0, epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 0, epsilon);
  }

  BOOST_AUTO_TEST_CASE(InvalidCompositeShapeScale)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    BOOST_CHECK_THROW(composite_shape.scale(-5), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidRemovingShapeNumber)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    BOOST_CHECK_THROW(composite_shape.removeShape(-5), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(EmptyCompositeShapeWhenRemoving)
  {
    volkov::Circle circ({0, 0}, 1);
    volkov::Rectangle rect({1, 2}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    composite_shape.deleteShapes();
    BOOST_CHECK_THROW(composite_shape.removeShape(1), std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTests)

  BOOST_AUTO_TEST_CASE(MatrixConstructor)
  {
    volkov::Circle circ({5,0}, 2);
    volkov::Rectangle rect({1,2}, 4, 8);
    volkov::Rectangle rect2({7,7}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    SP rectPtr2 = std::make_shared <volkov::Rectangle> (rect2);
    volkov::Matrix new_matrix(circPtr);
    new_matrix.addShape(rectPtr);
    new_matrix.addShape(rectPtr2);
    UP new_layer1 = new_matrix[0];
    UP new_layer2 = new_matrix[1];
    BOOST_CHECK(new_layer1[0] == circPtr);
    BOOST_CHECK(new_layer1[1] == rectPtr2);
    BOOST_CHECK(new_layer2[0] == rectPtr);
    BOOST_CHECK(new_layer2[1] == nullptr);
    BOOST_CHECK_CLOSE(new_layer1[0]->getFrameRect().pos.x, 5, epsilon);
    BOOST_CHECK_CLOSE(new_layer1[0]->getFrameRect().pos.y, 0, epsilon);
    BOOST_CHECK_CLOSE(new_layer1[1]->getFrameRect().pos.x, 7, epsilon);
    BOOST_CHECK_CLOSE(new_layer1[1]->getFrameRect().pos.y, 7, epsilon);
    BOOST_CHECK_CLOSE(new_layer2[0]->getFrameRect().pos.x, 1, epsilon);
    BOOST_CHECK_CLOSE(new_layer2[0]->getFrameRect().pos.y, 2, epsilon);
    BOOST_REQUIRE_EQUAL(new_matrix.getMaxLaySize(), 2);
  }

  BOOST_AUTO_TEST_CASE(MatrixCompositeShapeConstructor)
  {
    volkov::Circle circ({5,0}, 2);
    volkov::Rectangle rect({1,2}, 4, 8);
    volkov::Rectangle rect2({7,7}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    SP rectPtr2 = std::make_shared <volkov::Rectangle> (rect2);
    volkov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    composite_shape.addShape(rectPtr2);
    volkov::Matrix new_matrix(composite_shape);
    UP new_layer1 = new_matrix[0];
    UP new_layer2 = new_matrix[1];
    BOOST_CHECK(new_layer1[0] == circPtr);
    BOOST_CHECK(new_layer1[1] == rectPtr2);
    BOOST_CHECK(new_layer2[0] == rectPtr);
    BOOST_CHECK(new_layer2[1] == nullptr);
    BOOST_CHECK_CLOSE(new_layer1[0]->getFrameRect().pos.x, 5, epsilon);
    BOOST_CHECK_CLOSE(new_layer1[0]->getFrameRect().pos.y, 0, epsilon);
    BOOST_CHECK_CLOSE(new_layer1[1]->getFrameRect().pos.x, 7, epsilon);
    BOOST_CHECK_CLOSE(new_layer1[1]->getFrameRect().pos.y, 7, epsilon);
    BOOST_CHECK_CLOSE(new_layer2[0]->getFrameRect().pos.x, 1, epsilon);
    BOOST_CHECK_CLOSE(new_layer2[0]->getFrameRect().pos.y, 2, epsilon);
  }

  BOOST_AUTO_TEST_CASE(CopyInformationConstructor)
  {
    volkov::Circle circ({5,0}, 2);
    volkov::Rectangle rect({1,2}, 4, 8);
    volkov::Rectangle rect2({7,7}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    SP rectPtr2 = std::make_shared <volkov::Rectangle> (rect2);
    volkov::Matrix new_matrix(circPtr);
    new_matrix.addShape(rectPtr);
    new_matrix.addShape(rectPtr2);
    volkov::Matrix new_matrix1(new_matrix);
    UP new1_layer1 = new_matrix1[0];
    UP new1_layer2 = new_matrix1[1];
    BOOST_CHECK(new1_layer1[0] == circPtr);
    BOOST_CHECK(new1_layer1[1] == rectPtr2);
    BOOST_CHECK(new1_layer2[0] == rectPtr);
    UP new_layer1 = new_matrix[0];
    UP new_layer2 = new_matrix[1];
    BOOST_CHECK(new1_layer1[0] == new_layer1[0]);
    BOOST_CHECK(new1_layer1[1] == new_layer1[1]);
    BOOST_CHECK(new1_layer2[0] == new_layer2[0]);
  }

  BOOST_AUTO_TEST_CASE(CopyInformationOperator)
  {
    volkov::Circle circ({5,0}, 2);
    volkov::Rectangle rect({1,2}, 4, 8);
    volkov::Rectangle rect2({7,7}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    SP rectPtr2 = std::make_shared <volkov::Rectangle> (rect2);
    volkov::Matrix new_matrix(circPtr);
    new_matrix.addShape(rectPtr);
    volkov::Matrix new_matrix1(rectPtr2);
    new_matrix1 = new_matrix;
    UP new1_layer1 = new_matrix1[0];
    UP new1_layer2 = new_matrix1[1];
    BOOST_CHECK(new1_layer1[0] == circPtr);
    BOOST_CHECK(new1_layer2[0] == rectPtr);
    UP new_layer1 = new_matrix[0];
    UP new_layer2 = new_matrix[1];
    BOOST_CHECK(new1_layer1[0] == new_layer1[0]);
    BOOST_CHECK(new1_layer2[0] == new_layer2[0]);
  }

  BOOST_AUTO_TEST_CASE(MoveInformationConstructor)
  {
    volkov::Circle circ({5,0}, 2);
    volkov::Rectangle rect({1,2}, 4, 8);
    volkov::Rectangle rect2({7,7}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    SP rectPtr2 = std::make_shared <volkov::Rectangle> (rect2);
    volkov::Matrix new_matrix(circPtr);
    new_matrix.addShape(rectPtr);
    new_matrix.addShape(rectPtr2);
    volkov::Matrix new_matrix1(std::move(new_matrix));
    UP new_layer1 = new_matrix1[0];
    UP new_layer2 = new_matrix1[1];
    BOOST_CHECK(new_layer1[0] == circPtr);
    BOOST_CHECK(new_layer1[1] == rectPtr2);
    BOOST_CHECK(new_layer2[0] == rectPtr);
    BOOST_REQUIRE_EQUAL(new_matrix.getLayersN(), 0);
    BOOST_REQUIRE_EQUAL(new_matrix.getMaxLaySize(), 0);
  }

  BOOST_AUTO_TEST_CASE(MoveInformationOperator)
  {
    volkov::Circle circ({5,0}, 2);
    volkov::Rectangle rect({1,2}, 4, 8);
    volkov::Rectangle rect2({7,7}, 4, 2);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    SP rectPtr2 = std::make_shared <volkov::Rectangle> (rect2);
    volkov::Matrix new_matrix(circPtr);
    new_matrix.addShape(rectPtr);
    volkov::Matrix new_matrix1(rectPtr2);
    new_matrix1 = std::move(new_matrix);
    UP new_layer1 = new_matrix1[0];
    UP new_layer2 = new_matrix1[1];
    BOOST_CHECK(new_layer1[0] == circPtr);
    BOOST_CHECK(new_layer2[0] == rectPtr);
    BOOST_REQUIRE_EQUAL(new_matrix.getLayersN(), 0);
    BOOST_REQUIRE_EQUAL(new_matrix.getMaxLaySize(), 0);
  }

  BOOST_AUTO_TEST_CASE(EqualityOperator)
  {
    volkov::Circle circ({5,0}, 2);
    volkov::Rectangle rect({1,2}, 4, 8);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::Matrix new_matrix(circPtr);
    new_matrix.addShape(rectPtr);
    volkov::Matrix new_matrix1(circPtr);
    new_matrix1.addShape(rectPtr);
    bool equal = (new_matrix == new_matrix1);
    BOOST_CHECK(equal);
  }

  BOOST_AUTO_TEST_CASE(NonEqualityOperator)
  {
    volkov::Circle circ({5,0}, 2);
    volkov::Rectangle rect({1,2}, 4, 8);
    SP circPtr = std::make_shared <volkov::Circle> (circ);
    SP rectPtr = std::make_shared <volkov::Rectangle> (rect);
    volkov::Matrix new_matrix(circPtr);
    volkov::Matrix new_matrix1(rectPtr);
    bool notequal = (new_matrix != new_matrix1);
    BOOST_CHECK(notequal == true);
  }

BOOST_AUTO_TEST_SUITE_END()
