#include <cmath>
#include <stdexcept>
#include "circle.hpp"

volkov::Circle::Circle(const volkov::point_t & centre, const double radius):
  centre_ (centre),
  radius_(radius),
  angle_(0)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("invalid value");
  }
}

double volkov::Circle::getRadius() const
{
  return radius_;
}

double volkov::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

double volkov::Circle::getAngle() const
{
  return angle_;
}

volkov::rectangle_t volkov::Circle::getFrameRect() const
{
  return {centre_, radius_ * 2 , radius_ * 2};
}

void volkov::Circle::move(const volkov::point_t & n_centre)
{
  centre_ = n_centre;
}

void volkov::Circle::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void volkov::Circle::scale(const double f)
{
  if (f < 0.0)
  {
    throw std::invalid_argument("f be sure >= 0");
  }
  else
  {
  radius_ *= f;
  }
}

void volkov::Circle::rotate(const double t)
{
  angle_ += t;
  if (angle_ >= 360)
  {
    angle_ = fmod(angle_, 360);
  }
}

std::ostream & volkov::operator<<(std::ostream & out, const Circle & circle)
{
  out << "Area: " << circle.getArea() << std::endl;
  out <<"pos (" << circle.getFrameRect().pos.x << ", " << circle.getFrameRect().pos.y << ")" << std::endl;
  out << "radius: " << circle.radius_;

  return out;
}

