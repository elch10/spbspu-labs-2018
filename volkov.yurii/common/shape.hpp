#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace volkov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual double getAngle() const = 0;
    virtual void scale(const double f) = 0;
    virtual void rotate(const double t) = 0;
    virtual volkov::rectangle_t getFrameRect() const = 0;
    virtual void move(const volkov::point_t & centre_n) = 0;
    virtual void move(const double dx, const double dy) = 0;  
  };
}

#endif // SHAPE_HPP
