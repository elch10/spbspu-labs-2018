#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle: public Shape
{
public:
  Circle(const point_t &point, const double &radius);
  double getArea() const;
  double getRadius() const;
  point_t getPosition() const;
  void printInformation() const;
  rectangle_t getFrameRect() const;
  void move(const point_t &point);
  void move(const double &dx, const double &dy);

private:
  point_t position_;
  double radius_;
};

#endif
