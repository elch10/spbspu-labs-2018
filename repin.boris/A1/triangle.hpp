#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include <stdexcept>
#include "shape.hpp"

class Triangle:
  public Shape
{
public:
  Triangle(const point_t &, const point_t &, const point_t &);
  point_t getPosition() const noexcept;
  void printInformation() const noexcept override;
  rectangle_t getFrameRect() const noexcept override;
  double getArea() const noexcept override;
  void move(const point_t &point) noexcept override;
  void move(const double &dx, const double &dy) noexcept override;

private:
  point_t corners_[3];
};

#endif
