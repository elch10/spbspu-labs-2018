#include "circle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

Circle::Circle(const point_t &point, const double &radius):
  position_(point),
  radius_(radius)
{
  assert(radius >= 0);
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

double Circle::getRadius() const
{
  return radius_;
}

point_t Circle::getPosition() const
{
  return position_;
}

rectangle_t Circle::getFrameRect() const
{
  rectangle_t rectangle = {radius_ * 2, radius_ * 2, position_};
  return rectangle;
}

void Circle::move(const point_t &point)
{
  position_ = point;
}

void Circle::move(const double &dx, const double &dy)
{
  position_.x += dx;
  position_.y += dy;
}

void Circle::printInformation() const
{
  std::cout << "Radius -" << getRadius() << ";\n"
    << "Circle area - " << getArea() << ";\n"
    << "Rectangle around Circle:\n"
    << "  width - " << getFrameRect().width << ";\n"
    << "  height - " << getFrameRect().height << ";\n"
    << "  position - (" << getFrameRect().pos.x << ","
    << getFrameRect().pos.y << ");\n\n";
}
