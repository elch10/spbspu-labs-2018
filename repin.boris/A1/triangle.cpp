#include "triangle.hpp"
#include <iostream>
#include <cmath>

Triangle::Triangle(const point_t &pos_a, const point_t &pos_b, const point_t &pos_c):
  corners_{pos_a, pos_b, pos_c}
{}

point_t Triangle::getPosition() const noexcept
{
  point_t pos = {0, 0};

  for (size_t i = 0; i < 3; i++){
    pos.x += corners_[i].x / 3;
    pos.y += corners_[i].y / 3;
  }

  return pos;
}

void Triangle::printInformation() const noexcept
{
  std::cout << "Position - (" << getPosition().x << "," << getPosition().y << ");\n"
    << "Rectangle area - " << getArea() << ";\n"
    << "Rectangle around Rectangle:\n"
    << "  width - " << getFrameRect().width << ";\n"
    << "  height - " << getFrameRect().height << ";\n"
    << "  position - (" << getFrameRect().pos.x << ","
    << getFrameRect().pos.y << ");\n\n";
}

rectangle_t Triangle::getFrameRect() const noexcept
{ 
  point_t top_left = getPosition();
  point_t bottom_right = getPosition();

  for (size_t i = 0; i < 3; i++){
    if (corners_[i].x < top_left.x){
      top_left.x = corners_[i].x;
    }

    if (corners_[i].y > top_left.y){
      top_left.y = corners_[i].y;
    }

    if (corners_[i].x > bottom_right.x){
      bottom_right.x = corners_[i].x;
    }

    if (corners_[i].y < bottom_right.y){
      bottom_right.y = corners_[i].y;
    }
  }

  point_t center = {(top_left.x + bottom_right.x) / 2, (top_left.y + bottom_right.y) / 2};

  return {bottom_right.x - top_left.x, top_left.y - bottom_right.y, center};
}

double Triangle::getArea() const noexcept
{
  return 0.5 * abs(((corners_[0].x - corners_[2].x) * (corners_[1].y - corners_[2].y)
    - (corners_[1].x - corners_[2].x) * (corners_[0].y - corners_[2].y)));
}

void Triangle::move(const point_t &point) noexcept
{
  point_t center = getPosition();
  move(point.x - center.x, point.y - center.y);
}

void Triangle::move(const double &dx, const double &dy) noexcept
{
  for (size_t i = 0; i < 3; i++){
    corners_[i].x += dx;
    corners_[i].y += dy;
  }
}
