#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

ivanov::Rectangle::Rectangle(const ivanov::rectangle_t & rectangle):
  rectangle_(rectangle),
  vertices_{{rectangle.pos.x - (rectangle.width / 2), rectangle.pos.x + (rectangle.height / 2)},
    {rectangle.pos.x + (rectangle.width / 2), rectangle.pos.x + (rectangle.height / 2)},
      {rectangle.pos.x - (rectangle.width / 2), rectangle.pos.x - (rectangle.height / 2)},
        {rectangle.pos.x + (rectangle.width / 2), rectangle.pos.x - (rectangle.height / 2)}}
{
  if ((rectangle_.width < 0.0) || (rectangle_.height < 0.0))
  {
    throw std::invalid_argument ("Wrong WIDTH or HEIGHT!");
  }
}

double ivanov::Rectangle::getArea() const noexcept
{
  return (rectangle_.width * rectangle_.height);
}

ivanov::rectangle_t ivanov::Rectangle::getFrameRect() const noexcept
{
  double left = std::min(std::min(vertices_[0].x, vertices_[3].x), std::min(vertices_[1].x, vertices_[2].x));
  double right = std::max(std::max(vertices_[0].x, vertices_[3].x), std::max(vertices_[1].x, vertices_[2].x));
  double down = std::min(std::min(vertices_[0].y, vertices_[3].y), std::min(vertices_[1].y, vertices_[2].y));
  double up = std::max(std::max(vertices_[0].y, vertices_[3].y), std::max(vertices_[1].y, vertices_[2].y));

  return {right - left, up - down, rectangle_.pos};

}

void ivanov::Rectangle::move(const ivanov::point_t &newCentrePoint) noexcept
{
  const double dx = newCentrePoint.x - rectangle_.pos.x;
  const double dy = newCentrePoint.x - rectangle_.pos.y;
  rectangle_.pos = newCentrePoint;
  for (int i = 0; i < 4; i++)
  {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

void ivanov::Rectangle::move(const double dx, const double dy) noexcept
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
  for (int i = 0; i < 4; i++)
  {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

void ivanov::Rectangle::scale(const double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Wrong RATIO!");
  }
  rectangle_.height *= ratio;
  rectangle_.width *= ratio;
  for (int i = 0; i < 4; i++)
  {
    vertices_[i].x += (rectangle_.pos.x - vertices_[i].x) * ratio;
    vertices_[i].y += (rectangle_.pos.y - vertices_[i].y) * ratio;
  }
}

void ivanov::Rectangle::rotate(const double angle) noexcept
{
  for (int i = 0; i < 4; i++)
  {
    vertices_[i] = {rectangle_.pos.x + (vertices_[i].x - rectangle_.pos.x) * cos(angle * M_PI / 180.0)
      - (vertices_[i].y - rectangle_.pos.y) * sin(angle * M_PI / 180.0),
        rectangle_.pos.y + (vertices_[i].y - rectangle_.pos.y) * cos(angle * M_PI / 180.0)
          + (vertices_[i].x - rectangle_.pos.x) * sin(angle * M_PI / 180.0)};
  }

}

void ivanov::Rectangle::printInfo() noexcept
{
  std::cout << "Width >> " << rectangle_.width << std::endl;
  std::cout << "Height >> " << rectangle_.height << std::endl;
  std::cout << "Position >> " << rectangle_.pos.x << ", " << rectangle_.pos.y << std::endl;

  std::cout << "Frame width >> " << getFrameRect().width << std::endl;
  std::cout << "Frame height >> " << getFrameRect().height << std::endl;
  std::cout << "Frame position >> " << getFrameRect().pos.x << ", " << getFrameRect().pos.y << std::endl;
}
