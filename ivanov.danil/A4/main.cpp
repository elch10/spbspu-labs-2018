#include <iostream>
#include <memory>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace ivanov;

void printMatrix(const Matrix localMatrix)
{
  for (size_t i = 0; i < localMatrix.getLayers(); ++i) {
    std::unique_ptr<std::shared_ptr<Shape>[]> layer = localMatrix[i];
    std::cout << "Layer - " << i + 1 << ":" << std::endl;
    for (size_t j = 0; j < localMatrix.getLayerSize(); ++j) {
      if (!layer[j]) {
        break;
      }
      std::cout << "Element - " << j + 1 << ":" << std::endl;
      layer[j]->printInfo();
    }
  }
}


int main()
{
  std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({1.1, 0.0}, 3.3));
  std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle( {0.1, 0.2, {0.3, 0.4}}));
  std::shared_ptr<Shape> rectangle2 = std::make_shared<Rectangle>(Rectangle( {8.8, 7.7, {0.6, 0.5}}));

  Matrix matrix1;
  matrix1.add(circle1);
  matrix1.add(rectangle1);
  matrix1.add(rectangle2);
  printMatrix(matrix1);

  CompositeShape compositeshape1;

  compositeshape1.addShape(circle1);
  compositeshape1.addShape(rectangle1);
  compositeshape1.addShape(rectangle2);

  matrix1 = compositeshape1.split();
  printMatrix(matrix1);

  rectangle1->rotate(69.2);
  rectangle1->printInfo();
  compositeshape1.rotate(69.2);
  compositeshape1[1]->printInfo();

}
