#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void testShape(Shape & myShape)
{
  std::cout << "Area: " << myShape.getArea() << std::endl;
  std::cout << "Frame: width = " << myShape.getFrameRect().width;
  std::cout << " height = " << myShape.getFrameRect().height << std::endl;

  myShape.move(1.1, 2);
  std::cout << "Pos: (" << myShape.getFrameRect().pos.x << ", "
            << myShape.getFrameRect().pos.y << ")" << std::endl;
  myShape.move({2.2, 1});
  std::cout << "Pos: (" << myShape.getFrameRect().pos.x << ", "
            << myShape.getFrameRect().pos.y << ")" << std::endl;
  std::cout << std::endl;
}

int main()
{
  Rectangle myRectangle({11.0, 5.0, {5, 1}});
  Circle myCircle({2, 1}, 5);
  Triangle myTriangle({1, 3} , {4 , 1} , {6 , 4});

  std::cout<< "Rectangle" << std::endl;
  testShape(myRectangle);
  std::cout << "Circle: " << std::endl;
  testShape(myCircle);
  std::cout << "Triangle: " << std::endl;
  testShape(myTriangle);

  return 0;
}

