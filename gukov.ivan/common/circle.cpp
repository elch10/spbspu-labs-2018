#include "circle.hpp"
#include <iostream>
#include <stdexcept >
#define _USE_MATH_DEFINES
#include <math.h>

using namespace gukov;

gukov::Circle::Circle(double r, const point_t & center) :
  rad_(r),
  center_(center)
{
  if (rad_ < 0.0)
  {
    throw std::invalid_argument("Error!");

  }
}


double gukov::Circle::getArea() const
{
  return rad_ * rad_ * M_PI;
}

rectangle_t gukov::Circle::getFrameRect() const
{
  return {2 * rad_, 2 * rad_, center_ };
}

void gukov::Circle::move(const point_t & pos)
{
  center_ = pos;
}

void gukov::Circle::move(double px, double py)
{
  center_.x += px;
  center_.y += py;
}

void gukov::Circle::scale(const double k)
{
  if (k < 0.0)
    {

    throw std::invalid_argument("Error!");

    }

  rad_ = rad_ * k;
}

point_t gukov::Circle::getCenter() const
{
  return center_;
}

