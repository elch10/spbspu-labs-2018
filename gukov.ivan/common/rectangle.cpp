#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>

using namespace gukov;

gukov::Rectangle::Rectangle(const rectangle_t & rect) :
  rect_(rect)
{
  if (rect.height < 0.0 || rect.width < 0.0)
  {
    throw std::invalid_argument("Error! Invalid height or width");
  }
}

double gukov::Rectangle::getArea() const
{
  return rect_.height * rect_.width;
}

rectangle_t gukov::Rectangle::getFrameRect() const
{
  return rect_;
}

void gukov::Rectangle::move(const point_t & pos)
{
  rect_.pos = pos;
}

void gukov::Rectangle::move(double px, double py)
{
  rect_.pos.x += px;
  rect_.pos.y += py;
}

void gukov::Rectangle::scale(const double k)
{
  if (k < 0.0)
  {

    throw std::invalid_argument("Error!");

  }

  rect_.height *= k;
  rect_.width *= k;
}

point_t gukov::Rectangle::getCenter() const
{
  return rect_.pos;
}
