#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using namespace lebedev;

int main()
{
  std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({-4.0, -4.0}, 3.0, 3.0));
  std::shared_ptr<Shape> rectangle2 = std::make_shared<Rectangle>(Rectangle({-1.0, -1.0}, 2.0, 2.0));
  std::shared_ptr<Shape> rectangle3 = std::make_shared<Rectangle>(Rectangle({1.0, 1.0}, 3.0, 3.0));

  CompositeShape compositeShape;
  compositeShape.addShape(rectangle1);
  compositeShape.addShape(rectangle2);
  compositeShape.addShape(rectangle3);

  std::cout << compositeShape.getQuantity() << " " << compositeShape.getArea() << std::endl;

  return 0;
}
