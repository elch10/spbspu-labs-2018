#include <iostream>
#include <stdexcept>
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  //------------------------------
  //Тестирование CompositeShape
  std::cout << "Test 1. EmptyShape" << std::endl;
  std::unique_ptr<CompositeShape>testCompositeShape (new CompositeShape());
  testCompositeShape->printShape();

  std::cout << "Test 2. Add 6 Shapes" << std::endl;
  std::shared_ptr<Shape> testShape0Circle ( new Circle({3,0}, 3));
  testCompositeShape->addShape(testShape0Circle);
  std::shared_ptr<Shape> testShape1Circle ( new Circle({-5,-5}, 5));
  testCompositeShape->addShape(testShape1Circle);
  std::shared_ptr<Shape> testShape2Rect ( new RectangleC({{0,4}, 8, 4}));
  testCompositeShape->addShape(testShape2Rect);
  std::shared_ptr<Shape> testShape3Rect ( new RectangleC({{-6,1}, 6, 6}));
  testCompositeShape->addShape(testShape3Rect);
  std::shared_ptr<Shape> testShape4Rect ( new RectangleC({{0,0}, 2, 2}));
  testCompositeShape->addShape(testShape4Rect);
  std::shared_ptr<Shape> testShape5Triangle ( new Triangle({2,-7}, {5,-1}, {8,-7}));
  testCompositeShape->addShape(testShape5Triangle);

  testCompositeShape->rebuildLayers();
  testCompositeShape->printShape();

  std::cout << "Test 3. Rotate and rebuild layers" << std::endl;
  std::cout << "Test 3.1. Rotate, 90 and rebuild layers" << std::endl;
  testCompositeShape->rotateShape(90);
  testCompositeShape->rebuildLayers();
  testCompositeShape->printShape();
  std::cout << "Test 3.2. Rotate, -90 and rebuild layers" << std::endl;
  testCompositeShape->rotateShape(-90);
  testCompositeShape->rebuildLayers();
  testCompositeShape->printShape();

  std::cout << "Test 4. Copy constructor" << std::endl;
  std::unique_ptr<CompositeShape>copyCompShape1 = std::move(testCompositeShape);
  copyCompShape1->printShape();

  std::cout << "Test 5. Copy constructor" << std::endl;
  CompositeShape copyCompShape2 (*copyCompShape1);
  copyCompShape2.printShape();

  return 0;
}
