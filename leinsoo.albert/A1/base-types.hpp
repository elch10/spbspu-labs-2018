#ifndef AL_BASE_TYPES
#define AL_BASE_TYPES

struct point_t
{
  double x;
  double y;
};

struct rectangle_t
{
  point_t pos;
  double width, height;
};

#endif // AL_BASE_TYPES
