#include "triangle.hpp"
#include <cmath> // нужны математические функции
#include <iostream>

Triangle::Triangle(const point_t &pA, const point_t &pB, const point_t &pC): // конструктор
  pA_(pA),
  pB_(pB),
  pC_(pC)
{
  double a = sqrt((pB_.y - pC_.y) * (pB_.y - pC_.y) + (pB_.x - pC_.x) * (pB_.x - pC_.x));
  double b = sqrt((pA_.y - pC_.y) * (pA_.y - pC_.y) + (pA_.x - pC_.x) * (pA_.x - pC_.x));
  double c = sqrt((pB_.y - pA_.y) * (pB_.y - pA_.y) + (pB_.x - pA_.x) * (pB_.x - pA_.x));
  double cosA = (b*b + c*c - a*a) / (2 * b * c);
  if (trunc(((1 - cosA) * 1000)) == 0 )
  {
    std::cerr << std::endl << "Init. error: incorrect triangle " << std::endl;
  }
}

rectangle_t Triangle::getFrameRect() // получение ограничивающего пр¤моугольника
{

  double minX = std::min(pA_.x,std::min(pB_.x,pC_.x));//minx
  double minY = std::min(pA_.y,std::min(pB_.y,pC_.y));//miny
  double maxX = std::max(pA_.x,std::max(pB_.x,pC_.x));//maxx
  double maxY = std::max(pA_.y,std::max(pB_.y,pC_.y));//maxy

  // возвращаем структуру описывающего прамоугольника
  return
  {
    {
      (minX +(maxX-minX)/2),
      (minY +(maxY-minY)/2)
    },
    fabs(maxX-minX),
    fabs(maxY-minY)
  };

}

void Triangle::move(const point_t &toPoint) // перемещение треугольника в точку
                                           // оносительно центра масс
{
  double dx = toPoint.x - getMassCenter().x;
  double dy = toPoint.y - getMassCenter().y;
  move(dx,dy);
}

void Triangle::move(const double dx, const double dy) // перемещение пр¤моугольника по смещению
{
  pA_.x+=dx;
  pB_.x+=dx;
  pC_.x+=dx;
  pA_.y+=dy;
  pB_.y+=dy;
  pC_.y+=dy;
}

double Triangle::getArea() // вычисление площади треугольника по ‘ормуле полупериметра
{
  return std::abs( ((pB_.x - pA_.x) * (pC_.y - pA_.y)) - ((pC_.x - pA_.x) * (pB_.y - pA_.y)) ) / 2;
}

void Triangle::printShape() // вывод
{
  std::cout << "\t pA  X; Y: " << pA_.x << " ; " << pA_.y << std::endl;
  std::cout << "\t pB  X; Y: " << pB_.x << " ; " << pB_.y << std::endl;
  std::cout << "\t pC  X; Y: " << pC_.x << " ; " << pC_.y << std::endl;
  std::cout << "\t Area = " << getArea() <<  std::endl;
  std::cout << "\t Mass center X; Y = " << getMassCenter().x << "; " << getMassCenter().y << std::endl;
  std::cout << "\t outRectangle:"<<  std::endl;
  std::cout << "\t\t Center X; Y= " << getFrameRect().pos.x << "; " << getFrameRect().pos.y << std::endl;
  std::cout << "\t\t Width  = " << getFrameRect().width << std::endl;
  std::cout << "\t\t Height = " << getFrameRect().height << std::endl;
}

point_t Triangle::getMassCenter()  // определение  центра масс
{
  return
  {
    ((pA_.x+pB_.x+pC_.x)/3),
    ((pA_.y+pB_.y+pC_.y)/3)
  };
}

Triangle::~Triangle()
{

}
