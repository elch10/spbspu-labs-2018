#include "circle.hpp"
#include <iostream>
#include <cmath>  // нужны математические функции и константа M_PI

namespace
{
  const double Al_PI = 3.14159265358979323846; // toDo -- разобраться, почему не работает константа M_PI
}

Circle::Circle(const point_t &center, const double r):
  center_(center), // присвоение структур - координата центра окружности
  r_(r)           // инициализация радиуса
{
  if (r_ <= 0 )
  {
    std::cerr << std::endl << "Init. error: incorrect radius " << std::endl;
  }
}

double Circle::getArea() // вычисление площади окружности
{
  return Al_PI * r_ * r_;
}

rectangle_t Circle::getFrameRect()  //  получение ограничивающего прямоугольника окружности
{
  return
  {
    center_,
    (2 * r_),
    (2 * r_)
  };
}

void Circle::move(const point_t &toPoint) // перемещение окружности в точку
{
  center_ = toPoint;
}

void Circle::move(const double dx, const double dy) // перемещение окружности по смещению
{
  center_.x +=dx;
  center_.y +=dy;
}

void Circle::printShape() //   вывод параметров окружности
{
  std::cout << "\t Area = " << getArea() <<  std::endl;
  std::cout << "\t Radius = " << r_ << std::endl;
  std::cout << "\t Center X; Y= " << center_.x << "; " << center_.y << std::endl;
  std::cout << "\t outRectangle:"<<  std::endl;
  std::cout << "\t\t Center X; Y= " << getFrameRect().pos.x << "; " << getFrameRect().pos.y << std::endl;
  std::cout << "\t\t Width  = " << getFrameRect().width << std::endl;
  std::cout << "\t\t Height = " << getFrameRect().height << std::endl;
}

Circle::~Circle() // деструктор
{

}
