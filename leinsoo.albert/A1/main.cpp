#include <iostream>
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

int main()
{

  Shape * testShape; // указатель на базовый класс фигур

  // Тесты геометрических фигур
  // -----------------------------------------------------------

  //  Тестирование окружности
  testShape = new Circle({10,10}, 5 );
  std::cout << "Circle initialization test result:" << std::endl;
  testShape->printShape();
  // тестирование перемещения в точку
  std::cout << std::endl << "Testing Circle Move to point" << std::endl;
  testShape->move({0,-10});
  testShape->printShape();

  // тестирование перемещения по смещению
  std::cout << std::endl << "Testing Circle Move by Shift" << std::endl;
  testShape->move(-5,5);
  testShape->printShape();
  delete (testShape);
  // -----------------------------------------------------------
  // Тестирование прямоугольника
  testShape = new Rectangle({{5,10},10,5});
  std::cout << std::endl << "Rectangle initialization test result:" << std::endl;
  testShape->printShape();

  // Тестирование перемещения в точку
  std::cout << std::endl << "Testing Rectangle Move to Point" << std::endl;
  testShape->move({-10,-10});
  testShape->printShape();

  // тестирование прямоугольника по смещению
  testShape->move(15,15);
  std::cout << std::endl << "Rectangle Move by Shift test result:" << std::endl;
  testShape->printShape();
  delete (testShape);
  // -----------------------------------------------------------
  // Тестирование треугольника
  testShape = new Triangle({0,0},{0,10},{10,0});
  std::cout << std::endl << "Triangle initialization test result:" << std::endl;
  testShape->printShape();

  // тестирование перемещения треугольника
  std::cout << std::endl << "Testing Triangle Move (mass center) to Point" << std::endl;
  testShape->move({5,5});
  testShape->printShape();

  // тестирование треугольника по смещению
  std::cout << std::endl << "Testing Triangle Move by Shift" << std::endl;
  testShape->move(10,10);
  testShape->printShape();
  delete (testShape);

  return 0;
}
