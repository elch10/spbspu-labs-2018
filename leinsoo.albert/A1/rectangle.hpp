#ifndef AL_RECTANLE_HPP
#define AL_RECTANLE_HPP
#include "shape.hpp"

class Rectangle: public Shape
{
public:
  Rectangle(const rectangle_t &pRectangle);
  ~Rectangle();
  virtual double getArea(); // вычисление площади
  virtual rectangle_t getFrameRect() ; //  получение ограничивающего прямоугольника
  virtual void move(const point_t &toPoint); // перемещение в точку
  virtual void move(const double dx, const double dy); // перемещение по смещению
  double getWidth(); // расчет ширины по координатам
  double getHeight(); // расчет высоты по координатам
  virtual void printShape(); // вывод данных
private:
  rectangle_t rectangle_;
};

#endif // AL_RECTANLE_HPP
