﻿#ifndef AL_TRIANGLE_HPP
#define AL_TRIANGLE_HPP
#include "shape.hpp"

class Triangle: public Shape
{
public:
  Triangle(const point_t &pA, const point_t &pB, const point_t &pC);
  ~Triangle();
  virtual double getArea(); // вычисление площади треугольника
  virtual rectangle_t getFrameRect() ; //  получение ограничивающего прямоугольника для треугольника
  virtual void move(const point_t &toPoint); // перемещение в точку по центру врисанной окружности
  virtual void move(const double dx, const double dy); // перемещение трекгольника по смещению
  virtual void scale(const double scaleFactor); // масштабирование фигуры
  virtual point_t getMassCenter(); // определение  центра масс
  virtual void printShape(); // вывод данных
  virtual void printShapeType(); // Вывести тип фигуры
  virtual void rotateShape(const double angle); //вращение фигуры
private:
  point_t pA_, pB_, pC_; // задается координатами вершин
};
#endif // AL_TRIANGLE_HPP
