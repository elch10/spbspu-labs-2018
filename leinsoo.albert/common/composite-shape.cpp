#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

CompositeShape::CompositeShape(): // ����������� �� ���������
  shapeLayers_(nullptr),
  shapeCount_ (0),
  shapeArray_(nullptr)
{

}

CompositeShape::CompositeShape(const CompositeShape &compShape): // ����������� �����������
  shapeLayers_(nullptr),
  shapeCount_(compShape.shapeCount_),
  shapeArray_(new std::shared_ptr<Shape>[shapeCount_])

{
  for (int i=0; i < shapeCount_; i++)
  {
    shapeArray_[i] = compShape.shapeArray_[i];
  }
  if (compShape.shapeLayers_ != nullptr)
  {
    shapeLayers_.reset(new CompositeShape(*compShape.shapeLayers_));
  }
}
// �������� ����������� ������������
CompositeShape & CompositeShape::operator = (const CompositeShape &compShape)
{
  if (this != &compShape)
  {
    shapeCount_ = compShape.shapeCount_;
    shapeArray_.reset(new std::shared_ptr<Shape>[shapeCount_]);
    for (int i=0; i < shapeCount_; i++)
    {
       shapeArray_[i] = compShape. shapeArray_[i];
    }

    if (compShape.shapeLayers_ != nullptr)
    {
      shapeLayers_.reset(new CompositeShape(*compShape.shapeLayers_));
    }
  }
  return *this;
}

double CompositeShape::getArea() // ���������� �������
{
  if (shapeCount_ == 0)
  {
    return 0;
  }
  else
  {
    double totalArea = 0;
    for(int i = 0; i < shapeCount_; i++)
    {
      totalArea += shapeArray_[i]->getArea();
    }
    return (totalArea);
  }
}

rectangle_t CompositeShape::getFrameRect() //  ��������� ��������������� ��������������
{
  if (shapeCount_ == 0)
  {
    throw std::logic_error("CompositeShape is empty");
  }
  else
  {
    double minX = shapeArray_[0]->getFrameRect().pos.x - (shapeArray_[0]->getFrameRect().width/2);
    double minY = shapeArray_[0]->getFrameRect().pos.y - (shapeArray_[0]->getFrameRect().height/2);
    double maxX = shapeArray_[0]->getFrameRect().pos.x + (shapeArray_[0]->getFrameRect().width/2);
    double maxY = shapeArray_[0]->getFrameRect().pos.y + (shapeArray_[0]->getFrameRect().height/2);

    for (int i=1; i < shapeCount_; i++)
    {
      if ( minX > (shapeArray_[i]->getFrameRect().pos.x -(shapeArray_[i]->getFrameRect().width/2) ) )
      {
        minX =  shapeArray_[i]->getFrameRect().pos.x -(shapeArray_[i]->getFrameRect().width/2);
      }
      if ( minY > (shapeArray_[i]->getFrameRect().pos.y -(shapeArray_[i]->getFrameRect().height/2) ) )
      {
        minY =  shapeArray_[i]->getFrameRect().pos.y -(shapeArray_[i]->getFrameRect().height/2);
      }
      if ( maxX < (shapeArray_[i]->getFrameRect().pos.x + (shapeArray_[i]->getFrameRect().width/2) ) )
      {
        maxX =  shapeArray_[i]->getFrameRect().pos.x + (shapeArray_[i]->getFrameRect().width/2);
      }
      if ( maxY < (shapeArray_[i]->getFrameRect().pos.y + (shapeArray_[i]->getFrameRect().height/2) ) )
      {
        maxY =  shapeArray_[i]->getFrameRect().pos.y + (shapeArray_[i]->getFrameRect().height/2);
      }

    }
    return{{maxX - (maxX - minX)/2, maxY-(maxY - minY)/2},fabs(maxX - minX),fabs(maxY - minY)};
  }
}

void CompositeShape::move(const point_t &toPoint) //����������� � �����
{
  if(shapeCount_ != 0)
  {
    double dX = toPoint.x - getFrameRect().pos.x;
    double dY = toPoint.y - getFrameRect().pos.y;

    for(int i = 0; i < shapeCount_; i++)
    {
      shapeArray_[i]->move(dX,dY);
    }
  }
}

void CompositeShape::move(const double dx, const double dy) //����������� �� ��������
{
  if (shapeCount_ != 0)
  {
    for(int i = 0; i < shapeCount_; i++)
    {
      shapeArray_[i]->move(dx,dy);
    }
  }
}

void CompositeShape::scale(const double scaleFactor) //���������������
{
  if (scaleFactor <= 0)
  {
    throw std::invalid_argument("Incorrect value");
  }
  if (shapeCount_ != 0)
  {
    rectangle_t tmpRect = getFrameRect();
    for(int i = 0; i < shapeCount_; i++)
    {
      shapeArray_[i]->scale(scaleFactor);
      shapeArray_[i]->move({tmpRect.pos.x + (shapeArray_[i]->getFrameRect().pos.x - tmpRect.pos.x )*scaleFactor,
                            tmpRect.pos.y + (shapeArray_[i]->getFrameRect().pos.y - tmpRect.pos.y )*scaleFactor
                           });
    }
  }
}

void CompositeShape::printShape() // ����� ������
{
  if (shapeCount_ == 0)
  {
    std::cout << std::endl;
    std::cout << "CompositeShape is empty!" << std::endl;
  }
  else
  {
    std::cout << std::endl;
    std::cout << "CompositeShape:" << std::endl;
    std::cout << "-----------------------------------------------" << std::endl;
    std::cout << "\t Shape count = "  << shapeCount_<< std::endl;
    std::cout << "\t CompositeShape area = "  << getArea() << std::endl;
    std::cout << "\t CompositeShape frame rectangle: "  << std::endl;
    std::cout << "\t\t Center X; Y= " << getFrameRect().pos.x << "; " << getFrameRect().pos.y << std::endl;
    std::cout << "\t\t Width  = " << getFrameRect().width << std::endl;
    std::cout << "\t\t Height = " << getFrameRect().height << std::endl;

    for (int i = 0; i < shapeCount_; i++)
    {
      std::cout << std::endl;
      shapeArray_[i] -> printShape();
    }
    std::cout << "-----------------------------------------------" << std::endl;
    std::cout << "-----------------------------------------------" << std::endl;

    if (shapeLayers_ != nullptr)
    {
      printLayers();
    }
  }
}

void CompositeShape::addShape(std::shared_ptr<Shape> newShape) //���������� ������ � ������
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Shape pointer is null. Object does not exist.");
  }
  else
  {
 //   Shape **tmp_shape_array = new Shape*[shapeCount_ + 1]; // ��������� ������
    std::unique_ptr<std::shared_ptr<Shape>[]> tmp_shape_array(new std::shared_ptr<Shape>[shapeCount_ + 1]); // ��������� ������
    for(int i = 0; i < shapeCount_; i++)
    {
      tmp_shape_array[i] = shapeArray_[i]; // ��������
    }

    tmp_shape_array[shapeCount_] = newShape;
    shapeArray_ = std::move(tmp_shape_array);
//   addShapeToLayer(newShape);
    shapeCount_++;
  }
}

void CompositeShape::rotateShape(const double angle)
{
  if (shapeCount_ != 0)
  {
    //���������� frameRect ���� �������� ������� � �����������
    double x1=getFrameRect().pos.x;
    double y1=getFrameRect().pos.y;
    for(int i = 0; i < shapeCount_; i++)
    {
      //Xnew = x1+(x2-x1)*cos(A)-(y2-y1)*sin(A)
      //Ynew = y1+(x2-x1)*sin(A)+(y2-y1)*cos(A)
      double newX = x1
                    + (x1 - shapeArray_[i]->getMassCenter().x) * cos((360 - angle) * Al_PI / 180)
                    - (y1 - shapeArray_[i]->getMassCenter().y) * sin((360 - angle) * Al_PI / 180);
      double newY = y1
                    + (x1 - shapeArray_[i]->getMassCenter().x) * sin((360 - angle) * Al_PI / 180)
                    + (y1 - shapeArray_[i]->getMassCenter().y) * cos((360 - angle)* Al_PI / 180);
      shapeArray_[i]->move({newX,newY});

      shapeArray_[i]->rotateShape(angle);
    }
  }
}


int CompositeShape::getShapeCount()
{
  return shapeCount_;
}

void CompositeShape::printShapeType() //   ����� ���������� ����������
{
  std::cout << "CompositeShape";
}

void CompositeShape::rebuildLayers()
{

  if (shapeCount_ != 0)  // ���� ���� ������ - �� ������ ����� ���������� ������������ �����
  {
    if (shapeLayers_ != nullptr)  // ���� ������ ����� ���� - ����� ����������� ������
    {
      // ������� � ����� ���������
      shapeLayers_.reset(nullptr);
    }
    shapeLayers_.reset(new CompositeShape());  // �������� (��� ������������) ������ �����

    for (int i=0; i<=shapeCount_ - 1; i++)  // ��������� ������ ������ (i) � ������ ����� (j) � �������� � ���� ���� (k)
    {

      if (shapeLayers_->shapeCount_ == 0)  // ���� ����� ���, �� ���� ������� ���� � �������� � ���� ������
      {
        shapeLayers_ -> addShape( std::shared_ptr<CompositeShape> (new CompositeShape)); // �������� ���� !!!!!!!!!!!!!!!

        // ���������� ������ � ����. �.�. �� ������� ���� - �� ��� ������ ������ (0). i - � ������ ������=0 (������ ������)
        std::dynamic_pointer_cast<CompositeShape>(shapeLayers_ ->shapeArray_[0]) -> addShape(shapeArray_[i]);
      }
      else // ���� ����
      {
        bool isIntersect = false;  // ����� �������� ������������
        int j = shapeLayers_->shapeCount_-1; // ������� �����, � ���������� ����
        while ((j >= 0) && (!isIntersect)) // ������ �� ���� �����, ����� � ��������� �� ������ ������� ��� �� �����������
        {
          // ���������� ������� ����
          std::shared_ptr<CompositeShape> CurLayer = std::dynamic_pointer_cast<CompositeShape>(shapeLayers_ -> shapeArray_[j]);
          int k = 0; // ������� ����� � ����
          int lastShape=CurLayer->shapeCount_;
          while (((k+1) <= lastShape) && (!isIntersect))  // ���������� ����������� � ������ ������� �������� ����
          {
            rectangle_t rectI = shapeArray_[i]->getFrameRect(); // �������������� ������������ ������� ������
            rectangle_t rectK = CurLayer ->shapeArray_[k] ->getFrameRect(); // �������������� ������������ ������ � ����

            if ((fabs(rectI.pos.x - rectK.pos.x)<=(rectI.width + rectK.width)/2) &&
                (fabs(rectI.pos.y - rectK.pos.y)<=(rectI.height + rectK.height)/2)) //  ���� ����������� ���� - �������� � ����� ���������� (j+1), �.�. ���� � �����
            {
              isIntersect = true; // ��������� �������� ������� ����������� ��� ��������� ������� �� �����
              if   (j == (shapeLayers_ -> shapeCount_-1)) // ���� ��� ��������� ���� - �� ������� ����� ���� � �������� � ���� ������
              {
//                shapeLayers_->addShape(new CompositeShape);
//                CompositeShape *nextLayer = (CompositeShape*)shapeLayers_ -> shapeArray_[j+1];
//                nextLayer->addShape(shapeArray_[i]);

                shapeLayers_->addShape(std::shared_ptr<CompositeShape> (new CompositeShape)); //�������� ���� (j+1). j+1 - ��� ����� ����

              };
              std::dynamic_pointer_cast<CompositeShape> (shapeLayers_ -> shapeArray_[j+1]) -> addShape(shapeArray_[i]); // �������� ������ � ����

            }
            else // ����������� ��� - ���������, ������ ���� ��� ��������� ����
            {
              if (j == 0)  // ���������� ���������� ������ � ������ ����
              {
                CurLayer->addShape(shapeArray_[i]);
              }
            }
            k++; // � ��������� ������ �������� ����
          }
          j--; // ������� � ����������� ����
        }
      }
    }
  }
}


void CompositeShape::printLayers ()
{
  if (shapeCount_== 0)
  {
    std::cout << "\t\t Shape has no layers";
  }
  else
  {
    std::cout << "\t Layers count = "  << shapeLayers_->shapeCount_ << std::endl;
    for (int i = 0; i < shapeLayers_->shapeCount_; i++)  // ���������� �����
    {
      std::cout << "\t\t Layer num= " << i << " : ";
//      CompositeShape *curLayer = (CompositeShape*)shapeLayers_ -> shapeArray_[i];
        std::shared_ptr<CompositeShape> curLayer = std::dynamic_pointer_cast<CompositeShape>(shapeLayers_ -> shapeArray_[i]);

      for (int j = 0; (j < curLayer->shapeCount_); j++)
      {
        curLayer->shapeArray_[j]->printShapeType();
        std::cout <<" ";
      }
      std::cout << std::endl;
    }
  }
}

point_t CompositeShape::getMassCenter()
{
  return {getFrameRect().pos.x,
          getFrameRect().pos.y};
}

CompositeShape::~CompositeShape()
{

}
