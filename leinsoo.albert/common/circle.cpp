#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

Circle::Circle(const point_t &center, const double r):
  center_(center), // присвоение структур - координата центра окружности
  r_(r)           // инициализация радиуса
{
  if (r_ <= 0 )
  {
    throw std::invalid_argument("Error: Invalid radius argument.");
  }
}

double Circle::getArea() // вычисление площади окружности
{
  return Al_PI * r_ * r_;
}

double Circle::getRadius()
{
  return r_;
}

rectangle_t Circle::getFrameRect()  //  получение ограничивающего прямоугольника окружности
{
  return
  {
    center_,
    (2 * r_),
    (2 * r_)
  };
}

void Circle::move(const point_t &toPoint) // перемещение окружности в точку
{
  center_ = toPoint;
}

void Circle::move(const double dx, const double dy) // перемещение окружности по смещению
{
  center_.x +=dx;
  center_.y +=dy;
}

void Circle::scale(const double scaleFactor) // масштабирование фигуры
{
  if (scaleFactor <= 0)
  {
    throw std::invalid_argument("Error: Invalid circle scale argument.");
  }
  r_ *= scaleFactor;
}

void Circle::printShape() //   вывод параметров окружности
{
  std::cout << "\t Circle:" << std::endl;
  std::cout << "\t Area = " << getArea() <<  std::endl;
  std::cout << "\t Radius = " << r_ << std::endl;
  std::cout << "\t Center X =  " << center_.x <<" ; Y=  "<< center_.y << "; " << std::endl;
  std::cout << "\t outRectangle:"<<  std::endl;
  std::cout << "\t\t Center X = "<< getFrameRect().pos.x <<" ; Y= " << getFrameRect().pos.y<< "; "  << std::endl;
  std::cout << "\t\t Width  = " << getFrameRect().width << std::endl;
  std::cout << "\t\t Height = " << getFrameRect().height << std::endl;
}

void Circle::rotateShape(const double )
{

}

point_t Circle::getMassCenter()
{
  return {getFrameRect().pos.x,
          getFrameRect().pos.y};
}

void Circle::printShapeType() //   вывод параметров окружности
{
  std::cout << "Circle";
}
