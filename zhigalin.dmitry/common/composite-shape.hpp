#ifndef A3_COMPOSITE_SHAPE_HPP
#define A3_COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace zhigalin
{
  class CompositeShape:
    public Shape
  {
    public:
      CompositeShape(const point_t & pos);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t & pos) override;
      void move(const double dx, const double dy) override;
      void scale(const double coeff) override;
      void addShape(const std::shared_ptr<Shape> addedShape);
      void removeShape(const int index);
    private:
      int shapes_;
      std::unique_ptr< std::shared_ptr<Shape>[]> arrShapes_;
  };
} //zhigalin

#endif //A3_COMPOSITE_SHAPE_HPP
