#include "composite-shape.hpp"
#include <cmath>
#include <stdexcept>

zhigalin::CompositeShape::CompositeShape(const point_t & pos):
  Shape(pos),
  shapes_(0),
  arrShapes_(nullptr)
{
}

double zhigalin::CompositeShape::getArea() const
{
  double area = 0.0;
  for (int i = 0; i < shapes_; ++i)
  {
    area += arrShapes_[i] -> getArea();
  }
  return area;
}

zhigalin::rectangle_t zhigalin::CompositeShape::getFrameRect() const
{
  if (arrShapes_ == 0)
  {
    return {0.0, 0.0, {0.0, 0.0}};
  } 
  zhigalin::rectangle_t frameRect = arrShapes_[0] -> getFrameRect();
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  for (int i = 1; i < shapes_; ++i)
  {
    frameRect = arrShapes_[i] -> getFrameRect();
    if (frameRect.pos.x - frameRect.width / 2 < minX)
    {
      minX = frameRect.pos.x - frameRect.width / 2;
    }
    if (frameRect.pos.x + frameRect.width / 2 > maxX)
    {
      maxX = frameRect.pos.x + frameRect.width / 2;
    }
    if (frameRect.pos.y - frameRect.height / 2 < minY)
    {
      minY = frameRect.pos.y - frameRect.height / 2;
    }
    if (frameRect.pos.y + frameRect.height / 2 > maxY)
    {
      maxY = frameRect.pos.y + frameRect.height / 2;
    }
  }
  return {maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}};
}

void zhigalin::CompositeShape::move(const point_t & pos)
{
  for (int i = 0; i < shapes_; ++i)
  {
    arrShapes_[i] -> move(pos.x - getFrameRect().pos.x, pos.y - getFrameRect().pos.y);
  }
}

void zhigalin::CompositeShape::move(const double dx, const double dy)
{
  for (int i = 0; i < shapes_; ++i)
  {
    arrShapes_[i] -> move(dx, dy);
  }
}

void zhigalin::CompositeShape::scale(const double coeff)
{
  if (coeff <= 0.0) 
  {
    throw std::invalid_argument("Scale coefficient must be positeive!");
  }
  point_t positeion = getFrameRect().pos;
  for (int i = 0; i < shapes_; ++i)
  {
    arrShapes_[i] -> move((coeff - 1.0) * (arrShapes_[i] -> getFrameRect().pos.x - positeion.x), 
                          (coeff - 1.0) * (arrShapes_[i] -> getFrameRect().pos.y - positeion.y));
    arrShapes_[i] -> scale(coeff);
  }
}


void zhigalin::CompositeShape::addShape(const std::shared_ptr<Shape> addedShape)
{
  if (addedShape == nullptr)
  {
    throw std::invalid_argument("Null pointer!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tmpArr(new std::shared_ptr<Shape>[shapes_ + 1]);
  for (int i = 0; i < shapes_; ++i)
  {
    tmpArr[i] = arrShapes_[i];
  }
  tmpArr[shapes_] = addedShape;
  shapes_++;
  arrShapes_.swap(tmpArr);
}

void zhigalin::CompositeShape::removeShape(const int index)
{
  if (shapes_ == 0 || index >= shapes_)
  {
    throw std::invalid_argument("Invalid index!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tmpArr(new std::shared_ptr<Shape>[shapes_ - 1]);
  for (int i = 0; i < index; ++i)
  {
    tmpArr[i] = arrShapes_[i];
  }
  for (int i = index; i < shapes_; ++i)
  {
    tmpArr[i] = arrShapes_[i + 1];
  }
  shapes_--;
  arrShapes_.swap(tmpArr);
}
