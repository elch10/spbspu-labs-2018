#include "rectangle.hpp"
#include <cassert>

Rectangle::Rectangle(const point_t & position, const double heigth, const double width):
  m_pos(position),
  m_height(heigth),
  m_width(width)
{
  assert((heigth >= 0) && (width >= 0));
}

double Rectangle::getArea() const
{
  return (m_width * m_height);
}

rectangle_t Rectangle::getFrameRect() const
{
  return {m_pos, m_height, m_width};
}

void Rectangle::move(const point_t &newPoint)
{
  m_pos = newPoint;
}

void Rectangle::move(const double shiftInOx, const double shiftInOy)
{
  m_pos.x += shiftInOx;
  m_pos.y += shiftInOy;
}
