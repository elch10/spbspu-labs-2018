#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(const point_t & position, const double heigth, const double width);
  virtual double getArea() const override;
  virtual rectangle_t getFrameRect() const override;
  virtual void move(const point_t &newPoint) override;
  virtual void move(const double shiftInOx, const double shiftInOy) override;
protected:
  point_t m_pos;
  double m_height;
  double m_width;
};

#endif
