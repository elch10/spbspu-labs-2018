#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

namespace kudelka
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t & position, const double radius);
    virtual double getArea() const override;
    virtual rectangle_t getFrameRect() const override;
    virtual void move(const point_t &newPoint) override;
    virtual void move(const double shiftInOx, const double shiftInOy) override;
    virtual void scale(double coefficient) override;
  protected:
    point_t m_pos;
    double m_radius;
  };
}

#endif
