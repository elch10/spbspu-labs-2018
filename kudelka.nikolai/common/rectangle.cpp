#include "rectangle.hpp"
#include <stdexcept>

kudelka::Rectangle::Rectangle(const point_t & position, const double heigth, const double width):
  m_pos(position),
  m_height(heigth),
  m_width(width)
{
  if (m_width < 0.0 || m_height < 0.0)
  {
    throw std::invalid_argument("Height and width must be non-negative");
  }
}

kudelka::Rectangle::Rectangle(const rectangle_t & rectangle):
  m_pos(rectangle.pos),
  m_height(rectangle.height),
  m_width(rectangle.width)
{
  if (m_width < 0.0 || m_height < 0.0)
  {
    throw std::invalid_argument("Height and width must be non-negative");
  }
}

double kudelka::Rectangle::getArea() const
{
  return (m_width * m_height);
}

kudelka::rectangle_t kudelka::Rectangle::getFrameRect() const
{
  return {m_pos, m_height, m_width};
}

void kudelka::Rectangle::move(const point_t &newPoint)
{
  m_pos = newPoint;
}

void kudelka::Rectangle::move(const double shiftInOx, const double shiftInOy)
{
  m_pos.x += shiftInOx;
  m_pos.y += shiftInOy;
}

void kudelka::Rectangle::scale(double coefficient)
{
  if (coefficient < 0.0)
  {
    throw std::invalid_argument("Coefficient must be non-negative");
  }
  m_height *= coefficient;
  m_width *= coefficient;
}
