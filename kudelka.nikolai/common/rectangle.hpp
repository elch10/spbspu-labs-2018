#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

namespace kudelka
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t & position, const double heigth, const double width);
    Rectangle(const rectangle_t & rectangle);
    virtual double getArea() const override;
    virtual rectangle_t getFrameRect() const override;
    virtual void move(const point_t &newPoint) override;
    virtual void move(const double shiftInOx, const double shiftInOy) override;
    virtual void scale(double coefficient) override;
  protected:
    point_t m_pos;
    double m_height;
    double m_width;
  };
}

#endif
