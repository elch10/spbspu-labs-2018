#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace kudelka
{
  class CompositeShape: public Shape
  {
    public:
      using element_type = std::shared_ptr< Shape >;

      CompositeShape() noexcept;
      CompositeShape(const CompositeShape & rhs);
      CompositeShape(CompositeShape && rhs) noexcept;

      CompositeShape & operator= (const CompositeShape & rhs);
      CompositeShape & operator= (CompositeShape && rhs) noexcept;
      element_type & operator[] (const size_t index) const;

      double getArea() const noexcept override;
      rectangle_t getFrameRect() const noexcept override;
      void move(const point_t &newPoint) noexcept override;
      void move(const double shiftInOx, const double shiftInOy) noexcept override;
      void scale(const double coefficient) override;

      void add(const element_type & shape);
      void remove(size_t index);
      size_t getSize() const noexcept;
      void clear() noexcept;
    protected:
      std::unique_ptr< element_type[] > m_array;
      size_t m_size;
  };
}

#endif
