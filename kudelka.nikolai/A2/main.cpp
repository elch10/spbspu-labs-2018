#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"

void outputInfo(const kudelka::Shape & shape);

int main ()
{
  const double coefficient = 0.5;

  std::cout << "Rectangle:" << std::endl << std::endl
      << "\tInitialization..." << std::endl << std::endl;
  kudelka::Rectangle rectangle({0.0,0.0},5.0,10.0);
  outputInfo(rectangle);
  std::cout << "\tMove to point (10;-10)..." << std::endl << std::endl;
  rectangle.move({10.0,-10.0});
  outputInfo(rectangle);
  std::cout << "\tShift in OX on -10, in OY on 10..." << std::endl << std::endl;
  rectangle.move(-10.0,10.0);
  outputInfo(rectangle);
  std::cout << "\tScaling with coefficient: " << coefficient << "..." << std::endl << std::endl;
  rectangle.scale(coefficient);
  outputInfo(rectangle);

  std::cout << "Circle:" << std::endl << std::endl
      << "\tInitialization..." << std::endl << std::endl;
  kudelka::Circle circle({0.0,0.0},5.0);
  outputInfo(circle);
  std::cout << "\tMove to point (-5;5)..." << std::endl << std::endl;
  circle.move({-5.0,5.0});
  outputInfo(circle);
  std::cout << "\tShift in OX on 5, in OY on -5..." << std::endl << std::endl;
  circle.move(5.0,-5.0);
  outputInfo(circle);
  std::cout << "\tScaling with coefficient: " << coefficient << "..." << std::endl << std::endl;
  circle.scale(coefficient);
  outputInfo(circle);

  return 0;
}

void outputInfo(const kudelka::Shape & shape)
{
  kudelka::rectangle_t frameRect;
  frameRect = shape.getFrameRect();
  std::cout << "\t\tFrameRect:" << std::endl
      << "\t\t\tPosition:\t(" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl
      << "\t\t\tWidth:\t\t" << frameRect.width << std::endl
      << "\t\t\tHeight:\t\t" << frameRect.height << std::endl
      << "\t\tArea:\t\t\t" << shape.getArea() << std::endl << std::endl;
}
