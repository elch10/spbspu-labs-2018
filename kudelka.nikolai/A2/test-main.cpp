#define BOOST_TEST_MODULE Main
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"

using namespace kudelka;

const double EPSILON = 1e-6;

BOOST_AUTO_TEST_SUITE(RectangleSuite)

  BOOST_AUTO_TEST_CASE(InvalidConstructorParameters)
  {
    BOOST_CHECK_THROW(Rectangle rectangle({2.0,4.0},8.0,-16.0), std::invalid_argument);
    BOOST_CHECK_THROW(Rectangle rectangle({2.0,4.0},-8.0,16.0), std::invalid_argument);
    BOOST_CHECK_THROW(Rectangle rectangle({2.0,4.0},-8.0,-16.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
  {
    Rectangle rectangle({2.0,4.0},8.0,16.0);
    BOOST_CHECK_THROW( rectangle.scale(-1.0), std::invalid_argument );
  }
  
  BOOST_AUTO_TEST_CASE(MoveToPointTest)
  {
    Rectangle rectangle({2.0,4.0},8.0,16.0);
    rectangle_t rectBeforeMoving = rectangle.getFrameRect();
    double areaBeforeMoving = rectangle.getArea();
    
    rectangle.move({3.2, 6.4});
    
    BOOST_CHECK_EQUAL(areaBeforeMoving, rectangle.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, rectangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, rectangle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(RelativeMovingTest)
  {
    Rectangle rectangle({2.0,4.0},8.0,16.0);
    rectangle_t rectBeforeMoving = rectangle.getFrameRect();
    double areaBeforeMoving = rectangle.getArea();

    rectangle.move(32.0, -64.0);
    
    BOOST_CHECK_EQUAL(areaBeforeMoving, rectangle.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, rectangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, rectangle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    Rectangle rectangle({2.0,4.0},8.0,16.0);
    double areaBeforeScaling = rectangle.getArea();
    const double coefficient = 0.5;

    rectangle.scale(coefficient);
    
    BOOST_CHECK_CLOSE(coefficient * coefficient * areaBeforeScaling, rectangle.getArea(), EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CircleSuite)

  BOOST_AUTO_TEST_CASE(InvalidConstructorParameters)
  {
    BOOST_CHECK_THROW( Circle circle({2.0,4.0},-8.0), std::invalid_argument );
  }

  BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
  {
    Circle circle({2.0,4.0},8.0);
    BOOST_CHECK_THROW( circle.scale(-1.0), std::invalid_argument );
  }
  
  BOOST_AUTO_TEST_CASE(MoveToPointTest)
  {
    Circle circle({2.0,4.0},8.0);
    rectangle_t rectBeforeMoving = circle.getFrameRect();
    double areaBeforeMoving = circle.getArea();

    circle.move({1.6, 3.2});

    BOOST_CHECK_EQUAL(areaBeforeMoving, circle.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(RelativeMovingTest)
  {
    Circle circle({2.0,4.0},8.0);
    rectangle_t rectBeforeMoving = circle.getFrameRect();
    double areaBeforeMoving = circle.getArea();

    circle.move(16.0, -32.0);

    BOOST_CHECK_EQUAL(areaBeforeMoving, circle.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    Circle circle{ {5.0, 3.0}, 22.0 };
    double areaBeforeScaling = circle.getArea();
    const double coefficient = 0.5;

    circle.scale(coefficient);

    BOOST_CHECK_CLOSE(coefficient * coefficient * areaBeforeScaling, circle.getArea(), EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()
