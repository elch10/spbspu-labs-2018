#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"
namespace reznikov
{
class Circle :
  public Shape 
{
public:
  Circle(const double radius, const point_t &center);

  double getArea() const noexcept override;
  rectangle_t getFrameRect() const noexcept override;
  void move(const point_t &point) noexcept override;
  void move(const double dx, const double dy) noexcept override;
  void scale(const double coefficient) override;
  void inf() const noexcept override;
  double getRadius() const noexcept;
  point_t getPosition() const noexcept override;
  void rotate(double angle) noexcept override;
  
private:
  double radius_;
  point_t center_;
};
}

#endif //CIRCLE_HPP
