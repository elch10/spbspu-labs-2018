#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>
namespace reznikov {

  class CompositeShape :
    public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const std::shared_ptr< Shape > &shape);
    CompositeShape(const CompositeShape & composite_shape);
    CompositeShape(CompositeShape && composite_shape) noexcept;
    CompositeShape & operator=(const CompositeShape & compsoite_shape);
    CompositeShape & operator=(CompositeShape && compsoite_shape) noexcept;
    const std::shared_ptr<Shape> & operator[](size_t size) const;

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & point) noexcept override;
    void move(const double dx, const double dy) noexcept  override;
    void scale(const double coefficient) override;
    void addShape(const std::shared_ptr< Shape > &shape);
    void removeElement(size_t position);
    void deleteAllElement() noexcept;
    point_t getPosition() const noexcept override;
    size_t getSize() const noexcept;
    void inf() const noexcept override;
    void rotate(double angle) noexcept override;

  private:
    std::unique_ptr< std::shared_ptr< Shape >[] > elements_;
    size_t depth_;
  };
}

#endif

