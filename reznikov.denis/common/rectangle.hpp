#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"
namespace reznikov
{
class Rectangle :
  public Shape
{
public:
  Rectangle(const double width, const double height, const point_t &center);

  double getArea() const noexcept override;
  rectangle_t getFrameRect() const noexcept override;
  void move(const point_t &point) noexcept override;
  void move(const double dx, const double dy) noexcept override;
  void scale(const double coefficient) override;
  void inf() const noexcept override;
  point_t getPosition() const noexcept  override;
  void rotate(double angle) noexcept override;

private:
  double width_;
  double height_;
  point_t center_;
  point_t dots_[4];
};
}
#endif //RECTANGLE_HPP
