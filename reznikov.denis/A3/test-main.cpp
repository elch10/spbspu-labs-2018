#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>
#include <cmath>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double epsilon = 0.0001;

BOOST_AUTO_TEST_SUITE(CircleTest)

  BOOST_AUTO_TEST_CASE(MoveByIncrementation)
  {
    reznikov::Circle circle(4, {15.5, 25});
    circle.move(4.5, 25);
    BOOST_REQUIRE_EQUAL(circle.getRadius(),4);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), M_PI * 4 * 4, epsilon);
    BOOST_CHECK_EQUAL(circle.getPosition().x, 20);
    BOOST_CHECK_EQUAL(circle.getPosition().y, 50);
  }
  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    reznikov::Circle circle(4, {20, 50});
    circle.move({10, 20});
    BOOST_REQUIRE_EQUAL(circle.getRadius(),4);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(),M_PI * 4 * 4, epsilon);
    BOOST_CHECK_EQUAL(circle.getPosition().x, 10);
    BOOST_CHECK_EQUAL(circle.getPosition().y, 20);
  }
  BOOST_AUTO_TEST_CASE(AreaScaleTest)
  {
    reznikov::Circle circle(4, {20, 50});
    circle.scale(3);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(),M_PI * 12 * 12, epsilon);
  }
  BOOST_AUTO_TEST_CASE(InvalidScaleCircleTest)
  {
    reznikov::Circle circle(4, {20, 50});
    BOOST_CHECK_THROW(circle.scale(-2),std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(Constructor_invalid_radius)
  {
    BOOST_CHECK_THROW(reznikov::Circle(-0.5, {1, 1}), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(RectangleTest)

  BOOST_AUTO_TEST_CASE(MoveByIncrementation)
  {
    reznikov::Rectangle rectangle(4, 3, {8.9, 15.5});
    rectangle.move(1.1, 4.5);
    BOOST_CHECK_EQUAL(rectangle.getPosition().x, 10);
    BOOST_CHECK_EQUAL(rectangle.getPosition().y, 20);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(),12,epsilon);
  }
  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    reznikov::Rectangle rectangle(4, 3, {100, 200});
    rectangle.move({10,20});
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(),12,epsilon);
    BOOST_CHECK_EQUAL(rectangle.getPosition().x, 10);
    BOOST_CHECK_EQUAL(rectangle.getPosition().y, 20);
  }
  BOOST_AUTO_TEST_CASE(AreaScaleTest)
  {
    reznikov::Rectangle rectangle(15,20,{10,10});
    rectangle.scale(2);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(),1200,epsilon);
  }
  BOOST_AUTO_TEST_CASE(InvalidScaleRectTest)
  {
   BOOST_CHECK_THROW(reznikov::Rectangle(5, 4, {100, 200}).scale(-1), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(InvalidArgsTest)
  {
    BOOST_CHECK_THROW(reznikov::Rectangle(-0.5, 5.5, {1, 1}), std::invalid_argument);
    BOOST_CHECK_THROW(reznikov::Rectangle(0.5, -5.5, {1, 1}), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(CompositeShapeTest)

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    reznikov::CompositeShape object;
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(1, 1, { 1, 1 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Circle(2, { 2, 2 })));
    reznikov::CompositeShape object2(object);
    BOOST_CHECK_EQUAL(object.getArea(), object2.getArea());
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    reznikov::CompositeShape object, object2;
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(1, 1, { 1, 1 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Circle(2, { 2, 2 })));
    object2 = object;
    BOOST_CHECK_EQUAL(object.getArea(), object2.getArea());
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    reznikov::CompositeShape object,object3;
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(1, 1, { 1, 1 })));
    object3.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(1, 1, { 1, 1 })));
    reznikov::CompositeShape object2 (std::move(object));
    BOOST_CHECK_EQUAL(object3.getArea(), object2.getArea());
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    reznikov::CompositeShape object, object2,object3;
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(1, 1, { 1, 1 })));
    object3.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(1, 1, { 1, 1 })));
    object2 = std::move(object);
    BOOST_REQUIRE_EQUAL(object3.getArea(), object2.getArea());
  }

  BOOST_AUTO_TEST_CASE(OperatorIndexTest)
  {
    reznikov::CompositeShape object;
    std::shared_ptr< reznikov::Shape > shape(new reznikov::Rectangle(1, 1, { 1, 1}));
    object.addShape(shape);
    BOOST_REQUIRE_EQUAL(object[0], shape);
  }
  BOOST_AUTO_TEST_CASE(ThrowIndexTest)
  {
    reznikov::CompositeShape object;
    std::shared_ptr< reznikov::Shape > shape(new reznikov::Rectangle(1, 1, { 1, 1}));
    object.addShape(shape);
    BOOST_CHECK_THROW(object[7], std::out_of_range);
  }
  
  BOOST_AUTO_TEST_CASE(AddShapeTest)
  {
    reznikov::CompositeShape object;
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(1, 1, { 1, 1 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Circle(2, { 2, 2 })));
    BOOST_REQUIRE_EQUAL(object.getSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(AreaTest)
  {
    reznikov::CompositeShape object;
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(1, 1, { 1, 1 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Circle(2, { 2, 2 })));
    BOOST_CHECK_CLOSE(object.getArea(), M_PI * 2 * 2 + 1, epsilon);
  }

  BOOST_AUTO_TEST_CASE(ScaleAreaTest)
  {
    reznikov::CompositeShape object;
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Circle(22, { 22, 22 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(22, 22, { 22, 22 })));
    object.scale(5);
    BOOST_CHECK_CLOSE(object[0]->getArea(), M_PI * 22 * 22 * 5 * 5, epsilon);
    BOOST_CHECK_CLOSE(object[1]->getArea(), 22 * 22 * 5 * 5, epsilon);
    BOOST_CHECK_CLOSE(object[0]->getFrameRect().width, 5 * 22 * 2, epsilon);
    BOOST_CHECK_CLOSE(object[0]->getFrameRect().height, 5 * 22 * 2, epsilon);
    BOOST_CHECK_CLOSE(object[1]->getFrameRect().width, 5 * 22, epsilon);
    BOOST_CHECK_CLOSE(object[1]->getFrameRect().height, 5 * 22, epsilon);
    BOOST_CHECK_CLOSE(object.getFrameRect().pos.x, 22, epsilon);
    BOOST_CHECK_CLOSE(object.getFrameRect().pos.y, 22, epsilon);
    BOOST_CHECK_CLOSE(object.getFrameRect().width, 5 * 22 * 2 , epsilon);
    BOOST_CHECK_CLOSE(object.getFrameRect().height, 5 * 22 * 2, epsilon);
  }
    
  BOOST_AUTO_TEST_CASE(InvalidArgumentScale)
  {
    reznikov::CompositeShape object;
    std::shared_ptr< reznikov::Shape > shape(new reznikov::Rectangle(1, 1, { 1, 1}));
    object.addShape(shape);
    BOOST_CHECK_THROW(object.scale(-10), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MoveToPointTest)
  {
    reznikov::CompositeShape object;
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(2, 2, { -20, 15 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(2, 2, { -20, -15 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(2, 2, { 10, 15 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(2, 2, { 10, -15 })));
    object.move({ 10, 10 });
    BOOST_CHECK_CLOSE(object.getFrameRect().pos.x, 10, epsilon);
    BOOST_CHECK_CLOSE(object.getFrameRect().pos.y, 10, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveTest)
  {
    reznikov::CompositeShape object;
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(2, 2, { -20, 15 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(2, 2, { -20, -15 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(2, 2, { 20, 15 })));
    object.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle(2, 2, { 20, -15 })));
    object.move( 10, 0 );
    BOOST_CHECK_CLOSE(object.getPosition().x, 10, epsilon);
    BOOST_CHECK_CLOSE(object.getPosition().y, 0, epsilon);
  }

  BOOST_AUTO_TEST_CASE(RemoveElementThrowTest)
  {
    reznikov::CompositeShape object;
    std::shared_ptr< reznikov::Shape > shape(new reznikov::Circle(1, { 2, 2 }));
    std::shared_ptr< reznikov::Shape > shape1(new reznikov::Circle(3, { 2, 2 }));
    object.addShape(shape);
    object.addShape(shape1);
    BOOST_CHECK_THROW(object.removeElement(100), std::out_of_range);
  }
  

  BOOST_AUTO_TEST_CASE(DeleteShapeTest)
  {
    reznikov::CompositeShape object;
    std::shared_ptr< reznikov::Shape > shape(new reznikov::Circle(1, { 2, 2 }));
    std::shared_ptr< reznikov::Shape > shape1(new reznikov::Circle(3, { 2, 2 }));
    object.addShape(shape);
    object.addShape(shape1);
    object.removeElement(0);
    BOOST_REQUIRE_EQUAL(object.getSize(), 1);
    BOOST_CHECK_CLOSE(object[0]->getFrameRect().height, 6, epsilon);
    BOOST_CHECK_CLOSE(object[0]->getFrameRect().width, 6, epsilon);
  }
  
  BOOST_AUTO_TEST_CASE(GetSizeTest)
  {
    reznikov::CompositeShape object;
    std::shared_ptr< reznikov::Shape > shape(new reznikov::Circle(1, { 2, 2 }));
    std::shared_ptr< reznikov::Shape > shape1(new reznikov::Rectangle(1, 1, { 2, 2 }));
    object.addShape(shape);
    object.addShape(shape1);
    BOOST_REQUIRE_EQUAL(object.getSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(deleteAllObjectTest)
  {
    reznikov::CompositeShape object;
    std::shared_ptr< reznikov::Shape > shape(new reznikov::Circle(1, { 2, 2 }));
    std::shared_ptr< reznikov::Shape > shape1(new reznikov::Rectangle(1, 1, { 2, 2 }));
    object.addShape(shape);
    object.addShape(shape1);
    object.deleteAllElement();
    BOOST_REQUIRE_EQUAL(object.getSize(), 0);
  }
  
BOOST_AUTO_TEST_SUITE_END()


