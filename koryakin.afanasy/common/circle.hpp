#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace koryakin 
{
  class Circle : public Shape
  {
  public:
    Circle(const double r, const point_t & point);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & point) override;
    void move(const double dx, const double dy) override;
    void scale(const double factor) override;
    void rotate(double deg) noexcept override;
    void print() const noexcept override;
    point_t getPos() const noexcept override;
  private:
    double radius_;
    point_t center_;
    double ang;
  };
}

#endif
