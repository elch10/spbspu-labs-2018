#ifndef RECT_HPP
#define RECT_HPP

#include "shape.hpp"

namespace koryakin 
{
  class Rectangle : public Shape {
  public:
    Rectangle(const double width, const double height, const point_t & center);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & point) override;
    void move(const double dx, const double dy) override;
    void scale(const double factor) override;
    void rotate(const double angle) noexcept override;
    void print() const noexcept override;
    point_t getPos() const noexcept override;
  private:
    double height_;
    double width_;
    point_t center_;
    point_t dots_[4];
  };
} 

#endif //RECT_HPP
