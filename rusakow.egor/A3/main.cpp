#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/triangle.hpp"
#include "../common/composite-shape.hpp"

int main(){
  try {
    rusakow::CompositeShape comp;

    std::shared_ptr <rusakow::Shape> rect = std::make_shared <rusakow::Rectangle> (rusakow::Rectangle ({2.0, 2.0}, 2.0, 2.0));
    rect -> getInfo();

    std::shared_ptr <rusakow::Shape> circ = std::make_shared  <rusakow::Circle> (rusakow::Circle ({6.0, 2.0}, 1));
    circ -> getInfo();

    std::shared_ptr <rusakow::Shape> trian = std::make_shared <rusakow::Triangle> (rusakow::Triangle ({2.0, 6.0}, {2.0, 4.0}, {4.0, 4.0}));
    trian -> getInfo();

    comp.addShape(rect);
    comp.addShape(circ);
    comp.addShape(trian);
    comp.getInfo();

    comp.move({9.0, 9.0});
    comp.move(1.0, 1.0);
    comp.scale(2.0);
    comp.getInfo();
  }
  catch (std::exception & err) {
    std::cerr << err.what() << std::endl;
  }
}
