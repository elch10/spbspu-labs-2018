#include "polygon.hpp"

#include <cmath>
#include <iostream>
#include <stdexcept>

rusakow::Polygon::Polygon (std::initializer_list<point_t> points){
  if (points.size() < 3){
    throw (std::invalid_argument ("Polygon must have 3 vertexes at least!"));
  }
  else
  {
    count_ = points.size();
    vertexes_ = std::unique_ptr <rusakow::point_t[]> (new rusakow::point_t[count_]);
    int i = 0;
    for (std::initializer_list<rusakow::point_t>::const_iterator vertex = points.begin(); vertex != points.end(); vertex++)
    {
      vertexes_[i] = *vertex;
      i++;
    }
    bool zeroAreaIndicator = true;
    bool convexIndicator = true;
    for (size_t i = 0; i < (count_ - 3); i++) {
      double dx1 = vertexes_[i + 1].x - vertexes_[i].x;
      double dy1 = vertexes_[i + 1].y - vertexes_[i].y;
      double dx2 = vertexes_[i + 2].x - vertexes_[i + 1].x;
      double dy2 = vertexes_[i + 2].y - vertexes_[i + 1].y;
      double dx3 = vertexes_[i + 3].x - vertexes_[i + 2].x;
      double dy3 = vertexes_[i + 3].y - vertexes_[i + 2].y;
      if ((((dx1 * dy2) - (dx2 * dy1)) < 0.0) ^ (((dx2 * dy3) - (dx3 * dy2)) < 0.0)){
        convexIndicator = false;
      }
      if ((((dx1 * dy2) - (dx2 * dy1)) != 0.0) | (((dx2 * dy3) - (dx3 * dy2)) != 0.0)){
        zeroAreaIndicator = false;
      }
    }
    if (!convexIndicator){
      throw (std::invalid_argument ("Polygon is not convex!"));
    }

    if (zeroAreaIndicator){
      throw(std::invalid_argument ("Polygon's area is zero!"));
    }
    center_ = {0.0, 0.0};
    for (size_t i = 0; i < count_; i++){
      center_.x += vertexes_[i].x;
      center_.y += vertexes_[i].y;
    }
    center_.x /= count_;
    center_.y /= count_;
  }
}

rusakow::point_t rusakow::Polygon::operator [] (size_t index) const{
  if (index >= count_){
    throw (std::out_of_range ("Point index is out of range!"));
  }
  else
  {
    return (vertexes_[index]);
  }
}

rusakow::rectangle_t rusakow::Polygon::getFrameRect() const{
  double maxX = vertexes_[0].x;
  double maxY = vertexes_[0].y;
  double minX = maxX;
  double minY = maxY;
  for (size_t i = 1; i < count_; i++){
    maxX = std::max(maxX, vertexes_[i].x);
    maxY = std::max(maxY, vertexes_[i].y);
    minX = std::min(minX, vertexes_[i].x);
    minY = std::min(minY, vertexes_[i].y);
  }
  rusakow::rectangle_t framerect;
  framerect.pos = {((maxX + minX) / 2.0), ((maxY + minY) / 2.0)};
  framerect.height = maxY - minY;
  framerect.width = maxX - minX;
  return (framerect);
}

double rusakow::Polygon::getArea() const{
  double dx1 = vertexes_[count_ - 1].x - center_.x;
  double dy1 = vertexes_[count_ - 1].y - center_.y;
  double dx2 = vertexes_[0].x - center_.x;
  double dy2 = vertexes_[0].y - center_.y;
  double overallArea = (dx1 * dy2 - dx2 * dy1) / 2.0;
  for (size_t i = 0; i < (count_ - 1); i++){
    double dx1 = vertexes_[i].x - center_.x;
    double dy1 = vertexes_[i].y - center_.y;
    double dx2 = vertexes_[i + 1].x - center_.x;
    double dy2 = vertexes_[i + 1].y - center_.y;
    overallArea += (dx1 * dy2 - dx2 * dy1) / 2.0;
  }
  return (abs (overallArea));
}

void rusakow::Polygon::move(double dx, double dy){
  for (size_t i = 0; i < count_; i++){
    vertexes_[i].x += dx;
    vertexes_[i].y += dy;
  }
  center_.x += dx;
  center_.y += dy;
}

void rusakow::Polygon::move(rusakow::point_t point){
  move ((point.x - center_.x), (point.y - center_.y));
}

void rusakow::Polygon::getInfo() const{
  std::cout << "This is a polygon with " << count_ << " vertexes" << std::endl;
  std::cout << "It is centered in " << center_.x << " " << center_.y << std::endl;
  std::cout << "Its area is " << getArea() << std::endl;
  rusakow::rectangle_t frameRect = getFrameRect();
  std::cout << "Its frame rectangle has height " << frameRect.height << " and width " << frameRect.width << std::endl;
  std::cout << std::endl;
}

void rusakow::Polygon::scale(const double coeff){
  if (coeff < 0.0) {
    throw (std::invalid_argument("Scaling coefficient must not be negative!"));
  }
  else
  {
    for (size_t i = 0; i < count_; i++){
      double dx = vertexes_[i].x - center_.x;
      double dy = vertexes_[i].y - center_.y;
      vertexes_[i].x += dx * (coeff - 1);
      vertexes_[i].y += dy * (coeff - 1);
    }
  }
}

rusakow::point_t rusakow::Polygon::getCenter() const{
  return (center_);
}

void rusakow::Polygon::rotate (const double angle){
  double angSin = std::sin (angle * M_PI / 180);
  double angCos = std::cos (angle * M_PI / 180);
  for (size_t i = 0; i < count_; i++){
    rusakow::point_t newVertex = vertexes_[i];
    newVertex.x = center_.x + angCos * (vertexes_[i].x - center_.x) - angSin * (vertexes_[i].y - center_.y);
    newVertex.y = center_.y + angCos * (vertexes_[i].y - center_.y) + angSin * (vertexes_[i].x - center_.x);
    vertexes_[i] = newVertex;
  }
}

