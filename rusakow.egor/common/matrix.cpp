#include <stdexcept>
#include <cmath>

#include "matrix.hpp"

rusakow::Matrix::Matrix():
  matrix_(nullptr),
  layers_(0),
  numberOfShapes_(0)
{
}

rusakow::Matrix::Matrix(const std::shared_ptr<rusakow::Shape> object):
  matrix_(nullptr),
  layers_(0),
  numberOfShapes_(0)
{
  if (object == nullptr){
    throw std::invalid_argument("Invalid pointer");
  }
  else
  {
    add(object);
  }
}

rusakow::Matrix::Matrix(const CompositeShape & comp):
  Matrix()
{
  for (size_t i = 0; i < comp.getSize(); i++){
    add(comp[i]);
  }
}

std::unique_ptr <std::shared_ptr <rusakow::Shape> []> rusakow::Matrix::operator[](const int index) const
{
  if ((index < 0) || (index >= layers_)){
    throw std::out_of_range("Invalid index");
  }
  std::unique_ptr <std::shared_ptr <rusakow::Shape> []> layer (new std::shared_ptr <rusakow::Shape> [numberOfShapes_]);
  for (int i = 0; i < numberOfShapes_; ++i){
    layer[i] = matrix_[index * numberOfShapes_ + i];
  }
  return layer;
}

size_t rusakow::Matrix::getLayersNum() const noexcept {
  return layers_;
}

void rusakow::Matrix::add (const std::shared_ptr <rusakow::Shape> object) noexcept {
  if (layers_ == 0){
    std::unique_ptr<std::shared_ptr<rusakow::Shape>[]> newMatrix (new std::shared_ptr<rusakow::Shape>[(layers_ + 1) * (numberOfShapes_ + 1)]);
    numberOfShapes_++;
    layers_++;
    matrix_.swap (newMatrix);
    matrix_[0] = object;
  }
  else
  {
    bool addedShape = false;
    for (int i = 0; !addedShape ; ++i){
      for (int j = 0; j < numberOfShapes_; ++j){
        if (matrix_[i * numberOfShapes_ + j] == nullptr){
          matrix_[i * numberOfShapes_ + j] = object;
          addedShape = true;
          break;
        }
        else
        {
          if (checkOverlap (i * numberOfShapes_ + j, object)){
            break;
          }
        }
        if (j == (numberOfShapes_ - 1)){
          std::unique_ptr<std::shared_ptr<rusakow::Shape>[]> newMatrix(new std::shared_ptr<rusakow::Shape>[layers_ * (numberOfShapes_ + 1)]);
          numberOfShapes_++;
          for (int n = 0; n < layers_; ++n){
            for (int m = 0; m < numberOfShapes_ - 1; ++m){
              newMatrix[n * numberOfShapes_ + m] = matrix_[n * (numberOfShapes_ - 1) + m];
            }
            newMatrix[(n + 1) * numberOfShapes_ - 1] = nullptr;
          }
          newMatrix[(i + 1) * numberOfShapes_ - 1] = object;
          matrix_.swap(newMatrix);
          addedShape = true;
          break;
        }
      }
      if ((i == (layers_ - 1)) && !addedShape){
        std::unique_ptr<std::shared_ptr<rusakow::Shape>[]> newMatrix(new std::shared_ptr <rusakow::Shape> [(layers_ + 1) * numberOfShapes_]);
        layers_++;
        for (int n = 0; n < ((layers_ - 1) * numberOfShapes_); ++n){
          newMatrix[n] = matrix_[n];
        }
        for (int n = ((layers_ - 1) * numberOfShapes_) ; n < (layers_ * numberOfShapes_); ++n){
          newMatrix[n] = nullptr;
        }
        newMatrix[(layers_ - 1) * numberOfShapes_ ] = object;
        matrix_.swap(newMatrix);
        addedShape = true;
      }
    }
  }
}

void rusakow::Matrix::add(const CompositeShape & object)
{
  for (size_t i = 0; i < object.getSize(); i++){
    add(object[i]);
  }
}

bool rusakow::Matrix::checkOverlap(const int index, std::shared_ptr<rusakow::Shape> object) const noexcept {
  rusakow::rectangle_t nShapeFrameRect = object->getFrameRect();
  rusakow::rectangle_t mShapeFrameRect = matrix_[index]->getFrameRect();
  rusakow::point_t newPoints[4] =
  {
    {nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0}
  };
  rusakow::point_t matrixPoints[4] =
  {
    {mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0}
  };
  for (int i = 0; i < 4; ++i){
    if (((newPoints[i].x >= matrixPoints[0].x) && (newPoints[i].x <= matrixPoints[2].x)
        && (newPoints[i].y >= matrixPoints[3].y) && (newPoints[i].y <= matrixPoints[1].y))
            || ((matrixPoints[i].x >= newPoints[0].x) && (matrixPoints[i].x <= newPoints[2].x)
                && (matrixPoints[i].y >= newPoints[3].y) && (matrixPoints[i].y <= newPoints[1].y)))
    {
      return (true);
    }
  }
  return (false);
}

