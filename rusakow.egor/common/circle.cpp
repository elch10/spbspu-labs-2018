#include "circle.hpp"

#include <iostream>
#include <cmath>
#include <stdexcept>

rusakow::Circle::Circle (const rusakow::point_t center, const double rad):
  center_(center),
  radius_(rad)
{
  if (rad < 0) {
    throw std::invalid_argument("Circle's radius must not be negative!");
  }
}

double rusakow::Circle::getArea() const{
  return (M_PI * radius_ * radius_);
}

rusakow::rectangle_t rusakow::Circle::getFrameRect() const{
  return (rusakow::rectangle_t {center_, 2 * radius_, 2 * radius_});
}

void rusakow::Circle::move(const double dx, const double dy){
  center_.x += dx;
  center_.y += dy;
}

void rusakow::Circle::move(const rusakow::point_t point){
  center_ = point;
}

void rusakow::Circle::getInfo() const{
  std::cout << "This is a circle whith center in (" << center_.x << "," << center_.y << ")" << std::endl;
  std::cout << "Its radius is " << radius_ << std::endl;
  std::cout << "Its area is " << getArea() << std::endl;
  rusakow::rectangle_t rect = getFrameRect();
  std::cout << "Its frame rectangle is centered in (" << rect.pos.x << "," << rect.pos.y << ")" << std::endl;
  std::cout << "Its frame rectangle has height and width " << rect.height << " " << rect.width << std::endl << std::endl;
}

void rusakow::Circle::scale(const double coeff){
  if (coeff < 0.0) {
    throw std::invalid_argument("The scale multiplier must not be negative!");
  }
  else
  {
    radius_ *= coeff;
  }
}

rusakow::point_t rusakow::Circle::getCenter() const{
  return (center_);
}

void rusakow::Circle::rotate(const double)
{
  //Rotation does not change the circle
  //So why should I write realisation for it?
  ;
}

