#include "circle.hpp"
#include <cmath>
#include <stdexcept>
#include <iostream>

using namespace kramarov;

Circle::Circle(const point_t & center, const double radius):
  center_(center),
  radius_(radius)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("Negative radius is inacceptable");
  };
}

double Circle::getArea() const noexcept
{
  return M_PI*radius_*radius_;
}

rectangle_t Circle::getFrameRect() const noexcept
{
  return rectangle_t{center_, 2*radius_, 2*radius_};
}

void Circle::move (const point_t & newLocation) noexcept
{
  center_ = newLocation;
}

void Circle::move (const double dx, const double dy) noexcept
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::scale (const double scaleRate)
{
  if (scaleRate < 0.0)
  {
    throw std::invalid_argument ("Scale rate must be above 0");
  }
  radius_ *= scaleRate;
}

point_t kramarov::Circle::getXY() const noexcept
{
  return getFrameRect().pos;
}

void Circle::rotate(double) noexcept
{
}

std::string Circle::getName() const  noexcept
{
  return "Circle";
}
