#ifndef COMPOSITESHAPE_HPP_INCLUDED
#define COMPOSITESHAPE_HPP_INCLUDED
#include "shape.hpp"
#include <memory>

namespace korovin
{
  class CompositeShape:
    public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const std::shared_ptr<Shape> &);
    CompositeShape(const CompositeShape &);
    CompositeShape & operator=(const CompositeShape &);
    friend bool korovin::operator==(const CompositeShape &, const CompositeShape &);
    void addShape(const std::shared_ptr<Shape> &);
    std::string getName() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    point_t getPos() const override;
    void scale(double k) override;
    void rotate(double angle) override;
    size_t getSize() const noexcept;

  private:
    std::unique_ptr <std::shared_ptr<Shape>[]> shapes_;
    size_t size_;
  };

  bool operator==(const CompositeShape &, const CompositeShape &);
}

#endif
