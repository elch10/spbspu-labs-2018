#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "base-types.hpp"
#include "shape.hpp"
#include "composite-shape.hpp"

namespace vasilev
{
class Matrix
{
public:
  Matrix();
  Matrix(const Matrix &array);
  Matrix(Matrix &&array);
  Matrix &operator=(const Matrix &array);
  Matrix &operator=(Matrix &&array);
  void addShape(const std::shared_ptr<Shape> newshape);
  void addComposite(const std::shared_ptr<CompositeShape> &comp);
  std::shared_ptr<Shape> getShape(unsigned int n) const;
  unsigned int getLayersQuantity() const;
  unsigned int getShapesQuantity() const;
  unsigned int getLayerSize(unsigned int n) const;
  void getInfo() const;
private:
  unsigned int quant_shapes_;
  unsigned int quant_layers_;
  std::unique_ptr<std::shared_ptr<Shape>[]> arr_;
  std::unique_ptr<unsigned int []> layer_size_;
};
}
#endif // MATRIX_HPP
