#include <cmath>
#include <iostream>
#include "matrix.hpp"

vasilev::Matrix::Matrix():
  quant_shapes_(0),
  quant_layers_(0),
  arr_(nullptr),
  layer_size_(nullptr)
{}
vasilev::Matrix::Matrix(const Matrix &array):
  quant_shapes_(array.quant_shapes_),
  quant_layers_(array.quant_layers_),
  arr_(new std::shared_ptr<Shape>[quant_shapes_]),
  layer_size_(new unsigned int[quant_layers_])
{
  for(unsigned int i = 0; i < quant_shapes_; i++)
  {
    arr_[i] = array.arr_[i];
  }
  for(unsigned int i = 0; i < quant_layers_; i++)
  {
    layer_size_[i] = array.layer_size_[i];
  }
}
vasilev::Matrix::Matrix(Matrix &&array):
  quant_shapes_(array.quant_shapes_),
  quant_layers_(array.quant_layers_)
{
  arr_.swap(array.arr_);
  layer_size_.swap(array.layer_size_);
  array.arr_.reset();
  array.layer_size_.reset();
  array.quant_layers_ = 0;
  array.quant_shapes_ = 0;
}
vasilev::Matrix &vasilev::Matrix::operator=(const Matrix &array)
{
  std::unique_ptr<std::shared_ptr<Shape>[]> tmparr_ = std::unique_ptr<std::shared_ptr<Shape>[]>(new std::shared_ptr<Shape>[array.quant_shapes_]);
  std::unique_ptr<unsigned int []> tmplayer_size_ = std::unique_ptr<unsigned int[]>(new unsigned int[array.quant_layers_]);
  arr_.swap(tmparr_);
  layer_size_.swap(tmplayer_size_);
  quant_shapes_ = array.quant_shapes_;
  quant_layers_ = array.quant_layers_;


  for(unsigned int i = 0; i < quant_layers_; i++)
  {
    layer_size_[i] = array.layer_size_[i];
  }

  for(unsigned int i = 0; i < quant_shapes_; i++)
  {
    arr_[i] = array.arr_[i];
  }
  return *this;
}
vasilev::Matrix &vasilev::Matrix::operator=(Matrix &&array)
{
  layer_size_.swap(array.layer_size_);
  arr_.swap(array.arr_);
  quant_layers_ = array.quant_layers_;
  quant_shapes_ = array.quant_shapes_;
  array.quant_layers_ = 0;
  array.quant_shapes_ = 0;
  array.layer_size_.reset();
  array.arr_.reset();
  return *this;
}
unsigned int vasilev::Matrix::getLayersQuantity() const
{
  return quant_layers_;
}
unsigned int vasilev::Matrix::getShapesQuantity() const
{
  return quant_shapes_;
}
void vasilev::Matrix::addComposite(const std::shared_ptr<CompositeShape> &comp)
{
  if(comp->getQuantity() == 0)
  {
    throw std::invalid_argument("Empty composite shape");
  }
  for(unsigned int i = 0; i < comp->getQuantity(); i++)
  {
    addShape(comp->getShape(i));
  }
}
void vasilev::Matrix::addShape(const std::shared_ptr<Shape> newshape)
{
  if (newshape == nullptr)
  {
    throw std::invalid_argument("Shape has a nullptr value");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tmparr_(new std::shared_ptr<Shape>[quant_shapes_ + 1]);
  bool imposition = false;
  unsigned int  offset = 0;

  for (unsigned int  i = 0; i < quant_layers_; i++)
  {
    imposition = true;
    for (unsigned int  j = 0; j < layer_size_[i]; j++)
    {
      rectangle_t rect1 = arr_[offset + j]->getFrameRect();
      rectangle_t rect2 = newshape->getFrameRect();
      if (fabs(rect1.pos.x - rect2.pos.x) <= rect1.width / 2 + rect2.width / 2 &&
          fabs(rect1.pos.y - rect2.pos.y) <= rect1.height / 2 + rect2.height / 2)
      {
        imposition = false;
      }
      tmparr_[offset + j] = arr_[offset + j];
    }
    offset += layer_size_[i];

    if (imposition)
    {
      tmparr_[offset] = newshape;
      for (unsigned int  j = offset; j < quant_shapes_; j++)
      {
        tmparr_[j + 1] = arr_[j];
      }
      layer_size_[i]++;
      quant_shapes_++;
      break;
    }
  }
  if(!imposition)
  {
    tmparr_[quant_shapes_++] = newshape;
    std::unique_ptr<unsigned int []> tmplayer_size_(new unsigned int [quant_layers_ + 1]);
    for (unsigned int  i = 0; i < quant_layers_; i++)
    {
      tmplayer_size_[i] = layer_size_[i];
    }
    tmplayer_size_[quant_layers_++] = 1;
    layer_size_.swap(tmplayer_size_);
  }
  arr_.swap(tmparr_);
}
std::shared_ptr<vasilev::Shape> vasilev::Matrix::getShape(unsigned int n) const
{
  return arr_[n];
}
unsigned int vasilev::Matrix::getLayerSize(unsigned int n) const
{
  if(n > quant_layers_)
  {
    throw std::invalid_argument("Error: This Matrix does not exist this layer");
  }
  return layer_size_[n];
}
void vasilev::Matrix::getInfo() const
{
  std::cout << "Count of layers: " <<  getLayersQuantity() << std::endl;
  std::cout << "Count of shapes: " << getShapesQuantity() << std::endl;
  std::cout << std::endl;
  for(unsigned int i = 0; i < getLayersQuantity(); i++)
  {
    std::cout << "Layer number: " << i + 1 << std::endl;
    std::cout << "Count shapes in layer: " << getLayerSize(i) << std::endl;
  }
}
