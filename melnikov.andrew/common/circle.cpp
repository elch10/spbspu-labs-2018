#include <iostream>
#include <cmath>
#include "circle.hpp"

using namespace melnikov;

Circle::Circle(const point_t & pos, const double radius): Shape(pos) {
  if (radius < 0.0) {
    throw std::invalid_argument("Radius is Invalid!");
  }
  radius_ = radius;
  angle_ = 0.0;
}

rectangle_t Circle::getFrameRect() const {
  return rectangle_t{2.0 * radius_, 2.0 * radius_, pos_};
}

double Circle::getArea() const {
  return radius_ * radius_* M_PI;
}

void Circle::move(const point_t & pos) {
  pos_ = pos;
}

void Circle::move(const double dx, const double dy) {
  pos_.x += dx;
  pos_.y += dy;
}

void Circle::scale(const double coef) {
  if (coef >= 0.0) {
    radius_ *= coef;
  } else {
    throw std::invalid_argument("Scaling coefficient is Invalid!");
  }
}

void Circle::rotate(const double angle) {
  angle_ += angle;
}
