#include <iostream>
#include <memory>
#include <cmath>
#include "matrix.hpp"

using namespace melnikov;

Matrix::Matrix(const std::shared_ptr<melnikov::Shape> shape):
  layerSize_(0),
  shapeSize_(0),
  shapeArr_(nullptr) {
  if (shape == nullptr) {
    throw std::invalid_argument("Wrong shape");
  }
  if ((shape->getFrameRect().height < 0.0) || (shape->getFrameRect().width < 0.0)) {
    throw std::invalid_argument("Wrong values");
  }
  addShape(shape);
}

Matrix::Matrix(const Matrix & matrix):
  layerSize_(matrix.layerSize_),
  shapeSize_(matrix.shapeSize_),
  shapeArr_(new std::shared_ptr<Shape>[matrix.layerSize_ * matrix.shapeSize_]) {
  for (int i = 0; i < layerSize_*shapeSize_; ++i) {
    shapeArr_[i] = matrix.shapeArr_[i];
  }
}

Matrix::Matrix():
  layerSize_(0),
  shapeSize_(0),
  shapeArr_(nullptr) {
}

Matrix::~Matrix() {
}

void Matrix::addShape(const std::shared_ptr<melnikov::Shape> addedShape) {
  if (addedShape == nullptr) {
    throw std::invalid_argument("Null pointer!");
  }
  if (addedShape->getFrameRect().height < 0.0) {
    throw std::invalid_argument("Invalid height!");
  }
  if (addedShape->getFrameRect().width < 0.0) {
    throw std::invalid_argument("Invalid width!");
  }
  if ((layerSize_ == 0) && (shapeSize_ == 0)) {
    std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(new std::shared_ptr<Shape>[1]);
    tempArr[0] = addedShape;
    shapeArr_.swap(tempArr);
    layerSize_ = 1;
    shapeSize_ = 1;
    return;
  }
  int count1 = 0;
  for (int i = 0; i < shapeSize_; i++) {
    int count2 = 0;
    for (int j = 0; j < layerSize_; j++) {
      if (shapeArr_[i*layerSize_+j] == nullptr) {
        shapeArr_[i*layerSize_+j] = addedShape;
        return;
      }
      count2 = j + 1;
      if (crossing(shapeArr_[i*layerSize_+j], addedShape)) {
        break;
      }
    }
    if (count2 == layerSize_) {
      std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(new std::shared_ptr<Shape>[shapeSize_*(layerSize_+1)]);
      for (int m = 0; m < shapeSize_; m++) {
        for (int n = 0; n < layerSize_; n++) {
          tempArr[m*layerSize_+n+m] = shapeArr_[m*layerSize_+n];
        }
      }
      layerSize_++;
      shapeSize_++;
      tempArr[(i+1)*layerSize_ - 1] = addedShape;
      shapeArr_ = std::move(tempArr);
      return;
    }
    count1 = i + 1;
  }
  if (count1 == shapeSize_) {
    std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(new std::shared_ptr<Shape>[(shapeSize_+1)*layerSize_]);
    for (int i = 0; i < shapeSize_; i++) {
      tempArr[i] = shapeArr_[i];
    }
    tempArr[shapeSize_*layerSize_] = addedShape;
    shapeSize_++;
    shapeArr_ = std::move(tempArr);
  }
}

int Matrix::getLayersNumber() {
  return layerSize_;
}

int Matrix::getShapesNumber() {
  return shapeSize_;
}

bool Matrix::crossing(const std::shared_ptr <Shape> currentShape, const std::shared_ptr <Shape> newShape) {
  if (currentShape == nullptr || newShape == nullptr) {
    throw std::invalid_argument("Null pointer!");
  }
  rectangle_t currentFrame = currentShape->getFrameRect();
  rectangle_t newFrame = newShape->getFrameRect();
  return((fabs(currentFrame.pos.x-newFrame.pos.x) < currentFrame.width/2 + newFrame.width/2)
      && fabs(currentFrame.pos.y - newFrame.pos.y) < currentFrame.height/2 + newFrame.height/2);
}
