#include <cmath>
#include <math.h>
#include <stdexcept>
#include "composite-shape.hpp"

using namespace melnikov;

CompositeShape::CompositeShape(const point_t & pos):
  Shape(pos),
  size_(0),
  angle_(0.0),
  array_(nullptr) {
}

CompositeShape::~CompositeShape() {
}

double CompositeShape::getArea() const {
  double area = 0.0;
  for (int i = 0; i < size_; ++i) {
    area += array_[i] -> getArea();
  }
  return area;
}

rectangle_t CompositeShape::getFrameRect() const {
  if (array_ == 0) {
    return {0.0, 0.0, {0.0, 0.0}};
  }
  rectangle_t shapeFrame = array_[0] -> getFrameRect();
  double minX = shapeFrame.pos.x - shapeFrame.width/2;
  double maxX = shapeFrame.pos.x + shapeFrame.width/2;
  double minY = shapeFrame.pos.y - shapeFrame.height/2;
  double maxY = shapeFrame.pos.y + shapeFrame.height/2;
  for (int i = 0; i < size_; ++i) {
    shapeFrame = array_[i] -> getFrameRect();
    if (shapeFrame.pos.x - shapeFrame.width / 2 < minX) {
      minX = shapeFrame.pos.x - shapeFrame.width / 2;
    }
    if (shapeFrame.pos.x + shapeFrame.width / 2 > maxX ){
      maxX = shapeFrame.pos.x + shapeFrame.width / 2;
    }
    if (shapeFrame.pos.y - shapeFrame.height / 2 < minY) {
      minY = shapeFrame.pos.y - shapeFrame.height / 2;
    }
    if (shapeFrame.pos.y + shapeFrame.height / 2 > maxY) {
      maxY = shapeFrame.pos.y + shapeFrame.height / 2;
    }
  }
  return { maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2} };
}

void CompositeShape::move(const point_t & pos) {
  for (int i = 0; i < size_; ++i) {
    array_[i]->move((pos.x - getFrameRect().pos.x), (pos.y - getFrameRect().pos.y));
  }
}

void CompositeShape::move(const double dx, const double dy) {
  for (int i = 0; i < size_; ++i) {
    array_[i]->move(dx, dy);
  }
}

void CompositeShape::scale(const double coef) {
  if (coef <= 0.0) {
    throw std::invalid_argument("Scaling coefficient is Invalid!");
  }
  point_t initialPosition = getFrameRect().pos;
  for (int i = 0; i < size_; ++i) {
    array_[i]->move((coef - 1) * (array_[i]->getFrameRect().pos.x - initialPosition.x),
        (coef - 1) * (array_[i]->getFrameRect().pos.y - initialPosition.y));
    array_[i] -> scale(coef);
  }
}

void CompositeShape::rotate(const double angle) {
  angle_ += angle;
  const point_t compPos = getFrameRect().pos;
  for (int i = 0; i < size_; ++i) {
    const point_t elemPos = array_[i]->getFrameRect().pos;
    array_[i]->move({(elemPos.x - compPos.x) * cos(angle * M_PI / 180)
        - (elemPos.y - compPos.y) * sin(angle * M_PI / 180) + compPos.x,
        (elemPos.y - compPos.y) * cos(angle * M_PI / 180)
        + (elemPos.x - compPos.x) * sin(angle * M_PI / 180) + compPos.y});
    array_[i] -> rotate(angle);
  }
}

void CompositeShape::addElement(const std::shared_ptr <Shape> addedShape) {
  if (addedShape == nullptr) {
    throw std::invalid_argument("Null pointer!");
  }
  std::unique_ptr <std::shared_ptr <Shape> []> tempArray(new std::shared_ptr <Shape> [size_ + 1]);
  for (int i = 0; i < size_; ++i) {
    tempArray[i] = array_[i];
  }
  tempArray[size_] = addedShape;
  size_++;
  array_.swap(tempArray);
}

void CompositeShape::removeElement(const int index) {
  if ((size_ == 0) || (index >= size_)) {
    throw std::invalid_argument("Index is out of range!");
  }
  std::unique_ptr <std::shared_ptr <Shape> []> tempArray(new std::shared_ptr <Shape> [size_ - 1]);
  for (int i = 0; i < index; ++i) {
    tempArray[i] = array_[i];
  }
  for (int i = index; i < size_ - 1; ++i) {
    tempArray[i] = array_[i + 1];
  }
  array_.swap(tempArray);
  size_--;
}
