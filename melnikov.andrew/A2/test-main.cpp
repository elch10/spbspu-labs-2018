#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

using namespace melnikov;

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(CircleTest)

  BOOST_AUTO_TEST_CASE(InvalidConstructor) {
    BOOST_CHECK_THROW(Circle circleObject({30.0, 30.0}, -5.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingTo) {
    Circle circleObject({30.0, 30.0}, 10.0);
    
    const double areaBeforeMoving = circleObject.getArea();
    const rectangle_t rectBeforeMoving = circleObject.getFrameRect();

    circleObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(areaBeforeMoving, circleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingFor) {
    Circle circleObject({30.0, 30.0}, 10.0);
    
    const double areaBeforeMoving = circleObject.getArea();
    const rectangle_t rectBeforeMoving = circleObject.getFrameRect();

    circleObject.move(20.0, 20.0);

    BOOST_CHECK_EQUAL(areaBeforeMoving, circleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(Scaling) {
    Circle circleObject({30.0, 30.0}, 10.0);
    
    const double areaBeforeScaling = circleObject.getArea();
    const double coef = 3.5;

    circleObject.scale(coef);

    BOOST_CHECK_CLOSE(coef * coef * areaBeforeScaling, circleObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling) {
    Circle circleObject({30.0, 30.0}, 10.0);
    
    BOOST_CHECK_THROW(circleObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(RectangleTest)

  BOOST_AUTO_TEST_CASE(InvalidConstructor) {
    BOOST_CHECK_THROW(Rectangle rectangleObject({30.0, 30.0}, -5.0, 10), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingTo) {
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    
    const double areaBeforeMoving = rectangleObject.getArea();
    const rectangle_t rectBeforeMoving = rectangleObject.getFrameRect();

    rectangleObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(areaBeforeMoving, rectangleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, rectangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, rectangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingFor) {
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    
    const double areaBeforeMoving = rectangleObject.getArea();
    const rectangle_t rectBeforeMoving = rectangleObject.getFrameRect();

    rectangleObject.move(20.0, 20.0);

    BOOST_CHECK_EQUAL(areaBeforeMoving, rectangleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, rectangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, rectangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(Scaling) {
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    
    const double areaBeforeScaling = rectangleObject.getArea();
    const double coef = 3.5;

    rectangleObject.scale(coef);

    BOOST_CHECK_CLOSE(coef * coef * areaBeforeScaling, rectangleObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling) {
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    
    BOOST_CHECK_THROW(rectangleObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TriangleTest)

  BOOST_AUTO_TEST_CASE(InvalidConstructor) {
    BOOST_CHECK_THROW(Triangle triangleObject({0.0, 0.0}, {0.0, 0.0}, {0.0, 30.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingTo) {
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    
    const double areaBeforeMoving = triangleObject.getArea();
    const rectangle_t rectBeforeMoving = triangleObject.getFrameRect();

    triangleObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(areaBeforeMoving, triangleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, triangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, triangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingFor) {
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    
    const double areaBeforeMoving = triangleObject.getArea();
    const rectangle_t rectBeforeMoving = triangleObject.getFrameRect();

    triangleObject.move(20.0, 20.0);

    BOOST_CHECK_EQUAL(areaBeforeMoving, triangleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, triangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, triangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(Scaling) {
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    
    const double areaBeforeScaling = triangleObject.getArea();
    const double coef = 3.5;

    triangleObject.scale(coef);

    BOOST_CHECK_CLOSE(coef * coef * areaBeforeScaling, triangleObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling) {
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    
    BOOST_CHECK_THROW(triangleObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
