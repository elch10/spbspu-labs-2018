#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

using namespace melnikov;

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(CircleTest)

  BOOST_AUTO_TEST_CASE(InvalidConstructor) {
    BOOST_CHECK_THROW(Circle circleObject({30.0, 30.0}, -5.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingTo) {
    Circle circleObject({30.0, 30.0}, 10.0);
    
    const double areaBeforeMoving = circleObject.getArea();
    const rectangle_t rectBeforeMoving = circleObject.getFrameRect();

    circleObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(areaBeforeMoving, circleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingFor) {
    Circle circleObject({30.0, 30.0}, 10.0);
    
    const double areaBeforeMoving = circleObject.getArea();
    const rectangle_t rectBeforeMoving = circleObject.getFrameRect();

    circleObject.move(20.0, 20.0);

    BOOST_CHECK_EQUAL(areaBeforeMoving, circleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(Scaling) {
    Circle circleObject({30.0, 30.0}, 10.0);
    
    const double areaBeforeScaling = circleObject.getArea();
    const double coef = 3.5;

    circleObject.scale(coef);

    BOOST_CHECK_CLOSE(coef * coef * areaBeforeScaling, circleObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling) {
    Circle circleObject({30.0, 30.0}, 10.0);
    
    BOOST_CHECK_THROW(circleObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(RectangleTest)

  BOOST_AUTO_TEST_CASE(InvalidConstructor) {
    BOOST_CHECK_THROW(Rectangle rectangleObject({30.0, 30.0}, -5.0, 10), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingTo) {
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    
    const double areaBeforeMoving = rectangleObject.getArea();
    const rectangle_t rectBeforeMoving = rectangleObject.getFrameRect();

    rectangleObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(areaBeforeMoving, rectangleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, rectangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, rectangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingFor) {
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    
    const double areaBeforeMoving = rectangleObject.getArea();
    const rectangle_t rectBeforeMoving = rectangleObject.getFrameRect();

    rectangleObject.move(20.0, 20.0);

    BOOST_CHECK_EQUAL(areaBeforeMoving, rectangleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, rectangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, rectangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(Scaling) {
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    
    const double areaBeforeScaling = rectangleObject.getArea();
    const double coef = 3.5;

    rectangleObject.scale(coef);

    BOOST_CHECK_CLOSE(coef * coef * areaBeforeScaling, rectangleObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling) {
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    
    BOOST_CHECK_THROW(rectangleObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TriangleTest)

  BOOST_AUTO_TEST_CASE(InvalidConstructor) {
    BOOST_CHECK_THROW(Triangle triangleObject({0.0, 0.0}, {0.0, 0.0}, {0.0, 30.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingTo) {
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    
    const double areaBeforeMoving = triangleObject.getArea();
    const rectangle_t rectBeforeMoving = triangleObject.getFrameRect();

    triangleObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(areaBeforeMoving, triangleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, triangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, triangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingFor) {
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    
    const double areaBeforeMoving = triangleObject.getArea();
    const rectangle_t rectBeforeMoving = triangleObject.getFrameRect();

    triangleObject.move(20.0, 20.0);

    BOOST_CHECK_EQUAL(areaBeforeMoving, triangleObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, triangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, triangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(Scaling) {
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    
    const double areaBeforeScaling = triangleObject.getArea();
    const double coef = 3.5;

    triangleObject.scale(coef);

    BOOST_CHECK_CLOSE(coef * coef * areaBeforeScaling, triangleObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling) {
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    
    BOOST_CHECK_THROW(triangleObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeTest)

  BOOST_AUTO_TEST_CASE(MovingTo) {
    Circle circleObject({30.0, 30.0}, 10.0);
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    std::shared_ptr <Shape> circleObjectPtr = std::make_shared <Circle> (circleObject);
    std::shared_ptr <Shape> rectangleObjectPtr = std::make_shared <Rectangle> (rectangleObject);
    std::shared_ptr <Shape> triangleObjectPtr = std::make_shared <Triangle> (triangleObject);

    CompositeShape compositeObject({0.0, 0.0});
    compositeObject.addElement(circleObjectPtr);
    compositeObject.addElement(rectangleObjectPtr);
    compositeObject.addElement(triangleObjectPtr);

    const double areaBeforeMoving = compositeObject.getArea();
    const rectangle_t rectBeforeMoving = compositeObject.getFrameRect();

    compositeObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(areaBeforeMoving, compositeObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, compositeObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, compositeObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingFor) {
    Circle circleObject({30.0, 30.0}, 10.0);
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    std::shared_ptr <Shape> circleObjectPtr = std::make_shared <Circle> (circleObject);
    std::shared_ptr <Shape> rectangleObjectPtr = std::make_shared <Rectangle> (rectangleObject);
    std::shared_ptr <Shape> triangleObjectPtr = std::make_shared <Triangle> (triangleObject);

    CompositeShape compositeObject({0.0, 0.0});
    compositeObject.addElement(circleObjectPtr);
    compositeObject.addElement(rectangleObjectPtr);
    compositeObject.addElement(triangleObjectPtr);

    const double areaBeforeMoving = compositeObject.getArea();
    const rectangle_t rectBeforeMoving = compositeObject.getFrameRect();

    compositeObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(areaBeforeMoving, compositeObject.getArea());
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, compositeObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, compositeObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(Scaling) {
    Circle circleObject({30.0, 30.0}, 10);
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    std::shared_ptr <Shape> rectangleObjectPtr = std::make_shared <Rectangle> (rectangleObject);
    std::shared_ptr <Shape> circleObjectPtr = std::make_shared <Circle> (circleObject);
    std::shared_ptr <Shape> triangleObjectPtr = std::make_shared <Triangle> (triangleObject);

    CompositeShape compositeObject({0.0, 0.0});
    compositeObject.addElement(rectangleObjectPtr);
    compositeObject.addElement(circleObjectPtr);
    compositeObject.addElement(triangleObjectPtr);

    const double areaBeforeScaling = compositeObject.getArea();
    const double coef = 3.0;

    compositeObject.scale(coef);

    BOOST_CHECK_CLOSE(coef * coef * areaBeforeScaling, compositeObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling) {
    Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    std::shared_ptr <Shape> rectangleObjectPtr = std::make_shared <Rectangle> (rectangleObject);
    CompositeShape compositeObject({0, 0});
    compositeObject.addElement(rectangleObjectPtr);

    BOOST_CHECK_THROW(compositeObject.scale(-2.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidAdding) {
    CompositeShape compositeObject({0.0, 0.0});
    BOOST_CHECK_THROW(compositeObject.addElement(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidRemoving) {
    CompositeShape compositeObject({0.0, 0.0});
    BOOST_CHECK_THROW(compositeObject.removeElement(-1.0), std::invalid_argument);
    BOOST_CHECK_THROW(compositeObject.removeElement(3.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
